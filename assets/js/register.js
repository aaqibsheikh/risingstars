/**
 * @js file
 * 
 * http://risingstar.com/auth/register_user
 * http://risingstar.com/auth/upgrade_package
 * http://risingstar.com/super_admin/fund_e_cash
 * Use style.css for basic styling.
 * For event in register form upgrade form fund e cash forms.
 *
 * Copyright (c) 2016 
 * 
 *
 */

 var ajaxurl = site_url,
 MSG_UNABLE_TO_VERIFY = "Unable to verify the given code.",
 MSG_UNABLE_TO_VERIFY_PIN = "Unable to verify the given pin code.",
 MSG_Make_Entry = "Congragulation You can make this entry.",
 MSG_UNABLE_TO_Make_Entry = "You can't make this entry beacuse you dont have enough e Cash or Commisson.",
 MSG_REQUIRED_FIELD = "Please fill out all required fields correctly.",
 VERIFIED_INtRODUCER_TEXT = "The above user is verified by our system, please confirm that it is the right person by watching his/her picture other details.",
 loading_element = "#verify",
 msg_code_verification_element = ".code-verification-error",
 distributor_detail_container = "#verified-distributor-detail",
 registration_submit_btn = ".register-submit",
 package_list = "#available-package-list",
 total_packages = 5,
 package_id = "#package_id",
 user_id = "#user_id",
 packages = ["HS 1000", "SS 3500", "GS 7000", "PS 10500", "DS 14000"],
 full_name = "#full_name",
 e_cash = "#verified_member",
 pgurl = '',
 fund_e_cash_btn = ".process-btn",
        /*
         * get img clicked
         * --------------------*/
         find_introducer_by_code = function (controller) {

            //alert(obj);
            var min_length = 19, // min caracters to display the autocomplet

            introducer = $("#introducer").val(),
            introducer_pin= $("#introducer_pin").val(),
            qtyre         = $("#qtyre").val(),
            qtymeenu      = $("#qtymeenu").val(),
            package_id    = $("#package").val();

            if(package_id != undefined)
                package_id = package_id.join("a");
            else{
                package_id = 0;
            }
            if( !qtyre ) {
              qtyre=0;
          }
          if( !qtymeenu ){
              qtymeenu=0;
          }
              // console.log(pack);
          // console.log(package_id);

          if (introducer.length == min_length && introducer != '' && introducer_pin != '') {

                $(msg_code_verification_element).hide(); // hide the error msg	
                var data = {};

                // loader on verified button
                $(loading_element).val('Loading...');
                $(loading_element).prop('disabled', true);

                // ajaxurl user site_url that is always defined in the layout header and points to your controller/function url
                $.post(ajaxurl + 'auth/verify_and_get_introducer_details/' + introducer + '/' + introducer_pin + '/' + qtyre + '/' + qtymeenu + '/' + package_id , data, function (response) {

                    // reset verified button
                    $(loading_element).val('verify').prop('disabled', false);


                    var data = $.parseJSON(response);
                    console.log(data);

                    if(data.errorr){
                        $(e_cash).val('');     
                        $(msg_code_verification_element).html(MSG_UNABLE_TO_VERIFY_PIN).show();
                    }else{
                        $(msg_code_verification_element).hide();
                        if (data.error) {
                            $(e_cash).val('');     
                            $(msg_code_verification_element).html(MSG_UNABLE_TO_VERIFY).show();
                        } else {

                            $(msg_code_verification_element).hide();
                            $(distributor_detail_container + " #introducer_profile_picture").attr('src', data[0].user_pic_path);
                            $(distributor_detail_container + " #introducer_name").html(data[0].full_name);
                            $(distributor_detail_container + " #introducer_address").html(data[0].address);
                            $(distributor_detail_container + " #verified_introducer_text").html(VERIFIED_INtRODUCER_TEXT);
                            $(package_id).val(data[0].package_id);
                            $(user_id).val(data[0].user_id);
                            $(full_name).val(data[0].full_name);
                            $(distributor_detail_container).fadeOut(400).fadeIn(400);
                            
                            if(data.allow)//check amount of current user
                            {
                                $(msg_code_verification_element).html(MSG_Make_Entry).show();
                                $(registration_submit_btn).removeClass('hide').show().css('display', 'inline-block');
                            }else{
                             $(e_cash).val('');     
                             $(msg_code_verification_element).html(MSG_UNABLE_TO_Make_Entry).show();
                             $(registration_submit_btn).hide();
                         }

                         pgurl = window.location.href.substr(window.location.href
                            .lastIndexOf("/") + 1);
                         console.log(pgurl);

                         if (controller == 'upgrade_package' && data[0].package_id < total_packages) {
                            //console.log(obj + '----me');
                            var start_index = parseInt(data[0].package_id) + 1;

                            for (var i = start_index; i <= total_packages; i++) {

                                $(package_list).append('<option value="' + i + '">' + packages[i - 1] + '</option>');

                            }
                        }
                        if (pgurl == 'fund_e_cash') {

                            $(e_cash).val(introducer);
                            $(e_cash).attr('readonly', true);
                            $(fund_e_cash_btn).removeClass('hide').show().css('display', 'inline-block');

                        }
                        if (pgurl == 'transfer_e_commison') {

                            $(e_cash).val(introducer);
                            $(e_cash).attr('readonly', true);
                            $(fund_e_cash_btn).removeClass('hide').show().css('display', 'inline-block');

                        }
                    }

                }

            });
            } else {

                $(msg_code_verification_element).html(MSG_REQUIRED_FIELD).show();
            }



        }
        ,
        /*
         * text edit functions
         * ------------------------*/

// make bold
zp_bold = function () {

    document.execCommand('bold', false, null);
};

$(document).ready(function () {

    //Enter keystroke closes all editboxes and saves changes 
    $("#user_profile_picture").keyup(function (event) {

        if ($("#user_profile_picture").val() != '') {
            $("#profile_picture_holder").attr('src', $("#user_profile_picture").val()).fadeOut(400).fadeIn(400);
        } else {
            $("#profile_picture_holder").attr('src', $(site_url + "assets/images/no-pic.png").val());
        }
    });
    $('#withdrawprocess').on('shown.bs.modal', function () {
        console.log('hello');
    });
});
//this is for auto close alerts
//$(document).ready (function(){
//            $(".alert_messages").hide();
//            $("click").click(function showAlert() {
//                $(".alert_messages").alert();
//                $(".alert_messages").fadeTo(2000, 500).slideUp(500, function(){
//               $(".alert_messages").slideUp(500);
//                });   
//            });
// });
