/**
 * @js file
 * FullCalendar v1.0.0
 * http://risingstar.com/auth/register_user
 *
 * Use style.css for basic styling.
 * For event in register form.
 *
 * Copyright (c) 2016 
 * Date: Tue Nov 8 2016
 *
 */

 var ajaxurl = site_url,
		commission_total_paid = "#commission-total-paid",
		commission_eighty = "#commission-eighty",
		commission_twenty = "#commission-twenty",
		commission_one = "#commission-one",
		current_bonus = "#current-bonus",
		withdraw_id = "#withdraw-id",
		withdraw_amount = "#withdraw-amount",
		withdraw_model_loader = "#withdraw-model-loader",
		amount_exceed_error = "#amount-exceed-error",
		current_user_pic = "#current-user-pic";
		
		// log amount that is withdraw by admin
		withdraw_bonus = function(obj) {            
			bootbox.confirm(" Are you sure to process "+ $(withdraw_amount).val() + "Rs ? you cannot roll back this.", function(result) {
				
				if(result){
					
					$(obj).val('processing...');$(withdraw_model_loader).show();$(withdraw_model_loader+" .splash").show();
					
					$.post(ajaxurl + 'super_admin/withdraw_bonus/' + $(withdraw_id).val() + '/'+ $(withdraw_amount).val(), function (response) {
					var  data = $.parseJSON(response);
					
					if(data.error) {
						 $(obj).val('Withdraw');$(amount_exceed_error).show();$(withdraw_model_loader).hide();$(withdraw_model_loader+" .splash").hide();
					} else {
						fill_Withdraw_model(data);
						$(obj).val('Withdraw more');$(withdraw_model_loader).hide();$(withdraw_model_loader+" .splash").hide();$(amount_exceed_error).hide();
						$(withdraw_amount).val('');
					}
					
					});              
				}  else {
					$(obj).val('Withdraw more');$(withdraw_model_loader).hide();$(amount_exceed_error).hide();
					$(withdraw_amount).val('');
				}
			
			});
			
		},
		// fill data in model 
		fill_Withdraw_model = function(data){
			var bonus = parseInt( parseInt(data.eighty) + parseInt(data.twenty) + parseInt(data.one) - (parseInt(data.total_paid))); // calculate total bonus
				$(commission_total_paid + " .font-extra-bold").html(data.total_paid);
				$(commission_eighty + " .font-extra-bold").html(data.eighty); // commission 80%
				$(commission_twenty + " .font-extra-bold").html(data.twenty);// commission 20%
				$(commission_one + " .font-extra-bold").html(data.one);// commission 1%
				$(current_bonus + " .font-extra-bold").html( bonus ); 
				$(withdraw_id).val(data.id)// sum of all commissions
				$(current_user_pic).attr("src",$("#user-img-"+data.id).val()).fadeOut(400).fadeIn(400);
				
		};

$(document).ready(function () { 
   
	// on open of withdraw amount model - get all the commission info
	$('#exampleModal_withdraw').on('show.bs.modal', function (event){
		var my_btn = $(event.relatedTarget) // Button that triggered the modal
		var withdrw_id = my_btn.data('withdrw-id') // Extract info from data-* attributes

		// ajaxurl user site_url that is always defined in the layout header and points to your controller/function url
		$.post(ajaxurl + 'super_admin/withdraw_process_modal_info/' + withdrw_id + '/', function (response) {
			var  data = $.parseJSON(response);
			fill_Withdraw_model(data);          
		});
	});
	
	$('#exampleModal_withdraw').on('hidden.bs.modal', function (event){
		window.location.reload();
	});
	
	$(".active_rs_user").on('click', function(obj){
		
		var check_box_id = "#"+$(this).attr("id"), // get the id of clicked checkbox
		rs_user_id = $(this).attr("id").split(/__/g);
		
		// alert for warning
		bootbox.confirm(" Are you sure to activate <strong>"+ $(this).attr("activate-full-name") + "</strong>?", function(result) {
				
				if(result){
					$(check_box_id).parent().find("label").html("Activating...");
					
					$.post(ajaxurl + 'super_admin/active_rs_user/' + rs_user_id[1] , function (response) {
					
						$(check_box_id).parent().find("label").html("active");
						
					});            
				}  else {
					 $(check_box_id).parent().find("label").html("Active");
					
				}
			
			});
	});
	
	$(".block_rs_user").on('click', function(obj){
		
		var check_box_id = "#"+$(this).attr("id"), // get the id of clicked checkbox
		rs_user_id = $(this).attr("id").split(/__/g);
		
		// alert for warning
		bootbox.confirm(" Are you sure to block <strong>"+ $(this).attr("block-full-name") + "</strong>?", function(result) {
				
				if(result){
					$(check_box_id).parent().find("label").html("Blocking...");
					
					$.post(ajaxurl + 'super_admin/block_rs_user/' + rs_user_id[1] , function (response) {
					
						$(check_box_id).parent().find("label").html("Blcoked");
						
					});            
				}  else {
					 $(check_box_id).parent().find("label").html("Blocked");
					
				}
			
			});
	});
});