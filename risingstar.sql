-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.34-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table risingstardemo.aauth_groups
CREATE TABLE IF NOT EXISTS `aauth_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `aauth_groups` DISABLE KEYS */;
INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
	(1, 'Admin', 'Super Admin Group'),
	(2, 'Public', 'Public Access Group');
/*!40000 ALTER TABLE `aauth_groups` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_group_to_group
CREATE TABLE IF NOT EXISTS `aauth_group_to_group` (
  `group_id` int(11) unsigned NOT NULL,
  `subgroup_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`subgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_group_to_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `aauth_group_to_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `aauth_group_to_group` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_login_attempts
CREATE TABLE IF NOT EXISTS `aauth_login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(39) DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.aauth_login_attempts: ~0 rows (approximately)
/*!40000 ALTER TABLE `aauth_login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `aauth_login_attempts` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_perms
CREATE TABLE IF NOT EXISTS `aauth_perms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `definition` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_perms: ~0 rows (approximately)
/*!40000 ALTER TABLE `aauth_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `aauth_perms` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_perm_to_group
CREATE TABLE IF NOT EXISTS `aauth_perm_to_group` (
  `perm_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_perm_to_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `aauth_perm_to_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `aauth_perm_to_group` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_perm_to_user
CREATE TABLE IF NOT EXISTS `aauth_perm_to_user` (
  `perm_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`perm_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_perm_to_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `aauth_perm_to_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `aauth_perm_to_user` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_pms
CREATE TABLE IF NOT EXISTS `aauth_pms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) unsigned NOT NULL,
  `receiver_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT '0',
  `pm_deleted_receiver` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `full_index` (`id`,`sender_id`,`receiver_id`,`date_read`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_pms: ~0 rows (approximately)
/*!40000 ALTER TABLE `aauth_pms` DISABLE KEYS */;
/*!40000 ALTER TABLE `aauth_pms` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_users
CREATE TABLE IF NOT EXISTS `aauth_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_users: ~18 rows (approximately)
/*!40000 ALTER TABLE `aauth_users` DISABLE KEYS */;
INSERT INTO `aauth_users` (`id`, `email`, `pass`, `username`, `banned`, `last_login`, `last_activity`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `totp_secret`, `ip_address`) VALUES
	(1, 'admin@gmail.com', '0a464906490b8c7e21255b762cc0c5a34c5a09004ff0e3d7cbda8ede1c798357', '1809MeeNuu.00000001', 0, '2018-09-23 21:29:37', '2018-09-23 21:29:37', '2018-09-06 03:09:06', NULL, NULL, NULL, NULL, NULL, '::1'),
	(2, 'admin@pharmalinks.com', 'b778efd029a720b8d2121d2714d693ec68e614cb3c913de71e249774ed6a8aaf', '1809MeeNuu.00000002', 0, '2018-09-16 02:29:46', '2018-09-16 02:29:46', '2018-09-06 03:10:03', NULL, NULL, NULL, NULL, NULL, '::1'),
	(3, 'admin@pharma.com', '4b1fbcf811f49742fac555ff6ecb931ebf35a396cc933877acbd13fe369f1793', '1809MeeNuu.00000003', 0, NULL, NULL, '2018-09-06 03:11:39', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'admin@pharm.com', '9e0db257262003bf1a269878a0da0124623b40530f21bc64d4c6da373c407d69', '1809MeeNuu.00000004', 0, NULL, NULL, '2018-09-06 03:13:03', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'admin@pharmza.com', '34a521c0444e42ecde050fe21275f20ad090b7f074ead4ab90e047bc9fe5014d', '1809MeeNuu.00000005', 0, NULL, NULL, '2018-09-06 03:15:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'admin@pharmzan.com', '1e57e2e8c4390b54572415d095a108e422ef8c1c08df3d72b527c87586e39b1a', '1809MeeNuu.00000006', 0, NULL, NULL, '2018-09-06 03:17:24', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'admin@pharmzanaa.com', 'b11c97a5a069f713bf9f47ad54397a996cf3a0b91fd6df5d542a80d23ab1414f', '1809MeeNuu.00000007', 0, NULL, NULL, '2018-09-06 03:19:23', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'min@pharma.com', 'bb744cbbc9b6cd044280a603deafd710c5909bfcd6164caa15883b32a98ba887', '1809MeeNuu.00000008', 0, NULL, NULL, '2018-09-06 03:20:55', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'min@threetwo.com', '9bf279b1edf43e2d8428f6bc5ef228ac4bff901f641106ecc79afa2debc62806', '1809MeeNuu.00000009', 0, NULL, NULL, '2018-09-06 03:22:38', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'min@threetwoone.com', '06f3b03c31a6c3851d7b0bec7fc046134890f3892708158ccceea0a8f08fb9a4', '1809MeeNuu.00000010', 0, NULL, NULL, '2018-09-06 03:25:16', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'min@threetwooneasd.com', '87b2c2c74376bbdb8142d2a8013d5d75da65c13acfd795a699e21a72343fabdd', '1809MeeNuu.00000011', 0, NULL, NULL, '2018-09-06 03:27:07', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 'min@threneasd.com', '487b27c1dcb838c59bc7fb673cf85a2c05b523279c819c44ffac9fc45f24ccc6', '1809MeeNuu.00000012', 0, NULL, NULL, '2018-09-06 03:28:46', NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 'min@979798798.com', 'e5583bcbefebd1422f1ef2f7f2b98a7684e4849208fe5ce09e1192459e94e273', '1809MeeNuu.00000013', 0, NULL, NULL, '2018-09-06 03:30:34', NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 'min@98.com', 'f4faa5bbb1a5d9dd16c8d384ada3b62d05667c675e3a5caf68a0d899a4bbd816', '1809MeeNuu.00000014', 0, NULL, NULL, '2018-09-06 03:31:40', NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 'kikkikki@gmail.com', '02de8983bca9a1725a717b2d583a35562b989e66f7ec90d02d081b5a5af46a49', '1809MeeNuu.00000015', 0, NULL, NULL, '2018-09-06 03:34:09', NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 'kki@gmail.com', '94014f8aab10d529168647d91714a91af51a5ef670225ffe3835e0460dd61a46', '1809MeeNuu.00000016', 0, NULL, NULL, '2018-09-06 03:36:36', NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 'kkadasdi@gmail.com', '772c01cbab281ea227de1224cbadcb957efb8e7c1e4111adce5d4ff6db011e84', '1809MeeNuu.00000017', 0, NULL, NULL, '2018-09-06 03:40:21', NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 'huhuhuhuhhu@g.com', '05ee011e318c76d2995571ab58b95712b45702539241ca2f4c2af6de5bf1420c', '1809MeeNuu.00000018', 0, NULL, NULL, '2018-09-06 03:42:32', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `aauth_users` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_user_to_group
CREATE TABLE IF NOT EXISTS `aauth_user_to_group` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_user_to_group: ~17 rows (approximately)
/*!40000 ALTER TABLE `aauth_user_to_group` DISABLE KEYS */;
INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 2),
	(4, 2),
	(5, 2),
	(6, 2),
	(7, 2),
	(8, 2),
	(9, 2),
	(10, 2),
	(11, 2),
	(12, 2),
	(13, 2),
	(14, 2),
	(15, 2),
	(16, 2),
	(17, 2),
	(18, 2);
/*!40000 ALTER TABLE `aauth_user_to_group` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.aauth_user_variables
CREATE TABLE IF NOT EXISTS `aauth_user_variables` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table risingstardemo.aauth_user_variables: ~0 rows (approximately)
/*!40000 ALTER TABLE `aauth_user_variables` DISABLE KEYS */;
/*!40000 ALTER TABLE `aauth_user_variables` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.blocked_user_commissions
CREATE TABLE IF NOT EXISTS `blocked_user_commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rising_user_id` int(11) NOT NULL,
  `eligible_user_id` int(11) NOT NULL,
  `commision_amount` float NOT NULL,
  `package` enum('1','2','3','4','5') NOT NULL,
  `commision_type` enum('80','20','100','1') NOT NULL,
  `transaction_type` enum('direct','indirect','one_percent') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `ind_user_ids` (`eligible_user_id`,`rising_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.blocked_user_commissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `blocked_user_commissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `blocked_user_commissions` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.fund_e_cash_histry
CREATE TABLE IF NOT EXISTS `fund_e_cash_histry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_code` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(250) NOT NULL DEFAULT '0',
  `credit` varchar(250) NOT NULL DEFAULT '0',
  `debit` varchar(250) DEFAULT '0',
  `type` varchar(250) DEFAULT '0',
  `deduct_from` enum('e','c') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.fund_e_cash_histry: ~24 rows (approximately)
/*!40000 ALTER TABLE `fund_e_cash_histry` DISABLE KEYS */;
INSERT INTO `fund_e_cash_histry` (`id`, `member_code`, `description`, `credit`, `debit`, `type`, `deduct_from`, `created_at`) VALUES
	(1, '1809MeeNuu.00000001', 'admin cash', '2000000', '0', 'transfer', NULL, '2018-08-12 22:50:57'),
	(227, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000002', '0', '4500', 'entry', 'e', '2018-09-06 03:10:03'),
	(228, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000003', '0', '4500', 'entry', 'e', '2018-09-06 03:11:39'),
	(229, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000004', '0', '4500', 'entry', 'e', '2018-09-06 03:13:03'),
	(230, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000005', '0', '4500', 'entry', 'e', '2018-09-06 03:15:09'),
	(231, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000006', '0', '4500', 'entry', 'e', '2018-09-06 03:17:24'),
	(232, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000007', '0', '4500', 'entry', 'e', '2018-09-06 03:19:23'),
	(233, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000008', '0', '1000', 'entry', 'e', '2018-09-06 03:20:55'),
	(234, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000009', '0', '4500', 'entry', 'e', '2018-09-06 03:22:38'),
	(235, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000010', '0', '4500', 'entry', 'e', '2018-09-06 03:25:16'),
	(236, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000011', '0', '4500', 'entry', 'e', '2018-09-06 03:27:07'),
	(237, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000012', '0', '4500', 'entry', 'e', '2018-09-06 03:28:46'),
	(238, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000013', '0', '1000', 'entry', 'e', '2018-09-06 03:30:34'),
	(239, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000014', '0', '4500', 'entry', 'e', '2018-09-06 03:31:40'),
	(240, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000015', '0', '4500', 'entry', 'e', '2018-09-06 03:34:09'),
	(241, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000016', '0', '4500', 'entry', 'e', '2018-09-06 03:36:36'),
	(242, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000017', '0', '4500', 'entry', 'e', '2018-09-06 03:40:21'),
	(243, '1809MeeNuu.00000001', 'make entry by your account code is:1809MeeNuu.00000018', '0', '4500', 'entry', 'e', '2018-09-06 03:42:32'),
	(244, '1809MeeNuu.00000001', 'upgradePackage by your account.code is:1809MeeNuu.00000018', '0', '3500', 'entry', 'e', '2018-09-17 03:18:24'),
	(245, '1809MeeNuu.00000001', 'upgradePackage by your account.code is:1809MeeNuu.00000018', '0', '7000', 'entry', 'e', '2018-09-17 03:20:40'),
	(246, '1809MeeNuu.00000001', 'upgradePackage by your account.code is:1809MeeNuu.00000009', '0', '10500', 'entry', 'e', '2018-09-24 00:37:37'),
	(247, '1809MeeNuu.00000001', 'upgradePackage by your account.code is:1809MeeNuu.00000010', '0', '3500', 'entry', 'e', '2018-09-24 00:39:29'),
	(248, '1809MeeNuu.00000001', 'upgradePackage by your account.code is:1809MeeNuu.00000010', '0', '3500', 'entry', 'e', '2018-09-24 00:39:50'),
	(249, '1809MeeNuu.00000001', 'upgradePackage by your account.code is:1809MeeNuu.00000012', '0', '7000', 'entry', 'e', '2018-09-24 00:40:08');
/*!40000 ALTER TABLE `fund_e_cash_histry` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.package
CREATE TABLE IF NOT EXISTS `package` (
  `Package_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PackageName` varchar(50) DEFAULT NULL,
  `PackagePrice` int(11) DEFAULT NULL,
  PRIMARY KEY (`Package_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.package: ~5 rows (approximately)
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
INSERT INTO `package` (`Package_ID`, `PackageName`, `PackagePrice`) VALUES
	(1, 'MeeNuu stars', 1000),
	(2, 'SS', 3500),
	(3, 'GS', 7000),
	(4, 'PS', 10500),
	(5, 'DS', 14000);
/*!40000 ALTER TABLE `package` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.rising_commissions
CREATE TABLE IF NOT EXISTS `rising_commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rising_user_id` int(11) NOT NULL,
  `eligible_user_id` int(11) NOT NULL,
  `commision_amount` float NOT NULL,
  `package` enum('1','2','3','4','5') NOT NULL,
  `commision_type` enum('80','20','100','1','0') NOT NULL,
  `transaction_type` enum('direct','indirect','one_percent','azadi','meenustar') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `ind_user_ids` (`eligible_user_id`,`rising_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.rising_commissions: ~87 rows (approximately)
/*!40000 ALTER TABLE `rising_commissions` DISABLE KEYS */;
INSERT INTO `rising_commissions` (`id`, `rising_user_id`, `eligible_user_id`, `commision_amount`, `package`, `commision_type`, `transaction_type`, `created_at`) VALUES
	(1, 2, 1, 500, '1', '0', 'meenustar', '2018-09-06 03:10:03'),
	(2, 2, 1, 500, '1', '0', 'azadi', '2018-09-06 03:10:03'),
	(3, 2, 1, 1400, '2', '80', 'direct', '2018-09-06 03:10:03'),
	(4, 2, 1, 350, '2', '20', 'direct', '2018-09-06 03:10:03'),
	(5, 2, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:10:03'),
	(6, 3, 1, 500, '1', '0', 'meenustar', '2018-09-06 03:11:39'),
	(7, 3, 1, 500, '1', '0', 'azadi', '2018-09-06 03:11:39'),
	(8, 3, 1, 1400, '2', '80', 'indirect', '2018-09-06 03:11:39'),
	(9, 3, 1, 350, '2', '20', 'indirect', '2018-09-06 03:11:39'),
	(10, 3, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:11:39'),
	(11, 4, 3, 500, '1', '0', 'meenustar', '2018-09-06 03:13:03'),
	(12, 4, 1, 500, '1', '0', 'azadi', '2018-09-06 03:13:03'),
	(13, 4, 3, 1400, '2', '80', 'direct', '2018-09-06 03:13:03'),
	(14, 4, 1, 350, '2', '20', 'direct', '2018-09-06 03:13:03'),
	(15, 4, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:13:03'),
	(16, 5, 3, 500, '1', '0', 'meenustar', '2018-09-06 03:15:09'),
	(17, 5, 1, 500, '1', '0', 'azadi', '2018-09-06 03:15:09'),
	(18, 5, 1, 1400, '2', '80', 'indirect', '2018-09-06 03:15:09'),
	(19, 5, 1, 350, '2', '20', 'indirect', '2018-09-06 03:15:09'),
	(20, 5, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:15:09'),
	(21, 6, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:17:24'),
	(22, 6, 1, 500, '1', '0', 'azadi', '2018-09-06 03:17:24'),
	(23, 6, 4, 1400, '2', '80', 'direct', '2018-09-06 03:17:24'),
	(24, 6, 1, 350, '2', '20', 'direct', '2018-09-06 03:17:24'),
	(25, 6, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:17:24'),
	(26, 7, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:19:23'),
	(27, 7, 1, 500, '1', '0', 'azadi', '2018-09-06 03:19:23'),
	(28, 7, 3, 1400, '2', '80', 'indirect', '2018-09-06 03:19:23'),
	(29, 7, 1, 350, '2', '20', 'indirect', '2018-09-06 03:19:23'),
	(30, 7, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:19:23'),
	(31, 8, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:20:55'),
	(32, 8, 1, 500, '1', '0', 'azadi', '2018-09-06 03:20:55'),
	(33, 9, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:22:38'),
	(34, 9, 1, 500, '1', '0', 'azadi', '2018-09-06 03:22:38'),
	(35, 9, 3, 1400, '2', '80', 'indirect', '2018-09-06 03:22:38'),
	(36, 9, 1, 350, '2', '20', 'indirect', '2018-09-06 03:22:38'),
	(37, 9, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:22:38'),
	(38, 10, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:25:16'),
	(39, 10, 1, 500, '1', '0', 'azadi', '2018-09-06 03:25:16'),
	(40, 10, 4, 1400, '2', '80', 'direct', '2018-09-06 03:25:16'),
	(41, 10, 1, 350, '2', '20', 'direct', '2018-09-06 03:25:16'),
	(42, 10, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:25:16'),
	(43, 11, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:27:07'),
	(44, 11, 1, 500, '1', '0', 'azadi', '2018-09-06 03:27:07'),
	(45, 11, 3, 1400, '2', '80', 'indirect', '2018-09-06 03:27:07'),
	(46, 11, 1, 350, '2', '20', 'indirect', '2018-09-06 03:27:07'),
	(47, 11, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:27:07'),
	(48, 12, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:28:46'),
	(49, 12, 1, 500, '1', '0', 'azadi', '2018-09-06 03:28:46'),
	(50, 12, 4, 1400, '2', '80', 'direct', '2018-09-06 03:28:46'),
	(51, 12, 1, 350, '2', '20', 'direct', '2018-09-06 03:28:46'),
	(52, 12, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:28:46'),
	(53, 13, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:30:34'),
	(54, 13, 1, 500, '1', '0', 'azadi', '2018-09-06 03:30:34'),
	(55, 14, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:31:40'),
	(56, 14, 1, 500, '1', '0', 'azadi', '2018-09-06 03:31:40'),
	(57, 14, 4, 1400, '2', '80', 'direct', '2018-09-06 03:31:40'),
	(58, 14, 1, 350, '2', '20', 'direct', '2018-09-06 03:31:40'),
	(59, 14, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:31:40'),
	(60, 15, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:34:09'),
	(61, 15, 1, 500, '1', '0', 'azadi', '2018-09-06 03:34:09'),
	(62, 15, 3, 1400, '2', '80', 'indirect', '2018-09-06 03:34:09'),
	(63, 15, 1, 350, '2', '20', 'indirect', '2018-09-06 03:34:09'),
	(64, 15, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:34:09'),
	(65, 16, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:36:36'),
	(66, 16, 1, 500, '1', '0', 'azadi', '2018-09-06 03:36:36'),
	(67, 16, 4, 1400, '2', '80', 'direct', '2018-09-06 03:36:36'),
	(68, 16, 1, 350, '2', '20', 'direct', '2018-09-06 03:36:36'),
	(69, 16, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:36:36'),
	(70, 17, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:40:21'),
	(71, 17, 1, 500, '1', '0', 'azadi', '2018-09-06 03:40:21'),
	(72, 17, 3, 1400, '2', '80', 'indirect', '2018-09-06 03:40:21'),
	(73, 17, 1, 350, '2', '20', 'indirect', '2018-09-06 03:40:21'),
	(74, 17, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:40:21'),
	(75, 18, 4, 500, '1', '0', 'meenustar', '2018-09-06 03:42:32'),
	(76, 18, 1, 500, '1', '0', 'azadi', '2018-09-06 03:42:32'),
	(77, 18, 4, 1400, '2', '80', 'direct', '2018-09-06 03:42:32'),
	(78, 18, 1, 350, '2', '20', 'direct', '2018-09-06 03:42:32'),
	(79, 18, 1, 35, '2', '1', 'one_percent', '2018-09-06 03:42:32'),
	(92, 12, 4, 1400, '3', '80', 'direct', '2018-09-17 02:49:40'),
	(93, 12, 1, 350, '3', '20', 'direct', '2018-09-17 02:49:40'),
	(94, 12, 1, 35, '3', '1', 'one_percent', '2018-09-17 02:49:40'),
	(95, 18, 4, 1400, '2', '80', 'direct', '2018-09-17 03:18:24'),
	(96, 18, 1, 350, '2', '20', 'direct', '2018-09-17 03:18:24'),
	(97, 18, 1, 35, '2', '1', 'one_percent', '2018-09-17 03:18:24'),
	(98, 18, 4, 2800, '4', '80', 'direct', '2018-09-17 03:20:40'),
	(99, 18, 1, 700, '4', '20', 'direct', '2018-09-17 03:20:40'),
	(100, 18, 1, 70, '4', '1', 'one_percent', '2018-09-17 03:20:40'),
	(101, 9, 1, 105, '5', '1', 'one_percent', '2018-09-24 00:37:37'),
	(102, 10, 4, 1400, '3', '80', 'direct', '2018-09-24 00:39:29'),
	(103, 10, 1, 35, '3', '1', 'one_percent', '2018-09-24 00:39:29'),
	(104, 10, 4, 1400, '4', '80', 'direct', '2018-09-24 00:39:50'),
	(105, 10, 1, 35, '4', '1', 'one_percent', '2018-09-24 00:39:50'),
	(106, 12, 4, 2800, '5', '80', 'direct', '2018-09-24 00:40:09'),
	(107, 12, 1, 70, '5', '1', 'one_percent', '2018-09-24 00:40:09');
/*!40000 ALTER TABLE `rising_commissions` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.rising_missed_commissions
CREATE TABLE IF NOT EXISTS `rising_missed_commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rising_user_id` int(11) NOT NULL,
  `eligible_user_id` int(11) NOT NULL,
  `commision_amount` float NOT NULL,
  `package` enum('1','2','3','4','5') NOT NULL,
  `commision_type` enum('80','20','meenustar') NOT NULL,
  `transaction_type` enum('direct','indirect','meenustar') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `ind_user_ids` (`eligible_user_id`,`rising_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.rising_missed_commissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `rising_missed_commissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `rising_missed_commissions` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.rising_users
CREATE TABLE IF NOT EXISTS `rising_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cnic` int(11) unsigned NOT NULL,
  `address` varchar(200) NOT NULL,
  `kin_name` varchar(50) NOT NULL,
  `kin_relation` varchar(50) NOT NULL,
  `kin_cnic` int(11) NOT NULL,
  `mobile_no` varchar(11) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `introducer_code` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `user_pin` int(11) NOT NULL,
  `user_pic_path` varchar(500) NOT NULL,
  `previous` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `package_updated_at` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `package_id` (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.rising_users: ~18 rows (approximately)
/*!40000 ALTER TABLE `rising_users` DISABLE KEYS */;
INSERT INTO `rising_users` (`id`, `cnic`, `address`, `kin_name`, `kin_relation`, `kin_cnic`, `mobile_no`, `father_name`, `parent_id`, `introducer_code`, `user_id`, `package_id`, `location`, `full_name`, `user_pin`, `user_pic_path`, `previous`, `created_at`, `updated_at`, `package_updated_at`) VALUES
	(1, 35201, 'Lahore cantt', 'faraz', 'brother', 35202, '0343-483289', '', 0, '1808MeeNuu.00000000', 1, 5, 0, 'Imran Rehmat', 1234, 'https://scontent.flhe3-1.fna.fbcdn.net/v/t1.0-9/18557027_1657527027610639_2858257023122103052_n.jpg?_nc_cat=0&oh=6ab94ab7f84a7060537f70df3ba304db&oe=5B7CD36B', '2018-09-06 00:00:00', '2018-09-06 03:09:06', '0000-00-00 00:00:00', ''),
	(2, 62371, 'cantt', 'jill', 'bro', 0, '76687686786', 'mr.doe', 1, '1809MeeNuu.00000001', 2, 3, 1, 'john doe', 1947, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-16 00:26:43', '2018-09-16 00:26:43', 'a:3:{i:0;a:2:{s:4:"date";s:19:"2018-09-16 00:22:10";s:10:"package_id";s:1:"3";}i:1;a:2:{s:4:"date";s:19:"2018-09-16 00:26:33";s:10:"package_id";s:1:"3";}i:2;a:2:{s:4:"date";s:19:"2018-09-16 00:26:43";s:10:"package_id";s:1:"3";}}'),
	(3, 62371, 'cantt', 'jill', 'bro', 0, '67576576567', 'mr.doe', 1, '1809MeeNuu.00000001', 3, 2, 2, 'john doe', 1024, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-06 03:11:39', '0000-00-00 00:00:00', 'a:1:{i:2;s:19:"2018-09-06 03:11:39";}'),
	(4, 55555, 'cantt', 'jill', 'bro', 0, '21312312314', 'mr.doe', 3, '1809MeeNuu.00000003', 4, 3, 1, 'john doe', 9526, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-16 00:27:39', '2018-09-16 00:27:39', 'a:1:{i:0;a:2:{s:4:"date";s:19:"2018-09-16 00:27:39";s:10:"package_id";s:1:"3";}}'),
	(5, 77777, 'cantt', 'jill', 'bro', 0, '12121212122', 'mr.doe', 3, '1809MeeNuu.00000003', 5, 2, 2, 'john doe', 4357, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-06 03:15:09', '0000-00-00 00:00:00', 'a:1:{i:2;s:19:"2018-09-06 03:15:09";}'),
	(6, 62371, 'cantt', 'jill', 'bro', 0, '87878787878', 'mr.doe', 4, '1809MeeNuu.00000004', 6, 2, 1, 'john doe', 9251, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-06 03:17:24', '0000-00-00 00:00:00', 'a:1:{i:2;s:19:"2018-09-06 03:17:24";}'),
	(7, 77777, 'cantt', 'jill', 'bro', 0, '66676746567', 'mr.doe', 4, '1809MeeNuu.00000004', 7, 3, 2, 'john doe', 5102, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-17 02:41:14', '2018-09-17 02:41:14', 'a:2:{i:0;a:2:{s:4:"date";s:19:"2018-09-17 02:40:44";s:10:"package_id";s:1:"3";}i:1;a:2:{s:4:"date";s:19:"2018-09-17 02:41:14";s:10:"package_id";s:1:"3";}}'),
	(8, 55555, 'cantt', 'jill', 'bro', 0, '65656656666', 'mr.doe', 4, '1809MeeNuu.00000004', 8, 1, 3, 'john doe', 962, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-06 03:20:55', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:"2018-09-06 03:20:55";}'),
	(9, 88888, 'cantt', 'jill', 'bro', 0, '23232323232', 'mr.doe', 4, '1809MeeNuu.00000004', 9, 5, 4, 'john doe', 3947, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-24 00:37:37', '2018-09-24 00:37:37', 'a:1:{i:0;a:2:{s:4:"date";s:19:"2018-09-24 00:37:37";s:10:"package_id";s:1:"5";}}'),
	(10, 32344, 'cantt', 'jill', 'bro', 0, '35353535464', 'mr.doe', 4, '1809MeeNuu.00000004', 10, 4, 5, 'john doe', 5238, '1809MeeNuu.00000001', '1970-01-01 00:00:00', '2018-09-24 00:39:50', '2018-09-24 00:39:50', 'a:2:{i:0;a:2:{s:4:"date";s:19:"2018-09-24 00:39:29";s:10:"package_id";s:1:"3";}i:1;a:2:{s:4:"date";s:19:"2018-09-24 00:39:50";s:10:"package_id";s:1:"4";}}'),
	(11, 56656, 'cantt', 'jill', 'bro', 0, '66656565343', 'mr.doe', 4, '1809MeeNuu.00000004', 11, 3, 6, 'john doe', 7841, '1809MeeNuu.00000001', '2018-09-03 00:00:00', '2018-09-17 02:46:27', '2018-09-17 02:46:27', 'a:1:{i:0;a:2:{s:4:"date";s:19:"2018-09-17 02:46:27";s:10:"package_id";s:1:"3";}}'),
	(12, 32344, 'cantt', 'jill', 'bro', 0, '66546588865', 'mr.doe', 4, '1809MeeNuu.00000004', 12, 5, 7, 'john doe', 2716, '1809MeeNuu.00000001', '1970-01-01 00:00:00', '2018-09-24 00:40:08', '2018-09-24 00:40:08', 'a:2:{i:0;a:2:{s:4:"date";s:19:"2018-09-17 02:49:40";s:10:"package_id";s:1:"3";}i:1;a:2:{s:4:"date";s:19:"2018-09-24 00:40:08";s:10:"package_id";s:1:"5";}}'),
	(13, 62371, 'cantt', 'jill', 'bro', 0, '76767676767', 'mr.doe', 4, '1809MeeNuu.00000004', 13, 2, 8, 'john doe', 8301, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-17 02:54:28', '2018-09-17 02:54:28', 'a:1:{i:0;a:2:{s:4:"date";s:19:"2018-09-17 02:54:28";s:10:"package_id";s:1:"2";}}'),
	(14, 62371, 'cantt', 'jill', 'bro', 0, '76576674654', 'mr.doe', 4, '1809MeeNuu.00000004', 14, 2, 9, 'john doe', 1854, '1809MeeNuu.00000001', '2018-09-04 00:00:00', '2018-09-06 03:31:40', '0000-00-00 00:00:00', 'a:1:{i:2;s:19:"2018-09-06 03:31:40";}'),
	(15, 32234, 'cantt', 'jill', 'bro', 0, '63663878786', 'mr.doe', 4, '1809MeeNuu.00000004', 15, 2, 10, 'john doe', 763, '1809MeeNuu.00000001', '2018-09-04 00:00:00', '2018-09-06 03:34:09', '0000-00-00 00:00:00', 'a:1:{i:2;s:19:"2018-09-06 03:34:09";}'),
	(16, 32344, 'cantt', 'jill', 'bro', 0, '28928655555', 'mr.doe', 4, '1809MeeNuu.00000004', 16, 2, 11, 'john doe', 5892, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-06 03:36:36', '0000-00-00 00:00:00', 'a:1:{i:2;s:19:"2018-09-06 03:36:36";}'),
	(17, 32344, 'cantt', 'jill', 'bro', 0, '73737337373', 'mr.doe', 4, '1809MeeNuu.00000004', 17, 2, 12, 'john doe', 3960, '1809MeeNuu.00000001', '2018-09-01 00:00:00', '2018-09-06 03:40:21', '0000-00-00 00:00:00', 'a:1:{i:2;s:19:"2018-09-06 03:40:21";}'),
	(18, 55555, 'cantt', 'jill', 'bro', 0, '12121212121', 'mr.doe', 4, '1809MeeNuu.00000004', 18, 4, 13, 'john doe', 2460, 'http://localhost/risingstar.com/assets//uploads/no-pic.png', '2018-09-01 00:00:00', '2018-09-17 03:20:40', '2018-09-17 03:20:40', 'a:2:{i:0;a:2:{s:4:"date";s:19:"2018-09-17 03:18:24";s:10:"package_id";s:1:"2";}i:1;a:2:{s:4:"date";s:19:"2018-09-17 03:20:40";s:10:"package_id";s:1:"4";}}');
/*!40000 ALTER TABLE `rising_users` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.rising_users_tree
CREATE TABLE IF NOT EXISTS `rising_users_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aauth_user_id` int(11) NOT NULL,
  `parents` varchar(8000) NOT NULL,
  `user_tree_level` varchar(255) NOT NULL DEFAULT '0',
  `user_team_members` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.rising_users_tree: 18 rows
/*!40000 ALTER TABLE `rising_users_tree` DISABLE KEYS */;
INSERT INTO `rising_users_tree` (`id`, `aauth_user_id`, `parents`, `user_tree_level`, `user_team_members`) VALUES
	(1, 1, '0', '3', '17'),
	(2, 2, '1', '0', '0'),
	(3, 3, '1', '2', '15'),
	(4, 4, '3,1', '1', '13'),
	(5, 5, '3,1', '0', '0'),
	(6, 6, '4,3,1', '0', '0'),
	(7, 7, '4,3,1', '0', '0'),
	(8, 8, '4,3,1', '0', '0'),
	(9, 9, '4,3,1', '0', '0'),
	(10, 10, '4,3,1', '0', '0'),
	(11, 11, '4,3,1', '0', '0'),
	(12, 12, '4,3,1', '0', '0'),
	(13, 13, '4,3,1', '0', '0'),
	(14, 14, '4,3,1', '0', '0'),
	(15, 15, '4,3,1', '0', '0'),
	(16, 16, '4,3,1', '0', '0'),
	(17, 17, '4,3,1', '0', '0'),
	(18, 18, '4,3,1', '0', '0');
/*!40000 ALTER TABLE `rising_users_tree` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.transfer_amount_history
CREATE TABLE IF NOT EXISTS `transfer_amount_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_code` varchar(50) NOT NULL DEFAULT '0',
  `reciver_code` varchar(50) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL DEFAULT '0',
  `debit` int(11) NOT NULL DEFAULT '0',
  `deduct_from` char(50) NOT NULL DEFAULT '0',
  `description` varchar(50) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.transfer_amount_history: ~0 rows (approximately)
/*!40000 ALTER TABLE `transfer_amount_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `transfer_amount_history` ENABLE KEYS */;

-- Dumping structure for table risingstardemo.withdraw_payment_histry
CREATE TABLE IF NOT EXISTS `withdraw_payment_histry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `member_code` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table risingstardemo.withdraw_payment_histry: ~0 rows (approximately)
/*!40000 ALTER TABLE `withdraw_payment_histry` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdraw_payment_histry` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
