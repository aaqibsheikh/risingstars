<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class my_mail extends CI_Email {

    public $superadmin = "imrran.rehmat@risingstar.com";

    public function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->library('aauth');
        $CI->load->model('UserModel');
    }

    public function mail_to_one($data) {
        $message = $data['message'];

//        if (isset($data['reply_to'])) {
//            $this->reply_to($data['reply_to'], $data['reply_to_name']);
//            $this->from('no-reply@amps-global.com', $data['reply_to_name']);
//        } else {
//            $this->reply_to('', '');
//            $this->from('no-reply@risingstar.com', 'Bookingware');
//        }
//        $headers = 'jorikafla@gmail.com';
//        $this->from($headers, 'risingstar');
//        $this->to($data['to']);
//        $this->subject($data['subject']);
//        $this->message($message);
        $from_email = "no-rply@risingStarInternational.com";
        $to_email = "jorikafla@gmail.com";

        //Load email library 


        $this->from($from_email);
        $this->to($data['to']);
        $this->subject($data['subject']);
        $this->message($message);

        $CI = & get_instance();

//        $response=$CI->email->send();
        if (!$this->send()) {
            echo $this->print_debugger();
        }
    }

    public function mail_user_register($data) {



        $mail['subject'] = 'Welcome to risingStar';
        $mail['message'] = 'Dear ' . $data['username'] . ',
                            
                            This is a confirmation that you are added to risingStar:
                            your login code is: ' . $data['login_code'] . ' 
                            your Password is: ' . $data['password'] . ' 
                            your PIN Code is: ' . $data['password'] . '    
                            your Introducer is: ' . $data['introducer'] . ' 
                           
                            To change the password please login to your account

                            -------					
                            risingStar Team';


        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

    public function mail_chnage_password($data) {

        $mail['subject'] = 'Password updated Successfully';
        $mail['message'] = 'Dear ' . $data['username'] . ',

                            This is a confirmation that you are added to risingStar:
                             
                            your new password is: ' . $data['password'] . ' 
                         
                           
                           

                            -------					
                            risingStar Team';


        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

    public function mail_reset_password($data) {

        $mail['subject'] = 'Password reset Successfully';
        $mail['message'] = 'Dear ' . $data['username'] . ',

                            please find your new password in following details:
                            Email: ' . $data['email'] . ' 
                           
                            your new Password: ' . $data['password'] . ' 
                           
                            click here '.SITE_URL.' to Login Risising Star App
                                
                            Note: Contact rising star Administrator if you did not request a password reset.
                            ---------------					
                            risingStar Team';


        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

    public function mail_reset_pin($data) {

        $mail['subject'] = 'Password reset Successfully';
        $mail['message'] = 'Dear ' . $data['username'] . ',

                            This is a confirmation that you are added to risingStar:
                            Email: ' . $data['email'] . ' 
                           
                            your new PIN code is: ' . $data['PIN_code'] . ' 
                           
                            To reset the pin please login to your account

                            -------					
                            risingStar Team';


        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

    public function mail_package_update($data) {

        $mail['subject'] = 'package updated Successfully';
        $mail['message'] = 'Dear ' . $data['username'] . ',

                            This is a confirmation that you are added to risingStar:
                            
                           using this code package upgradet: ' . $data['introducer'] . ' 
                           
                            New package is : ' . $data['package'] . ' 
                                
                          

                            -------					
                            risingStar Team';


        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

    public function mail_log_eighty_percent_commission($data) {

        $mail['subject'] = 'Congragulations you get 80% Percent benift with given details';
        $mail['message'] = 'Dear ' . $data['username'] . ',

                           
                            
                          You get commission against this user code: ' . $data['rising_user_id'] . ' 
                              
                          Your calculated commission amount is: ' . $data['commission_amount'] . ' 
                              
                          you commission type is : ' . $data['commission_type'] . ' 
                            transaction type is : ' . $data['transaction_type'] . '    
                          and get commission against this package : ' . $data['package'] . ' 
                                
                          
                            -------					
                            risingStar Team';
        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

    public function mail_log_twenty_percent_commission($data) {

        $mail['subject'] = 'Congragulations you get 20%  Percent benift with given details';
        $mail['message'] = 'Dear ' . $data['username'] . ',

                           
                            
                          You get commission against this user code: ' . $data['rising_user_id'] . ' 
                              
                          Your calculated commission amount is: ' . $data['commission_amount'] . ' 
                              
                          you commission type is : ' . $data['commission_type'] . ' 
                           transaction type is : ' . $data['transaction_type'] . '     
                          and get commission against this package : ' . $data['package'] . ' 
                                
                          
                            -------					
                            risingStar Team';
        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

    public function mail_log_One_percent_commission($data) {

        $mail['subject'] = 'Congragulations you get 1% benift with given details';
        $mail['message'] = 'Dear ' . $data['username'] . ',

                           
                            
                          You get commission against this user code: ' . $data['rising_user_id'] . ' 
                              
                          Your calculated commission amount is: ' . $data['commission_amount'] . ' 
                              
                          you commission type is : ' . $data['commission_type'] . ' 
                            transaction type is : ' . $data['transaction_type'] . '    
                          and get commission against this package : ' . $data['package'] . ' 
                                
                          
                            -------					
                            risingStar Team';
        $mail['to'] = $data['email'];
        return($this->mail_to_one($mail));
    }

}