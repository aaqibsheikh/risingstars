-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 18, 2018 at 07:31 PM
-- Server version: 5.5.59-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stacktri_risingstar`
--

-- --------------------------------------------------------

--
-- Table structure for table `aauth_groups`
--

CREATE TABLE `aauth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_groups`
--

INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
(1, 'Admin', 'Super Admin Group'),
(2, 'Public', 'Public Access Group');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_group_to_group`
--

CREATE TABLE `aauth_group_to_group` (
  `group_id` int(11) UNSIGNED NOT NULL,
  `subgroup_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_login_attempts`
--

CREATE TABLE `aauth_login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(39) DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aauth_login_attempts`
--

INSERT INTO `aauth_login_attempts` (`id`, `ip_address`, `timestamp`, `login_attempts`) VALUES
(2, '119.158.32.20', '2018-07-17 10:37:04', 11),
(3, '182.182.193.121', '2018-07-17 10:38:44', 3),
(4, '119.158.32.20', '2018-07-17 11:00:52', 1),
(15, '39.46.207.155', '2018-07-18 16:43:12', 2);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perms`
--

CREATE TABLE `aauth_perms` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_group`
--

CREATE TABLE `aauth_perm_to_group` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_user`
--

CREATE TABLE `aauth_perm_to_user` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_pms`
--

CREATE TABLE `aauth_pms` (
  `id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT '0',
  `pm_deleted_receiver` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_users`
--

CREATE TABLE `aauth_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_users`
--

INSERT INTO `aauth_users` (`id`, `email`, `pass`, `username`, `banned`, `last_login`, `last_activity`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `totp_secret`, `ip_address`) VALUES
(1, 'aloedoctor555@gmail.com', '0a464906490b8c7e21255b762cc0c5a34c5a09004ff0e3d7cbda8ede1c798357', '180700001', 0, '2018-07-18 16:57:22', '2018-07-18 16:57:22', '2018-07-17 17:07:33', NULL, NULL, NULL, NULL, NULL, '137.59.230.93'),
(2, 'mzahidofficial29@gmail.com', 'b778efd029a720b8d2121d2714d693ec68e614cb3c913de71e249774ed6a8aaf', '180700002', 0, NULL, NULL, '2018-07-17 17:35:42', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'kjdfkj@gmail.com', '4b1fbcf811f49742fac555ff6ecb931ebf35a396cc933877acbd13fe369f1793', '180700003', 0, NULL, NULL, '2018-07-17 17:59:15', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'kjdfkjhjhjhj@gmail.com', '9e0db257262003bf1a269878a0da0124623b40530f21bc64d4c6da373c407d69', '180700004', 0, NULL, NULL, '2018-07-17 18:00:07', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'kjdfkjhjhj@gmail.com', '34a521c0444e42ecde050fe21275f20ad090b7f074ead4ab90e047bc9fe5014d', '180700005', 0, NULL, NULL, '2018-07-17 18:01:28', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'kokikiikkikiikik@gmail.com', '1e57e2e8c4390b54572415d095a108e422ef8c1c08df3d72b527c87586e39b1a', '180700006', 0, NULL, NULL, '2018-07-17 18:02:58', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'dfadsfdfdsaf@gmail.com', 'b11c97a5a069f713bf9f47ad54397a996cf3a0b91fd6df5d542a80d23ab1414f', '180700007', 0, NULL, NULL, '2018-07-17 18:08:54', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'dfadsfdfsaf@gmail.com', 'bb744cbbc9b6cd044280a603deafd710c5909bfcd6164caa15883b32a98ba887', '180700008', 0, NULL, NULL, '2018-07-17 18:09:57', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'dfdsfdfsaf@gmail.com', '9bf279b1edf43e2d8428f6bc5ef228ac4bff901f641106ecc79afa2debc62806', '180700009', 0, NULL, NULL, '2018-07-17 18:11:07', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'dfsfdfsaf@gmail.com', '06f3b03c31a6c3851d7b0bec7fc046134890f3892708158ccceea0a8f08fb9a4', '180700010', 0, NULL, NULL, '2018-07-17 18:11:47', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'dffdfsaf@gmail.com', '87b2c2c74376bbdb8142d2a8013d5d75da65c13acfd795a699e21a72343fabdd', '180700011', 0, NULL, NULL, '2018-07-17 18:12:34', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'dfdfsaf@gmail.com', '487b27c1dcb838c59bc7fb673cf85a2c05b523279c819c44ffac9fc45f24ccc6', '180700012', 0, NULL, NULL, '2018-07-17 18:13:46', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'dfgdfsaf@gmail.com', 'e5583bcbefebd1422f1ef2f7f2b98a7684e4849208fe5ce09e1192459e94e273', '180700013', 0, NULL, NULL, '2018-07-17 18:14:36', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'dfsagsc@gmail.com', 'f4faa5bbb1a5d9dd16c8d384ada3b62d05667c675e3a5caf68a0d899a4bbd816', '180700014', 0, NULL, NULL, '2018-07-17 18:22:17', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'dfsagscr@gmail.com', '02de8983bca9a1725a717b2d583a35562b989e66f7ec90d02d081b5a5af46a49', '180700015', 0, NULL, NULL, '2018-07-17 18:23:07', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'dfsascr@gmail.com', '94014f8aab10d529168647d91714a91af51a5ef670225ffe3835e0460dd61a46', '180700016', 0, NULL, NULL, '2018-07-17 18:23:56', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'dfsasccqr@gmail.com', '772c01cbab281ea227de1224cbadcb957efb8e7c1e4111adce5d4ff6db011e84', '180700017', 0, NULL, NULL, '2018-07-17 18:25:22', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'uiafdhiuefheiuf@gmail.com', '05ee011e318c76d2995571ab58b95712b45702539241ca2f4c2af6de5bf1420c', '180700018', 0, NULL, NULL, '2018-07-17 18:28:13', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'uiafdhiuefhiuf@gmail.com', 'f29fb4972e84ea5e08ea865cba87f7958097f5578b23c7c532e064318193ec15', '180700019', 0, NULL, NULL, '2018-07-17 18:29:07', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'uiafdhiufhiuf@gmail.com', '99257a07e20535831df61f5083977d5ff309008ebbf75f3ee7e2f83f3a73b445', '180700020', 0, NULL, NULL, '2018-07-17 18:30:01', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'uafdhiufhiuf@gmail.com', '2c23f99da2adeb572f1427b6877240e6a422b831a60611a2cc984614730f5d17', '180700021', 0, NULL, NULL, '2018-07-17 18:30:48', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'uafdhiufhif@gmail.com', '79e79356a02e0447e7fff92566873809ea8ec7946585afad270b09f3a0816361', '180700022', 0, NULL, NULL, '2018-07-17 18:31:51', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'uafdiufhif@gmail.com', 'a62e25cfa40ed6b02e941a87112c7646811fe37854a4bd0f84119c57892ee706', '180700023', 0, NULL, NULL, '2018-07-17 18:41:26', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'uafiufhif@gmail.com', 'c8fd9e64bc73b82c97e2222adeb691bf6e597f653e0b8aa16c9d48063fa5a453', '180700024', 0, NULL, NULL, '2018-07-17 18:42:07', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'uafufhif@gmail.com', '9ab1a0bdddef82808f56b37fc89fff65c8c2e576a00d5002104ff076a01b694d', '180700025', 0, NULL, NULL, '2018-07-17 18:43:09', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'ufufhif@gmail.com', '93710d407a8a9212dec4018c7f767932775188344f4245ee9d102b4fc2f9ff39', '180700026', 0, NULL, NULL, '2018-07-17 18:44:01', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'adsfsadfds@gmail.com', '466a4c6bb650cc0bbbfb22e84d646707941f6da5b2ab24cd8e6e2fc5d5c1cf7a', '180700027', 0, NULL, NULL, '2018-07-17 18:56:01', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'adsfdfds@gmail.com', 'c66eba351c841085618efc9dcc4ba249dc9c199e1bd33f2a5b55056e5ed3b8f5', '180700028', 0, NULL, NULL, '2018-07-17 18:56:36', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'sdjkfhsf@gmail.com', '3cea9ae034703e67eb70df3c7f154068c256588ec2e258649b771aaa49925fdb', '180700029', 0, NULL, NULL, '2018-07-17 18:58:45', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'sdjfhsf@gmail.com', 'f4b9a66f2b61869766bd33e7700aa9870df5db3a220a541f077c53b872647de1', '180700030', 0, NULL, NULL, '2018-07-17 18:59:16', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'sdjfhs@gmail.com', '2db7f146ef27b5017de7af788d07d3c632259ba507e91b1bed7f2603f12e0ae0', '180700031', 0, NULL, NULL, '2018-07-17 18:59:53', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'sdjfhsaass@gmail.com', '7e13437d91cb457281ff965bf43071e243120012cf38d9e29c620f3a014ab74d', '180700032', 0, NULL, NULL, '2018-07-17 19:00:28', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'ferer454454@yahoo.com', 'f5a465049d2443026c486b5876285fc3d7a17ac988196c713417115716589dd2', '180700033', 0, NULL, NULL, '2018-07-18 12:49:20', NULL, NULL, NULL, NULL, NULL, NULL),
(34, '6456554@yahoo.com', 'ea1b585a3530f5046957c69dd696e481a8ad777ff49e7afb7549b657a0a2ed48', '180700034', 0, NULL, NULL, '2018-07-18 12:51:06', NULL, NULL, NULL, NULL, NULL, NULL),
(35, '4564545@yahoo.com', '935a673ec3c34c17e2fcad7451afa5216f230fe50ad3c48ad4c056f684b559b2', '180700035', 0, NULL, NULL, '2018-07-18 12:52:26', NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'fgdfgdfg45645@yahoo.com', '973692e698431a763f98368497dbdd6190b2292b78b991e0cdf78cbccca7c509', '180700036', 0, NULL, NULL, '2018-07-18 12:55:24', NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'hght5656645@yahoo.com', 'b73b3f282d977b72dd0f61ed20ddcb109310557f4ce518b1cbb3b3abb9e2b414', '180700037', 0, NULL, NULL, '2018-07-18 12:56:27', NULL, NULL, NULL, NULL, NULL, NULL),
(38, '4564564565@yahoo.com', '9b03486c541ae1d5fcdc9e00c6838f0d83aa89bc0b87e00c15e84f1f3a532f99', '180700038', 0, NULL, NULL, '2018-07-18 12:57:17', NULL, NULL, NULL, NULL, NULL, NULL),
(39, '75675675675665@yahoo.com', '307764ef7bd5cb6abbf429cfa04369afbdabc22d11fbb6868f3ff9ea8a853e35', '180700039', 0, NULL, NULL, '2018-07-18 12:58:14', NULL, NULL, NULL, NULL, NULL, NULL),
(40, '56756hfgh@yahoo.com', 'c79c462bbda008419172af42b54d4876ee48974e5a15604303325beafd732735', '180700040', 0, NULL, NULL, '2018-07-18 12:59:00', NULL, NULL, NULL, NULL, NULL, NULL),
(41, '534534534dfgdfgdf@yahoo.com', 'fad8a41f6ef4be5bd19a91e37d850cc7e14e34c73f8535c78361bc12b8286742', '180700041', 0, NULL, NULL, '2018-07-18 13:00:58', NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'retert456456gdfgdf@yahoo.com', '85c7228569fbccbf4a3039224e9404ec467d9d4ff69d33f469e74be79bef4e9a', '180700042', 0, NULL, NULL, '2018-07-18 13:01:59', NULL, NULL, NULL, NULL, NULL, NULL),
(43, '76876gfhgfhhgfdfgdf@yahoo.com', 'b85f19246ba43492f18fef9bfc47aa6642c011ac7406b33f70b4c8ebe08c6d5e', '180700043', 0, NULL, NULL, '2018-07-18 13:03:16', NULL, NULL, NULL, NULL, NULL, NULL),
(44, '5676fghfgfdsgdf@yahoo.com', 'f3470d8a375a01f47c67fb8e786c29f5b2624d60ce0425aaf0e4470cf76b9410', '180700044', 0, NULL, NULL, '2018-07-18 13:04:44', NULL, NULL, NULL, NULL, NULL, NULL),
(45, '45645tggfhfgh@yahoo.com', 'bb15e016fc3ad9ca4b6a0b6e11b5c57560cf139f01d61e38f41cee31b7452f46', '180700045', 0, NULL, NULL, '2018-07-18 13:06:38', NULL, NULL, NULL, NULL, NULL, NULL),
(46, '675675675675hfgh@yahoo.com', '58e0c3455a7039c3acbcaf0f3d29e5b72ea02cbc13d0901135dc1a4c3ab7916a', '180700046', 0, NULL, NULL, '2018-07-18 13:07:26', NULL, NULL, NULL, NULL, NULL, NULL),
(47, '345345fdgdfgdfgh@yahoo.com', 'a6600a7a8014987fb760d1e897190a04ce36b9d7d42093cd6a11686027ddef5e', '180700047', 0, NULL, NULL, '2018-07-18 13:09:24', NULL, NULL, NULL, NULL, NULL, NULL),
(48, '4564565dfgdfgdfgh@yahoo.com', 'de5190bc0be3c80ec5a610dc61b0997a8c59b2be3c5b021597ee90bc4439c7bc', '180700048', 0, NULL, NULL, '2018-07-18 13:10:32', NULL, NULL, NULL, NULL, NULL, NULL),
(49, '567567567567dfgdfgh@yahoo.com', '141bf6c1fb782174492127adf0242c4d58fe8b1c1ff5211ba23287a83af4e885', '180700049', 0, NULL, NULL, '2018-07-18 13:11:18', NULL, NULL, NULL, NULL, NULL, NULL),
(50, '6456454564567dfgdfgh@yahoo.com', '6f62a9b70b28c45f66f1f7a0832ee9d9523b533c0e02ffa6f806a2605bf84500', '180700050', 0, NULL, NULL, '2018-07-18 13:12:41', NULL, NULL, NULL, NULL, NULL, NULL),
(51, '876876hjfjfgh@yahoo.com', '2b81d30feb213565bd5181da71f7b3d3f977eb90a516950f10d172c7d234759e', '180700051', 0, NULL, NULL, '2018-07-18 13:13:20', NULL, NULL, NULL, NULL, NULL, NULL),
(52, '6456456456fgh@yahoo.com', '02cfc4ccc4c4c154c4b32db09e01bda99e1ce0fa8cdd3381f13da21cfd610d81', '180700052', 0, NULL, NULL, '2018-07-18 13:14:15', NULL, NULL, NULL, NULL, NULL, NULL),
(53, '6456456456f6756756756gh@yahoo.com', '2d91ef2f484c01a0cd1e06fdd892f324c11c4691c7327f111c9fee957ff43840', '180700053', 0, NULL, NULL, '2018-07-18 13:15:55', NULL, NULL, NULL, NULL, NULL, NULL),
(54, '76876hjgfhgfgh@yahoo.com', '6e6501565d238aab6fe750a0b5f9a72b48e121c8bae5217a8cf64a89aa90f643', '180700054', 0, NULL, NULL, '2018-07-18 13:16:45', NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'hgjghjghjghjghjgghjh@yahoo.com', '904330bd7062e421f575583784a61da7dd035b5b7f2e79e9b68a407c08bf98a9', '180700055', 0, NULL, NULL, '2018-07-18 13:17:32', NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'jghjghj57567567@yahoo.com', '47eec31e657002f71d115fe08247e220d0756fc29338f01a1a375761a2e3a087', '180700056', 0, NULL, NULL, '2018-07-18 13:18:25', NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'gthgfhfgh464564564567@yahoo.com', '0ce49afed66c47bbf3e2aaefa72c36bd76563e1f96e9f3691c1c33311751483c', '180700057', 0, NULL, NULL, '2018-07-18 13:19:06', NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'fgdfg45665564567@yahoo.com', '81954bf3f57d00ab7e1567737510ec213afb8520734e4f392d72eb89f2ee79e7', '180700058', 0, NULL, NULL, '2018-07-18 13:19:46', NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'sdfsdf345345fr76@yahoo.com', 'aef2fa1f9370fe094a2e993c95aeab4f033f1d2b1476d5b14f0f1c74681a4a3c', '180700059', 0, NULL, NULL, '2018-07-18 13:20:49', NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'gggg3sdfsdfsdf6@yahoo.com', '05e755a338a34fcc2ee660f4567e0bd6b0554ebe476941faa902dcef86d6ad19', '180700060', 0, NULL, NULL, '2018-07-18 13:21:41', NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'gdfgfgfg5676@yahoo.com', '0b47a7ecde5b81c3ca35ef367852ee27a2b02e8cec7cbf49b2533dfd3dbb5976', '180700061', 0, NULL, NULL, '2018-07-18 13:22:55', NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'trtrtrtrrtfgdfg@yahoo.com', '906d66719b310dca177e3778c7c8d76e893809676b528e19a63d8cb0d313d6ec', '180700062', 0, NULL, NULL, '2018-07-18 13:23:31', NULL, NULL, NULL, NULL, NULL, NULL),
(63, '55555566656565fg@yahoo.com', '4a48f1f12620a82f6ca04008f52cf6c9da024c6ae0ceeeacdbbdc202641f09b0', '180700063', 0, NULL, NULL, '2018-07-18 13:24:20', NULL, NULL, NULL, NULL, NULL, NULL),
(64, '567hgfhgfhfg@yahoo.com', 'c8f423c2614d47cc77ecd4da3460bdd80ceb0876a9511f815eb5c5dea952bd44', '180700064', 0, NULL, NULL, '2018-07-18 13:24:52', NULL, NULL, NULL, NULL, NULL, NULL),
(65, '78768tghgghghgfg@yahoo.com', '09e26500fd0870a5025a69b44645f4409bce79fb342808222bebb454a3d7c64c', '180700065', 0, NULL, NULL, '2018-07-18 13:25:49', NULL, NULL, NULL, NULL, NULL, NULL),
(66, '7567567gfhgfhgfhgfg@yahoo.com', 'e2c6704f2a6e191b7683e7beab7624e5360f0b78279eb0a01a70261d1e6d4642', '180700066', 0, NULL, NULL, '2018-07-18 13:26:30', NULL, NULL, NULL, NULL, NULL, NULL),
(67, '56567567567gfhgfg@yahoo.com', '8e7257aa445709bdd73665e32cf44ae6f4af46f0c2d0bbe800c04640c412a7c2', '180700067', 0, NULL, NULL, '2018-07-18 13:29:00', NULL, NULL, NULL, NULL, NULL, NULL),
(68, '78978yutyfhgfgfg@yahoo.com', '096735f78ab82b5b23ad9800d7eca13b6200933227bf64887f2682980df46dce', '180700068', 0, NULL, NULL, '2018-07-18 13:29:47', NULL, NULL, NULL, NULL, NULL, NULL),
(69, '456456456456@yahoo.com', 'fc4c13ec0d1dd1468231c80e504084d9c5582ea8df64807f3ec9685c718ee7b4', '180700069', 0, NULL, NULL, '2018-07-18 13:31:55', NULL, NULL, NULL, NULL, NULL, NULL),
(70, '2354254236@yahoo.com', 'b4afe5e3d8a57f1b3f37a32379b937cc911eb22747874c7900aba31cf264484c', '180700070', 0, NULL, NULL, '2018-07-18 13:33:45', NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'cfasdsac@yahoo.com', 'bcced0481ed93ec571408e4aa40d1cb9c5bb53fded1bfa42452e23797fa16b81', '180700071', 0, NULL, NULL, '2018-07-18 13:35:33', NULL, NULL, NULL, NULL, NULL, NULL),
(72, '346656fac@yahoo.com', 'bc885c2de5e85a04e5b9a656e605a4b7c899f7ba0844ad8d01711d438f39c044', '180700072', 0, NULL, NULL, '2018-07-18 13:36:41', NULL, NULL, NULL, NULL, NULL, NULL),
(73, '5654654c@yahoo.com', '36f34645bc80e58b222c2fb27f4f8f898883e21de3d9b11875aa5c6292dc505a', '180700073', 0, NULL, NULL, '2018-07-18 13:37:37', NULL, NULL, NULL, NULL, NULL, NULL),
(74, '562345634fcvvc@yahoo.com', '680409b9031a8d60646d492cd138fa0bc29503235f32e1b64367aea642ce7845', '180700074', 0, NULL, NULL, '2018-07-18 13:39:23', NULL, NULL, NULL, NULL, NULL, NULL),
(75, '345624534fcvvc@yahoo.com', 'edf3c297deaf68f73e3f91b5a07929b161733a9df63b95d719345e1aea6302f1', '180700075', 0, NULL, NULL, '2018-07-18 13:41:40', NULL, NULL, NULL, NULL, NULL, NULL),
(76, '43543534fcvvc@yahoo.com', '0fece0d459882c22d64b1ebf795fb66d64d217882c116b654e6683a7f97513cf', '180700076', 0, NULL, NULL, '2018-07-18 13:43:06', NULL, NULL, NULL, NULL, NULL, NULL),
(77, '435234233454fcvvc@yahoo.com', '66f185b6bd90bdf0b040d716b43455adce1e54f1eaad9c7f2add09721c23e228', '180700077', 0, NULL, NULL, '2018-07-18 13:45:54', NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'yrsdyhrcdry4fcvvc@yahoo.com', 'fc4aae37a692ed2ddf3789097d3337fcfe22671582339223641120d5d7c67d27', '180700078', 0, NULL, NULL, '2018-07-18 13:47:58', NULL, NULL, NULL, NULL, NULL, NULL),
(79, '45635454fcvvc@yahoo.com', '633d993de133f01e7474ade757bcfb01df0b933f05f4e1f3b900337b79be4788', '180700079', 0, NULL, NULL, '2018-07-18 13:53:31', NULL, NULL, NULL, NULL, NULL, NULL),
(80, '5555554444fcvvc@yahoo.com', '1b6ba4bb6b69bb91445a23f629717b9e622096a525dc3a4cea97bceeffa7bd7f', '180700080', 0, NULL, NULL, '2018-07-18 13:54:33', NULL, NULL, NULL, NULL, NULL, NULL),
(81, '56737346444fcvvc@yahoo.com', '09eb54462bd0d6e3a50807eae5c7f42420b1903f6715b38c4ffe0399b534106d', '180700081', 0, NULL, NULL, '2018-07-18 13:55:58', NULL, NULL, NULL, NULL, NULL, NULL),
(82, '3562342436356444fcvvc@yahoo.com', '04d50472d9a3f58bb77dc66f7cb3a93c30a7cafe8ecfa76bbf77571df1572bcc', '180700082', 0, NULL, NULL, '2018-07-18 13:58:31', NULL, NULL, NULL, NULL, NULL, NULL),
(83, '354345454536356444fcvvc@yahoo.com', '38ee9979e9e2edfd0d08e6ade31fc4913d32ca42e249164da60fe8022c0a0b6c', '180700083', 0, NULL, NULL, '2018-07-18 13:59:41', NULL, NULL, NULL, NULL, NULL, NULL),
(84, '4365464562523356444fcvvc@yahoo.com', '776deecbe84bb53471ecd28519b6539ddcc102ea87c820d8a3b5722223e06bf7', '180700084', 0, NULL, NULL, '2018-07-18 14:01:25', NULL, NULL, NULL, NULL, NULL, NULL),
(85, '434bfdgh3356444fcvvc@yahoo.com', '873a2f0da2d9b5db62ad4f1ff0bec462859dca6dc0e9372fc840cf4ca9cc8c2c', '180700085', 0, NULL, NULL, '2018-07-18 14:02:38', NULL, NULL, NULL, NULL, NULL, NULL),
(86, 'bghgh6444fcvvc@yahoo.com', '1224b17236982f8964762023a19b2cb76936059561523166a8794f10f96c003e', '180700086', 0, NULL, NULL, '2018-07-18 14:04:21', NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'bngh4fcvvc@yahoo.com', 'b85fe7db3b7cdeedfaab084113f4953492e4aa4081eb91992fa9cbf08cbad745', '180700087', 0, NULL, NULL, '2018-07-18 14:06:12', NULL, NULL, NULL, NULL, NULL, NULL),
(88, 'ghsghgvvc@yahoo.com', 'a74a60014892581434cb0fd9ac1ca88bef1b2b8b6fe3eea28c1c0d44ef6267df', '180700088', 0, NULL, NULL, '2018-07-18 14:08:34', NULL, NULL, NULL, NULL, NULL, NULL),
(89, 'ghhgcbsdc@yahoo.com', '14b76dafcc925d9eda95106f4de50150e618a89243e31ea8280bfa220a80c1a5', '180700089', 0, NULL, NULL, '2018-07-18 14:09:51', NULL, NULL, NULL, NULL, NULL, NULL),
(90, 'gefshgdhdhddc@yahoo.com', '94e245a7ae0ca1b7b78b63b1d27f2f38be6f540095408310e4bc777b424e3823', '180700090', 0, NULL, NULL, '2018-07-18 14:11:14', NULL, NULL, NULL, NULL, NULL, NULL),
(91, '54645265hddc@yahoo.com', 'a1e1117b4311f3f5682da45d54548a3f9bfa33caacf76e61a42fedc7054fd299', '180700091', 0, NULL, NULL, '2018-07-18 14:12:50', NULL, NULL, NULL, NULL, NULL, NULL),
(92, '6734764735hddc@yahoo.com', '68715b4ebf59d6a5381a056be36a4e6915cfba047a405a33eb18035933f50d49', '180700092', 0, NULL, NULL, '2018-07-18 14:19:01', NULL, NULL, NULL, NULL, NULL, NULL),
(93, '4535453453435hddc@yahoo.com', 'c1f173579fcb718187951f9f40473671f826cda94e723eaa17400cc5d7372dff', '180700093', 0, NULL, NULL, '2018-07-18 14:23:51', NULL, NULL, NULL, NULL, NULL, NULL),
(94, 'fdgfdvdf3435hddc@yahoo.com', '81ad572be69a8059e475b36a8fa1919e3dbd6a94dc906621c8e0b0fcbca38b2d', '180700094', 0, NULL, NULL, '2018-07-18 14:24:50', NULL, NULL, NULL, NULL, NULL, NULL),
(95, 'gfasdfsd3435hddc@yahoo.com', '21fe23dc7350652232dbaf66fc8aa586d7a2c22d563dc418d3a4e657421dfb2a', '180700095', 0, NULL, NULL, '2018-07-18 14:25:39', NULL, NULL, NULL, NULL, NULL, NULL),
(96, '63563563563435hddc@yahoo.com', '5fcec469c81824f7e5a711557f3923723395583fcaf470806bbcd895743b00a6', '180700096', 0, NULL, NULL, '2018-07-18 14:26:24', NULL, NULL, NULL, NULL, NULL, NULL),
(97, '745764563435hddc@yahoo.com', 'b724cd076a5d4e73166f7201c992b945c544021d60b1a0f87442f5881f275a88', '180700097', 0, NULL, NULL, '2018-07-18 14:28:21', NULL, NULL, NULL, NULL, NULL, NULL),
(98, '565656563435hddc@yahoo.com', '0a0f28a820b9074a744a1fb101975759d03f2b6b24ee92e8d3d8f9956f7edfb0', '180700098', 0, NULL, NULL, '2018-07-18 14:29:13', NULL, NULL, NULL, NULL, NULL, NULL),
(99, '66346563543435hddc@yahoo.com', '66b2d900d0f2d2b9dcc8c1d36258d0244e4ad251d66733f6df0e4dcd6c6030a9', '180700099', 0, NULL, NULL, '2018-07-18 14:30:18', NULL, NULL, NULL, NULL, NULL, NULL),
(100, '56656435hddc@yahoo.com', '1e97d08f3cac6f397487a0cf724d60fed69733df77ba549835301e6f55926cd8', '180700100', 0, NULL, NULL, '2018-07-18 14:36:51', NULL, NULL, NULL, NULL, NULL, NULL),
(101, '566526556435hddc@yahoo.com', 'b976591a4acd96b66c2532738d2feadbf9079dfb3d3206b3abd46eebbea01f07', '180700101', 0, NULL, NULL, '2018-07-18 14:37:44', NULL, NULL, NULL, NULL, NULL, NULL),
(102, '4534556435hddc@yahoo.com', '980864a3aacc1db20a2c26bdccf39ef3ad613c12b68916420826580fd922bf49', '180700102', 0, NULL, NULL, '2018-07-18 14:38:57', NULL, NULL, NULL, NULL, NULL, NULL),
(103, '457456435hddc@yahoo.com', '4df4b29113e9a81b2876f3e8a2cdbde0d7c2a6403a1491d3c7c36aea41b34fc6', '180700103', 0, NULL, NULL, '2018-07-18 14:39:55', NULL, NULL, NULL, NULL, NULL, NULL),
(104, 'ghdh5hddc@yahoo.com', '1d3194f28aadba6c28f3e76b16ddbefeb711e7ccd0dfaf027220fe34eda1b8a1', '180700104', 0, NULL, NULL, '2018-07-18 14:40:51', NULL, NULL, NULL, NULL, NULL, NULL),
(105, '636ddc@yahoo.com', 'cf76a111c3463d798c651ab737d9d8d6a1320fcc5c59205fdc058af8f2dd07c0', '180700105', 0, NULL, NULL, '2018-07-18 14:42:23', NULL, NULL, NULL, NULL, NULL, NULL),
(106, '655365656dc@yahoo.com', '3f97b9d46a0023f9e2e6ec2a8fddd5a9bdf287945e9b93129ad5c7d57e2fe5fc', '180700106', 0, NULL, NULL, '2018-07-18 14:43:37', NULL, NULL, NULL, NULL, NULL, NULL),
(107, '6557746747365656dc@yahoo.com', '94258db5168fe5f55956ec1b047d56d9987ba2630d5dd1a3afd8d561a848d3e3', '180700107', 0, NULL, NULL, '2018-07-18 14:44:28', NULL, NULL, NULL, NULL, NULL, NULL),
(108, '574564647365656dc@yahoo.com', 'ce0acfa3d310ec458412d148d434837083ebe77ae4eaf302ef04bb121243834f', '180700108', 0, NULL, NULL, '2018-07-18 14:45:24', NULL, NULL, NULL, NULL, NULL, NULL),
(109, '6365665656dc@yahoo.com', '371ba5450193f9c37ce1c40466939317778cf35583a6d3ac170b592f25f3bcf0', '180700109', 0, NULL, NULL, '2018-07-18 14:46:08', NULL, NULL, NULL, NULL, NULL, NULL),
(110, '34563463566c@yahoo.com', '7c9c229a803dcbb735dc083ca682081451b1a9de48de3d9199b0c548119903b2', '180700110', 0, NULL, NULL, '2018-07-18 14:47:00', NULL, NULL, NULL, NULL, NULL, NULL),
(111, '5453545@yahoo.com', 'cfd0f665a9c8af5fc3357e572b5243f36c713593b0e0baa05b91aa2733abfbbd', '180700111', 0, NULL, NULL, '2018-07-18 14:48:13', NULL, NULL, NULL, NULL, NULL, NULL),
(112, '7346354545445454534@gmail.com', '204ae10d7f951473002913e556dd88c04c19c153831e20096a7c182ee49b3c2e', '180700112', 0, NULL, NULL, '2018-07-18 14:49:36', NULL, NULL, NULL, NULL, NULL, NULL),
(113, '4764545454534@gmail.com', 'e448bc3d42c4a19bca41aec3e264c015fc9e2207cf1ca56761f1f81c1d780721', '180700113', 0, NULL, NULL, '2018-07-18 14:51:10', NULL, NULL, NULL, NULL, NULL, NULL),
(114, '674554534@gmail.com', '922b91f3f281f432d190e204533bff31889a8e4a88e935825927fe818d6af5d4', '180700114', 0, NULL, NULL, '2018-07-18 14:51:50', NULL, NULL, NULL, NULL, NULL, NULL),
(115, '6745545757@gmail.com', '104363bf5d2b5200f1daef0ec07515316a7f3a8578b86637c764a9118b31027a', '180700115', 0, NULL, NULL, '2018-07-18 14:53:08', NULL, NULL, NULL, NULL, NULL, NULL),
(116, '6456545757@gmail.com', 'a64c18469ecfa63a7552b8721e962a67702ef1bb943b8c98107f763d28702a12', '180700116', 0, NULL, NULL, '2018-07-18 14:53:47', NULL, NULL, NULL, NULL, NULL, NULL),
(117, '6456463634545757@gmail.com', '96e8ebc6f5d51da68137d14e3b7c50192d551636f9adc120d6f63b9de844ef39', '180700117', 0, NULL, NULL, '2018-07-18 14:54:27', NULL, NULL, NULL, NULL, NULL, NULL),
(118, '5363563563634545757@gmail.com', 'e4c1863f6e84f65106fcc3f13738c7ffb30f4eb7ddbf93afd887addf424be492', '180700118', 0, NULL, NULL, '2018-07-18 14:55:10', NULL, NULL, NULL, NULL, NULL, NULL),
(119, '7457457563563563634545757@gmail.com', '1d68498ad48e9ef09ba7a9c458bdb1c45615ec867da994baae94c5afb16d7fbd', '180700119', 0, NULL, NULL, '2018-07-18 14:57:00', NULL, NULL, NULL, NULL, NULL, NULL),
(120, '3463463634545757@gmail.com', '2c67df1bc43c8d2a4a5d107d02e9a91672f41d0a7a3c872be1785c0b510ff95c', '180700120', 0, NULL, NULL, '2018-07-18 14:57:44', NULL, NULL, NULL, NULL, NULL, NULL),
(121, '43634635634545757@gmail.com', '71b01f2056556d1cfa25782f5f0adfec5e4e624a4781d1cec5d9a2899a78e794', '180700121', 0, NULL, NULL, '2018-07-18 14:59:18', NULL, NULL, NULL, NULL, NULL, NULL),
(122, '634t63464545757@gmail.com', 'fd151d46cad51650f37e82f916660c9b658d5356d395687ff9a964789c23f72d', '180700122', 0, NULL, NULL, '2018-07-18 15:00:04', NULL, NULL, NULL, NULL, NULL, NULL),
(123, '65346356635664545757@gmail.com', '964742894a6d6f8b079a5514df10fc10b0c6f293bdcc0521c38c61c986c0382d', '180700123', 0, NULL, NULL, '2018-07-18 15:00:58', NULL, NULL, NULL, NULL, NULL, NULL),
(124, '563565664545757@gmail.com', '676f9ef684a8c11f195cc4845c77d3e8ab6744f0b08197fc867e2b603b845625', '180700124', 0, NULL, NULL, '2018-07-18 15:01:39', NULL, NULL, NULL, NULL, NULL, NULL),
(125, '4574564575664545757@gmail.com', '04e77a315a34c3db2a41cdb0217a6a80c87eb96bffcbe81d0e75e4c5a5e58998', '180700125', 0, NULL, NULL, '2018-07-18 15:02:21', NULL, NULL, NULL, NULL, NULL, NULL),
(126, '636536664545757@gmail.com', 'fe34f9d3efed9f103b26742d830b55f3a410eae138b4806c4cf57c3820c601a0', '180700126', 0, NULL, NULL, '2018-07-18 15:03:06', NULL, NULL, NULL, NULL, NULL, NULL),
(127, '363464545757@gmail.com', '07954ec8b0e0ec0891ba40596210ee9ceac500f96a6252ae542e43b74fd1cebd', '180700127', 0, NULL, NULL, '2018-07-18 15:03:51', NULL, NULL, NULL, NULL, NULL, NULL),
(128, '4534563445757@gmail.com', '3ddf8d95638c8318674e2de96132be0543a21aefd6cfb1a69b26a02174046620', '180700128', 0, NULL, NULL, '2018-07-18 15:05:30', NULL, NULL, NULL, NULL, NULL, NULL),
(129, '453456374746746445757@gmail.com', 'b54a7b7bca6650ae5fc7cda536493b7da7261df18a6c35bb5e021f0e600ccce0', '180700129', 0, NULL, NULL, '2018-07-18 15:06:17', NULL, NULL, NULL, NULL, NULL, NULL),
(130, '3563566746746445757@gmail.com', '2457ed7603a89d534e0376e88207a855115d693ce057cf35d68ef380cfd420e2', '180700130', 0, NULL, NULL, '2018-07-18 15:09:12', NULL, NULL, NULL, NULL, NULL, NULL),
(131, '56565656445757@gmail.com', '06afb6fef4378e694d9cbb793de6cdfe094225885d88e90ee679c4a4db47dc7d', '180700131', 0, NULL, NULL, '2018-07-18 15:10:13', NULL, NULL, NULL, NULL, NULL, NULL),
(132, '563565746746445757@gmail.com', 'ec31f02c66fcde968cf6b5331cb37f0f9a826e26484440d2d7c71d0b8e932a79', '180700132', 0, NULL, NULL, '2018-07-18 15:11:28', NULL, NULL, NULL, NULL, NULL, NULL),
(133, '54gfgfghsx6445757@gmail.com', 'bfa95cfc8826244ce3c7bbc566f64dcf9f5fe264e99f9ce7e3dbdb7e37ddd984', '180700133', 0, NULL, NULL, '2018-07-18 15:12:42', NULL, NULL, NULL, NULL, NULL, NULL),
(134, 'addsd7@gmail.com', '3bc8c0dc454c258c110f74d18e65ffdad94277918c799caaf95d17e2763be451', '180700134', 0, NULL, NULL, '2018-07-18 15:16:32', NULL, NULL, NULL, NULL, NULL, NULL),
(135, 'gsggfd@gmail.com', 'db164d903bda624fca4fcf0d5a569bfcc2f2667757594e50cedb481d9a4a098b', '180700135', 0, NULL, NULL, '2018-07-18 16:24:39', NULL, NULL, NULL, NULL, NULL, NULL),
(136, 'testcontinue@gmail.com', '74cb84ced14d3b48c48711eb86aeabf90c6baa097b860651d3d3c0173b1c7976', '180700136', 0, NULL, NULL, '2018-07-18 17:03:03', NULL, NULL, NULL, NULL, NULL, NULL),
(137, 'testcontinue1@gmail.com', 'faebf601fa226ddb7c71db15d0d2a549f5ecd66d1d22a80f99a90c43398a7501', '180700137', 0, NULL, NULL, '2018-07-18 17:03:54', NULL, NULL, NULL, NULL, NULL, NULL),
(138, 'jhkaskasdhka@gmail.com', 'd7215da672c1b10a2d7162acdbac3bc7068771d44b48e4fb7cfc06247ffd76ea', '180700138', 0, NULL, NULL, '2018-07-18 17:07:47', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_to_group`
--

CREATE TABLE `aauth_user_to_group` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_user_to_group`
--

INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2),
(50, 2),
(51, 2),
(52, 2),
(53, 2),
(54, 2),
(55, 2),
(56, 2),
(57, 2),
(58, 2),
(59, 2),
(60, 2),
(61, 2),
(62, 2),
(63, 2),
(64, 2),
(65, 2),
(66, 2),
(67, 2),
(68, 2),
(69, 2),
(70, 2),
(71, 2),
(72, 2),
(73, 2),
(74, 2),
(75, 2),
(76, 2),
(77, 2),
(78, 2),
(79, 2),
(80, 2),
(81, 2),
(82, 2),
(83, 2),
(84, 2),
(85, 2),
(86, 2),
(87, 2),
(88, 2),
(89, 2),
(90, 2),
(91, 2),
(92, 2),
(93, 2),
(94, 2),
(95, 2),
(96, 2),
(97, 2),
(98, 2),
(99, 2),
(100, 2),
(101, 2),
(102, 2),
(103, 2),
(104, 2),
(105, 2),
(106, 2),
(107, 2),
(108, 2),
(109, 2),
(110, 2),
(111, 2),
(112, 2),
(113, 2),
(114, 2),
(115, 2),
(116, 2),
(117, 2),
(118, 2),
(119, 2),
(120, 2),
(121, 2),
(122, 2),
(123, 2),
(124, 2),
(125, 2),
(126, 2),
(127, 2),
(128, 2),
(129, 2),
(130, 2),
(131, 2),
(132, 2),
(133, 2),
(134, 2),
(135, 2),
(136, 2),
(137, 2),
(138, 2);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_variables`
--

CREATE TABLE `aauth_user_variables` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blocked_user_commissions`
--

CREATE TABLE `blocked_user_commissions` (
  `id` int(11) NOT NULL,
  `rising_user_id` int(11) NOT NULL,
  `eligible_user_id` int(11) NOT NULL,
  `commision_amount` float NOT NULL,
  `package` enum('1','2','3','4','5') NOT NULL,
  `commision_type` enum('80','20','100','1') NOT NULL,
  `transaction_type` enum('direct','indirect','one_percent') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fund_e_cash_histry`
--

CREATE TABLE `fund_e_cash_histry` (
  `id` int(11) NOT NULL,
  `member_code` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(250) NOT NULL DEFAULT '0',
  `credit` varchar(250) NOT NULL DEFAULT '0',
  `debit` varchar(250) DEFAULT '0',
  `type` varchar(250) DEFAULT '0',
  `deduct_from` enum('e','c') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fund_e_cash_histry`
--

INSERT INTO `fund_e_cash_histry` (`id`, `member_code`, `description`, `credit`, `debit`, `type`, `deduct_from`, `created_at`) VALUES
(1, '180700001', 'ADMIN CASH', '500000', '0', 'transfer', NULL, '2018-07-14 13:51:53'),
(59, '180700001', 'make entry by your account code is:180700002', '0', '11000', 'entry', 'e', '2018-07-17 12:35:42'),
(60, '180700001', 'make entry by your account code is:180700003', '0', '500', 'entry', 'e', '2018-07-17 12:59:15'),
(61, '180700001', 'make entry by your account code is:180700004', '0', '500', 'entry', 'e', '2018-07-17 13:00:07'),
(62, '180700001', 'make entry by your account code is:180700005', '0', '500', 'entry', 'e', '2018-07-17 13:01:28'),
(63, '180700001', 'make entry by your account code is:180700006', '0', '500', 'entry', 'e', '2018-07-17 13:02:58'),
(64, '180700001', 'make entry by your account code is:180700007', '0', '500', 'entry', 'e', '2018-07-17 13:08:54'),
(65, '180700001', 'make entry by your account code is:180700008', '0', '500', 'entry', 'e', '2018-07-17 13:09:57'),
(66, '180700001', 'make entry by your account code is:180700009', '0', '500', 'entry', 'e', '2018-07-17 13:11:07'),
(67, '180700001', 'make entry by your account code is:180700010', '0', '500', 'entry', 'e', '2018-07-17 13:11:47'),
(68, '180700001', 'make entry by your account code is:180700011', '0', '500', 'entry', 'e', '2018-07-17 13:12:34'),
(69, '180700001', 'make entry by your account code is:180700012', '0', '500', 'entry', 'e', '2018-07-17 13:13:46'),
(70, '180700001', 'make entry by your account code is:180700013', '0', '500', 'entry', 'e', '2018-07-17 13:14:36'),
(71, '180700001', 'make entry by your account code is:180700014', '0', '500', 'entry', 'e', '2018-07-17 13:22:17'),
(72, '180700001', 'make entry by your account code is:180700015', '0', '500', 'entry', 'e', '2018-07-17 13:23:07'),
(73, '180700001', 'make entry by your account code is:180700016', '0', '500', 'entry', 'e', '2018-07-17 13:23:56'),
(74, '180700001', 'make entry by your account code is:180700017', '0', '500', 'entry', 'e', '2018-07-17 13:25:22'),
(75, '180700001', 'make entry by your account code is:180700018', '0', '500', 'entry', 'e', '2018-07-17 13:28:13'),
(76, '180700001', 'make entry by your account code is:180700019', '0', '500', 'entry', 'e', '2018-07-17 13:29:07'),
(77, '180700001', 'make entry by your account code is:180700020', '0', '500', 'entry', 'e', '2018-07-17 13:30:01'),
(78, '180700001', 'make entry by your account code is:180700021', '0', '500', 'entry', 'e', '2018-07-17 13:30:48'),
(79, '180700001', 'make entry by your account code is:180700022', '0', '500', 'entry', 'e', '2018-07-17 13:31:51'),
(80, '180700001', 'make entry by your account code is:180700023', '0', '500', 'entry', 'e', '2018-07-17 13:41:26'),
(81, '180700001', 'make entry by your account code is:180700024', '0', '500', 'entry', 'e', '2018-07-17 13:42:07'),
(82, '180700001', 'make entry by your account code is:180700025', '0', '500', 'entry', 'e', '2018-07-17 13:43:09'),
(83, '180700001', 'make entry by your account code is:180700026', '0', '500', 'entry', 'e', '2018-07-17 13:44:01'),
(84, '180700001', 'make entry by your account code is:180700027', '0', '500', 'entry', 'e', '2018-07-17 13:56:01'),
(85, '180700001', 'make entry by your account code is:180700028', '0', '500', 'entry', 'e', '2018-07-17 13:56:36'),
(86, '180700001', 'make entry by your account code is:180700029', '0', '500', 'entry', 'e', '2018-07-17 13:58:45'),
(87, '180700001', 'make entry by your account code is:180700030', '0', '500', 'entry', 'e', '2018-07-17 13:59:16'),
(88, '180700001', 'make entry by your account code is:180700031', '0', '500', 'entry', 'e', '2018-07-17 13:59:53'),
(89, '180700001', 'make entry by your account code is:180700032', '0', '500', 'entry', 'e', '2018-07-17 14:00:28'),
(90, '180700001', 'make entry by your account code is:180700033', '0', '500', 'entry', 'e', '2018-07-18 07:49:20'),
(91, '180700001', 'make entry by your account code is:180700034', '0', '500', 'entry', 'e', '2018-07-18 07:51:06'),
(92, '180700001', 'make entry by your account code is:180700035', '0', '500', 'entry', 'e', '2018-07-18 07:52:26'),
(93, '180700001', 'make entry by your account code is:180700036', '0', '500', 'entry', 'e', '2018-07-18 07:55:24'),
(94, '180700001', 'make entry by your account code is:180700037', '0', '500', 'entry', 'e', '2018-07-18 07:56:27'),
(95, '180700001', 'make entry by your account code is:180700038', '0', '500', 'entry', 'e', '2018-07-18 07:57:17'),
(96, '180700001', 'make entry by your account code is:180700039', '0', '500', 'entry', 'e', '2018-07-18 07:58:14'),
(97, '180700001', 'make entry by your account code is:180700040', '0', '500', 'entry', 'e', '2018-07-18 07:59:00'),
(98, '180700001', 'make entry by your account code is:180700041', '0', '500', 'entry', 'e', '2018-07-18 08:00:58'),
(99, '180700001', 'make entry by your account code is:180700042', '0', '500', 'entry', 'e', '2018-07-18 08:01:59'),
(100, '180700001', 'make entry by your account code is:180700043', '0', '500', 'entry', 'e', '2018-07-18 08:03:16'),
(101, '180700001', 'make entry by your account code is:180700044', '0', '500', 'entry', 'e', '2018-07-18 08:04:44'),
(102, '180700001', 'make entry by your account code is:180700045', '0', '500', 'entry', 'e', '2018-07-18 08:06:38'),
(103, '180700001', 'make entry by your account code is:180700046', '0', '500', 'entry', 'e', '2018-07-18 08:07:26'),
(104, '180700001', 'make entry by your account code is:180700047', '0', '500', 'entry', 'e', '2018-07-18 08:09:24'),
(105, '180700001', 'make entry by your account code is:180700048', '0', '500', 'entry', 'e', '2018-07-18 08:10:32'),
(106, '180700001', 'make entry by your account code is:180700049', '0', '500', 'entry', 'e', '2018-07-18 08:11:18'),
(107, '180700001', 'make entry by your account code is:180700050', '0', '500', 'entry', 'e', '2018-07-18 08:12:41'),
(108, '180700001', 'make entry by your account code is:180700051', '0', '500', 'entry', 'e', '2018-07-18 08:13:20'),
(109, '180700001', 'make entry by your account code is:180700052', '0', '500', 'entry', 'e', '2018-07-18 08:14:15'),
(110, '180700001', 'make entry by your account code is:180700053', '0', '500', 'entry', 'e', '2018-07-18 08:15:55'),
(111, '180700001', 'make entry by your account code is:180700054', '0', '500', 'entry', 'e', '2018-07-18 08:16:45'),
(112, '180700001', 'make entry by your account code is:180700055', '0', '500', 'entry', 'e', '2018-07-18 08:17:32'),
(113, '180700001', 'make entry by your account code is:180700056', '0', '500', 'entry', 'e', '2018-07-18 08:18:25'),
(114, '180700001', 'make entry by your account code is:180700057', '0', '500', 'entry', 'e', '2018-07-18 08:19:06'),
(115, '180700001', 'make entry by your account code is:180700058', '0', '500', 'entry', 'e', '2018-07-18 08:19:46'),
(116, '180700001', 'make entry by your account code is:180700059', '0', '500', 'entry', 'e', '2018-07-18 08:20:49'),
(117, '180700001', 'make entry by your account code is:180700060', '0', '500', 'entry', 'e', '2018-07-18 08:21:41'),
(118, '180700001', 'make entry by your account code is:180700061', '0', '500', 'entry', 'e', '2018-07-18 08:22:55'),
(119, '180700001', 'make entry by your account code is:180700062', '0', '500', 'entry', 'e', '2018-07-18 08:23:31'),
(120, '180700001', 'make entry by your account code is:180700063', '0', '500', 'entry', 'e', '2018-07-18 08:24:20'),
(121, '180700001', 'make entry by your account code is:180700064', '0', '500', 'entry', 'e', '2018-07-18 08:24:52'),
(122, '180700001', 'make entry by your account code is:180700065', '0', '500', 'entry', 'e', '2018-07-18 08:25:49'),
(123, '180700001', 'make entry by your account code is:180700066', '0', '500', 'entry', 'e', '2018-07-18 08:26:30'),
(124, '180700001', 'make entry by your account code is:180700067', '0', '500', 'entry', 'e', '2018-07-18 08:29:00'),
(125, '180700001', 'make entry by your account code is:180700068', '0', '500', 'entry', 'e', '2018-07-18 08:29:47'),
(126, '180700001', 'make entry by your account code is:180700069', '0', '500', 'entry', 'e', '2018-07-18 08:31:55'),
(127, '180700001', 'make entry by your account code is:180700070', '0', '500', 'entry', 'e', '2018-07-18 08:33:45'),
(128, '180700001', 'make entry by your account code is:180700071', '0', '500', 'entry', 'e', '2018-07-18 08:35:33'),
(129, '180700001', 'make entry by your account code is:180700072', '0', '500', 'entry', 'e', '2018-07-18 08:36:41'),
(130, '180700001', 'make entry by your account code is:180700073', '0', '500', 'entry', 'e', '2018-07-18 08:37:37'),
(131, '180700001', 'make entry by your account code is:180700074', '0', '500', 'entry', 'e', '2018-07-18 08:39:23'),
(132, '180700001', 'make entry by your account code is:180700075', '0', '500', 'entry', 'e', '2018-07-18 08:41:40'),
(133, '180700001', 'make entry by your account code is:180700076', '0', '500', 'entry', 'e', '2018-07-18 08:43:06'),
(134, '180700001', 'make entry by your account code is:180700077', '0', '500', 'entry', 'e', '2018-07-18 08:45:54'),
(135, '180700001', 'make entry by your account code is:180700078', '0', '500', 'entry', 'e', '2018-07-18 08:47:58'),
(136, '180700001', 'make entry by your account code is:180700079', '0', '500', 'entry', 'e', '2018-07-18 08:53:31'),
(137, '180700001', 'make entry by your account code is:180700080', '0', '500', 'entry', 'e', '2018-07-18 08:54:33'),
(138, '180700001', 'make entry by your account code is:180700081', '0', '500', 'entry', 'e', '2018-07-18 08:55:58'),
(139, '180700001', 'make entry by your account code is:180700082', '0', '500', 'entry', 'e', '2018-07-18 08:58:31'),
(140, '180700001', 'make entry by your account code is:180700083', '0', '500', 'entry', 'e', '2018-07-18 08:59:41'),
(141, '180700001', 'make entry by your account code is:180700084', '0', '500', 'entry', 'e', '2018-07-18 09:01:25'),
(142, '180700001', 'make entry by your account code is:180700085', '0', '500', 'entry', 'e', '2018-07-18 09:02:38'),
(143, '180700001', 'make entry by your account code is:180700086', '0', '500', 'entry', 'e', '2018-07-18 09:04:21'),
(144, '180700001', 'make entry by your account code is:180700087', '0', '500', 'entry', 'e', '2018-07-18 09:06:13'),
(145, '180700001', 'make entry by your account code is:180700088', '0', '500', 'entry', 'e', '2018-07-18 09:08:34'),
(146, '180700001', 'make entry by your account code is:180700089', '0', '500', 'entry', 'e', '2018-07-18 09:09:51'),
(147, '180700001', 'make entry by your account code is:180700090', '0', '500', 'entry', 'e', '2018-07-18 09:11:14'),
(148, '180700001', 'make entry by your account code is:180700091', '0', '500', 'entry', 'e', '2018-07-18 09:12:50'),
(149, '180700001', 'make entry by your account code is:180700092', '0', '500', 'entry', 'e', '2018-07-18 09:19:01'),
(150, '180700001', 'make entry by your account code is:180700093', '0', '500', 'entry', 'e', '2018-07-18 09:23:51'),
(151, '180700001', 'make entry by your account code is:180700094', '0', '500', 'entry', 'e', '2018-07-18 09:24:50'),
(152, '180700001', 'make entry by your account code is:180700095', '0', '500', 'entry', 'e', '2018-07-18 09:25:39'),
(153, '180700001', 'make entry by your account code is:180700096', '0', '500', 'entry', 'e', '2018-07-18 09:26:24'),
(154, '180700001', 'make entry by your account code is:180700097', '0', '500', 'entry', 'e', '2018-07-18 09:28:21'),
(155, '180700001', 'make entry by your account code is:180700098', '0', '500', 'entry', 'e', '2018-07-18 09:29:13'),
(156, '180700001', 'make entry by your account code is:180700099', '0', '500', 'entry', 'e', '2018-07-18 09:30:18'),
(157, '180700001', 'make entry by your account code is:180700100', '0', '500', 'entry', 'e', '2018-07-18 09:36:51'),
(158, '180700001', 'make entry by your account code is:180700101', '0', '500', 'entry', 'e', '2018-07-18 09:37:44'),
(159, '180700001', 'make entry by your account code is:180700102', '0', '500', 'entry', 'e', '2018-07-18 09:38:57'),
(160, '180700001', 'make entry by your account code is:180700103', '0', '500', 'entry', 'e', '2018-07-18 09:39:55'),
(161, '180700001', 'make entry by your account code is:180700104', '0', '500', 'entry', 'e', '2018-07-18 09:40:51'),
(162, '180700001', 'make entry by your account code is:180700105', '0', '500', 'entry', 'e', '2018-07-18 09:42:23'),
(163, '180700001', 'make entry by your account code is:180700106', '0', '500', 'entry', 'e', '2018-07-18 09:43:37'),
(164, '180700001', 'make entry by your account code is:180700107', '0', '500', 'entry', 'e', '2018-07-18 09:44:28'),
(165, '180700001', 'make entry by your account code is:180700108', '0', '500', 'entry', 'e', '2018-07-18 09:45:24'),
(166, '180700001', 'make entry by your account code is:180700109', '0', '500', 'entry', 'e', '2018-07-18 09:46:08'),
(167, '180700001', 'make entry by your account code is:180700110', '0', '500', 'entry', 'e', '2018-07-18 09:47:00'),
(168, '180700001', 'make entry by your account code is:180700111', '0', '500', 'entry', 'e', '2018-07-18 09:48:13'),
(169, '180700001', 'make entry by your account code is:180700112', '0', '500', 'entry', 'e', '2018-07-18 09:49:36'),
(170, '180700001', 'make entry by your account code is:180700113', '0', '500', 'entry', 'e', '2018-07-18 09:51:10'),
(171, '180700001', 'make entry by your account code is:180700114', '0', '500', 'entry', 'e', '2018-07-18 09:51:50'),
(172, '180700001', 'make entry by your account code is:180700115', '0', '500', 'entry', 'e', '2018-07-18 09:53:08'),
(173, '180700001', 'make entry by your account code is:180700116', '0', '500', 'entry', 'e', '2018-07-18 09:53:47'),
(174, '180700001', 'make entry by your account code is:180700117', '0', '500', 'entry', 'e', '2018-07-18 09:54:28'),
(175, '180700001', 'make entry by your account code is:180700118', '0', '500', 'entry', 'e', '2018-07-18 09:55:10'),
(176, '180700001', 'make entry by your account code is:180700119', '0', '500', 'entry', 'e', '2018-07-18 09:57:00'),
(177, '180700001', 'make entry by your account code is:180700120', '0', '500', 'entry', 'e', '2018-07-18 09:57:44'),
(178, '180700001', 'make entry by your account code is:180700121', '0', '500', 'entry', 'e', '2018-07-18 09:59:18'),
(179, '180700001', 'make entry by your account code is:180700122', '0', '500', 'entry', 'e', '2018-07-18 10:00:04'),
(180, '180700001', 'make entry by your account code is:180700123', '0', '500', 'entry', 'e', '2018-07-18 10:00:58'),
(181, '180700001', 'make entry by your account code is:180700124', '0', '500', 'entry', 'e', '2018-07-18 10:01:39'),
(182, '180700001', 'make entry by your account code is:180700125', '0', '500', 'entry', 'e', '2018-07-18 10:02:21'),
(183, '180700001', 'make entry by your account code is:180700126', '0', '500', 'entry', 'e', '2018-07-18 10:03:07'),
(184, '180700001', 'make entry by your account code is:180700127', '0', '500', 'entry', 'e', '2018-07-18 10:03:51'),
(185, '180700001', 'make entry by your account code is:180700128', '0', '500', 'entry', 'e', '2018-07-18 10:05:30'),
(186, '180700001', 'make entry by your account code is:180700129', '0', '500', 'entry', 'e', '2018-07-18 10:06:17'),
(187, '180700001', 'make entry by your account code is:180700130', '0', '500', 'entry', 'e', '2018-07-18 10:09:12'),
(188, '180700001', 'make entry by your account code is:180700136', '0', '500', 'entry', 'e', '2018-07-18 12:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `Package_ID` int(11) NOT NULL,
  `PackageName` varchar(50) DEFAULT NULL,
  `PackagePrice` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`Package_ID`, `PackageName`, `PackagePrice`) VALUES
(1, 'MeeNuu stars', 500),
(2, 'SS', 3500),
(3, 'GS', 7000),
(4, 'PS', 10500),
(5, 'DS', 14000);

-- --------------------------------------------------------

--
-- Table structure for table `rising_commissions`
--

CREATE TABLE `rising_commissions` (
  `id` int(11) NOT NULL,
  `rising_user_id` int(11) NOT NULL,
  `eligible_user_id` int(11) NOT NULL,
  `commision_amount` float NOT NULL,
  `package` enum('1','2','3','4','5') NOT NULL,
  `commision_type` enum('80','20','100','1','0') NOT NULL,
  `transaction_type` enum('direct','indirect','one_percent','azadi') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rising_commissions`
--

INSERT INTO `rising_commissions` (`id`, `rising_user_id`, `eligible_user_id`, `commision_amount`, `package`, `commision_type`, `transaction_type`, `created_at`) VALUES
(1, 2, 1, 500, '1', '0', 'azadi', '2018-07-17 12:35:42'),
(2, 2, 1, 4200, '4', '80', 'direct', '2018-07-17 12:35:42'),
(3, 2, 1, 1050, '4', '20', 'direct', '2018-07-17 12:35:42'),
(4, 2, 1, 105, '4', '1', 'one_percent', '2018-07-17 12:35:42'),
(5, 3, 1, 500, '1', '0', 'azadi', '2018-07-17 12:59:15'),
(6, 4, 1, 500, '1', '0', 'azadi', '2018-07-17 13:00:07'),
(7, 5, 1, 500, '1', '0', 'azadi', '2018-07-17 13:01:28'),
(8, 6, 1, 500, '1', '0', 'azadi', '2018-07-17 13:02:58'),
(9, 7, 1, 500, '1', '0', 'azadi', '2018-07-17 13:08:54'),
(10, 8, 1, 500, '1', '0', 'azadi', '2018-07-17 13:09:57'),
(11, 9, 1, 500, '1', '0', 'azadi', '2018-07-17 13:11:07'),
(12, 10, 1, 500, '1', '0', 'azadi', '2018-07-17 13:11:47'),
(13, 11, 1, 500, '1', '0', 'azadi', '2018-07-17 13:12:34'),
(14, 12, 1, 500, '1', '0', 'azadi', '2018-07-17 13:13:46'),
(15, 13, 1, 500, '1', '0', 'azadi', '2018-07-17 13:14:36'),
(16, 14, 1, 500, '1', '0', 'azadi', '2018-07-17 13:22:17'),
(17, 15, 1, 500, '1', '0', 'azadi', '2018-07-17 13:23:07'),
(18, 16, 1, 500, '1', '0', 'azadi', '2018-07-17 13:23:56'),
(19, 17, 1, 500, '1', '0', 'azadi', '2018-07-17 13:25:22'),
(20, 18, 1, 500, '1', '0', 'azadi', '2018-07-17 13:28:13'),
(21, 19, 1, 500, '1', '0', 'azadi', '2018-07-17 13:29:07'),
(22, 20, 1, 500, '1', '0', 'azadi', '2018-07-17 13:30:01'),
(23, 21, 1, 500, '1', '0', 'azadi', '2018-07-17 13:30:48'),
(24, 22, 1, 500, '1', '0', 'azadi', '2018-07-17 13:31:51'),
(25, 23, 1, 500, '1', '0', 'azadi', '2018-07-17 13:41:26'),
(26, 24, 1, 500, '1', '0', 'azadi', '2018-07-17 13:42:07'),
(27, 25, 1, 500, '1', '0', 'azadi', '2018-07-17 13:43:09'),
(28, 26, 1, 500, '1', '0', 'azadi', '2018-07-17 13:44:01'),
(29, 27, 1, 500, '1', '0', 'azadi', '2018-07-17 13:56:01'),
(30, 28, 1, 500, '1', '0', 'azadi', '2018-07-17 13:56:36'),
(31, 29, 1, 500, '1', '0', 'azadi', '2018-07-17 13:58:45'),
(32, 30, 1, 500, '1', '0', 'azadi', '2018-07-17 13:59:16'),
(33, 31, 1, 500, '1', '0', 'azadi', '2018-07-17 13:59:53'),
(34, 32, 1, 500, '1', '0', 'azadi', '2018-07-17 14:00:28'),
(35, 33, 1, 500, '1', '0', 'azadi', '2018-07-18 07:49:20'),
(36, 34, 1, 500, '1', '0', 'azadi', '2018-07-18 07:51:06'),
(37, 35, 1, 500, '1', '0', 'azadi', '2018-07-18 07:52:26'),
(38, 36, 1, 500, '1', '0', 'azadi', '2018-07-18 07:55:24'),
(39, 37, 1, 500, '1', '0', 'azadi', '2018-07-18 07:56:27'),
(40, 38, 1, 500, '1', '0', 'azadi', '2018-07-18 07:57:17'),
(41, 39, 1, 500, '1', '0', 'azadi', '2018-07-18 07:58:14'),
(42, 40, 1, 500, '1', '0', 'azadi', '2018-07-18 07:59:00'),
(43, 41, 1, 500, '1', '0', 'azadi', '2018-07-18 08:00:58'),
(44, 42, 1, 500, '1', '0', 'azadi', '2018-07-18 08:01:59'),
(45, 43, 1, 500, '1', '0', 'azadi', '2018-07-18 08:03:16'),
(46, 44, 1, 500, '1', '0', 'azadi', '2018-07-18 08:04:44'),
(47, 45, 1, 500, '1', '0', 'azadi', '2018-07-18 08:06:38'),
(48, 46, 1, 500, '1', '0', 'azadi', '2018-07-18 08:07:26'),
(49, 47, 1, 500, '1', '0', 'azadi', '2018-07-18 08:09:24'),
(50, 48, 1, 500, '1', '0', 'azadi', '2018-07-18 08:10:32'),
(51, 49, 1, 500, '1', '0', 'azadi', '2018-07-18 08:11:18'),
(52, 50, 1, 500, '1', '0', 'azadi', '2018-07-18 08:12:41'),
(53, 51, 1, 500, '1', '0', 'azadi', '2018-07-18 08:13:20'),
(54, 52, 1, 500, '1', '0', 'azadi', '2018-07-18 08:14:15'),
(55, 53, 1, 500, '1', '0', 'azadi', '2018-07-18 08:15:55'),
(56, 54, 1, 500, '1', '0', 'azadi', '2018-07-18 08:16:45'),
(57, 55, 1, 500, '1', '0', 'azadi', '2018-07-18 08:17:32'),
(58, 56, 1, 500, '1', '0', 'azadi', '2018-07-18 08:18:25'),
(59, 57, 1, 500, '1', '0', 'azadi', '2018-07-18 08:19:06'),
(60, 58, 1, 500, '1', '0', 'azadi', '2018-07-18 08:19:46'),
(61, 59, 1, 500, '1', '0', 'azadi', '2018-07-18 08:20:49'),
(62, 60, 1, 500, '1', '0', 'azadi', '2018-07-18 08:21:41'),
(63, 61, 1, 500, '1', '0', 'azadi', '2018-07-18 08:22:55'),
(64, 62, 1, 500, '1', '0', 'azadi', '2018-07-18 08:23:31'),
(65, 63, 1, 500, '1', '0', 'azadi', '2018-07-18 08:24:20'),
(66, 64, 1, 500, '1', '0', 'azadi', '2018-07-18 08:24:52'),
(67, 65, 1, 500, '1', '0', 'azadi', '2018-07-18 08:25:49'),
(68, 66, 1, 500, '1', '0', 'azadi', '2018-07-18 08:26:30'),
(69, 67, 1, 500, '1', '0', 'azadi', '2018-07-18 08:29:00'),
(70, 68, 1, 500, '1', '0', 'azadi', '2018-07-18 08:29:47'),
(71, 69, 1, 500, '1', '0', 'azadi', '2018-07-18 08:31:55'),
(72, 70, 1, 500, '1', '0', 'azadi', '2018-07-18 08:33:45'),
(73, 71, 1, 500, '1', '0', 'azadi', '2018-07-18 08:35:33'),
(74, 72, 1, 500, '1', '0', 'azadi', '2018-07-18 08:36:41'),
(75, 73, 1, 500, '1', '0', 'azadi', '2018-07-18 08:37:37'),
(76, 74, 1, 500, '1', '0', 'azadi', '2018-07-18 08:39:23'),
(77, 75, 1, 500, '1', '0', 'azadi', '2018-07-18 08:41:40'),
(78, 76, 1, 500, '1', '0', 'azadi', '2018-07-18 08:43:06'),
(79, 77, 1, 500, '1', '0', 'azadi', '2018-07-18 08:45:54'),
(80, 78, 1, 500, '1', '0', 'azadi', '2018-07-18 08:47:58'),
(81, 79, 1, 500, '1', '0', 'azadi', '2018-07-18 08:53:31'),
(82, 80, 1, 500, '1', '0', 'azadi', '2018-07-18 08:54:33'),
(83, 81, 1, 500, '1', '0', 'azadi', '2018-07-18 08:55:58'),
(84, 82, 1, 500, '1', '0', 'azadi', '2018-07-18 08:58:31'),
(85, 83, 1, 500, '1', '0', 'azadi', '2018-07-18 08:59:41'),
(86, 84, 1, 500, '1', '0', 'azadi', '2018-07-18 09:01:25'),
(87, 85, 1, 500, '1', '0', 'azadi', '2018-07-18 09:02:38'),
(88, 86, 1, 500, '1', '0', 'azadi', '2018-07-18 09:04:21'),
(89, 87, 1, 500, '1', '0', 'azadi', '2018-07-18 09:06:13'),
(90, 88, 1, 500, '1', '0', 'azadi', '2018-07-18 09:08:34'),
(91, 89, 1, 500, '1', '0', 'azadi', '2018-07-18 09:09:51'),
(92, 90, 1, 500, '1', '0', 'azadi', '2018-07-18 09:11:14'),
(93, 91, 1, 500, '1', '0', 'azadi', '2018-07-18 09:12:50'),
(94, 92, 1, 500, '1', '0', 'azadi', '2018-07-18 09:19:01'),
(95, 93, 1, 500, '1', '0', 'azadi', '2018-07-18 09:23:51'),
(96, 94, 1, 500, '1', '0', 'azadi', '2018-07-18 09:24:50'),
(97, 95, 1, 500, '1', '0', 'azadi', '2018-07-18 09:25:39'),
(98, 96, 1, 500, '1', '0', 'azadi', '2018-07-18 09:26:24'),
(99, 97, 1, 500, '1', '0', 'azadi', '2018-07-18 09:28:21'),
(100, 98, 1, 500, '1', '0', 'azadi', '2018-07-18 09:29:13'),
(101, 99, 1, 500, '1', '0', 'azadi', '2018-07-18 09:30:18'),
(102, 100, 1, 500, '1', '0', 'azadi', '2018-07-18 09:36:51'),
(103, 101, 1, 500, '1', '0', 'azadi', '2018-07-18 09:37:44'),
(104, 102, 1, 500, '1', '0', 'azadi', '2018-07-18 09:38:57'),
(105, 103, 1, 500, '1', '0', 'azadi', '2018-07-18 09:39:55'),
(106, 104, 1, 500, '1', '0', 'azadi', '2018-07-18 09:40:51'),
(107, 105, 1, 500, '1', '0', 'azadi', '2018-07-18 09:42:23'),
(108, 106, 1, 500, '1', '0', 'azadi', '2018-07-18 09:43:37'),
(109, 107, 1, 500, '1', '0', 'azadi', '2018-07-18 09:44:28'),
(110, 108, 1, 500, '1', '0', 'azadi', '2018-07-18 09:45:24'),
(111, 109, 1, 500, '1', '0', 'azadi', '2018-07-18 09:46:08'),
(112, 110, 1, 500, '1', '0', 'azadi', '2018-07-18 09:47:00'),
(113, 111, 1, 500, '1', '0', 'azadi', '2018-07-18 09:48:13'),
(114, 112, 1, 500, '1', '0', 'azadi', '2018-07-18 09:49:36'),
(115, 113, 1, 500, '1', '0', 'azadi', '2018-07-18 09:51:10'),
(116, 114, 1, 500, '1', '0', 'azadi', '2018-07-18 09:51:50'),
(117, 115, 1, 500, '1', '0', 'azadi', '2018-07-18 09:53:08'),
(118, 116, 1, 500, '1', '0', 'azadi', '2018-07-18 09:53:47'),
(119, 117, 1, 500, '1', '0', 'azadi', '2018-07-18 09:54:28'),
(120, 118, 1, 500, '1', '0', 'azadi', '2018-07-18 09:55:10'),
(121, 119, 1, 500, '1', '0', 'azadi', '2018-07-18 09:57:00'),
(122, 120, 1, 500, '1', '0', 'azadi', '2018-07-18 09:57:44'),
(123, 121, 1, 500, '1', '0', 'azadi', '2018-07-18 09:59:18'),
(124, 122, 1, 500, '1', '0', 'azadi', '2018-07-18 10:00:04'),
(125, 123, 1, 500, '1', '0', 'azadi', '2018-07-18 10:00:58'),
(126, 124, 1, 500, '1', '0', 'azadi', '2018-07-18 10:01:39'),
(127, 125, 1, 500, '1', '0', 'azadi', '2018-07-18 10:02:21'),
(128, 126, 1, 500, '1', '0', 'azadi', '2018-07-18 10:03:07'),
(129, 127, 1, 500, '1', '0', 'azadi', '2018-07-18 10:03:51'),
(130, 128, 1, 500, '1', '0', 'azadi', '2018-07-18 10:05:30'),
(131, 129, 1, 500, '1', '0', 'azadi', '2018-07-18 10:06:17'),
(132, 130, 1, 500, '1', '0', 'azadi', '2018-07-18 10:09:12'),
(133, 136, 1, 500, '1', '0', 'azadi', '2018-07-18 12:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `rising_missed_commissions`
--

CREATE TABLE `rising_missed_commissions` (
  `id` int(11) NOT NULL,
  `rising_user_id` int(11) NOT NULL,
  `eligible_user_id` int(11) NOT NULL,
  `commision_amount` float NOT NULL,
  `package` enum('1','2','3','4','5') NOT NULL,
  `commision_type` enum('80','20') NOT NULL,
  `transaction_type` enum('direct','indirect') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rising_users`
--

CREATE TABLE `rising_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `cnic` int(11) UNSIGNED NOT NULL,
  `address` varchar(200) NOT NULL,
  `kin_name` varchar(50) NOT NULL,
  `kin_relation` varchar(50) NOT NULL,
  `kin_cnic` int(11) NOT NULL,
  `mobile_no` varchar(11) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `introducer_code` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `user_pin` int(11) NOT NULL,
  `user_pic_path` varchar(500) NOT NULL,
  `previous` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `package_updated_at` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rising_users`
--

INSERT INTO `rising_users` (`id`, `cnic`, `address`, `kin_name`, `kin_relation`, `kin_cnic`, `mobile_no`, `father_name`, `parent_id`, `introducer_code`, `user_id`, `package_id`, `location`, `full_name`, `user_pin`, `user_pic_path`, `previous`, `created_at`, `updated_at`, `package_updated_at`) VALUES
(1, 35202, 'lahore cantt', 'faraz', 'brother', 35202, '03018901001', '', 0, 180600000, 1, 5, 0, 'Imran Rehmat', 1234, 'https://scontent.flhe3-1.fna.fbcdn.net/v/t1.0-9/18557027_1657527027610639_2858257023122103052_n.jpg?_nc_cat=0&oh=6ab94ab7f84a7060537f70df3ba304db&oe=5B7CD36B', '2018-07-17 00:00:00', '2018-07-17 12:07:33', '0000-00-00 00:00:00', ''),
(2, 36601, 'fatehgargh mughalpura lahore', 'azeem', 'bro', 0, '03059829843', 'Abdul Hameed', 1, 180700001, 2, 4, 1, 'Muhammad Zahid', 7651, '180700001', '2018-07-17 00:00:00', '2018-07-17 12:35:42', '0000-00-00 00:00:00', 'a:1:{i:4;s:19:\"2018-07-17 17:35:42\";}'),
(3, 64564, 'cantt', 'jill', 'bro', 0, '86868776654', 'mr.doe', 1, 180700001, 3, 1, 2, 'john doe', 6487, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-13 00:00:00', '2018-07-17 12:59:15', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 17:59:15\";}'),
(4, 76876, 'cantt', 'jill', 'bro', 0, '56465453545', 'mr.doe', 1, 180700001, 4, 1, 3, 'john doe', 1487, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-06-05 00:00:00', '2018-07-17 13:00:07', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:00:07\";}'),
(5, 76876, 'cantt', 'jill', 'bro', 0, '54354354354', 'mr.doe', 1, 180700001, 5, 1, 4, 'john doe', 1327, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-12-13 00:00:00', '2018-07-17 13:01:28', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:01:28\";}'),
(6, 76687, 'cantt', 'jill', 'bro', 0, '43343434343', 'mr.doe', 2, 180700002, 6, 1, 1, 'john doe', 7069, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-01 00:00:00', '2018-07-17 13:02:58', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:02:58\";}'),
(7, 24512, 'cantt', 'jill', 'bro', 0, '42524534534', 'mr.doe', 4, 180700004, 7, 1, 1, 'john doe', 5064, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '1970-01-01 00:00:00', '2018-07-17 13:08:54', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:08:54\";}'),
(8, 24532, 'cantt', 'jill', 'bro', 0, '53635636235', 'mr.doe', 5, 180700005, 8, 1, 1, 'john doe', 4806, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-10-30 00:00:00', '2018-07-17 13:09:57', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:09:57\";}'),
(9, 25345, 'cantt', 'jill', 'bro', 0, '24523453464', 'mr.doe', 6, 180700006, 9, 1, 1, 'john doe', 7419, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-09-26 00:00:00', '2018-07-17 13:11:07', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:11:07\";}'),
(10, 31423, 'cantt', 'jill', 'bro', 0, '32421524135', 'mr.doe', 6, 180700006, 10, 1, 2, 'john doe', 2869, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-08-08 00:00:00', '2018-07-17 13:11:47', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:11:47\";}'),
(11, 43532, 'cantt', 'jill', 'bro', 0, '32563563563', 'mr.doe', 6, 180700006, 11, 1, 3, 'john doe', 8701, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-09 00:00:00', '2018-07-17 13:12:34', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:12:34\";}'),
(12, 45345, 'cantt', 'jill', 'bro', 0, '43524353245', 'mr.doe', 4, 180700004, 12, 1, 2, 'john doe', 6918, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-09-25 00:00:00', '2018-07-17 13:13:46', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:13:46\";}'),
(13, 34534, 'cantt', 'jill', 'bro', 0, '43534534534', 'mr.doe', 3, 180700003, 13, 1, 1, 'john doe', 4780, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-28 00:00:00', '2018-07-17 13:14:36', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:14:36\";}'),
(14, 24534, 'cantt', 'jill', 'bro', 0, '42543253453', 'mr.doe', 10, 180700010, 14, 1, 1, 'john doe', 4320, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-08-15 00:00:00', '2018-07-17 13:22:17', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:22:17\";}'),
(15, 32453, 'cantt', 'jill', 'bro', 0, '43534534253', 'mr.doe', 14, 180700014, 15, 1, 1, 'john doe', 6478, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-09-25 00:00:00', '2018-07-17 13:23:07', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:23:07\";}'),
(16, 24525, 'cantt', 'jill', 'bro', 0, '43534253453', 'mr.doe', 15, 180700015, 16, 1, 1, 'john doe', 9386, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-07-02 00:00:00', '2018-07-17 13:23:56', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:23:56\";}'),
(17, 56456, 'cantt', 'jill', 'bro', 0, '12345678999', 'mr.doe', 16, 180700016, 17, 1, 1, 'john doe', 1408, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '1970-01-01 00:00:00', '2018-07-17 13:25:22', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:25:22\";}'),
(18, 53645, 'cantt', 'jill', 'bro', 0, '64646565363', 'mr.doe', 17, 180700017, 18, 1, 1, 'john doe', 7381, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-08-27 00:00:00', '2018-07-17 13:28:13', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:28:13\";}'),
(19, 98989, 'cantt', 'jill', 'bro', 0, '98123938497', 'mr.doe', 18, 180700018, 19, 1, 1, 'john doe', 6037, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-07-30 00:00:00', '2018-07-17 13:29:07', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:29:07\";}'),
(20, 43567, 'cantt', 'jill', 'bro', 0, '11111111111', 'mr.doe', 19, 180700019, 20, 1, 1, 'john doe', 5836, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-08-07 00:00:00', '2018-07-17 13:30:01', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:30:01\";}'),
(21, 22222, 'cantt', 'jill', 'bro', 0, '11111122222', 'mr.doe', 20, 180700020, 21, 1, 1, 'john doe', 2859, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-10-30 00:00:00', '2018-07-17 13:30:48', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:30:48\";}'),
(22, 42353, 'cantt', 'jill', 'bro', 0, '53245232423', 'mr.doe', 16, 180700016, 22, 1, 2, 'john doe', 2138, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-12-05 00:00:00', '2018-07-17 13:31:51', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:31:51\";}'),
(23, 99999, 'cantt', 'jill', 'bro', 0, '99999988888', 'mr.doe', 8, 180700008, 23, 1, 1, 'john doe', 967, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-10-29 00:00:00', '2018-07-17 13:41:26', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:41:26\";}'),
(24, 88888, 'cantt', 'jill', 'bro', 0, '09890989088', 'mr.doe', 8, 180700008, 24, 1, 2, 'john doe', 3108, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-07-30 00:00:00', '2018-07-17 13:42:07', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:42:07\";}'),
(25, 12123, 'cantt', 'jill', 'bro', 0, '12312312312', 'mr.doe', 8, 180700008, 25, 1, 3, 'john doe', 3587, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-12-22 00:00:00', '2018-07-17 13:43:09', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:43:09\";}'),
(26, 45645, 'cantt', 'jill', 'bro', 0, '45454565656', 'mr.doe', 8, 180700008, 26, 1, 4, 'john doe', 5420, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-07-11 00:00:00', '2018-07-17 13:44:01', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:44:01\";}'),
(27, 43534, 'cantt', 'jill', 'bro', 0, '45345464756', 'mr.doe', 21, 180700021, 27, 1, 1, 'john doe', 7492, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-10-09 00:00:00', '2018-07-17 13:56:01', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:56:01\";}'),
(28, 38953, 'cantt', 'jill', 'bro', 0, '82938293829', 'mr.doe', 21, 180700021, 28, 1, 2, 'john doe', 1927, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-12-04 00:00:00', '2018-07-17 13:56:36', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:56:36\";}'),
(29, 87875, 'cantt', 'jill', 'bro', 0, '09877676754', 'mr.doe', 28, 180700028, 29, 1, 1, 'john doe', 7613, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-10-25 00:00:00', '2018-07-17 13:58:45', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:58:45\";}'),
(30, 34564, 'cantt', 'jill', 'bro', 0, '67867879658', 'mr.doe', 28, 180700028, 30, 1, 2, 'john doe', 9057, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-10-18 00:00:00', '2018-07-17 13:59:16', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:59:16\";}'),
(31, 53453, 'cantt', 'jill', 'bro', 0, '43536457657', 'mr.doe', 28, 180700028, 31, 1, 3, 'john doe', 1578, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-11-22 00:00:00', '2018-07-17 13:59:53', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 18:59:53\";}'),
(32, 49857, 'cantt', 'jill', 'bro', 0, '93874239847', 'mr.doe', 29, 180700029, 32, 1, 1, 'john doe', 5486, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-08-22 00:00:00', '2018-07-17 14:00:28', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-17 19:00:28\";}'),
(33, 45645, 'cantt', 'jill', 'bro', 0, '56457567567', 'mr.doe', 1, 180700001, 33, 1, 5, 'john doe', 8295, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-01 00:00:00', '2018-07-18 07:49:20', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:49:20\";}'),
(34, 78976, 'cantt', 'jill', 'bro', 0, '45645645645', 'mr.doe', 33, 180700033, 34, 1, 1, 'john doe', 2085, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-05 00:00:00', '2018-07-18 07:51:06', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:51:06\";}'),
(35, 45646, 'cantt', 'jill', 'bro', 0, '45238727257', 'mr.doe', 34, 180700034, 35, 1, 1, 'john doe', 943, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-10 00:00:00', '2018-07-18 07:52:26', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:52:26\";}'),
(36, 32352, 'cantt', 'jill', 'bro', 0, '67567567567', 'mr.doe', 35, 180700035, 36, 1, 1, 'john doe', 6752, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-14 00:00:00', '2018-07-18 07:55:24', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:55:24\";}'),
(37, 75687, 'cantt', 'jill', 'bro', 0, '23423423455', 'mr.doe', 36, 180700036, 37, 1, 1, 'john doe', 5297, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-17 00:00:00', '2018-07-18 07:56:27', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:56:27\";}'),
(38, 78978, 'cantt', 'jill', 'bro', 0, '68768563564', 'mr.doe', 37, 180700037, 38, 1, 1, 'john doe', 3107, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-20 00:00:00', '2018-07-18 07:57:17', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:57:17\";}'),
(39, 87689, 'cantt', 'jill', 'bro', 0, '56767567567', 'mr.doe', 38, 180700038, 39, 1, 1, 'john doe', 9217, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-23 00:00:00', '2018-07-18 07:58:14', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:58:14\";}'),
(40, 64564, 'cantt', 'jill', 'bro', 0, '34545675675', 'mr.doe', 39, 180700039, 40, 1, 1, 'john doe', 3502, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-26 00:00:00', '2018-07-18 07:59:00', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 12:59:00\";}'),
(41, 78976, 'cantt', 'jill', 'bro', 0, '09876543454', 'mr.doe', 40, 180700040, 41, 1, 1, 'john doe', 3724, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-28 00:00:00', '2018-07-18 08:00:58', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:00:58\";}'),
(42, 78976, 'cantt', 'jill', 'bro', 0, '65687696587', 'mr.doe', 41, 180700041, 42, 1, 1, 'john doe', 6194, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-01-31 00:00:00', '2018-07-18 08:01:59', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:01:59\";}'),
(43, 786, 'cantt', 'jill', 'bro', 0, '34546575674', 'mr.doe', 42, 180700042, 43, 1, 1, 'john doe', 4872, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-02-01 00:00:00', '2018-07-18 08:03:16', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:03:16\";}'),
(44, 756, 'cantt', 'jill', 'bro', 0, '34546545635', 'mr.doe', 43, 180700043, 44, 1, 1, 'john doe', 1480, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-02-03 00:00:00', '2018-07-18 08:04:44', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:04:44\";}'),
(45, 97534, 'cantt', 'jill', 'bro', 0, '78978953453', 'mr.doe', 44, 180700044, 45, 1, 1, 'john doe', 5179, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-02-07 00:00:00', '2018-07-18 08:06:38', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:06:38\";}'),
(46, 76876, 'cantt', 'jill', 'bro', 0, '67567687662', 'mr.doe', 45, 180700045, 46, 1, 1, 'john doe', 2739, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-02-10 00:00:00', '2018-07-18 08:07:26', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:07:26\";}'),
(47, 78452, 'cantt', 'jill', 'bro', 0, '53475674543', 'mr.doe', 35, 180700035, 47, 1, 2, 'john doe', 9081, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-10 00:00:00', '2018-07-18 08:09:24', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:09:24\";}'),
(48, 45645, 'cantt', 'jill', 'bro', 0, '56456456545', 'mr.doe', 35, 180700035, 48, 1, 3, 'john doe', 3541, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-24 00:00:00', '2018-07-18 08:10:32', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:10:32\";}'),
(49, 35656, 'cantt', 'jill', 'bro', 0, '56756756757', 'mr.doe', 35, 180700035, 49, 1, 4, 'john doe', 2769, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-17 00:00:00', '2018-07-18 08:11:18', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:11:18\";}'),
(50, 76745, 'cantt', 'jill', 'bro', 0, '45645645656', 'mr.doe', 35, 180700035, 50, 1, 5, 'john doe', 2680, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-10 00:00:00', '2018-07-18 08:12:41', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:12:41\";}'),
(51, 76786, 'cantt', 'jill', 'bro', 0, '45768687687', 'mr.doe', 39, 180700039, 51, 1, 2, 'john doe', 8342, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-30 00:00:00', '2018-07-18 08:13:20', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:13:20\";}'),
(52, 89078, 'cantt', 'jill', 'bro', 0, '56457568768', 'mr.doe', 39, 180700039, 52, 1, 3, 'john doe', 261, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-05 00:00:00', '2018-07-18 08:14:15', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:14:15\";}'),
(53, 45647, 'cantt', 'jill', 'bro', 0, '46463545234', 'mr.doe', 40, 180700040, 53, 1, 2, 'john doe', 9834, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-06-24 00:00:00', '2018-07-18 08:15:55', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:15:55\";}'),
(54, 46457, 'cantt', 'jill', 'bro', 0, '53453453453', 'mr.doe', 34, 180700034, 54, 1, 2, 'john doe', 9083, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-10 00:00:00', '2018-07-18 08:16:45', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:16:45\";}'),
(55, 64578, 'cantt', 'jill', 'bro', 0, '74579089797', 'mr.doe', 34, 180700034, 55, 1, 3, 'john doe', 6302, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-06-27 00:00:00', '2018-07-18 08:17:32', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:17:32\";}'),
(56, 89567, 'cantt', 'jill', 'bro', 0, '56756878976', 'mr.doe', 37, 180700037, 56, 1, 2, 'john doe', 367, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-02 00:00:00', '2018-07-18 08:18:25', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:18:25\";}'),
(57, 54645, 'cantt', 'jill', 'bro', 0, '76876876876', 'mr.doe', 37, 180700037, 57, 1, 3, 'john doe', 1620, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-03 00:00:00', '2018-07-18 08:19:06', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:19:06\";}'),
(58, 89078, 'cantt', 'jill', 'bro', 0, '45577464523', 'mr.doe', 44, 180700044, 58, 1, 2, 'john doe', 1095, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-17 00:00:00', '2018-07-18 08:19:46', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:19:46\";}'),
(59, 42346, 'cantt', 'jill', 'bro', 0, '45568678976', 'mr.doe', 43, 180700043, 59, 1, 2, 'john doe', 3480, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-06-05 00:00:00', '2018-07-18 08:20:49', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:20:49\";}'),
(60, 23423, 'cantt', 'jill', 'bro', 0, '32423454575', 'mr.doe', 43, 180700043, 60, 1, 3, 'john doe', 4156, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-14 00:00:00', '2018-07-18 08:21:41', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:21:41\";}'),
(61, 34534, 'cantt', 'jill', 'bro', 0, '75687652342', 'mr.doe', 38, 180700038, 61, 1, 2, 'john doe', 9824, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-06-05 00:00:00', '2018-07-18 08:22:55', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:22:55\";}'),
(62, 34456, 'cantt', 'jill', 'bro', 0, '53547566356', 'mr.doe', 36, 180700036, 62, 1, 2, 'john doe', 1697, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-11 00:00:00', '2018-07-18 08:23:31', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:23:31\";}'),
(63, 52345, 'cantt', 'jill', 'bro', 0, '64232222222', 'mr.doe', 45, 180700045, 63, 1, 2, 'john doe', 1398, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-17 00:00:00', '2018-07-18 08:24:20', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:24:20\";}'),
(64, 34545, 'cantt', 'jill', 'bro', 0, '56457687686', 'mr.doe', 41, 180700041, 64, 1, 2, 'john doe', 9132, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-17 00:00:00', '2018-07-18 08:24:52', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:24:52\";}'),
(65, 34563, 'cantt', 'jill', 'bro', 0, '56456564563', 'mr.doe', 42, 180700042, 65, 1, 2, 'john doe', 6217, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-09 00:00:00', '2018-07-18 08:25:49', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:25:49\";}'),
(66, 23423, 'cantt', 'jill', 'bro', 0, '34576575676', 'mr.doe', 46, 180700046, 66, 1, 1, 'john doe', 3481, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-10 00:00:00', '2018-07-18 08:26:30', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:26:30\";}'),
(67, 76575, 'cantt', 'jill', 'bro', 0, '56756768769', 'mr.doe', 66, 180700066, 67, 1, 1, 'john doe', 2045, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-25 00:00:00', '2018-07-18 08:29:00', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:29:00\";}'),
(68, 45645, 'cantt', 'jill', 'bro', 0, '47645645645', 'mr.doe', 67, 180700067, 68, 1, 1, 'john doe', 4315, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-19 00:00:00', '2018-07-18 08:29:47', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:29:47\";}'),
(69, 54645, 'cantt', 'jill', 'bro', 0, '53447856776', 'mr.doe', 68, 180700068, 69, 1, 1, 'john doe', 2894, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-02-22 00:00:00', '2018-07-18 08:31:55', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:31:55\";}'),
(70, 43234, 'cantt', 'jill', 'bro', 0, '32423423423', 'mr.doe', 69, 180700069, 70, 1, 1, 'john doe', 285, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-01 00:00:00', '2018-07-18 08:33:45', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:33:45\";}'),
(71, 56465, 'cantt', 'jill', 'bro', 0, '34553452324', 'mr.doe', 70, 180700070, 71, 1, 1, 'john doe', 6548, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-05 00:00:00', '2018-07-18 08:35:33', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:35:33\";}'),
(72, 45534, 'cantt', 'jill', 'bro', 0, '54353425345', 'mr.doe', 71, 180700071, 72, 1, 1, 'john doe', 5980, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-07 00:00:00', '2018-07-18 08:36:41', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:36:41\";}'),
(73, 53456, 'cantt', 'jill', 'bro', 0, '56452436257', 'mr.doe', 72, 180700072, 73, 1, 1, 'john doe', 514, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-10 00:00:00', '2018-07-18 08:37:37', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:37:37\";}'),
(74, 56456, 'cantt', 'jill', 'bro', 0, '45654444645', 'mr.doe', 73, 180700073, 74, 1, 1, 'john doe', 8963, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-12 00:00:00', '2018-07-18 08:39:23', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:39:23\";}'),
(75, 45346, 'cantt', 'jill', 'bro', 0, '54543253465', 'mr.doe', 74, 180700074, 75, 1, 1, 'john doe', 4605, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-14 00:00:00', '2018-07-18 08:41:40', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:41:40\";}'),
(76, 56234, 'cantt', 'jill', 'bro', 0, '56263562564', 'mr.doe', 75, 180700075, 76, 1, 1, 'john doe', 8014, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-16 00:00:00', '2018-07-18 08:43:06', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:43:06\";}'),
(77, 32453, 'cantt', 'jill', 'bro', 0, '34534535345', 'mr.doe', 76, 180700076, 77, 1, 1, 'john doe', 567, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-18 00:00:00', '2018-07-18 08:45:54', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:45:54\";}'),
(78, 48547, 'cantt', 'jill', 'bro', 0, '45849466484', 'mr.doe', 77, 180700077, 78, 1, 1, 'john doe', 4068, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-20 00:00:00', '2018-07-18 08:47:58', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:47:58\";}'),
(79, 54646, 'cantt', 'jill', 'bro', 0, '74564565464', 'mr.doe', 78, 180700078, 79, 1, 1, 'john doe', 8642, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-22 00:00:00', '2018-07-18 08:53:31', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:53:31\";}'),
(80, 56564, 'cantt', 'jill', 'bro', 0, '45654444444', 'mr.doe', 79, 180700079, 80, 1, 1, 'john doe', 345, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-24 00:00:00', '2018-07-18 08:54:33', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:54:33\";}'),
(81, 56345, 'cantt', 'jill', 'bro', 0, '73564565464', 'mr.doe', 80, 180700080, 81, 1, 1, 'john doe', 2643, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-26 00:00:00', '2018-07-18 08:55:58', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:55:58\";}'),
(82, 45656, 'cantt', 'jill', 'bro', 0, '43657656765', 'mr.doe', 81, 180700081, 82, 1, 1, 'john doe', 3249, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-28 00:00:00', '2018-07-18 08:58:31', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:58:31\";}'),
(83, 45525, 'cantt', 'jill', 'bro', 0, '73462235423', 'mr.doe', 82, 180700082, 83, 1, 1, 'john doe', 7561, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-30 00:00:00', '2018-07-18 08:59:41', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 13:59:41\";}'),
(84, 46565, 'cantt', 'jill', 'bro', 0, '63452365645', 'mr.doe', 83, 180700083, 84, 1, 1, 'john doe', 7803, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-02 00:00:00', '2018-07-18 09:01:25', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:01:25\";}'),
(85, 36735, 'cantt', 'jill', 'bro', 0, '46456456456', 'mr.doe', 84, 180700084, 85, 1, 1, 'john doe', 7238, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-04 00:00:00', '2018-07-18 09:02:38', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:02:38\";}'),
(86, 47367, 'cantt', 'jill', 'bro', 0, '46764763576', 'mr.doe', 85, 180700085, 86, 1, 1, 'john doe', 6198, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-06 00:00:00', '2018-07-18 09:04:21', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:04:21\";}'),
(87, 43656, 'cantt', 'jill', 'bro', 0, '56464556345', 'mr.doe', 86, 180700086, 87, 1, 1, 'john doe', 9308, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-08 00:00:00', '2018-07-18 09:06:13', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:06:13\";}'),
(88, 56564, 'cantt', 'jill', 'bro', 0, '56546454564', 'mr.doe', 87, 180700087, 88, 1, 1, 'john doe', 7180, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-10 00:00:00', '2018-07-18 09:08:34', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:08:34\";}'),
(89, 76575, 'cantt', 'jill', 'bro', 0, '63474765735', 'mr.doe', 88, 180700088, 89, 1, 1, 'john doe', 5647, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-12 00:00:00', '2018-07-18 09:09:51', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:09:51\";}'),
(90, 67657, 'cantt', 'jill', 'bro', 0, '53645645656', 'mr.doe', 78, 180700078, 90, 1, 2, 'john doe', 1257, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-24 00:00:00', '2018-07-18 09:11:14', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:11:14\";}'),
(91, 45374, 'cantt', 'jill', 'bro', 0, '56546546255', 'mr.doe', 33, 180700033, 91, 1, 2, 'john doe', 1497, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-09 00:00:00', '2018-07-18 09:12:50', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:12:50\";}'),
(92, 74564, 'cantt', 'jill', 'bro', 0, '76473567567', 'mr.doe', 67, 180700067, 92, 1, 2, 'john doe', 7634, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-07-09 00:00:00', '2018-07-18 09:19:01', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:19:01\";}'),
(93, 63356, 'cantt', 'jill', 'bro', 0, '63465535656', 'mr.doe', 89, 180700089, 93, 1, 1, 'john doe', 6802, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-29 00:00:00', '2018-07-18 09:23:51', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:23:51\";}'),
(94, 64564, 'cantt', 'jill', 'bro', 0, '56364562623', 'mr.doe', 93, 180700093, 94, 1, 1, 'john doe', 2791, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-06-05 00:00:00', '2018-07-18 09:24:50', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:24:50\";}'),
(95, 63635, 'cantt', 'jill', 'bro', 0, '36346135634', 'mr.doe', 94, 180700094, 95, 1, 1, 'john doe', 6841, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-09 00:00:00', '2018-07-18 09:25:39', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:25:39\";}'),
(96, 54764, 'cantt', 'jill', 'bro', 0, '56456456346', 'mr.doe', 95, 180700095, 96, 1, 1, 'john doe', 8450, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-09 00:00:00', '2018-07-18 09:26:24', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:26:24\";}'),
(97, 54764, 'cantt', 'jill', 'bro', 0, '74674676764', 'mr.doe', 96, 180700096, 97, 1, 1, 'john doe', 4589, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-22 00:00:00', '2018-07-18 09:28:21', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:28:21\";}'),
(98, 45764, 'cantt', 'jill', 'bro', 0, '67637476675', 'mr.doe', 97, 180700097, 98, 1, 1, 'john doe', 194, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-15 00:00:00', '2018-07-18 09:29:13', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:29:13\";}'),
(99, 54764, 'cantt', 'jill', 'bro', 0, '56456456456', 'mr.doe', 98, 180700098, 99, 1, 1, 'john doe', 164, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-06 00:00:00', '2018-07-18 09:30:18', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:30:18\";}'),
(100, 56456, 'cantt', 'jill', 'bro', 0, '45654645645', 'mr.doe', 99, 180700099, 100, 1, 1, 'john doe', 9074, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-02 00:00:00', '2018-07-18 09:36:51', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:36:51\";}'),
(101, 56565, 'cantt', 'jill', 'bro', 0, '35656562565', 'mr.doe', 100, 180700100, 101, 1, 1, 'john doe', 3157, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-29 00:00:00', '2018-07-18 09:37:44', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:37:44\";}'),
(102, 63635, 'cantt', 'jill', 'bro', 0, '34563456363', 'mr.doe', 101, 180700101, 102, 1, 1, 'john doe', 4371, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-10 00:00:00', '2018-07-18 09:38:57', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:38:57\";}'),
(103, 74576, 'cantt', 'jill', 'bro', 0, '74675675765', 'mr.doe', 102, 180700102, 103, 1, 1, 'john doe', 6803, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-17 00:00:00', '2018-07-18 09:39:55', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:39:55\";}'),
(104, 56456, 'cantt', 'jill', 'bro', 0, '56565764576', 'mr.doe', 103, 180700103, 104, 1, 1, 'john doe', 3429, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-09 00:00:00', '2018-07-18 09:40:51', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:40:51\";}'),
(105, 76767, 'cantt', 'jill', 'bro', 0, '63563563463', 'mr.doe', 104, 180700104, 105, 1, 1, 'john doe', 2854, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-10 00:00:00', '2018-07-18 09:42:23', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:42:23\";}'),
(106, 65656, 'cantt', 'jill', 'bro', 0, '53652627645', 'mr.doe', 105, 180700105, 106, 1, 1, 'john doe', 4729, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-06 00:00:00', '2018-07-18 09:43:37', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:43:37\";}'),
(107, 47473, 'cantt', 'jill', 'bro', 0, '74746747456', 'mr.doe', 106, 180700106, 107, 1, 1, 'john doe', 9013, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-25 00:00:00', '2018-07-18 09:44:28', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:44:28\";}'),
(108, 67457, 'cantt', 'jill', 'bro', 0, '74576547467', 'mr.doe', 107, 180700107, 108, 1, 1, 'john doe', 7162, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-20 00:00:00', '2018-07-18 09:45:24', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:45:24\";}'),
(109, 36537, 'cantt', 'jill', 'bro', 0, '56363563565', 'mr.doe', 108, 180700108, 109, 1, 1, 'john doe', 2843, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-17 00:00:00', '2018-07-18 09:46:08', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:46:08\";}'),
(110, 67474, 'cantt', 'jill', 'bro', 0, '53635635635', 'mr.doe', 109, 180700109, 110, 1, 1, 'john doe', 9253, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-24 00:00:00', '2018-07-18 09:47:00', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:47:00\";}'),
(111, 53675, 'cantt', 'jill', 'bro', 0, '67467674734', 'mr.doe', 110, 180700110, 111, 1, 1, 'john doe', 7291, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-03 00:00:00', '2018-07-18 09:48:13', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:48:13\";}'),
(112, 55476, 'cantt', 'jill', 'bro', 0, '23452352542', 'mr.doe', 111, 180700111, 112, 1, 1, 'john doe', 9485, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-09 00:00:00', '2018-07-18 09:49:36', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:49:36\";}'),
(113, 57467, 'cantt', 'jill', 'bro', 0, '47467457574', 'mr.doe', 112, 180700112, 113, 1, 1, 'john doe', 8157, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-27 00:00:00', '2018-07-18 09:51:10', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:51:10\";}'),
(114, 45764, 'cantt', 'jill', 'bro', 0, '47467674746', 'mr.doe', 113, 180700113, 114, 1, 1, 'john doe', 5193, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-25 00:00:00', '2018-07-18 09:51:50', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:51:50\";}'),
(115, 56745, 'cantt', 'jill', 'bro', 0, '46747467646', 'mr.doe', 114, 180700114, 115, 1, 1, 'john doe', 3802, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-09 00:00:00', '2018-07-18 09:53:08', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:53:08\";}'),
(116, 36363, 'cantt', 'jill', 'bro', 0, '63563563563', 'mr.doe', 115, 180700115, 116, 1, 1, 'john doe', 6943, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-11 00:00:00', '2018-07-18 09:53:47', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:53:47\";}'),
(117, 34633, 'cantt', 'jill', 'bro', 0, '56356356365', 'mr.doe', 116, 180700116, 117, 1, 1, 'john doe', 9254, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-22 00:00:00', '2018-07-18 09:54:27', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:54:27\";}'),
(118, 63656, 'cantt', 'jill', 'bro', 0, '65365635635', 'mr.doe', 117, 180700117, 118, 1, 1, 'john doe', 5483, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-09 00:00:00', '2018-07-18 09:55:10', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:55:10\";}'),
(119, 75767, 'cantt', 'jill', 'bro', 0, '75757457457', 'mr.doe', 118, 180700118, 119, 1, 1, 'john doe', 7963, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-02 00:00:00', '2018-07-18 09:57:00', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:57:00\";}'),
(120, 24534, 'cantt', 'jill', 'bro', 0, '46363563634', 'mr.doe', 119, 180700119, 120, 1, 1, 'john doe', 7908, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-01 00:00:00', '2018-07-18 09:57:44', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:57:44\";}'),
(121, 63576, 'cantt', 'jill', 'bro', 0, '635636363', 'mr.doe', 120, 180700120, 121, 1, 1, 'john doe', 3156, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-08 00:00:00', '2018-07-18 09:59:18', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 14:59:18\";}'),
(122, 56363, 'cantt', 'jill', 'bro', 0, '56356363634', 'mr.doe', 121, 180700121, 122, 1, 1, 'john doe', 3760, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-16 00:00:00', '2018-07-18 10:00:04', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:00:04\";}'),
(123, 67457, 'cantt', 'jill', 'bro', 0, '35635657654', 'mr.doe', 122, 180700122, 123, 1, 1, 'john doe', 7469, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-15 00:00:00', '2018-07-18 10:00:58', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:00:58\";}'),
(124, 35763, 'cantt', 'jill3', 'bro', 0, '35635636356', 'mr.doe', 123, 180700123, 124, 1, 1, 'john doe', 763, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-21 00:00:00', '2018-07-18 10:01:39', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:01:39\";}'),
(125, 56547, 'cantt', 'jill', 'bro', 0, '74674674574', 'mr.doe', 124, 180700124, 125, 1, 1, 'john doe', 7160, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-09 00:00:00', '2018-07-18 10:02:21', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:02:21\";}'),
(126, 63563, 'cantt', 'jill', 'bro', 0, '56356356363', 'mr.doe', 125, 180700125, 126, 1, 1, 'john doe', 4639, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-03-19 00:00:00', '2018-07-18 10:03:06', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:03:06\";}'),
(127, 46356, 'cantt', 'jill', 'bro', 0, '36363563636', 'mr.doe', 126, 180700126, 127, 1, 1, 'john doe', 652, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-02 00:00:00', '2018-07-18 10:03:51', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:03:51\";}'),
(128, 46346, 'cantt', 'jill', 'bro', 0, '34634634653', 'mr.doe', 127, 180700127, 128, 1, 1, 'john doe', 8734, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-14 00:00:00', '2018-07-18 10:05:30', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:05:30\";}'),
(129, 57467, 'cantt', 'jill', 'bro', 0, '74674674674', 'mr.doe', 128, 180700128, 129, 1, 1, 'john doe', 7906, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-06-10 00:00:00', '2018-07-18 10:06:17', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:06:17\";}'),
(130, 52463, 'cantt', 'jill', 'bro', 0, '35635635636', 'mr.doe', 129, 180700129, 130, 1, 1, 'john doe', 8175, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-27 00:00:00', '2018-07-18 10:09:12', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:09:12\";}'),
(131, 56665, 'cantt', 'jill', 'bro', 0, '56536565656', 'mr.doe', 130, 180700130, 131, 1, 1, 'john doe', 8056, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-21 00:00:00', '2018-07-18 10:10:13', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:10:13\";}'),
(132, 53635, 'cantt', 'jill', 'bro', 0, '56356356536', 'mr.doe', 130, 180700130, 132, 1, 2, 'john doe', 1356, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-10 00:00:00', '2018-07-18 10:11:28', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:11:28\";}'),
(133, 63724, 'cantt', 'jill', 'bro', 0, '12323111111', 'mr.doe', 130, 180700130, 133, 1, 3, 'john doe', 5680, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-05-14 00:00:00', '2018-07-18 10:12:42', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:12:42\";}'),
(134, 63735, 'cantt', 'jill', 'bro', 0, '64635453426', 'mr.doe', 130, 180700130, 134, 1, 4, 'john doe', 6498, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-02-22 00:00:00', '2018-07-18 10:16:32', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 15:16:32\";}'),
(135, 57345, 'cantt', 'jill', 'bro', 0, '53i73536536', 'mr.doe', 130, 180700130, 135, 1, 5, 'john doe', 5362, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2018-04-24 00:00:00', '2018-07-18 11:24:39', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 16:24:39\";}'),
(136, 29742, 'cantt', 'jill', 'bro', 0, '29364248932', 'mr.doe', 1, 180700001, 136, 1, 6, 'john doe', 3912, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-12-19 00:00:00', '2018-07-18 12:03:03', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 17:03:03\";}'),
(137, 98234, 'cantt', 'jill', 'bro', 0, '82374683274', 'mr.doe', 130, 180700130, 137, 1, 6, 'john doe', 568, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-11-15 00:00:00', '2018-07-18 12:03:54', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 17:03:54\";}'),
(138, 72836, 'cantt', 'jill', 'bro', 0, '23432423423', 'mr.doe', 130, 180700130, 138, 1, 7, 'john doe', 6289, 'http://stacktrio.com/risingstar/assets//uploads/no-pic.png', '2017-11-15 00:00:00', '2018-07-18 12:07:47', '0000-00-00 00:00:00', 'a:1:{i:1;s:19:\"2018-07-18 17:07:47\";}');

-- --------------------------------------------------------

--
-- Table structure for table `rising_users_tree`
--

CREATE TABLE `rising_users_tree` (
  `id` int(11) NOT NULL,
  `aauth_user_id` int(11) NOT NULL,
  `parents` varchar(2000) NOT NULL,
  `user_tree_level` varchar(255) NOT NULL DEFAULT '0',
  `user_team_members` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rising_users_tree`
--

INSERT INTO `rising_users_tree` (`id`, `aauth_user_id`, `parents`, `user_tree_level`, `user_team_members`) VALUES
(1, 1, '0', '76', '130'),
(2, 2, '1', '13', '19'),
(3, 3, '1', '1', '1'),
(4, 4, '1', '1', '2'),
(5, 5, '1', '2', '5'),
(6, 6, '2,1', '12', '18'),
(7, 7, '4,1', '0', '0'),
(8, 8, '5,1', '1', '4'),
(9, 9, '6,2,1', '0', '0'),
(10, 10, '6,2,1', '11', '15'),
(11, 11, '6,2,1', '0', '0'),
(12, 12, '4,1', '0', '0'),
(13, 13, '3,1', '0', '0'),
(14, 14, '10,6,2,1', '10', '14'),
(15, 15, '14,10,6,2,1', '9', '13'),
(16, 16, '15,14,10,6,2,1', '8', '12'),
(17, 17, '16,15,14,10,6,2,1', '7', '10'),
(18, 18, '17,16,15,14,10,6,2,1', '6', '9'),
(19, 19, '18,17,16,15,14,10,6,2,1', '5', '8'),
(20, 20, '19,18,17,16,15,14,10,6,2,1', '4', '7'),
(21, 21, '20,19,18,17,16,15,14,10,6,2,1', '3', '6'),
(22, 22, '16,15,14,10,6,2,1', '0', '0'),
(23, 23, '8,5,1', '0', '0'),
(24, 24, '8,5,1', '0', '0'),
(25, 25, '8,5,1', '0', '0'),
(26, 26, '8,5,1', '0', '0'),
(27, 27, '21,20,19,18,17,16,15,14,10,6,2,1', '0', '0'),
(28, 28, '21,20,19,18,17,16,15,14,10,6,2,1', '2', '4'),
(29, 29, '28,21,20,19,18,17,16,15,14,10,6,2,1', '1', '1'),
(30, 30, '28,21,20,19,18,17,16,15,14,10,6,2,1', '0', '0'),
(31, 31, '28,21,20,19,18,17,16,15,14,10,6,2,1', '0', '0'),
(32, 32, '29,28,21,20,19,18,17,16,15,14,10,6,2,1', '0', '0'),
(33, 33, '1', '76', '97'),
(34, 34, '33,1', '75', '95'),
(35, 35, '34,33,1', '74', '92'),
(36, 36, '35,34,33,1', '73', '87'),
(37, 37, '36,35,34,33,1', '72', '85'),
(38, 38, '37,36,35,34,33,1', '71', '82'),
(39, 39, '38,37,36,35,34,33,1', '70', '80'),
(40, 40, '39,38,37,36,35,34,33,1', '69', '77'),
(41, 41, '40,39,38,37,36,35,34,33,1', '68', '75'),
(42, 42, '41,40,39,38,37,36,35,34,33,1', '67', '73'),
(43, 43, '42,41,40,39,38,37,36,35,34,33,1', '66', '71'),
(44, 44, '43,42,41,40,39,38,37,36,35,34,33,1', '65', '68'),
(45, 45, '44,43,42,41,40,39,38,37,36,35,34,33,1', '64', '66'),
(46, 46, '45,44,43,42,41,40,39,38,37,36,35,34,33,1', '63', '64'),
(47, 47, '35,34,33,1', '0', '0'),
(48, 48, '35,34,33,1', '0', '0'),
(49, 49, '35,34,33,1', '0', '0'),
(50, 50, '35,34,33,1', '0', '0'),
(51, 51, '39,38,37,36,35,34,33,1', '0', '0'),
(52, 52, '39,38,37,36,35,34,33,1', '0', '0'),
(53, 53, '40,39,38,37,36,35,34,33,1', '0', '0'),
(54, 54, '34,33,1', '0', '0'),
(55, 55, '34,33,1', '0', '0'),
(56, 56, '37,36,35,34,33,1', '0', '0'),
(57, 57, '37,36,35,34,33,1', '0', '0'),
(58, 58, '44,43,42,41,40,39,38,37,36,35,34,33,1', '0', '0'),
(59, 59, '43,42,41,40,39,38,37,36,35,34,33,1', '0', '0'),
(60, 60, '43,42,41,40,39,38,37,36,35,34,33,1', '0', '0'),
(61, 61, '38,37,36,35,34,33,1', '0', '0'),
(62, 62, '36,35,34,33,1', '0', '0'),
(63, 63, '45,44,43,42,41,40,39,38,37,36,35,34,33,1', '0', '0'),
(64, 64, '41,40,39,38,37,36,35,34,33,1', '0', '0'),
(65, 65, '42,41,40,39,38,37,36,35,34,33,1', '0', '0'),
(66, 66, '46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '62', '63'),
(67, 67, '66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '61', '62'),
(68, 68, '67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '60', '60'),
(69, 69, '68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '59', '59'),
(70, 70, '69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '58', '58'),
(71, 71, '70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '57', '57'),
(72, 72, '71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '56', '56'),
(73, 73, '72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '55', '55'),
(74, 74, '73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '54', '54'),
(75, 75, '74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '53', '53'),
(76, 76, '75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '52', '52'),
(77, 77, '76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '51', '51'),
(78, 78, '77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '50', '50'),
(79, 79, '78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '49', '48'),
(80, 80, '79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '48', '47'),
(81, 81, '80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '47', '46'),
(82, 82, '81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '46', '45'),
(83, 83, '82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '45', '44'),
(84, 84, '83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '44', '43'),
(85, 85, '84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '43', '42'),
(86, 86, '85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '42', '41'),
(87, 87, '86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '41', '40'),
(88, 88, '87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '40', '39'),
(89, 89, '88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '39', '38'),
(90, 90, '78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '0', '0'),
(91, 91, '33,1', '0', '0'),
(92, 92, '67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '0', '0'),
(93, 93, '89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '38', '37'),
(94, 94, '93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '37', '36'),
(95, 95, '94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '36', '35'),
(96, 96, '95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '35', '34'),
(97, 97, '96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '34', '33'),
(98, 98, '97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '33', '32'),
(99, 99, '98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '32', '31'),
(100, 100, '99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '31', '30'),
(101, 101, '100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '30', '29'),
(102, 102, '101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '29', '28'),
(103, 103, '102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '28', '27'),
(104, 104, '103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '27', '26'),
(105, 105, '104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '26', '25'),
(106, 106, '105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '25', '24'),
(107, 107, '106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '24', '23'),
(108, 108, '107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '23', '22'),
(109, 109, '108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '22', '21'),
(110, 110, '109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '21', '20'),
(111, 111, '110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '20', '19'),
(112, 112, '111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '19', '18'),
(113, 113, '112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '18', '17'),
(114, 114, '113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '17', '16'),
(115, 115, '114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '16', '15'),
(116, 116, '115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '15', '14'),
(117, 117, '116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '14', '13'),
(118, 118, '117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '13', '12'),
(119, 119, '118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '12', '11'),
(120, 120, '119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '11', '10'),
(121, 121, '120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '10', '9'),
(122, 122, '121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '9', '8'),
(123, 123, '122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '8', '7'),
(124, 124, '123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '7', '6'),
(125, 125, '124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '6', '5'),
(126, 126, '125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '5', '4'),
(127, 127, '126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '4', '3'),
(128, 128, '127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '3', '2'),
(129, 129, '128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,1', '2', '1'),
(130, 130, '129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33,', '1', '0'),
(131, 131, '130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34', '0', '0'),
(132, 132, '130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34', '0', '0'),
(133, 133, '130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34', '0', '0'),
(134, 134, '130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34', '0', '0'),
(135, 135, '130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34', '0', '0'),
(136, 136, '1', '0', '0'),
(137, 137, '130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33', '0', '0'),
(138, 138, '130,129,128,127,126,125,124,123,122,121,120,119,118,117,116,115,114,113,112,111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94,93,89,88,87,86,85,84,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66,46,45,44,43,42,41,40,39,38,37,36,35,34,33', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_amount_history`
--

CREATE TABLE `transfer_amount_history` (
  `id` int(11) NOT NULL,
  `sender_code` varchar(50) NOT NULL DEFAULT '0',
  `reciver_code` varchar(50) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL DEFAULT '0',
  `debit` int(11) NOT NULL DEFAULT '0',
  `deduct_from` char(50) NOT NULL DEFAULT '0',
  `description` varchar(50) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `withdraw_payment_histry`
--

CREATE TABLE `withdraw_payment_histry` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `member_code` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_group_to_group`
--
ALTER TABLE `aauth_group_to_group`
  ADD PRIMARY KEY (`group_id`,`subgroup_id`);

--
-- Indexes for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perm_to_group`
--
ALTER TABLE `aauth_perm_to_group`
  ADD PRIMARY KEY (`perm_id`,`group_id`);

--
-- Indexes for table `aauth_perm_to_user`
--
ALTER TABLE `aauth_perm_to_user`
  ADD PRIMARY KEY (`perm_id`,`user_id`);

--
-- Indexes for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `full_index` (`id`,`sender_id`,`receiver_id`,`date_read`);

--
-- Indexes for table `aauth_users`
--
ALTER TABLE `aauth_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `aauth_user_to_group`
--
ALTER TABLE `aauth_user_to_group`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_index` (`user_id`);

--
-- Indexes for table `blocked_user_commissions`
--
ALTER TABLE `blocked_user_commissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ind_user_ids` (`eligible_user_id`,`rising_user_id`);

--
-- Indexes for table `fund_e_cash_histry`
--
ALTER TABLE `fund_e_cash_histry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`Package_ID`);

--
-- Indexes for table `rising_commissions`
--
ALTER TABLE `rising_commissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ind_user_ids` (`eligible_user_id`,`rising_user_id`);

--
-- Indexes for table `rising_missed_commissions`
--
ALTER TABLE `rising_missed_commissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ind_user_ids` (`eligible_user_id`,`rising_user_id`);

--
-- Indexes for table `rising_users`
--
ALTER TABLE `rising_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `rising_users_tree`
--
ALTER TABLE `rising_users_tree`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_amount_history`
--
ALTER TABLE `transfer_amount_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraw_payment_histry`
--
ALTER TABLE `withdraw_payment_histry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aauth_users`
--
ALTER TABLE `aauth_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blocked_user_commissions`
--
ALTER TABLE `blocked_user_commissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fund_e_cash_histry`
--
ALTER TABLE `fund_e_cash_histry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `Package_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `rising_commissions`
--
ALTER TABLE `rising_commissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `rising_missed_commissions`
--
ALTER TABLE `rising_missed_commissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rising_users`
--
ALTER TABLE `rising_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `rising_users_tree`
--
ALTER TABLE `rising_users_tree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `transfer_amount_history`
--
ALTER TABLE `transfer_amount_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `withdraw_payment_histry`
--
ALTER TABLE `withdraw_payment_histry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
