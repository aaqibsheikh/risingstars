<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('_fileupload')) {

    function _fileupload($path) {
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|xps';
        $config['max_size'] = '10240';
        $config['max_width'] = '3024';
        $config['max_height'] = '2768';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        return FALSE;
    }

}