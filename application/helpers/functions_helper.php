<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

if (!function_exists('_return_even_or_odd')) {

	function _return_even_or_odd($number) {
		return (($number % 2) == 0) ? 'Even' : 'Odd';
	}

}
if (!function_exists('_calculate_percentage')) {

	function _calculate_percentage($amount, $percent) {

		if ($percent != 1) {
			$actual_amount = $amount / 2;
			return (($percent / 100) * $actual_amount);
		} else {
			return (($percent / 100) * $amount);
		}
	}

}

if (!function_exists('_get_package_price')) {

	function _get_package_price($package_id) {
		$p_arr = array(1 => 1000, 2 => 3500, 3 => 7000, 4 => 10500, 5 => 14000);

		return $p_arr[$package_id];
	}

}
if (!function_exists('_get_package_by_price')) {

	function _get_package_by_price($price) {

		$p_arr = array(1000 => 1, 3500 => 2, 7000 => 3, 10500 => 4, 14000 => 5);

		if ($price > 14000) {
			return 5;
		} else {
			return $p_arr[$price];
		}

	}

}
if (!function_exists('_get_package_price_by_id')) {

	function _get_package_price_by_id($id) {

		$p_arr = array(1 => 1000, 2 => 3500, 3 => 7000, 4 => 10500, 5 => 14000);

		return $p_arr[$id];

	}

}

if (!function_exists('_send_brandsms_to_one')) {

	function _send_brandsms_to_one($data) {

		$mobile = $data['mobile'];

		$username = 'MeeNuu';
		$password = '80238';
		$to = $mobile;
		$from = 'risingstars';
		$message = $data['message'];
		$url = "http://Lifetimesms.com/plain?username=" . $username . "&password=" . $password . "&to=" . $to . "&from=" . urlencode($from) . "&message=" . urlencode($message) . "";
		//Curl Start
		$ch = curl_init();
		$timeout = 30;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$response = curl_exec($ch);
		curl_close($ch);
		// Write out the response
		return $response;
		return $data;
	}

	function clean($string) {
		$string = str_replace(" ", '-', $string); // Replaces all spaces with hyphens.
		return $string;
	}

	function cleanquote($string) {
		$string = str_replace("'", '', $string); // Replaces all spaces with hyphens.
		return $string;
	}

}
