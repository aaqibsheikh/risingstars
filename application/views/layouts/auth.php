<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>RISING STARS INTERNATIONAL</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/rising_style.css">
</head>
<body class="blank_rising_star">

    <!-- Simple splash screen-->
    <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>RISING STARS INTERNATIONAL</h1><p></p><img src="<?php echo base_url(); ?>assets/images/loading-bars.svg" width="64" height="64" /> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div class="color-line"></div>

    <div class="<?php echo $container_class; ?>-container">
        <div class="row">
            <!--edit-->

            <!--final-->
                <!--                <div class="col-md-12">
                                    <div class="text-center m-b-md">
                                        <h3 style="font-size:38px !important">PLEASE LOGIN TO APP</h3>
                                        <small style="margin-bottom:20px;"></small>
                                        <br><br>
                                    </div>
                                    <div class="col-md-5" style="border-right:2px solid #FFE20E">
                                        <div class="logo-risingstar">
                                            <span class="helper"></span><img src="<?php //echo base_url();       ?>assets/images/login/logo_rising.png" width="100%" height="64" /> 
                                        </div>
                                    </div>-->
                <!--                <div class="hpanel" >
                                    <div class="panel-body" style="background:none !important;border:none !important;">
                <?php //  echo $this->template->temp_yield(); ?>
                                    </div>
                                </div> 
                            </div>-->
                            <!--final-->

                            <!--edit-->

                            <div class="col-md-12">
                                <div class="text-center m-b-md">
                                    <h3 style="font-size:38px !important">PLEASE LOGIN TO APP</h3>
                                    <small>This is the best app ever!</small>
                    </div><!--
                -->                                    <div class="hpanel">

                    <?php if ($container_class == 'login') { ?>
                    <div class="panel-body" style="background:none !important;border:none !important;">
                        <?php echo $this->template->temp_yield(); ?>
                    </div>

                    <?php } ?>  
                    <?php if ($container_class == 'register') { ?>
                    <div class="panel-body">
                        <?php echo $this->template->temp_yield(); ?>
                    </div>

                    <?php } ?>  
                </div> 
            </div>

        </div>
        <br><br>
        <div class="row">
            <div class="col-md-12 text-center">
                <strong>RISING STARS INTERNATIONAL </strong><br/> Copyright - 2015 � risingstarsint.biz
            </div>
        </div>
    </div>


    <!-- Vendor scripts -->
    <script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/iCheck/icheck.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendor/sparkline/index.js"></script>

    <!-- App scripts -->
    <script src="<?php echo base_url(); ?>assets/scripts/homer.js"></script>
    <script src="<?php echo base_url(); ?>assets/scripts/validation/gen_validatorv4.js"></script>
</body>
</html>