<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Page title -->
	<title>RISING STARS INTERNATIONAL</title>

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

	<!-- Vendor styles -->
	<!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

	<!-- Vendor styles -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/fontawesome/css/font-awesome.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/metisMenu/dist/metisMenu.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/animate.css/animate.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/select2-develop/dist/css/select2.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css" />

	<!-- App styles -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/pe-icon-7-stroke/css/helper.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/rising_style.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">

	<script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.js"></script>

	<style>
		.selected { color: red; }
		.marginT30{margin-top: 30px !important;}
	    .margin-auto{
	        float: none;
	        margin: auto;
	    }
	    .table-responsive {
		    overflow-x: hidden !important;
		}
	</style>

	<?php if ($this->session->group_id == 1) {?>
		<style>
			#admin-visible{
				display:block !important;
			}
		</style>
		<?php
}
?>

	<script>
		var site_url = '<?php echo site_url(); ?>';
	</script>
</head>
<body>
	<div class="blank_rising_star">
		<div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>RISING STARS INTERNATIONAL</h1><p>please wait dashboard is loading </p><img src="<?php echo base_url(); ?>assets/images/loading-bars.svg" width="100" height="100" /> </div> </div>
		<!--[if lt IE 7]>
		<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		<!-- Header -->
		<div id="header">
			<div class="color-line">
			</div>
			<div id="logo" class="light-version">


				<a href="<?php echo site_url('dashboard/index') ?>"> <img src="<?php echo base_url(); ?>assets/images/login/logo_rising.png" width="100%" height="55" /> </a>

			</div>
			<nav role="navigation">
				<div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
				<div class="small-logo">
					<span class="text-primary">HOMER APP</span>
				</div>
				<!--<form role="search" class="navbar-form-custom" method="post" action="#">
					<div class="form-group">
						<input type="text" placeholder="Search something special" class="form-control" name="search">
					</div>
				</form>-->
				<div class="navbar-right">
					<ul class="nav navbar-nav no-borders">
						<li class="dropdown">
							<a href="<?php echo site_url('auth/logout') ?>">
								<i class="pe-7s-upload pe-rotate-90"></i>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>

		<!-- Navigation -->
		<aside id="menu">
			<div id="navigation">
				<div class="profile-picture">
					<a href="<?php echo site_url('dashboard/index') ?>">
						<img src="<?php echo $this->session->profile_pic_path; ?>" class="img-circle m-b" alt="logo" height="76" width="76">
					</a>


					<div class="stats-label text-color">
						<span class="font-extra-bold font-uppercase"><?php echo $this->session->name; ?></span>

						<div class="dropdown">
							<a class="dropdown-toggle" href="#" data-toggle="dropdown">
								<small class="text-muted">Profile Details <b class="caret"></b></small>
							</a>
							<ul class="dropdown-menu animated fadeInRight m-t-xs">
								<li><a href="<?php echo site_url('auth/profile') ?>">Profile</a></li>
								<!-- <?php if ($this->session->group_id == 1) {?>
								<li><a href="<?php echo site_url('auth/register_user') ?>">Register</a></li>
								<li ><a href="<?php echo site_url('auth/upgrade_package') ?>">Upgrade package</a></li>
								<li style="display:none;" id="admin-visible"><a href="<?php echo site_url('super_admin/index') ?>"> Admin panel </a></li>
								<?php }?> -->
								<li class="divider"></li>
								<li><a href="<?php echo site_url('auth/logout') ?>">Logout</a></li>
							</ul>
						</div>
						<div id="sparkline1" class="small-chart m-t-sm"></div>
						<div>
							<h4 class="font-extra-bold m-b-xs" style="font-size: 13px;">
								<?php echo $this->session->code; ?>
							</h4>

							<p class="text-muted"><span class="text-success">Current Month </span>: <?php echo $this->session->month; ?></p>
							<p class="text-muted"><span class="text-success">Remaining days </span>: <?php echo $this->session->days; ?></p>
							<!-- <p class="text-muted"><span class="text-success">Remaining days </span>: <?php //echo $this->session->pincode; ?></p> -->

						</div>
					</div>
				</div>
				<div id="activeclasshandle">
					<ul class="nav side-menu-risingstyle" id="side-menu">

						<li class="">
							<a href="<?php echo site_url('dashboard/index') ?>"><span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_introduce.png" height="30" width="30">Dashboard</span></a>
						</li>
						<!-- <li>
							<a href="<?php echo site_url('dashboard/introduce_by_user ') ?>"> <span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_introduce.png" height="30" width="30">Introduced By You</span></a>
						</li>
						<li>
							<a href="<?php echo site_url('dashboard/business_line') ?>"><span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_business_line.png" height="30" width="30"> Your business Line</span></a>
						</li>
						<li>
							<a href="<?php echo site_url('dashboard/commission_report') ?>" ><span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_commission_report.png" height="30" width="30">Commission Report</span></a>
						</li>
						<li>
							<a href="<?php echo site_url('dashboard/gold_star_rewards') ?>"><span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_gold_star_reward.png" height="30" width="30">Gold Star Rewards</span></a>

						</li>
						<li>
							<a href="<?php echo site_url('dashboard/account_statement') ?>"><span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_account_statment.png" height="30" width="30">Account Statement</span></a>
						</li>
						<li>
							<a href="<?php echo site_url('dashboard/transfer_e_commison') ?>"><span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_account_statment.png" height="30" width="30">Transfer E Cash / Commision</span></a>

						</li>
						<?php if ($this->session->group_id == 1) {?>
						<li>
							<a href="<?php echo site_url('auth/register_user') ?>"> <span class="nav-label"><img src="<?php echo base_url(); ?>assets/images/sidemenu/big_icon_register_distributor.png" height="30" width="30">Register Distributer</span></a>
						</li>
						<?php }?> -->


						<li>
			                <a href="#" aria-expanded="false">
			                	<span class="nav-label">
			                		<img src="<?php echo base_url(); ?>assets/images/sidemenu/shopping_mall.png" height="30" width="30"> Shopping Mall
			                	</span>
			                	<span class="fa arrow shopping_mall_arrow"></span>
			                </a>

			                <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
			                    <?php if ($this->session->group_id == 1) {?>
								<li>
									<a href="<?php echo site_url('product/add_product_form') ?>">
										<span class="nav-label">
											<img src="<?php echo base_url(); ?>assets/images/sidemenu/add_product.png" height="30" width="30">
											Add Product
										</span>
									</a>
								</li>
								<?php }?>

								<?php if ($this->session->group_id == 1) {?>
								<li>
									<a href="<?php echo site_url('product/main_banner') ?>">
										<span class="nav-label">
											<img src="<?php echo base_url(); ?>assets/images/sidemenu/add_product.png" height="30" width="30">
											Main Banner
										</span>
									</a>
								</li>
								<?php }?>


								<?php if ($this->session->group_id == 1) {?>
								<li>
									<a href="<?php echo site_url('category/add_category_form') ?>">
										<span class="nav-label">
											<img src="<?php echo base_url(); ?>assets/images/sidemenu/add_category.png" height="30" width="30">Add Category
										</span>
									</a>
								</li>
								<?php }?>

								<?php if ($this->session->group_id == 1) {?>
								<li>
									<a href="<?php echo site_url('category/add_sub_category') ?>">
										<span class="nav-label">
											<img src="<?php echo base_url(); ?>assets/images/sidemenu/add_category.png" height="30" width="30">Add Sub Category
										</span>
									</a>
								</li>
								<?php }?>


								<?php if ($this->session->group_id == 1) {?>
								<li>
									<a href="<?php echo site_url('brand/add_brand') ?>">
										<span class="nav-label">
											<img src="<?php echo base_url(); ?>assets/images/sidemenu/add_brand.png" height="30" width="30">Add Brand
										</span>
									</a>
								</li>
								<?php }?>

								<?php if ($this->session->group_id == 1) {?>
								<li>
									<a href="<?php echo site_url('order/order_list') ?>">
										<span class="nav-label">
											<img src="<?php echo base_url(); ?>assets/images/sidemenu/add_brand.png" height="30" width="30">Orders
										</span>
									</a>
								</li>
								<?php }?>
				            </ul>
				        </li>
					</ul>
				</div>
			</div>
		</aside>

		<!-- Main Wrapper -->
		<div id="wrapper">
			<?php echo $this->template->temp_yield(); ?>
		</div>
	</div>

	<!-- Vendor scripts -->
	<script src="<?php echo base_url(); ?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>

	<script src="<?php echo base_url(); ?>assets/vendor/select2-develop/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/iCheck/icheck.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/sparkline/index.js"></script>

	<!-- App scripts -->
	<script src="<?php echo base_url(); ?>assets/scripts/homer.js"></script>

	<script src="<?php echo base_url(); ?>assets/vendor/jquery-flot/jquery.flot.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/jquery-flot/jquery.flot.resize.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/jquery.flot.spline/index.js"></script>


	<script src="<?php echo base_url(); ?>assets/vendor/peity/jquery.peity.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/scripts/validation/gen_validatorv4.js"></script>




	<script>
		$(function() {
			var pgurl = window.location.href.substr(window.location.href
				.lastIndexOf("/") + 1);

			$("#activeclasshandle ul li a").each(function() {
				if ($(this).attr("href") == pgurl || $(this).attr("href") == '')
					$(this).addClass("active");
			})


		});

		var site_url = "<?php echo SITE_URL; ?>"
	</script>
	<?php
if ($this->router->fetch_method() == 'register_user' || $this->router->fetch_method() == 'profile' || $this->router->fetch_method() == 'upgrade_package') {
	echo js_asset('register.js');
}
if ($this->router->fetch_method() == 'transfer_e_commison') {
	echo js_asset('register.js');
}
?>

	<script type="text/javascript">
		$('#package').select2({
			tags: true,
			maximumSelectionLength: 2
		});
		$('.datepicker').datepicker({
			format: 'dd/mm/yyyy',
			"setDate": new Date(),
			autoclose:true,
			endDate: '+0d',
		});
		$("#package_entry_package").val('1');
		$("#package_entry_price").val('1000');
		$("#qtyre,#qtymeenu").keyup(function(){
			var qtyre = $("#qtyre").val();

			var qty = parseInt(qtyre);

			var qtymeenu = $("#qtymeenu").val();

			var qtym = parseInt(qtymeenu);

			if(isNaN(qtym)) {
				var qtym = 0;
			}
			if(isNaN(qty)) {
				var qty = 0;
			}

			repackage = qty*3500;
			Meenuupackage = qtym*7000;

			var total = repackage+Meenuupackage+1000;
			var package__arr = {1000 : 1, 4500 : 2, 8000 : 3, 11500:4,14000:5};

			if(total > 14000){
				var package_id =  5;
				$("#package_entry_price").val(total);
			}
			else if(total==0){
				$("#package_entry_price").val(total);
				var package_id =  1;
			}else{
				$("#package_entry_price").val(total);
				var package_id   = package__arr[total];
			}





			console.log('package_id_by_price'+package_id);

			console.log(qtym+"meeenupackage"+Meenuupackage+"--->"+qty+"repackage->"+repackage+"total->"+total);


			$("#package option").prop('selected', false).change();

			$("#package option[value='" + 1 + "']").prop('selected',true).change();

			$("#package option[value='" + (package_id) + "']").prop('selected',true).change();

			$("#package_entry_package").val(package_id);
			// $("#qtymeenu").val(qtyre);
		});

		var maxLength = 10;

		$("#mobile_num").on("keydown keyup change", function(){
			var value = $(this).val();
			if (value.length < maxLength)
				$(".msg").text("Number is invalid").css('color','red');
			else
				$(".msg").text("Number is valid").css('color','green');
		});
	</script>
	<script>

		$('#cnic,#kincnic').keydown(function(event){


		  if (event.keyCode == 8 || event.keyCode == 9
			|| event.keyCode == 27 || event.keyCode == 13
			|| (event.keyCode == 65 && event.ctrlKey === true) )
			return;
				if((event.keyCode < 48 || event.keyCode > 57))
				 event.preventDefault();

			 var length = $(this).val().length;

			 if(length == 5 || length == 13)
				 $(this).val($(this).val()+'-');

		 });
	</script>

</body>
</html>