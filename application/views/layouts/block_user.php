<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>RISING STARS INTERNATIONAL</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/fontawesome/css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/metisMenu/dist/metisMenu.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/animate.css/animate.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/css/bootstrap.css" />

        <!-- App styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/pe-icon-7-stroke/css/helper.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/styles/rising_style.css">


    </head>
    <body>



        <div class="blank_rising_star">



            <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>RISING STARS INTERNATIONAL</h1><p>please wait dashboard is loading </p><img src="<?php echo base_url(); ?>assets/images/loading-bars.svg" width="100" height="100" /> </div> </div>
            <!--[if lt IE 7]>
            <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <!-- Header -->
            <div id="header">
                <div class="color-line">
                </div>
                <div id="logo" class="light-version">


                    <a href="<?php echo site_url('dashboard/index') ?>"> <img src="<?php echo base_url(); ?>assets/images/login/logo_rising.png" width="100%" height="55" /> </a>

                </div>
                <nav role="navigation">
                    <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
                    <div class="small-logo">
                        <span class="text-primary">HOMER APP</span>
                    </div>
                    <!--                    <form role="search" class="navbar-form-custom" method="post" action="#">
                                            <div class="form-group">
                                                <input type="text" placeholder="Search something special" class="form-control" name="search">
                                            </div>
                                        </form>-->
                    <div class="navbar-right">
                        <ul class="nav navbar-nav no-borders">
                            <li class="dropdown">
                                <a href="<?php echo site_url('auth/logout') ?>">
                                    <i class="pe-7s-upload pe-rotate-90"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <!-- Navigation -->
            <aside id="menu">
                <div id="navigation">
                    <div class="profile-picture">
                        <a href="#">
                            <img src="<?php echo $_SESSION['profile_pic_path']; ?>" class="img-circle m-b" alt="logo" height="76" width="76">
                        </a>


                        <div class="stats-label text-color">
                            <span class="font-extra-bold font-uppercase"><?php echo $_SESSION['name']; ?></span>

                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                    <small class="text-muted">Profile Details <b class="caret"></b></small>
                                </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">                      
                                    <li><a href="<?php echo site_url('auth/logout') ?>">Logout</a></li>
                                </ul>
                            </div>


                            <div id="sparkline1" class="small-chart m-t-sm"></div>
                            <div>
                                <h4 class="font-extra-bold m-b-xs">
                                    <?php echo $_SESSION['code']; ?>
                                </h4>

                                <p class="text-muted"><span class="text-success">Current Month </span>: <?php echo $_SESSION['month']; ?></p>
                                <p class="text-muted"><span class="text-success">Remaining days </span>: <?php echo $_SESSION['days']; ?></p>
                                <p class="text-muted"><?php echo $_SESSION['registration']; ?></p>

                            </div>
                        </div>
                    </div>

                </div>
            </aside>

            <!-- Main Wrapper -->
            <div id="wrapper">


                <?php echo $this->template->temp_yield(); ?>


            </div>



        </div>

        <!-- Vendor scripts -->
        <script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/iCheck/icheck.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/sparkline/index.js"></script>

        <!-- App scripts -->
        <script src="<?php echo base_url(); ?>assets/scripts/homer.js"></script>

        <script src="<?php echo base_url(); ?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery-flot/jquery.flot.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery-flot/jquery.flot.resize.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery.flot.spline/index.js"></script>


        <script src="<?php echo base_url(); ?>assets/vendor/peity/jquery.peity.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/scripts/validation/gen_validatorv4.js"></script>




        <script>

//            $(function() {
//
//
//
//                // Initialize Example 2
//                $('#example2').dataTable();
//                $('#activeclasshandle li').click(function() {
//                    $(this).siblings('li').removeClass('active');
//                    $(this).addClass('active');
//                });
//
//            });
//
//
//

            $(function () {
                var pgurl = window.location.href.substr(window.location.href
                        .lastIndexOf("/") + 1);
                console.log(pgurl);
                $("#activeclasshandle ul li a").each(function () {
                    if ($(this).attr("href") == pgurl || $(this).attr("href") == '')
                        $(this).addClass("active");
                })


            });
//            $('#activeclasshandle li a').click(function(e) {
//                e.preventDefault(); //prevent the link from being followed
//                $('#activeclasshandle li a').removeClass('selected');
//                $(this).addClass('selected');
//            });
            var site_url = "<?php echo SITE_URL; ?>"
        </script>
        <?php
        if ($this->router->fetch_method() == 'register_user' || $this->router->fetch_method() == 'profile' || $this->router->fetch_method() == 'upgrade_package') {
            echo js_asset('register.js');
        }
        ?>


    </body>
</html>