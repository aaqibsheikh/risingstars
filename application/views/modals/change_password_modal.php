
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Change Password </h4>
            </div>

            <form method="post" name="changepasswordform" action="change_password" id="changepasswordform">
                <div class="modal-body me-style">



                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Old Password:</label>
                        <input type="password" class="form-control" id="old-pass" name="oldpassword">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">New Password:</label>
                        <input type="password" class="form-control" id="new-pass" name="newpassword">
                    </div>

                    <?php echo form_submit('submit', 'send me', 'class="btn btn-success"'); ?>



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>