
<div class="modal fade" id="exampleModal_reset" tabindex="-1" role="dialog" aria-labelledby="exampleModal_resetLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 375px !important">
        <div class="modal-content">
            <div class="modal-header" style="padding: 10px 29px !important;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModal_resetLabel">Reset Password </h4>
                <small>your password will be sent to the email given below.</small>
            </div>

            <form method="post" name="resetpasswordform" action="reset_password" id="resetpasswordform">
                <div class="modal-body me-style">



                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">please enter your email</label>
                        <input type="email" value="" placeholder="" class="form-control" id="useremail" name="useremail" required="">
                        
                    </div>


                    <?php echo form_submit('submit', 'reset password', 'class="btn btn-success"'); ?>



                </div>

                <div class="modal-footer" style=" padding: 10px;">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>