
<div class="modal fade" id="exampleModal_withdraw" tabindex="-1" role="dialog" aria-labelledby="exampleModal_resetLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width: 375px !important">
        <div class="modal-content">

            <div class="modal-header" style="padding: 10px 19px !important;" id="user_pic">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <img src="<?php echo base_url(); ?>assets/images/no-pic.png" id="current-user-pic" class="img-circle" alt="" height="50" width="50" style="  margin-left: 20px;">
            </div>


            <div class=" me-style">
                <div class="col-lg-12 animated-panel zoomIn" style="animation-delay: 1.3s;">

                    <div id="withdraw-model-loader" style="display: none;">
                    <div class="splash"> 
                        <div class="color-line"></div>
                        <div class="splash-title">
                            <h1>RISING STARS INTERNATIONAL</h1>
                            <img src="<?php echo base_url(); ?>assets/images/loading-bars.svg" width="64" height="64" />
                        </div>
                    </div>
                    </div>
                    <div class="alert alert-danger" id="amount-exceed-error" style="display:none;">
                        <small>Unable to process this amount.</small>
                    </div>
                    <div class="hpanel" style="margin-bottom: 10px !important;">            
                        <div class="panel-body list" style=" padding-top: 8px !important;">

                            <div class="list-item-container">
                                <div class="list-item" id="commission-total-paid">
                                    <h4 class="no-margins font-extra-bold text-color3">0</h4>
                                    <small>total paid</small>
                                    <div class="pull-right font-bold">paid <i class="fa fa-bolt text-success"></i></div>
                                </div>
                                <div class="list-item" id="commission-eighty">
                                    <h4 class="no-margins font-extra-bold text-success">0</h4>
                                    <small>eighty percent</small>
                                    <div class="pull-right font-bold">80% <i class="fa fa-bolt text-color3"></i></div>
                                </div>
                                <div class="list-item" id="commission-twenty">
                                    <h4 class="no-margins font-extra-bold text-color3">0</h4>
                                    <small>twenty percent</small>
                                    <div class="pull-right font-bold">20% <i class="fa fa-bolt text-success"></i></div>
                                </div>
                                <div class="list-item" id="commission-one">
                                    <h4 class="no-margins font-extra-bold text-color3 text-success">0</h4>
                                    <small>one percent</small>
                                    <div class="pull-right font-bold">1% <i class="fa fa-bolt text-color3"></i></div>
                                </div>
                                <div class="list-item" id="current-bonus">
                                    <h4 class="no-margins font-extra-bold text-success">0</h4>
                                    <small>current Bonus</small>
                                    <div class="pull-right font-bold">bonus <i class="fa fa-bolt text-success"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom: 10px !important;" >
                        <?php // echo form_open('Super_admin/withdraw_process_list', array('id' => 'editreport')); ?>
                        <div class="input-group">
                            <input type="hidden" id="withdraw-id" value="0">
                            <input type="text" class="form-control" id="withdraw-amount" placeholder="" name="bonus">
                            <span class="input-group-btn">
                                <input name="withdraw_btn" value="Withdraw" class="btn btn-success" type="button" onclick="withdraw_bonus(this);">
                                <?php //echo form_submit('submit', 'process', 'class="btn btn-success"'); ?>

                            </span>
                        </div>
                        <?php //form_close();?>
                    </div>    

                </div>

            </div>
            <div class="modal-footer" style=" padding: 10px;">

                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
            </div>


        </div>
    </div>
</div>