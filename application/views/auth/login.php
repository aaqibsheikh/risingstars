<?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>
<?php if (isset($loginerror)) { ?> <div class="alert alert-danger"> <?php echo $loginerror; ?></div> <?php }; ?>




<div style="background:none !important">
    <div class="col-md-5" style="border-right:2px solid #FFE20E">
        <div class="logo-risingstar">
            <img src="<?php echo base_url(); ?>assets/images/login/logo_rising.png" class="img-responsive" height=""/> 
        </div>
    </div>

    <div class="col-md-7">
        <?php echo form_open('auth/login', array('id' => 'loginForm')); ?>
        <div class="form-group">
            <label class="control-label" for="username">User Code (140600002)</label>
            <input type="text" placeholder="Member ID" title="Please enter you username" required="" value="<?php set_value('memeberid') ?>" name="memeberid" id="username" class="form-control">
            <span class="help-block small">Your unique Member ID to app</span>
        </div>
        <div class="form-group">
            <label class="control-label" for="password">Password</label>
            <input type="password" title="Please enter your password" placeholder="******"  value="<?php set_value('password') ?>" name="password" id="password" class="form-control">
            <span class="help-block small" data-toggle="modal" data-target="#exampleModal_reset" data-whatever="password" style="cursor:pointer">Reset your password</span>
        </div>
        <div class="checkbox">
            <input type="checkbox" class="i-checks" checked>
            Remember login
            <p class="help-block small">(if this is a private computer)</p>
        </div>
        <!--<button class="btn btn-success btn-block">Login</button>-->

        <!--                                <a class="btn btn-default btn-block" href="#">Register</a>--> 

        <?php echo form_submit('submit', 'Login', 'class="btn btn-success btn-block"'); ?>






        <!--                        <button class="btn btn-success btn-block">Login</button>-->


        <?php echo form_close(); ?>
        <?php $this->load->view('modals/reset_password_modal'); ?>    
    </div>

</div>