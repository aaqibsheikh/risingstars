<div class="content animate-panel">


    <br>
    <?php if (isset($success_message)) { ?><div class="alert_messages alert alert-success"><?php echo $success_message ?></div> <?php }; ?>

    <?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>

    <?php echo form_open('auth/upgrade_package', array('id' => 'updatepackage')); ?>

    <?php $this->load->view('auth/authenticate_view'); ?>

    <hr style="border-top-color: #62cb31;">
    <br>

    <h1 class="text-center">
        Upgrade Package Details
       
    </h1>
    <div class="row">
        <div class="form-group col-lg-4">
            <label>select your new package</label>

            <select class="form-control" name="package" id="available-package-list" required>
                <option value="">select package</option>
                <?php // foreach ($data->result() as $row) { ?>    
                    <!--//<option value="<?php // echo $row->Package_ID; ?>"><?php // echo $row->PackageName . ' ' . $row->PackagePrice; ?></option>-->
                <?php //} ?>

            </select>
        </div>
    </div>
    <div class="text-center">
        <!--<button type="submit" class="btn btn-success">Register</button>-->
        <?php echo form_submit('submit', 'Register', 'class="btn btn-success  register-submit hide"'); ?>
        <a class="btn btn-default" href="dashboard/index" >Cancel</a>
    </div>

    <?php form_close() ?>
</div>