<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$parts = explode("/", $actual_link);
$last_url = end($parts);

$introducer = 'introducer';
$page1 = 'upgrade_package';
$page2 ='fund_e_cash';
if ($last_url == $page1 || $last_url==$page2) {
    $introducer = ' ';
}
?>

<h1 class="text-left">
    Give <?php echo $introducer ?> Details
</h1>
<div class="row code-verification-error alert alert-danger" style="display: none;">
    <!-- in this div  verifcation error will display -->
</div>


<div class="row">
    <div class="col-lg-6">
        <div class="form-group mqrgin-bottom-20">
            <label><?php echo $introducer ?> code</label>
            <input type="text" value="<?php echo set_value($_SESSION['code']) ?>" id="introducer" required="required" class="form-control" name="introducer" >
            <input type="hidden" value="" id="package_id"  name="package_id"  >
            <input type="hidden" value="" id="user_id"  name="user_id"  >
            <input type="hidden" value="" id="full_name"  name="full_name"  >
        </div>
        <div class="form-group mqrgin-bottom-20">

            <label><?php echo $introducer ?> PIN</label>

            <input type="password"  value="<?php echo set_value('intrpin') ?>"  id="introducer_pin" required="required" class="form-control" name="introducer_pin">
        </div>

        <div class="form-group mqrgin-bottom-20">  
            <label>verify introducer details</label>
            <input style="display:block" class="btn btn-success" id="verify"  value="verify" onclick="find_introducer_by_code('upgrade_package')" <?php //echo site_url('Auth/verify_and_get_introducer_details')?>/>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="" id="verified-distributor-detail">
            <div class="hpanel hblue contact-panel">
                <div class="panel-body">
                    <img alt="user_profile_pic" id="introducer_profile_picture" class="img-circle m-b" src="<?php echo ASSETS_URL ?>images/no-pic.png" height="76" width="76"> 
                    <h3 ><a href="" id="introducer_name">Max Simson</a></h3>
                    <div class="text-muted font-bold m-b-xs" id="introducer_address">California, LA</div>
                    <p id="verified_introducer_text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>   