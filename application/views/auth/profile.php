<div class="content animate-panel">



    <h1 class="text-center">
        Your Profile
    </h1>
    <?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>

    <?php echo form_open('auth/update_user', array('id' => 'registerForm')); ?>

    <div class="row">
        <div class="form-group col-lg-12">
            <label>Full Name</label>
            <input type="text" value="<?php
            echo $profile_details['full_name'];
            set_value('username')
            ?>" id="" required="required" class="form-control" name="username">
        </div>
        <div class="form-group col-lg-4">
            <label>Father name</label>
            <input type="" value="<?php
            echo $profile_details['father_name'];
            set_value('fname')
            ?>" id="" required="required" class="form-control" name="fname">
        </div>
        <div class="form-group col-lg-4">
            <label>CNIC #</label>
            <input type="" value="<?php
            echo $profile_details['cnic'];
            set_value('cnic')
            ?>" id="" placeholder="13 digit CNIC number" required="required" class="form-control" name="cnic">
        </div>
        <div class="form-group col-lg-4">
            <label>Full Address </label>
            <input type="" value="<?php
            echo $profile_details['address'];
            set_value('address')
            ?>" id="" required="required" class="form-control" name="address">
        </div>
        <div class="form-group col-lg-4">
            <label>kin Name</label>
            <input type="" value="<?php
            echo $profile_details['kin_name'];
            set_value('kinname')
            ?>" id="" required="required" class="form-control" name="kinname">
        </div>
        <div class="form-group col-lg-4">
            <label>kin relation</label>
            <input type="" value="<?php
            echo $profile_details['kin_relation'];
            set_value('kinrelation')
            ?>" id="" required="required" class="form-control" name="kinrelation">
        </div>
        <div class="form-group col-lg-4">
            <label>kin CNIC #</label>
            <input type="" value="<?php
            echo $profile_details['kin_cnic'];
            set_value('kincnic')
            ?>" id="" placeholder="13 digit CNIC number" required="required" class="form-control" name="kincnic">
        </div> 


        <div class="form-group col-lg-4">
            <label>Mobile #</label>
            <input type="" value="<?php
            echo $profile_details['mobile_no'];
            set_value('mobile')
            ?>" id="" placeholder="10 digit Mobile number" required="required" class="form-control" name="mobile">
        </div>


        <div class="form-group col-lg-4">
            <label>Email Address</label>
            <input type="email"  value="<?php
            echo $profile_details['email'];
            set_value('email')
            ?>"  id="" required="required" disabled class="form-control" name="email">
        </div>


        <div class="form-group col-lg-4">

            <div class="col-lg-8" style=" padding-left:0px;">
                <label>Change profile picture</label>
                <input type="text" value="<?php
                echo $profile_details['user_pic_path'];
                set_value('user_pic')
                ?>" id="user_profile_picture"  class="form-control" name="user_pic">
            </div>
            <div class="col-lg-4 text-center" style="padding-right:0px">
                <img alt="profile_pic" class="img-circle m-b" id="profile_picture_holder" src="<?php echo ASSETS_URL ?>/images/no-pic.png" height="76" width="76"> 
            </div>


        </div>

        <!--        <div class="form-group col-lg-4">
        
                                <div class="col-lg-8" style=" padding-left:0px;">
                                    <label>Reset Password</label>
                                    <input type="button" value="Reset Password" id="password_reset"  class="form-control btn-info" name="resetpassword"> 
                                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Reset Password</button>
                                </div>
        
        
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#exampleModal" data-whatever="password">Reset Password</button>
        
        
        
        
                </div>-->

    </div>
    <br>
    <br>



    <!--    <div class="row">
            <div class="col-lg-8 text-center" id="verified-distributor-detail">
    
            </div>
        </div>-->

    <div class="text-center">
        <!--<button type="submit" class="btn btn-success">Register</button>-->
        <?php echo form_submit('submit', 'Update', 'class="btn btn-success"'); ?>

        <a class="btn btn-default" href="<?php echo site_url('dashboard/index') ?>" >Cancel</a>
    </div>

    <?php echo form_close() ?>

    <br>
    <br>
    <div class="form-group col-lg-3">

        <button type="button" class="btn btn-success btn-lg  col-lg-10" data-toggle="modal" data-target="#exampleModal" data-whatever="password">Change Password</button>

    </div>

    <div class="form-group col-lg-3" style="display: none;">

        <button type="button" class="btn btn-info btn-lg  col-lg-10" data-toggle="modal" data-target="#exampleModal_reset" data-whatever="password">Reset Password</button>

    </div>
    <div class="form-group col-lg-3">

        <button type="button" class="btn btn-success btn-lg  col-lg-10" data-toggle="modal" data-target="#exampleModal_reset_pin" data-whatever="password">Reset PIN</button>

    </div>


</div>
<?php
$this->load->view('modals/change_password_modal');

$this->load->view('modals/reset_userPin_modal');
?>

<script>

//    $(document).ready(function() {
//
//        //Enter keystroke closes all editboxes and saves changes 
//        $("#user_profile_picture").trigger("keyup");
//
//
//    });

</script>