<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <li>Dashboard</li>
                    <!--                    <li>
                                            <span>introduce by you</span>
                                        </li>-->
                                        <li class="active">
                                            <span>Register Distributor</span>
                                        </li>
                                    </ol>
                                </div>
                                <h2 class="font-light m-b-xs">
                                    Introduce By You 
                                </h2>
                                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
                            </div>
                        </div>
                    </div>

                    <div class="content animate-panel">



                        <h1 class="text-center">
                            User details
                        </h1>
                        <?php if (isset($success_message)) { ?><div class="alert_messages alert alert-success"><?php echo $success_message ?></div> <?php }; ?>

                        <?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>

                        <?php echo form_open('auth/register_user', array('id' => 'registerForm')); ?>

                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>Full Name</label>
                                <input type="text" value="john doe" id="username" required="required" class="form-control" name="username">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Father name</label>
                                <input type="" value="mr.doe" id="fname" required="required" class="form-control" name="fname">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>CNIC #</label>
                                <input type="" value="" id="cnic" placeholder="xxxxx-xxxxxxx-x" required="required" class="form-control" name="cnic" maxlength="15">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Full Address </label>
                                <input type="" value="cantt" id="address" required="required" class="form-control" name="address">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>kin Name</label>
                                <input type="" value="jill" id="kinname" required="required" class="form-control" name="kinname">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>kin relation</label>
                                <input type="" value="bro" id="kinrelation" required="required" class="form-control" name="kinrelation">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>kin CNIC #</label>
                                <input type="" value="xxxxx-xxxxxxx-x" id="kincnic" placeholder="13 digit CNIC number" required="required" class="form-control" name="kincnic" maxlength="15">
                            </div> 

                            <div class="form-group col-lg-4">
                                <label>Mobile # &nbsp;&nbsp;&nbsp;&nbsp;<span class="msg"></span></label>
                                <input type="" value="" id="mobile_num" placeholder="xxxxxxxxxxx" required="required" class="form-control" name="mobile"  maxlength="11">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Email Address</label>
                                <input type="email"  value="<?php echo set_value('email') ?>"  id="email" required="required" class="form-control" name="email">
                            </div>

                            <div class="form-group col-lg-3">
                                <label>Product 1</label>

                                <select class="form-control" name="p_re" id="p_re">
                                    <!-- <option value="">Select product</option> -->
                                    <option value="3500">R.E 3500</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-1">
                                <label>Qty</label>
                                <input type="number" name="qtyre" id="qtyre" min="0" class="form-control" />
                            </div>
                            <div class="form-group col-lg-3">
                                <label>Product 2</label>
                                <select class="form-control" name="p_meenuu" id="p_meenuu">
                                    <!-- <option value="">Select product</option> -->
                                    <option value="7000">Meenuu 7000</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-1">
                                <label>Qty</label>
                                <input type="number" name="qtymeenu" id="qtymeenu" min="0" class="form-control" />
                            </div>
                            <div class="form-group col-lg-4">
                                <div class="">
                                    <label>joining package</label>
                                    <style type="text/css">
                                    .select2-selection{
                                        width:87%;
                                    }
                                </style>
                                <select class="form-control " name="package[]" required id="package" multiple disabled="">
                                    <?php foreach ($data->result() as $row) { ?>    
                                    <option value="<?php echo $row->Package_ID; ?>" <?php echo ($row->Package_ID == 1) ? 'selected' : '' ?>> 
                                        <?php echo $row->PackageName . ' ' . $row->PackagePrice; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" value="" name="package_entry_package" id="package_entry_package">
                            <input type="hidden" value="" name="package_entry_price" id="package_entry_price">

                            <div class="form-group col-lg-4">
                                <div class="col-lg-8" style=" padding-left:0px;">
                                    <label>select profile picture</label>
                                    <input type="text" value="" id="user_profile_picture"  class="form-control" name="user_pic">
                                </div>
                                <div class="col-lg-4 text-center" style="padding-right:0px">
                                    <img alt="profile_pic" class="img-circle m-b" id="profile_picture_holder" src="<?php echo ASSETS_URL ?>/images/no-pic.png" height="76" width="76"> 
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Previous Date</label>
                                <input type="text" name="previous" readonly="readonly" class="form-control datepicker"/>
                            </div>
                        </div>
                        <br>
                        <br>
                        <hr style="border-top-color: #62cb31;">
    <!--    <h1 class="text-left">
            introducer Details
        </h1>
        <div class="row code-verification-error alert alert-danger" style="display: none;">
             in this div  verifcation error will display 
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group mqrgin-bottom-20">
                    <label>introducer code</label>
                    <input type="text" value="<?php //echo set_value($_SESSION['code'])  ?>" id="introducer" required="required" class="form-control" name="introducer" >
                </div>
                <div class="form-group mqrgin-bottom-20">
                    <label>introducer PIN</label>
                    <input type="password"  value="<?php //echo set_value('intrpin')  ?>"  id="introducer_pin" required="required" class="form-control" name="introducer_pin">
                </div>
    
                <div class="form-group mqrgin-bottom-20">  
                    <label>verify introducer details</label>
                    <input style="display:block" class="btn btn-success" id="verify"  value="verify" onclick="find_introducer_by_code()" <?php //echo site_url('Auth/verify_and_get_introducer_details')                   ?>/>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="" id="verified-distributor-detail">
                    <div class="hpanel hblue contact-panel">
                        <div class="panel-body">
                            <img alt="user_profile_pic" id="introducer_profile_picture" class="img-circle m-b" src="<?php // echo ASSETS_URL  ?>images/no-pic.png" height="76" width="76"> 
                            <h3 ><a href="" id="introducer_name">Max Simson</a></h3>
                            <div class="text-muted font-bold m-b-xs" id="introducer_address">California, LA</div>
                            <p id="verified_introducer_text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    
    
    
        </div>    --> 


        <?php $this->load->view('auth/authenticate_view'); ?>
        <br>
        <br>

    <!--    <div class="row">
            <div class="col-lg-8 text-center" id="verified-distributor-detail">
    
            </div>
        </div>-->

        <div class="text-center">
            <!--<button type="submit" class="btn btn-success">Register</button>-->
            <?php echo form_submit('submit', 'Register', 'class="btn btn-success  register-submit hide"'); ?>
            <a class="btn btn-default" href="<?php echo base_url() ?>dashboard/index" >Cancel</a>
        </div>

        <?php form_close() ?>
    </div>
    
    <script src="<?php echo base_url(); ?>assets/scripts/validation/gen_validatorv4.js"></script>
<!--<script type="text/javascript">
                    var frmvalidator = new Validator("registerForm");
                    frmvalidator.addValidation("package", "dontselect=-1", "required");

                </script>-->
