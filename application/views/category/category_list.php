
<div id="page-wrapper">

     <div class="row">
        <div class="col-lg-11 margin-auto marginT30">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="row">
                     <div class="col-lg-6">
                        <div class="panel-heading">
                            Category List
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <a href="<?php echo site_url('category/add_category_form'); ?>" class="btn btn-product-bg marginTR10 pull-right">Add Category</a>
                     </div>
                 </div>

                <p class="text-success"> <?php if (isset($success_message)) {
	echo $success_message;
}?>
                 </p>
                 <?php if ($this->session->flashdata('flsh_msg')): ?>
                 <div class="alert alert-success">
    <?php echo $this->session->flashdata('flsh_msg'); ?>
</div>
<?php endif;?>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Serial No</th>
                                    <th>Category Name</th>
                                    <th>Category Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$i = 0;
if (isset($all_cats)) {
	foreach ($all_cats as $value) {
		$i++;

		?>
                                <tr class="gradeC">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $value->category_name; ?></td>
                                    <?php if (!empty($value->category_image)): ?>
                                    <td>
                                        <img src="<?php echo base_url() . 'assets/upload/' . $value->category_image; ?>" width="80px" height="80px"/>
                                    </td>
                                    <?php else: ?>
                                        <td>Not Available</td>
                                    <?php endif;?>
                                   <td>
                                        <a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>category/edit_category/<?php echo $value->category_id; ?>">Edit</a>
                                        <a class="btn btn-danger btn-sm" href="<?php echo base_url(); ?>category/delete_category/<?php echo $value->category_id; ?>">Delete</a>
                                    </td>

                                </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
</div>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>