
    <!--  page-wrapper -->
    <div id="page-wrapper">

    <div class="row">
        <div class="col-lg-6 margin-auto marginT30">
            <!-- Form Elements -->
            <div class="panel panel-default">
                <div class="row">
                   <div class="col-lg-6">
                        <div class="panel-heading">
                            Add New Sub Category
                        </div>
                   </div>
                   <div class="col-lg-6">
                       <a href="<?php echo site_url('category/sub_category_list'); ?>" class="btn btn-product-bg marginTR10 pull-right">Sub Category List</a>
                   </div>
               </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                           <h5 style='color:red'> <?php echo validation_errors(); ?></h5>
                                <?php echo form_open_multipart('category/save_sub_category', ''); ?>
                                 <?php $cat = $this->CategoryModel->get_all_category();?>
                                 <select class="form-control" name="category_sub_id" required>
                                        <option value="#">Select Category</option>

                                         <?php
foreach ($cat as $category) {?>
                                        <option value="<?php echo $category->category_id; ?>"><?php echo $category->category_name ?></option>
                                        <?php }?>
                                </select>
                                <br>
                                <div class="form-group">
                                    <label>Add Sub Category</label>
                                    <input type="text" class="form-control" value="" name="sub_category_name" required="">
                                </div>

                                <div id="selectedFiles"></div>
                                <div class="marginT30"></div>

                                <button type="submit" class="btn btn-primary">Save</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Form Elements -->
        </div>
    </div>
</div>
<!-- end page-wrapper -->
