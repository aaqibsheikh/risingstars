    <!--  page-wrapper -->
    <div id="page-wrapper">

    <div class="row">
        <div class="col-lg-6 margin-auto marginT30">
            <!-- Form Elements -->
            <div class="panel panel-default">

                <div class="row">
                     <div class="col-lg-6">
                        <div class="panel-heading">
                            Update Sub Category
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <a href="<?php echo site_url('category/sub_category_list'); ?>" class="btn btn-product-bg marginTR10 pull-right">Add Sub Category</a>
                     </div>
                 </div>
                 <p class="text-success"> <?php if (isset($success_message)) {
	echo $success_message;
}?>
                 </p>
                 <?php if ($this->session->flashdata('flsh_msg')): ?>
                 <div class="alert alert-success">
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
        <?php endif;?>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                           <h5 style='color:red'> <?php echo validation_errors(); ?></h5>
                                <?php echo form_open('category/update_sub_category' . '/' . $sub_cat_by_id->sub_cat_id, ''); ?>
                                <div class="form-group">
                                    <label>Update Sub Category</label>
                                    <input type="text" class="form-control" value="<?php echo $sub_cat_by_id->sub_category_name; ?>" name="sub_category_name" required="">
                                </div>

                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Form Elements -->
        </div>
    </div>
</div>
<!-- end page-wrapper -->