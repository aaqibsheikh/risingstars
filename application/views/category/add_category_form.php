<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!--  page-wrapper -->
    <div id="page-wrapper">

    <div class="row">
        <div class="col-lg-6 margin-auto marginT30">
            <!-- Form Elements -->
            <div class="panel panel-default">
               <div class="row">
                   <div class="col-lg-6">
                        <div class="panel-heading">
                            Add New Category
                        </div>
                   </div>
                   <div class="col-lg-6">
                       <a href="<?php echo site_url('category/category_list'); ?>" class="btn btn-product-bg marginTR10 pull-right">Category List</a>
                   </div>
               </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                           <h5 style='color:red'> <?php echo validation_errors(); ?></h5>
                                <?php echo form_open_multipart('category/save_category', ''); ?>
                                <div class="form-group">
                                    <label>Add Category</label>
                                    <input type="text" class="form-control" value="<?php echo set_value('category_name') ?>" name="category_name" required="">
                                </div>

                                <div class="form-group">
                                    <label>Upload Category Image</label>
                                    <input type="file" id="category_image" name="category_image" required>
                                </div>

                                <button type="submit" class="btn btn-primary">Save</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Form Elements -->
        </div>
    </div>
</div>
<!-- end page-wrapper -->


<style>

    .img-width{
        width: 100px !important;
        height: 100px !important;
        margin-left: 0px !important;
        margin-right: 20px !important;
    }

    .block{display: inline-block;}

    .marginR20{margin-right: 20px !important;}
    .marginT30{margin-top: 30px !important;}

    .cross{
      position: relative;
      top: -43px;
      right: 38px;
      color: #000;
      background: #fff;
      font-weight: bold;
      border-radius: 50px;
      padding: 2px 7px 2px 7px;
      text-align: center;
      font-size: 11px;
      cursor: pointer;
    }
</style>

<script>

    var selDiv = "";
    var storedFiles = [];
    $(document).ready(function() {

        $("#category_image").on("change", handleFileSelect);
        selDiv = $("#selectedFiles");
        $("body").on("click", ".selFile", removeFile);

    });

    function handleFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function(f) {

            if(!f.type.match("image.*")) {
                return;
            }
            storedFiles.push(f);

            var reader = new FileReader();
            reader.onload = function (e) {
                var html = "<div class='block'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='img-rounded img-width' title='Click to remove'>\
                <input type='hidden' name='category_image' value='"+f.name+"' />\
                <span class='selFile cross'>X</span></div>";
                selDiv.append(html);

            }
            reader.readAsDataURL(f);
        });
    }

    function removeFile(e) {
        var file = $(this).siblings().data("file");
        for(var i=0;i<storedFiles.length;i++) {
            if(storedFiles[i].name === file) {
                storedFiles.splice(i,1);
                break;
            }
        }
        $(this).parent().remove();
    }
</script>