    <!--  page-wrapper -->
    <div id="page-wrapper">

    <div class="row">
        <div class="col-lg-6 margin-auto marginT30">
            <!-- Form Elements -->
            <div class="panel panel-default">

                <div class="panel-heading">
                    Update Category
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                           <h5 style='color:red'> <?php echo validation_errors(); ?></h5>
                                <?php echo form_open_multipart('category/update_category' . '/' . $cat_by_id->category_id, ''); ?>
                                <div class="form-group">
                                    <label>Update Category</label>
                                    <input type="text" class="form-control" value="<?php echo $cat_by_id->category_name; ?>" name="category_name" required="">
                                </div>

                                <div class="form-group">
                                    <label>Upload Category Image</label>
                                    <input type="file" name="category_image" id="category_image">
                                </div>

                                <div id="selectedFeaturedFiles">
                                    <?php if (!empty($cat_by_id->category_image)): ?>
                                    <div class='block'>
                                        <img src="<?php echo base_url() . 'assets/upload/' . $cat_by_id->category_image; ?>" data-file='<?php echo $cat_by_id->category_image; ?>' class='img-rounded img-width' title='Click to remove'>
                                        <input type='hidden' name='category_image' value='<?php echo $cat_by_id->category_image; ?>' />
                                        <span class='selFeaturedFile cross'>X</span>
                                    </div>
                                    <?php endif;?>

                                </div>
                                <div style="margin-top:20px;"></div>
                                <button type="submit" class="btn btn-primary">Update</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Form Elements -->
        </div>
    </div>
</div>
<!-- end page-wrapper -->
<style>

    .img-width{
        width: 100px !important;
        height: 100px !important;
        margin-left: 0px !important;
        margin-right: 20px !important;
    }

    .block{display: inline-block;}

    .marginR20{margin-right: 20px !important;}
    .marginT30{margin-top: 30px !important;}

    .cross{
      position: relative;
      top: -43px;
      right: 38px;
      color: #000;
      background: #fff;
      font-weight: bold;
      border-radius: 50px;
      padding: 2px 7px 2px 7px;
      text-align: center;
      font-size: 11px;
      cursor: pointer;
    }
</style>

<script>


    var selFeaturedDiv = "";
    var storedFeaturedFiles = [];

    $(document).ready(function() {

        // Featured Image
        $("#category_image").on("change", handleFeaturedFileSelect);
        selFeaturedDiv = $("#selectedFeaturedFiles");
        $("body").on("click", ".selFeaturedFile", removeFeaturedFile);

    });

    function handleFeaturedFileSelect(e) {
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function(f) {

            if(!f.type.match("image.*")) {
                return;
            }
            storedFeaturedFiles.push(f);

            var reader = new FileReader();
            reader.onload = function (e) {
                var html = "<div class='block'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='img-rounded img-width' title='Click to remove'>\
                <input type='hidden' name='category_image' value='"+f.name+"' />\
                <span class='selFeaturedFile cross'>X</span></div>";
                selFeaturedDiv.append(html);

            }
            reader.readAsDataURL(f);
        });
    }

    function removeFeaturedFile(e) {
        var file = $(this).siblings().data("file");
        for(var i=0;i<storedFeaturedFiles.length;i++) {
            if(storedFeaturedFiles[i].name === file) {
                storedFeaturedFiles.splice(i,1);
                break;
            }
        }
        $(this).parent().remove();
    }
</script>

