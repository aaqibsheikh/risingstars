
<div id="page-wrapper">

     <div class="row">
        <div class="col-lg-10 margin-auto marginT30">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="row">
                     <div class="col-lg-6">
                        <div class="panel-heading">
                           Sub Category List
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <a href="<?php echo site_url('category/add_sub_category'); ?>" class="btn btn-product-bg marginTR10 pull-right">Add Sub Category</a>
                     </div>
                 </div>

                <p class="text-success"> <?php if (isset($success_message)) {
	echo $success_message;
}?>
                 </p>
              <?php if ($this->session->flashdata('flsh_msg')): ?>
                 <div class="alert alert-success">
                <?php echo $this->session->flashdata('flsh_msg'); ?>
            </div>
        <?php endif;?>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Serial No</th>
                                    <th>Sub Category Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$i = 0;
if (isset($all_sub_cats)) {
	foreach ($all_sub_cats as $value) {
		$i++;

		?>
                                <tr class="gradeC">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $value->sub_category_name; ?></td>

                                   <td>
                                        <a class="btn btn-info btn-sm" href="<?php echo base_url(); ?>category/edit_sub_category/<?php echo $value->sub_cat_id; ?>">Edit</a>
                                        <a class="btn btn-danger btn-sm" href="<?php echo base_url(); ?>category/delete_sub_category/<?php echo $value->sub_cat_id; ?>">Delete</a>
                                    </td>

                                </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
</div>

    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>