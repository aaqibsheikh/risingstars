<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">

                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?php echo site_url('super_admin/index') ?>">Dashboard</a></li>

                    <!--                    <li>
                                            <span>introduce by you</span>
                                        </li>-->
                    <li class="active">
                        <span>Fund-E-Cash</span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Fund-E-Cash
            </h2>
            <small>Fund-E-Cash process</small>
        </div>
    </div>
</div>
<div class="content animate-panel">

    <br>
    <?php if (isset($success_message)) { ?><div class="alert_messages alert alert-success"><?php echo $success_message ?></div> <?php }; ?>

    <?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>



    <?php $this->load->view('auth/authenticate_view'); ?>

    <hr style="border-top-color: #62cb31;">


    <h2 class="text-center">Fund-E-Cash</h2>
    <div class="row">
        <div class="col-lg-4" id="fund_e_cash">
            <?php echo form_open('super_admin/fund_e_cash', array('id' => 'fund_e_cash')); ?>
             <div class="form-group">
                <label>Member Code</label>    
                <input type="text" value="" name="verified_member" id="verified_member" class="form-control" required="required"/>
            </div>
       
            <div class="form-group">
                <label>Add amount</label>
                
                <input type="text" value="" id="" placeholder="add amount here" required="required" class="form-control" name="amount">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea rows="4"  style="resize:none;width:100%" name="descrip" required="required">
                    
                </textarea>
            </div> 
            <?php echo form_submit('submit', 'process', 'class="btn btn-success hide process-btn"'); ?>
            <?php echo form_close(); ?>
        </div>
    </div> 
</div>    
