<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">

                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?php echo site_url('super_admin/index') ?>">Dashboard</a></li>
                    <!--                    <li>
                                            <span>introduce by you</span>
                                        </li>-->
                    <li class="active">
                        <span>Withdraw Process</span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                withdraw process 
            </h2>
            <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">

            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class=""><i class="fa fa-times"></i></a>
                    </div>
                    <h3>withdraw process</h3>
                </div>

                <div class="panel-body">
                    <table id="process-withdraw" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Member code</th>
                                <th>Name</th>                           
                                <th>Total Paid</th>                              
                                <th>20%</th>
                                <th>80%</th>
                                <th>1%</th>
                                <th>current bonus</th>                              
                                <th>action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($user_detail as $row) {
                               
                                $id = urlencode(base64_encode($row['id']));
                                ?>  
                                <tr>
                                    <td><?php echo $row['code']; ?></td>
                                    <td><?php echo $row['username']; ?></td>
                                    <td><?php echo $row['total_paid']; ?></td>
                                    <td><?php echo $row['twenty']; ?></td>
                                    <td><?php echo $row['eighty']; ?></td>
                                    <td><?php echo $row['one']; ?></td>                                    
                                    <td><?php echo ( ($row['twenty'] + $row['eighty'] + $row['one']) - ($row['total_paid']) ); ?></td>                              
                                    <td>
                                        <input type="hidden" id="user-img-<?php echo $row['id'];?>" value="<?php echo $row['user_pic_path'];?>" />
                                        <!--<a href="<?php //echo site_url('super_admin/withdraw_process_list')  ?>/<?php //echo $decoded_id  ?>" class="pull-left">-->
                                        <button type="button" class="btn btn-info btn-lg" id="withdrawprocess" data-toggle="modal" data-withdrw-id="<?php echo $id?>" data-target="#exampleModal_withdraw" data-whatever="password" style="padding: 4px 8px;font-size:12px;">withdraw</button>
                                        <!--// </a>-->
                                        
                                        <a href="<?php echo site_url('super_admin/user_detail') . '/' . $row['id'] ?>" class="pull-right">
                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="" data-whatever="password" style="padding: 4px 8px;font-size:12px;">detail</button>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('modals/withdraw_model');
?>