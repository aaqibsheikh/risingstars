<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">

                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?php echo site_url('super_admin/index') ?>">Dashboard</a></li>
                    <!--                    <li>
                                            <span>introduce by you</span>
                                        </li>-->
                    <li class="active">
                        <span>Withdraw History</span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Withdraw History
            </h2>
            <small>complete History</small>
        </div>
    </div>
</div>
<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">

            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class=""><i class="fa fa-times"></i></a>
                    </div>
                    <h3>History Report</h3>
                </div>

                <div class="panel-body">
                    <table id="process-withdraw" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>id</th>                           
                                <th>Name</th>                              
                                <th>Member Code</th>
                                <th>amount</th>
                                <th>Withdraw date</th>                              
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $serail_no = 1;
                            foreach ($Histry_detail as $row) {
                                ;
                                ?>  
                                <tr>
                                    <td><?php echo  $serail_no++ ?></td>
                                    <td><?php ?></td>
                                    <td><?php echo $row['member_code']; ?></td>
                                    <td><?php echo $row['amount']; ?></td>
                                    <td><?php echo $row['created_at']; ?></td>

                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
//$this->load->view('modals/withdraw_model');
?>