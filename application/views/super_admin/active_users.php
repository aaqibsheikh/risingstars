
<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">

                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?php echo site_url('super_admin/index') ?>">Dashboard</a></li>
                    <!--                    <li>
                                            <span>introduce by you</span>
                                        </li>-->
                    <li class="active">
                        <span>Active users</span>
                    </li>
                </ol>
            </div>
            <h2 class="font-light m-b-xs">
                Active Users
            </h2>
            <small>List of all Active users</small>
        </div>
    </div>
</div>

<div class="content animate-panel">
    <div class="row">
        <div class="col-lg-12">

            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class=""><i class="fa fa-times"></i></a>
                    </div>
                    <h3>Active Users</h3>
                </div>

                <div class="panel-body">
                    <table id="process-withdraw" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Serial Number</th>
                                <th>RegNo#</th>                           
                                <th>Name</th>                              
                                <th>Email</th>
                                <th>Member Code</th>
                                <!--<th>Password</th>-->
                                <th>Status</th>                              
                                <th>action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $serail_no = 1;
                            foreach ($user_detail as $row) {

                                ;
                                ?>  
                                <tr>
                                    <td><?php echo $serail_no++ ?></td>
                                    <td><?php echo $row['id']; ?></td>
                                    <td><?php echo $row['full_name']; ?></td>
                                    <td><?php echo $row['email']; ?></td>
                                    <td><?php echo $row['username']; ?></td>
                                    <!--<td></td>-->
                                    <td>
                                        <!--<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="" data-whatever="password" style="padding: 4px 8px;font-size:12px;">active</button></td>-->                                    
                                           <span class="text-success">Active</span>
                                    </td>    
                                    <td>
                                        <!--// </a>-->
                                        <a href="<?php echo site_url('super_admin/user_detail').'/'.$row['id']?>" class="pull-right">
                                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="" data-whatever="password" style="padding: 4px 8px;font-size:12px;">detail</button>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
//$this->load->view('modals/withdraw_model');
?>