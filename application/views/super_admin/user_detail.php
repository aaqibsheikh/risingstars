
<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">

                <ol class="hbreadcrumb breadcrumb">
                    <li><a href="<?php echo site_url('super_admin/index') ?>">Dashboard</a></li>
                    <li><a href="<?php echo site_url('super_admin/register_users') ?>">register_users</a></li>
                    <!--                    <li>
                                            <span>introduce by you</span>
                                        </li>-->
                                        <li class="active">
                                            <span>user details</span>
                                        </li>
                                    </ol>
                                </div>
                                <h2 class="font-light m-b-xs">
                                    user details
                                </h2>
                                <small>specific user detail</small>
                            </div>
                        </div>
                    </div>
                    <div class="content animate-panel">
                        <div class="row">
                            <div class="col-lg-offset-2 col-lg-8 animated-panel zoomIn" style="animation-delay: 0.1s;">
                                <div class="hpanel hgreen">
                                    <div class="panel-body">
                                        <div class="pull-right text-right">
                                            <div class="btn-group">
                                                <a href="https://www.facebook.com/"><i class="fa fa-facebook btn btn-default btn-xs"></i></a>
                                                <a href="https://twitter.com/"><i class="fa fa-twitter btn btn-default btn-xs"></i></a>
                                                <a href="https://www.linkedin.com/"><i class="fa fa-linkedin btn btn-default btn-xs"></i></a>
                                            </div>
                                        </div>
                                        <img alt="logo" class="img-circle m-b m-t-md" src="<?php echo $user_detail['user_pic_path']; ?>" height="100" width="100">
                                        <h3><a href=""><?php echo $user_detail['full_name']; ?></a></h3>
                                        <div class="text-muted font-bold m-b-xs"><?php echo $user_detail['address']; ?></div>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan.
                                        </p>
                    <!--                    <div class="progress m-t-xs full progress-small">
                                            <div style="width: 65%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="65" role="progressbar" class=" progress-bar progress-bar-success">
                                                <span class="sr-only">35% Complete (success)</span>
                                            </div>
                                        </div>-->
                                    </div>

                                    <div class="panel-body">
                                        <dl>
                                            <div class='col-lg-6'>
                                                <dt>Father Name </dt>
                                                <dd><?php echo $user_detail['father_name']; ?>.</dd>
                                                <dt>Mobile No</dt>
                                                <dd><?php echo $user_detail['mobile_no']; ?>.</dd>

                                                <dt>Cnic</dt>
                                                <dd><?php echo $user_detail['cnic']; ?></dd>
                                                <dt>user status</dt>
                                                <dd><?php $status = ($user_detail['banned'] > 0 ? "block" : "active");
                                                    echo $status;
                                                    ?></dd>
                                                </div>
                                                <div class='col-lg-6'>
                                                    <dt>Package Name</dt>
                                                    <dd><?php echo $package_name ?></dd>
                                                    <dt>email</dt>
                                                    <dd><?php echo $user_detail['email']; ?></dd>                           
                                                    <dt>code</dt>
                                                    <dd><?php echo $user_detail['username']; ?></dd>                           
                                                    <dt>Registration Date</dt>
                                                    <dd><?php echo $user_detail['previous']; ?></dd>
                                                </div>

                                            </dl>
                                        </div>
                                        <div class="panel-footer contact-footer">
                                            <div class="row">
                                                <div class="col-md-6 border-right animated-panel zoomIn" style="animation-delay: 0.2s;">
                                                    <div class="contact-stat"><span>team Levels: </span> <strong><?php echo $level ?></strong></div>
                                                </div>
                                                <div class="col-md-6 border-right animated-panel zoomIn" style="animation-delay: 0.2s;">
                                                    <div class="contact-stat"><span>total team members: </span> <strong><?php echo $member ?></strong></div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>    
