<div class="content animate-panel"> 
    <div class="row">
        <div class="col-lg-12 text-center m-t-md">
            <h2>


                Welcome to Admin Panel


            </h2>

            <p>
                Lorem ipsum <strong>Rising Star International </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="hpanel">
                <div class="panel-body text-center h-200">
                    <img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

                    <h1 class="m-xs">Rs:<?php echo $total_amount; ?></h1>

                    <h3 class="font-extra-bold no-margins " id="withdraw-process">
                        <a href="<?php echo site_url('super_admin/withdraw_process_list') ?>" class="text-success">Withdraw Process</a>
                    </h3>
                    <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
                </div>
                <div class="panel-footer">
                    Lorem ipsum dolor sit amet, consectetur 
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="hpanel">
                <div class="panel-body text-center h-200">
                    <img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

                    <h1 class="m-xs"><?php echo $total_users ?></h1>

                    <h3 class="font-extra-bold no-margins " id="withdraw-process">
                        <a href="<?php echo site_url('super_admin/register_users') ?>" class="text-success">Register Users</a>
                    </h3>
                    <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
                </div>
                <div class="panel-footer">
                    Lorem ipsum dolor sit amet, consectetur 
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="hpanel stats">
                <div class="panel-body h-200">
                    <div class="stats-title pull-left">
                        <h4> <a href="<?php echo site_url('super_admin/withdraw_history') ?>"class="text-success">withdraw History</a></h4>
                    </div>
                    <div class="stats-icon pull-right">
                        <i class="pe-7s-monitor fa-4x"></i>
                    </div>
                    <div class="m-t-xl">
                        <h1 class="text-success"></h1>
                        <span class="font-bold no-margins">
                            lorem ipsum
                        </span>
                        <br/>
                        <small>
                            Lorem Ipsum is simply dummy text of the printing and <strong>typesetting industry</strong>. Lorem Ipsum has been.
                        </small>
                    </div>
                </div>
                <div class="panel-footer">
                    Lorem ipsum dolor sit amet, consectetur 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="hpanel">
                <div class="panel-body text-center h-200">
                    <img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

                    <h1 class="m-xs"><?php echo $total_block_users ?></h1>

                    <h3 class="font-extra-bold no-margins " id="withdraw-process">
                        <a href="<?php echo site_url('super_admin/block_users') ?>" class="text-success">Block Users</a>
                    </h3>
                    <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
                </div>
                <div class="panel-footer">
                    Lorem ipsum dolor sit amet, consectetur 
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="hpanel">
                <div class="panel-body text-center h-200">
                    <img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

                    <h1 class="m-xs"><?php echo $total_active_users ?></h1>

                    <h3 class="font-extra-bold no-margins " id="withdraw-process">
                        <a href="<?php echo site_url('super_admin/active_users') ?>" class="text-success">Active Users</a>
                    </h3>
                    <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
                </div>
                <div class="panel-footer">
                    Lorem ipsum dolor sit amet, consectetur 
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="hpanel stats">
                <div class="panel-body h-200">
                    <div class="stats-title pull-left">
                        <h4></h4>
                    </div>
                    <div class="stats-icon pull-right">
                        <i class="pe-7s-monitor fa-4x"></i>
                    </div>
                    <div class="m-t-xl">
                        <h1 class="text-success"><a class="text-success " href="<?php echo site_url('super_admin/fund_e_cash') ?>">Fund E-Cash</a></h1>
                        <span class="font-bold no-margins">
                            <a class="text-success" href="<?php echo site_url('super_admin/fund_e_cash_histry')?>">fund_e_cash_history</a>
                        </span>
                        <br/>
                        <small>
                            Lorem Ipsum is simply dummy text of the printing and <strong>typesetting industry</strong>. Lorem Ipsum has been.
                        </small>
                    </div>
                </div>
                <div class="panel-footer">
                    Lorem ipsum dolor sit amet, consectetur 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-4 col-lg-4">
            <div class="hpanel">
                <div class="panel-body text-center h-200">
                    <img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

                    <h1 class="m-xs"></h1>

                    <h3 class="font-extra-bold no-margins " id="withdraw-process">
                        <a href="<?php echo site_url('super_admin/missed_commisson') ?>" class="text-success">Missed Commision Log Histry</a>
                    </h3>
                    <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
                </div>
                <div class="panel-footer">
                    Lorem ipsum dolor sit amet, consectetur 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 animated-panel zoomIn" style="animation-delay: 1.2s;">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
                    <span>Recent amount paid/withdraw <span style="margin-left:30px;"><a href="<?php echo site_url('super_admin/withdraw_history') ?>">full withdraw history</a></span></span>
                </div>
                <div class="panel-body list">
                    <div class="table-responsive project-list">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>                           
                                    <th>Name</th>                              
                                    <th>&nbsp;</th>
                                    <th>Amount</th>
                                    <th>Date</th>  
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $serail_no = 1;
                                foreach ($Histry_detail as $row) {
                                    ;
                                    ?>  
                                    <tr>
                                        <td><div class="icheckbox_square-green checked" style="position: relative;"><input class="i-checks" checked="" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div></td>
                                        <td>Amount paid to <strong><?php echo $row['full_name'] ?></strong>
                                            <br>
                                            <small><i class="fa fa-user"></i> User code:<?php echo $row['member_code'] ?></small>
                                        </td>
                                        <td>
                                            <span class="pie" style="display: none;">1/5</span><svg class="peity" height="16" width="16"><path d="M 8 0 A 8 8 0 0 1 15.60845213036123 5.52786404500042 L 8 8" fill="#62cb31"/><path d="M 15.60845213036123 5.52786404500042 A 8 8 0 1 1 7.999999999999998 0 L 8 8" fill="#edf0f5"/></svg>
                                        </td>
                                        <td><strong><?php echo $row['amount'] ?></strong></td>
                                        <td><?php echo $row['created_at'] ?></td>
                                        <td>&nbsp;</td>
                                    </tr>

                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 animated-panel zoomIn" style="animation-delay: 1.2s;">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        <span>Admin recent Commissions <span style="margin-left:30px;"><a href="<?php echo site_url('super_admin/getFullCommisionHistryForSuperAdmin') ?>">full Commission history</a></span></span>
                    </div>
                    <div class="panel-body list">
                        <div class="table-responsive project-list">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>                           
                                        <th>Name</th>                              
                                        <th>&nbsp;</th>
                                        <th>Amount</th>
                                        <th>package</th>
                                        <th>type</th>
                                        <th>Date</th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $serail_no = 1;
                                    foreach ($commision_detail as $row) {
                                        ;
                                        ?>  
                                        <tr>
                                            <td><div class="icheckbox_square-green checked" style="position: relative;"><input class="i-checks" checked="" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div></td>
                                            <td>Commision get by  <strong><?php echo $row['Username'] ?></strong>
                                                <br>
                                                <small><i class="fa fa-user"></i> User code:<?php echo $row['user_code'] ?></small>
                                            </td>
                                            <td>
                                                <span class="pie" style="display: none;">1/5</span><svg class="peity" height="16" width="16"><path d="M 8 0 A 8 8 0 0 1 15.60845213036123 5.52786404500042 L 8 8" fill="#62cb31"/><path d="M 15.60845213036123 5.52786404500042 A 8 8 0 1 1 7.999999999999998 0 L 8 8" fill="#edf0f5"/></svg>
                                            </td>
                                            <td><strong><?php echo $row['amount'] ?></strong></td>
                                            <td><?php echo $row['package'] ?></td>
                                            <td><?php echo $row['commision_type'] ?></td>
                                            <td><small><?php echo $row['created_at'] ?></small></td>

                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


