<div class="normalheader transition animated fadeIn">
    <div class="hpanel">
        <div class="panel-body">
            <a class="small-header-action" href="">
                <div class="clip-header">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </a>

            <div id="hbreadcrumb" class="pull-right m-t-lg">
                <ol class="hbreadcrumb breadcrumb">
                    <li>Dashboard</li>
                    <!--                    <li>
                                            <span>introduce by you</span>
                                        </li>-->
                                        <li class="active">
                                            <span>Commission reports</span>
                                        </li>
                                    </ol>
                                </div>
                                <h2 class="font-light m-b-xs">
                                    Commission Report
                                </h2>
                                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
                            </div>
                        </div>
                    </div>
                    <div class="content animate-panel">


                        <div class="row">
                            <div class="col-lg-12">

                                <div class="hpanel">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                            <a class=""><i class="fa fa-times"></i></a>
                                        </div>
                                        <h2>Commission Report  </h2>

                                    </div>

                                    <div class="panel-body">
                                        <table id="example2" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>

                                                    <th>Serial Number</th>
                                                    <th>Member ID</th>
                                                    <th>80 % Commission</th>
                                                    <th>20 % Commission</th>                                
                                                    <th>1 % Commission</th>
                                                    <th>JackPot MeeNuu</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $serial_number = 1;
                                                foreach ($commission_report as $row) {
                                                    ?>    
                                                    <tr>
                                                        <td><?php echo $serial_number++ ?></td>
                                                        <td><?php echo $row['user_code'] ?></td>
                                                        <td><?php echo ($row['commision_type'] == 80) ? $row['amount'] : 0; ?></td>
                                                        <td><?php echo ($row['commision_type'] == 20) ? $row['amount'] : 0; ?></td>
                                                        <td><?php echo ($row['commision_type'] == 1) ?  $row['amount'] : 0; ?></td>
                                                        <td><?php echo ($row['commision_type'] == 0) ?  $row['amount'] : 0; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
