<div class="tr-menu">
	<nav class="navbar navbar-toggleable-md">
		<div class="container">
			<div class="menu-content">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item <?php echo $this->uri->segment(1) == '' ? 'active' : ''; ?>">
							<a class="nav-link" href="<?php echo base_url(); ?>">Home</a>
						</li>
						<?php foreach ($brands as $brand): ?>
						<li class="nav-item <?php echo $this->uri->segment(3) == $brand->brand_id ? 'active' : ''; ?>">
							<a class="nav-link" style="text-transform: none;" href="<?php echo base_url(); ?>product/brand_list/<?php echo $brand->brand_id; ?>"><?php echo $brand->brand_name; ?></a>
						</li>
						<?php endforeach;?>
					</ul>
				</div>
				</div><!-- /.menu-content -->
				</div><!-- /.container -->
			</nav>
			</div><!-- /.tr-menu -->