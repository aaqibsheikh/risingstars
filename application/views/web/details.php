<?php
$this->load->view('web/header');
$this->load->view('web/navbar', $brands);
?>

		<div class="tr-breadcrumb">
			<div class="container">
				<div class="breadcrumb-info">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
						<li class="breadcrumb-item active">Product Details</li>
					</ol>
					<div class="page-title">
						<h1>Product Details</h1>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.tr-breadcrumb -->

		<?php $all_category = $this->CategoryModel->get_all_category();?>
		<?php $all_sub_category = $this->CategoryModel->get_all_sub_category();?>
		<div class="main-wrapper product-details">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-lg-3">
						<div class="product-categories tr-section">
							<div class="panel-group" id="accordion">
								<?php $i = 1;?>
								<?php foreach ($all_category as $maincat): ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#accordion" href="#faq-<?php echo $i; ?>">
											<span><?php echo $maincat->category_name; ?></span>
										</a>
									</div><!-- panel-heading -->

									<div id="faq-<?php echo $i; ?>" class="panel-collapse collapse in">
										<div class="panel-body">
											<ul class="tr-list">
												<?php foreach ($all_sub_category as $subcat) {?>
												<?php if ($subcat->category_sub_id == $maincat->category_id) {?>
												<li><a href="<?php echo base_url(); ?>product/product_list_by_category/<?php echo $subcat->sub_cat_id; ?>"><?php echo $subcat->sub_category_name ?></a></li>
												<?php }?>
												<?php }?>
											</ul>
										</div>
									</div>
								</div><!-- panel -->
								<?php $i++;?>
								<?php endforeach;?>
							</div>
						</div><!-- /.product-categories -->
					</div>
					<div class="col-md-8 col-lg-9">
						<div class="tr-section product-details">
							<div class="row">
								<div class="col-lg-6">
									<div class="product-details-slider">
										<?php foreach ($product_images as $img): ?>
										<div class="product">
											<img src="<?php echo base_url(); ?>/assets/upload/<?php echo $img->images; ?>" alt="Image" class="img-fluid">
										</div>
										<?php endforeach;?>
									</div><!-- /.product-details-slider -->
								</div>
								<div class="col-lg-6">
									<div class="products-details-info">
										<?php if (isset($product_info->brand_name)): ?>
										<span class="product-title"><?php echo $product_info->brand_name; ?></span>
									<?php endif;?>
										<h1><?php echo $product_info->pro_title; ?></h1>

										<form method="post" action="<?php echo base_url(); ?>product/buy">
										<div class="quantity-price">
											<div class="price">
												<span>Rs
													<div style="display: inline-block;" id="price"><?php echo $product_info->price_after_discount; ?></div>
												</span>
											</div>
											<div class="quantity" data-trigger="spinner">
												<a id="minus" class="btn" href="javascript:;" data-spin="down"><i class="fa fa-minus"></i></a>
												<input type="hidden" name="product_id" value="<?php echo $product_info->pro_id; ?>">
												<input type="text" name="quantity" id="quantity" title="quantity" class="input-text">
												<input type="hidden" name="total_price" id="total_price" readonly>
												<a id="plus" class="btn" href="javascript:;" data-spin="up"><i class="fa fa-plus"></i></a>
											</div>
										</div>


										<div class="add-to-cart">
											<button type="submit" class="btn btn-primary">BUY</button>
										</div>
										</form>
									</div>
								</div>
							</div>
						</div><!-- /.product-details -->


						<div class="tr-section products-description">
							<ul class="nav nav-tabs description-tabs" role="tablist">
								<li role="presentation" class="nav-item"><a class="nav-link active" href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
								<li class="nav-item" role="presentation"><a class="nav-link" href="#payment" aria-controls="payment" role="tab" data-toggle="tab" aria-expanded="true">Payment</a></li>
								<li class="nav-item" role="presentation"><a class="nav-link" href="#delivery" aria-controls="delivery" role="tab" data-toggle="tab" aria-expanded="true">Delivery</a></li>
								<li class="nav-item" role="presentation"><a class="nav-link" href="#packages" aria-controls="packages" role="tab" data-toggle="tab" aria-expanded="true">Packages</a></li>
							</ul>

							<div class="tab-content">
								<div role="tabpanel" class="tab-pane fade show active" id="details">

									<p><?php echo $product_info->pro_desc; ?></p>
								</div><!-- /.tab-pane -->

								<div role="tabpanel" class="tab-pane fade" id="payment">
								   <div class="payment">
										<h3>We Accept</h3>

										<div class="payment-card">
											<ul class="tr-list">
												<li><img src="<?php echo base_url(); ?>assets/upload/paymentimages/card1.png" alt="Image" class="img-fluid"></li>
												<li><img src="<?php echo base_url(); ?>assets/upload/paymentimages/card2.png" alt="Image" class="img-fluid"></li>
												<li><img src="<?php echo base_url(); ?>assets/upload/paymentimages/card3.png" alt="Image" class="img-fluid"></li>
												<li><img src="<?php echo base_url(); ?>assets/upload/paymentimages/card4.png" alt="Image" class="img-fluid"></li>
												<li><img src="<?php echo base_url(); ?>assets/upload/paymentimages/card5.png" alt="Image" class="img-fluid"></li>
												<li><img src="<?php echo base_url(); ?>assets/upload/paymentimages/card6.png" alt="Image" class="img-fluid"></li>
											</ul>
										</div><!-- /.payment-card -->
								   </div>
								</div><!-- /.tab-pane -->

								<div role="tabpanel" class="tab-pane fade" id="delivery">
									<div class="delivery">
										<div class="delivery-icon">
											<img src="<?php echo base_url(); ?>assets/upload/paymentimages/delivery-icon.png" alt="Image" class="img-fluid">
											<span>Delivery Charges: Rs <?php echo $product_info->delivery_charges; ?></span>
										</div>
										<p></p>
									</div>
								</div><!-- /.tab-pane -->

								<div role="tabpanel" class="tab-pane fade" id="packages">
									<div class="delivery">
										<table class="table">
											<tbody>
												<tr>
													<td>
														<strong>Jackpot</strong>
													</td>
													<td>
														Rs <?php echo $product_info->jackpot; ?>
													</td>
												</tr>

												<tr>
													<td>
														<strong>SS</strong>
													</td>
													<td>
														Rs <?php echo $product_info->ss; ?>
													</td>
												</tr>

												<tr>
													<td>
														<strong>GS</strong>
													</td>
													<td>
														Rs <?php echo $product_info->gs; ?>
													</td>
												</tr>

												<tr>
													<td>
														<strong>PS</strong>
													</td>
													<td>
														Rs <?php echo $product_info->ps; ?>
													</td>
												</tr>

												<tr>
													<td>
														<strong>DS</strong>
													</td>
													<td>
														Rs <?php echo $product_info->ds; ?>
													</td>
												</tr>

												<tr>
													<td>
														<strong>20%</strong>
													</td>
													<td>
														Rs <?php echo $product_info->twenty_percent; ?>
													</td>
												</tr>

												<tr>
													<td>
														<strong>1%</strong>
													</td>
													<td>
														Rs <?php echo $product_info->one_percent; ?>
													</td>
												</tr>
											</tbody>
										</table>
										<style>
											.table td{
												border-top: 0px !important;
											}
										</style>
									</div>
								</div><!-- /.tab-pane -->

							</div><!-- /.tab-content -->
						</div><!-- /.products-description -->
					</div>
				</div><!-- /.row -->

				<div class="related-product">
					<div class="section-title text-center">
						<h1><span>Related Product</span></h1>
					</div>
					<div class="random-product">
						<div class="row">
							<?php foreach ($related_products as $product): ?>
							<div class="col-md-6 col-lg-3">
								<div class="products">
									<div class="product" style="min-height: 390px;">
										<div class="image-slider">
											<div class="product-image">
												<a href="<?php echo base_url(); ?>product/product_details/<?php echo $product->pro_id; ?>"><img src="<?php echo base_url(); ?>assets/upload/<?php echo $product->pro_image; ?>" alt="Image" class="img-fluid"></a>
											</div>
										</div>
										<a href="<?php echo base_url(); ?>product/product_details/<?php echo $product->pro_id; ?>">
											<span class="product-title"><?php echo $product->pro_title; ?></span>
											<!-- <span class="price">Rs <?php echo $product->pro_price; ?></span> -->
											<strike>Rs <?php echo $product->pro_price; ?></strike>
											<strike>Rs <?php echo $product->discount_price; ?></strike>
											Rs <?php echo $product->price_after_discount; ?>
										</a>
										<div class="product-icon">
											<a href="#"><span class="icon icon-basket"></span></a>
										</div>
									</div>
								</div>
							</div>
							<?php endforeach;?>
						</div><!-- /.row -->
					</div><!-- /.random-product -->
				</div><!-- /.related-product -->
			</div><!-- /.container -->
		</div><!-- /.main-wrapper -->


<?php
$this->load->view('web/footer');
?>

<script>
	$(document).ready(function(){
		var qty = 1;
		var total = 0;
		var price = $('#price').html();
		console.log();
		$('#total_price').val(price);
		$("#quantity").val(qty);

		$(document).on('click','#minus',function(){
			if(qty > 1){
				qty--;
				$("#quantity").val(qty);
				total = price * qty;
				$('#price').html(total)
				$('#total_price').val(total);
			}else{
				$("#quantity").val(1);
			}

		});

		$(document).on('click','#plus',function(){

			qty++;
			$("#quantity").val(qty);
			total = price * qty;
			console.log(total);
			$('#price').html(total)
			$('#total_price').val(total);
		});
	});
</script>