<?php
$this->load->view('web/header');
$this->load->view('web/navbar', $brands);
?>


		<div class="tr-breadcrumb">
			<div class="container">
				<div class="breadcrumb-info">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
						<li class="breadcrumb-item active">Mens</li>
					</ol>
					<div class="page-title">
						<h1>Mens Shopping</h1>
					</div>
				</div>
			</div><!-- /.container -->
		</div><!-- /.tr-breadcrumb -->
		<?php $all_category = $this->CategoryModel->get_all_category();?>
		<?php $all_sub_category = $this->CategoryModel->get_all_sub_category();?>
		<div class="main-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-lg-3">
						<div class="product-categories tr-section">
							<div class="panel-group" id="accordion">
								<?php $i = 1;?>
								<?php foreach ($all_category as $maincat): ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#accordion" href="#faq-<?php echo $i; ?>">
											<span><?php echo $maincat->category_name; ?></span>
										</a>
									</div><!-- panel-heading -->

									<div id="faq-<?php echo $i; ?>" class="panel-collapse collapse in">
										<div class="panel-body">
											<ul class="tr-list">
												<?php foreach ($all_sub_category as $subcat) {?>
												<?php if ($subcat->category_sub_id == $maincat->category_id) {?>
												<li><a href="<?php echo base_url(); ?>product/product_list_by_category/<?php echo $subcat->sub_cat_id; ?>"><?php echo $subcat->sub_category_name ?></a></li>
												<?php }?>
												<?php }?>
											</ul>
										</div>
									</div>
								</div><!-- panel -->
								<?php $i++;?>
								<?php endforeach;?>
							</div>
						</div><!-- /.product-categories -->
						<div class="tr-section choices-option">
							<ul class="tr-list">
								<li>
									<span>Price Ranger</span>
									<form method="get" action="<?php echo base_url(); ?>product/search/">
										<div id="price_slider"></div>
										<div class="price_slider_amount">
											<input type="text" id="amount" name="amount">
											<button type="submit" class="btn btn-primary">Filter</button>
										</div>
									</form>
								</li><!-- /.price-range -->
								<li class="brands">
									<span>Brand</span>
									<form class="search-form" action="<?php echo base_url(); ?>product/search/" method="get">
										<ul class="tr-list">
											<?php foreach ($footer_brands as $brand): ?>
											<li><input type="checkbox" name="brandType[]" value="<?php echo $brand->brand_name; ?>" id="<?php echo $brand->brand_name; ?>">
												<label for="<?php echo $brand->brand_name; ?>"> <?php echo $brand->brand_name; ?></label></li>
											<?php endforeach;?>
										</ul>
								</li>
							</ul>
							<div class="apply-changes">
								<span>
									<button type="submit" id="brand_search" name="submit" style="border: none;background: none;">
										<i class="fa fa-check" aria-hidden="true"></i>
										Apply Changes
									</button>
								</span>
							</div>
									</form>
							<script type="text/javascript">
								$(document).ready(function () {
									$('#brand_search').click(function() {
									  checked = $("input[type=checkbox]:checked").length;

									  if(!checked) {
										alert("You must check at least one checkbox.");
										return false;
									  }

									});
								});

							</script>
						</div><!-- /.tr-section -->
					</div>
					<div class="col-md-8 col-lg-9">
						<div class="product-list">
							<?php if (empty($product_list)):
	echo '<div class="row">
																												<div class="col-lg-12 text-center">
																													<h2>No Product Available</h2>
																												</div>
																											</div><div style="margin:30px 0px;"></div>';
else:
?>
							<div class="row">
								<?php foreach ($product_list as $product): ?>
								<div class="col-md-6 col-lg-4">
									<div class="product">
										<a href="<?php echo base_url(); ?>product/product_details/<?php echo $product->pro_id; ?>">
											<span class="product-image">
												<img src="<?php echo base_url(); ?>assets/upload/<?php echo $product->pro_image; ?>" alt="Image" class="img-fluid">
											</span>
											<span class="product-title"><?php echo $product->pro_title; ?></span>
											<strike>Rs <?php echo $product->pro_price; ?></strike>
											<strike>Rs <?php echo $product->discount_price; ?></strike>
											Rs <?php echo $product->price_after_discount; ?>
										</a>
										<div class="product-icon">
											<a href="#"><span class="icon icon-basket"></span></a>
										</div>
									</div>
								</div>
								<?php endforeach;?>
							</div><!-- /.row -->
							<?php endif;?>

							<!-- <div class="tr-section tr-cta cta-2">
								<div class="cta-info">
									<h1>Apple Watch</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
									<div class="specification">
										<p>Specification <span> <a href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum"><img src="<?php echo base_url(); ?>assets/images/others/1.png" alt="Image" class="img-fluid"></a><a href="#">Detailes</a></span></p>
									</div>
									<div class="buy-now">
										<span>$1300</span>
										<a class="btn btn-primary" href="#">Buy now</a>
									</div>
								</div>
							</div> --><!-- /.product-middle -->


						<!-- 	<div class="tr-pagination tr-section text-center">
								<ul class="pagination">
									<li class="float-left"><a href="#">Prev</a></li>
									<li><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li class="active"><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li><a href="#">6</a></li>
									<li><a href="#">7</a></li>
									<li><a href="#">8</a></li>
									<li><a href="#">9</a></li>
									<li><a href="#">...</a></li>
									<li><a href="#">21</a></li>
									<li><a href="#">22</a></li>
									<li><a href="#">23</a></li>
									<li><a href="#">24</a></li>
									<li class="float-right"><a href="#">Next</a></li>
								</ul>
							</div>  -->
						</div><!-- /.product-list -->
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.main-wrapper -->

<?php
$this->load->view('web/footer');
?>