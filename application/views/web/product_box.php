
<?php foreach ($product_by_cat_id as $product): ?>

<div class="product" style="max-width: 212px !important;">
	<a href="<?php echo base_url(); ?>product/product_details/<?php echo $product->pro_id; ?>">
		<span class="product-image">
			<img style="width: 150px;height: 150px;" src="<?php echo base_url(); ?>assets/upload/<?php echo $product->pro_image; ?>" alt="Image" class="img-fluid">
		</span>
		<span class="product-title"><?php echo $product->pro_title; ?></span>
		<span class="price">
			<strike>Rs <?php echo $product->pro_price; ?></strike>
			<strike>Rs <?php echo $product->discount_price; ?></strike>
			Rs <?php echo $product->price_after_discount; ?>
		</span>
	</a>
	<div class="product-icon">
		<a href="#"><span class="icon icon-basket"></span></a>
	</div>
</div>
<?php endforeach;?>