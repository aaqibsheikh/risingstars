<div class="tr-convenience">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="convenience">
                            <div class="icon">
                                <img src="<?php echo base_url(); ?>assets/images/others/icon1.png" alt="Image" class="img-fluid">
                            </div>
                            <span>Free Delivery</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="convenience">
                            <div class="icon">
                                <img src="<?php echo base_url(); ?>assets/images/others/icon2.png" alt="Image" class="img-fluid">
                            </div>
                            <span>Clients Discounts</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="convenience">
                            <div class="icon">
                                <img src="<?php echo base_url(); ?>assets/images/others/icon3.png" alt="Image" class="img-fluid">
                            </div>
                            <span>Return Of Goods</span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="convenience">
                            <div class="icon">
                                <img src="<?php echo base_url(); ?>assets/images/others/icon4.png" alt="Image" class="img-fluid">
                            </div>
                            <span>Many Brands</span>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.tr-convenience -->

        <div class="tr-footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <!-- <div class="col-sm-6">
                            <div class="left-content">
                                <h3>Sign up for newsletter</h3>
                                <p>Enter your email to receive relevant messaging tips and examples.</p>
                                <form action="#">
                                    <input class="form-control" type="email" required="required" placeholder="Enter Your Email Id">
                                    <input type="submit" class="btn">
                                </form>
                            </div>
                        </div> -->
                        <div class="col-sm-6">
                            <h3>We Accept</h3>
                            <p>Enter your email to receive relevant messaging tips and examples.</p>
                            <div class="payment-card">
                                <ul class="tr-list">
                                    <li><img src="<?php echo base_url(); ?>assets/images/others/card1.png" alt="Image" class="img-fluid"></li>
                                    <li><img src="<?php echo base_url(); ?>assets/images/others/card2.png" alt="Image" class="img-fluid"></li>
                                    <li><img src="<?php echo base_url(); ?>assets/images/others/card3.png" alt="Image" class="img-fluid"></li>
                                    <li><img src="<?php echo base_url(); ?>assets/images/others/card4.png" alt="Image" class="img-fluid"></li>
                                    <li><img src="<?php echo base_url(); ?>assets/images/others/card5.png" alt="Image" class="img-fluid"></li>
                                </ul>
                            </div><!-- /.payment-card -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-top -->
            <div class="footer-bottom text-center">
                <div class="container">
                    <div class="footer-widget">
                        <div class="footer-logo">
                            <a href="#"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Logo" class="img-fluid"></a>
                        </div>
                        <span>Copyright &copy; 2018 MeeNuu</span>
                    </div><!-- /.footer-widget -->
                    <!-- <div class="footer-widget">
                        <h3>Product</h3>
                        <ul class="tr-list">
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Customers</a></li>
                            <li><a href="#">One-Click Apps</a></li>
                            <li><a href="#">API</a></li>
                        </ul>
                    </div> --><!-- /.footer-widget -->
                    <!-- <div class="footer-widget">
                        <h3>Company</h3>
                        <ul class="tr-list">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div> --><!-- /.footer-widget -->
                    <!-- <div class="footer-widget">
                        <h3>Help</h3>
                        <ul class="tr-list">
                            <li><a href="#">Getting Started</a></li>
                            <li><a href="#">Feedback</a></li>
                            <li><a href="#">Referral Program</a></li>
                            <li><a href="#">Network Status</a></li>
                            <li><a href="#">FAQ</a></li>
                        </ul>
                    </div> --><!-- /.footer-widget -->
                    <div class="footer-widget">
                        <h3>Social</h3>
                        <ul class="tr-list">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i>LinkedIn</a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i>Google</a></li>
                        </ul>
                    </div><!-- /.footer-widget -->
                </div><!-- /.container -->
            </div><!-- /.footer-bottom -->
        </div><!-- /.tr-footer -->



    </body>
</html>