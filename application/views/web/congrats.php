<?php
$this->load->view('web/header');
$this->load->view('web/navbar', $brands);
?>

        <div class="main-wrapper">
            <div class="container">
                <div class="tr-section tr-congrats text-center">
                    <div class="congrats-content">
                        <div class="icon">
                            <i class="fa fa-check-circle" aria-hidden="true"></i>
                        </div>
                        <h1>Congrats! Your Order has been Accepted</h1>
                        <p>Sale team will contact you soon on your number</p>
                    </div><!-- /.congrats-content -->
                </div><!-- /.tr-section -->
            </div><!-- /.container -->
        </div><!-- /.main-wrapper -->
<?php
$this->load->view('web/footer');
?>