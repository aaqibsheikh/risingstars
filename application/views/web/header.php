<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Theme Region">
		<meta name="description" content="">

		<title>MeeNuu | Shoppping ka Asal Maza.</title>

		<!-- CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" >
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">

		<!-- font -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">


		<!-- icons -->
		<link rel="icon" href="images/ico/favicon.ico">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-72-precomposed.html">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-57-precomposed.png">
		<!-- icons -->

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		        <!-- JS -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui-min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.spinner.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
		<script>
			var site_url = "<?php echo SITE_URL; ?>"
		</script>
	</head>
	<body>
		<div class="tr-topbar">
			<div class="container clearfix">
				<div class="topbar-right">
					<ul class="tr-list">
						<li><span><a href="#"><span class="icon icon-support"></span>+92-302-4343627</a></span></li>
						<li><a href="#"><span class="icon icon-send"></span>support@meenuu.biz</a></li>
						<li><span><a href="#"><span class="icon icon-dialog"></span>Live Chat</a></span></li>
					</ul>
					<a href="#" class="btn btn-primary">Help Centre</a>
				</div>
			</div><!-- /.container -->
		</div><!-- /.tr-topbar -->

		<div class="topbar-middle">
			<div class="container clearfix">
				<a class="tr-logo logo" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logobottom.png" alt="Logo" width="275" class="img-fluid"></a>
				<a class="tr-logo tr-logo-2" href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>assets/images/meenuulogo.png" alt="Logo"></a>
				<form class="search-form" action="<?php echo base_url(); ?>product/search/" id="search" method="get">
					<input class="form-control" name="search" type="text" placeholder="Search this Site">
					<button type="submit"><i class="fa fa-search"></i></button>
			  </form><!-- /.form -->

	  </div><!-- /.container -->
		</div><!-- /.topbar-middle -->

		<!-- user-modal -->
		<div class="user-modal">
			<div id="modal-signin" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<div class="user-account text-center">
								<h1>Create Account</h1>
								<p>Don’t worry, we won’t spam you or sell your information.</p>
								<form action="#" class="tr-form">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Username">
									</div>
									<div class="form-group">
										<input type="password" class="form-control" placeholder="Password">
									</div>
									<button type="submit" class="btn btn-primary">Sign In</button>
									<div class="forgot-password">
										<a href="#">Forgot Password</a>
									</div>
									<ul class="tr-list social">
										<li class="pull-left"><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li class="pull-right"><a href="#"><i class="fa fa-facebook"></i></a></li>
									</ul>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.modal-signin -->

			<div id="modal-signup" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<div class="user-account text-center">
								<h1>Create Account</h1>
								<p>Don’t worry, we won’t spam you or sell your information.</p>
								<form action="#" class="tr-form">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Username">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email">
									</div>
									<div class="form-group">
										<input type="password" class="form-control" placeholder="Password">
									</div>
									<span>By continuing, you agree to our Terms of Use and Privacy Policy.</span>
									<button type="submit" class="btn btn-primary">Create account</button>
									<div class="already-acount">
										<a href="#">Already have an acount?</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.modal-signup -->
		</div><!-- /.user-modal -->