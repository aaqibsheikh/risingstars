<div class="tr-banner">     
			<div id="home-carousel" class="carousel slide" data-ride="carousel">            
				<div class="carousel-inner" role="listbox">                
					<?php foreach($top_product as $tp): ?>
					<div class="carousel-item item">
						<div class="container">
							<div class="row">
								<div class="col-sm-6">
									<div class="banner-info">
										<div class="info-middle">
											<h1 data-animation="animated fadeInDown"><span><?php echo $tp->brand_name; ?></span> 
												<?php echo $tp->pro_title; ?>
											</h1>
											<div class="paragraphs" data-animation="animated fadeInDown">
												<p><?php echo $tp->pro_desc; ?></p>
											</div>
											<div data-animation="animated fadeInUp" class="buy-now">
												<span></span>
												<a class="btn btn-primary" href="<?php echo base_url(); ?>product/product_details/<?php echo $tp->pro_id;?>">Buy Now</a>
											</div>
										</div>
									</div>
								</div>              
								<div class="col-sm-6">
									<div class="banner-image" data-animation="animated fadeInUp">
										<div class="info-middle">
											<img src="<?php echo base_url(); ?>assets/upload/<?php echo $tp->pro_image; ?>" alt="Image" class="img-fluid">
										</div>
									</div>   
								</div>              
							</div><!-- /.row -->                                  
						</div><!-- /.container -->                     
					</div><!-- /.carousel-item --> 

					<?php endforeach; ?>
					<script>
					  $(document).ready(function () {
					    $('#home-carousel').find('.item').first().addClass('active');
					  });
					</script>
				</div><!-- /.carousel-inner -->
				<div class="container indicators-content">
					<ol class="carousel-indicators">
						<li data-target="#home-carousel" data-slide-to="0"></li>
						<li data-target="#home-carousel" data-slide-to="1"  class="active"></li>
						<li data-target="#home-carousel" data-slide-to="2"></li>
						<li data-target="#home-carousel" data-slide-to="3"></li>
					</ol>                   
				</div><!-- /.container -->                   
			</div><!-- /#home-carousel -->           
		</div><!-- /.tr-banner -->