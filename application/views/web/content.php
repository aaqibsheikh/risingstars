<div class="main-wrapper">
			<div class="container">
				<div class="tr-promotion" style="margin-top:0px;">
					<div class="row">
						<div class="col-md-5 col-lg-4">
							<div class="promotion promotion-left">
								<div class="left-content">
									<h1>New Arrivalls</h1>
									<span>1000+ New Accessories</span>
								</div>
							</div>
						</div>
						<?php if (!empty($discount_product)): ?>
						<div class="col-md-7 col-lg-8">
							<div class="promotion promotion-right" style="background-image: url(assets/upload/<?php echo $discount_product->pro_image; ?>);">
								<div class="right-content">
									<h1><?php echo $discount_product->pro_title; ?></h1>
									<p><?php echo $discount_product->pro_desc; ?></p>
									<!-- <a class="btn btn-primary" href="<?php echo base_url(); ?>product/product_details/<?php echo $discount_product->pro_id; ?>">Shop Now</a> -->
								</div>
							</div>
						</div>
						<?php endif;?>
					</div><!-- /.row -->
				</div><!-- /.tr-promotion -->

				<?php $all_category = $this->CategoryModel->get_all_category();?>
				<?php $all_sub_category = $this->CategoryModel->get_all_sub_category();?>

				<?php foreach ($all_category as $maincat): ?>
				<div class="tr-section products">
					<div class="product-category">
						<a href="#" class="category-title">
							<h1><?php echo $maincat->category_name; ?></h1>
							<span class="category-icon"> <img src="<?php echo base_url(); ?>assets/images/others/category1.png" alt="Icon" class="img-fluid"></span>
						</a>
						<ul class="tr-list category-list">
							<?php foreach ($all_sub_category as $subcat) {?>
							<?php if ($subcat->category_sub_id == $maincat->category_id) {?>
							<li>
								<span class="sub_category" data-catid="<?php echo $maincat->category_id; ?>" data-id="<?php echo $subcat->sub_cat_id ?>"><?php echo $subcat->sub_category_name ?></span>
								<div  class="category-info">
									<div class="category-image">
										<img src="<?php echo base_url(); ?>assets/upload/<?php echo $maincat->category_image; ?>" alt="Image" class="img-fluid">
									</div>
									<div class="overlay">
										<!-- <h2><?php echo $subcat->sub_category_name; ?></h2> -->
									</div>
								</div>
							</li>
							<?php }?>
							<?php }?>
						</ul>
					</div>


					<div class="random-product" style="overflow: hidden;">
						<div style="max-height:258px !important;padding-bottom: 45px; overflow: hidden;" class="product-slider box_slider_<?php echo $maincat->category_id; ?>" id="<?php echo $maincat->category_id; ?>">
						</div>
					</div>
				</div><!-- /.products -->
				<?php endforeach;?>

				<div class="daily-needs">
					<div class="section-title text-center">
						<h1><span>Daily Needs Week</span></h1>
					</div>
					<div class="random-product">
						<div class="row">
							<?php foreach ($daily_need_product as $dnp): ?>
								<?php $images = explode(',', $dnp->product_images);?>
							<div class="col-sm-6 col-lg-3">
								<div class="products">
									<div class="product">
										<div class="image-slider">
											<?php foreach ($images as $pi): ?>
											<div class="product-image">
												<a href="<?php echo base_url(); ?>product/product_details/<?php echo $dnp->pro_id; ?>"><img src="<?php echo base_url(); ?>assets/upload/<?php echo $pi; ?>" alt="Image" class="img-fluid"></a>
											</div>
											<?php endforeach;?>
										</div>
										<a href="<?php echo base_url(); ?>product/product_details/<?php echo $dnp->pro_id; ?>">
											<span class="product-title"><?php echo $dnp->pro_title; ?></span>
											<!-- <span class="price"><?php echo $dnp->pro_price; ?></span> -->
											<strike>Rs <?php echo $dnp->pro_price; ?></strike>
											<strike>Rs <?php echo $dnp->discount_price; ?></strike>
											Rs <?php echo $dnp->price_after_discount; ?>
										</a>
										<div class="product-icon">
											<a href="<?php echo base_url(); ?>product/product_details/<?php echo $dnp->pro_id; ?>"><span class="icon icon-basket"></span></a>
										</div>
									</div>
								</div>
							</div>
							<?php endforeach;?>
						</div><!-- /.row -->
					</div><!-- /.random-product -->
				</div><!-- /.products -->

				<div class="tr-brands">
					<div class="brands-slider">
						<?php foreach ($footer_brands as $brand): ?>
						<div class="brand">
							<img src="<?php echo base_url(); ?>assets/upload/<?php echo $brand->brand_image; ?>" alt="Image" class="img-fluid">
						</div>
						<?php endforeach;?>
					</div>
				</div><!-- /.tr-brands -->
			</div><!-- /.container -->
		</div><!-- /.main-wrapper -->

		<script>


$(document).ready(function(){
	var d = '';
	$.ajax({
		url: site_url+'home/get_all_first_subcat_id',
		type: "POST",
		error: function (error) {
			console.log(error);
		},
		success: function (data) {
			// console.log(JSON.parse(data));
			getdata(JSON.parse(data));
		}// success end
	}); // Ajax End


function getdata(data){
	var array = jQuery.makeArray(data);
	$.each(array, function(index, element) {

		var sub_cat_id = element.subcat_id;
		var catid = element.catid;

		<?php foreach ($all_category as $maincat) {?>
			if(catid == "<?php echo $maincat->category_id; ?>"){
				$(".box_slider_<?php echo $maincat->category_id; ?>").slick('unslick');
			}
		<?php }?>


		$.ajax({
			url: site_url+'home/get_all_product_by_subcat_id',
			data:{id:sub_cat_id, catid:catid},
			type: "POST",
			error: function (error) {
				console.log(error);
			},
			success: function (data) {
				console.log(data);
				$('#'+catid).html(data);

			<?php foreach ($all_category as $maincat) {?>
				if(catid == "<?php echo $maincat->category_id; ?>"){

			        $(".box_slider_<?php echo $maincat->category_id; ?>").slick({
				            infinite: true,
				            dots: true,
				            slidesToShow: 3,
				            autoplay:true,
				            autoplaySpeed:10000,
				            slidesToScroll: 1,
				            responsive: [
				            {
				              breakpoint: 1200,
				              settings: {
				                slidesToShow: 2,
				                dots: true

				              }
				            },
				            {
				              breakpoint: 990,
				              settings: {
				                slidesToShow: 1,
				                dots: true
				              }
				            },
				            {
				              breakpoint: 767,
				              settings: {
				                slidesToShow: 2,
				                dots: true
				              }
				            },
				            {
				              breakpoint: 480,
				              settings: {
				                slidesToShow: 1,
				                dots: true
				              }
				            }
				            ]
				        });
				}
			<?php }?>


			}// success end


		}); // Ajax End



	});
}







	$(document).on('click','.sub_category',function(){
		var sub_cat_id = $(this).data('id');
		var catid = $(this).data('catid');

		<?php foreach ($all_category as $maincat) {?>
			if(catid == "<?php echo $maincat->category_id; ?>"){
				$(".box_slider_<?php echo $maincat->category_id; ?>").slick('unslick');
			}
		<?php }?>


		$.ajax({
			url: site_url+'home/get_all_product_by_subcat_id',
			data:{id:sub_cat_id, catid:catid},
			type: "POST",
			error: function (error) {
				console.log(error);
			},
			success: function (data) {
				$('#'+catid).html(data);

			<?php foreach ($all_category as $maincat) {?>
				if(catid == "<?php echo $maincat->category_id; ?>"){

			        $(".box_slider_<?php echo $maincat->category_id; ?>").slick({
				            infinite: true,
				            dots: true,
				            slidesToShow: 3,
				            autoplay:true,
				            autoplaySpeed:10000,
				            slidesToScroll: 1,
				            responsive: [
				            {
				              breakpoint: 1200,
				              settings: {
				                slidesToShow: 2,
				                dots: true

				              }
				            },
				            {
				              breakpoint: 990,
				              settings: {
				                slidesToShow: 1,
				                dots: true
				              }
				            },
				            {
				              breakpoint: 767,
				              settings: {
				                slidesToShow: 2,
				                dots: true
				              }
				            },
				            {
				              breakpoint: 480,
				              settings: {
				                slidesToShow: 1,
				                dots: true
				              }
				            }
				            ]
				        });
				}
			<?php }?>


			}// success end


		}); // Ajax End


	}); // Click subcategory end
}); // ready end
		</script>