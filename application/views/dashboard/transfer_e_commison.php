<div class="normalheader transition animated fadeIn">
	<div class="hpanel">
		<div class="panel-body">
			<a class="small-header-action" href="">
				<div class="clip-header">
					<i class="fa fa-arrow-up"></i>
				</div>
			</a>

			<div id="hbreadcrumb" class="pull-right m-t-lg">

				<ol class="hbreadcrumb breadcrumb">
					<li><a href="<?php echo site_url('super_admin/index') ?>">Dashboard</a></li>

					<!--                    <li>
											<span>introduce by you</span>
										</li>-->
										<li class="active">
											<span>Fund-E-Cash / Commsion Transfer</span>
										</li>
									</ol>
								</div>
								<h2 class="font-light m-b-xs">
									Fund-E-Cash / Commsion Transfer
								</h2>
								<small>Fund-E-Cash / Commsion Transfer process</small>
							</div>
						</div>
					</div>
					<div class="content animate-panel">

						<br>
						<?php if (isset($success_message)) { ?><div class="alert_messages alert alert-success"><?php echo $success_message ?></div> <?php }; ?>

						<?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>


						<h1 class="text-left">
							Give Receiver Details
						</h1>
						<div class="row code-verification-error alert alert-danger" style="display: none;">
							<!-- in this div  verifcation error will display -->
						</div>


						<div class="row">
							<div class="col-lg-6">
								<div class="form-group mqrgin-bottom-20">
									<label>Receiver code</label>
									<input type="text" value="<?php echo set_value($_SESSION['code']) ?>" id="introducer" required="required" class="form-control" name="introducer" >
									<input type="hidden" value="" id="package_id"  name="package_id"  >
									<input type="hidden" value="" id="user_id"  name="user_id"  >
									<input type="hidden" value="" id="full_name"  name="full_name"  >
								</div>
								<div class="form-group mqrgin-bottom-20">

									<label>Sender PIN</label>

									<input type="password"  value="<?php echo set_value('intrpin') ?>"  id="introducer_pin" required="required" class="form-control" name="introducer_pin">
								</div>

								<div class="form-group mqrgin-bottom-20">  
									<label>verify introducer details</label>
									<input style="display:block" class="btn btn-success" id="verify"  value="verify" onclick="find_introducer_by_code('upgrade_package')"/>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="" id="verified-distributor-detail">
									<div class="hpanel hblue contact-panel">
										<div class="panel-body">
											<img alt="user_profile_pic" id="introducer_profile_picture" class="img-circle m-b" src="<?php echo ASSETS_URL ?>images/no-pic.png" height="76" width="76"> 
											<h3 ><a href="" id="introducer_name">Max Simson</a></h3>
											<div class="text-muted font-bold m-b-xs" id="introducer_address">California, LA</div>
											<p id="verified_introducer_text">
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div> 
						<hr style="border-top-color: #62cb31;">

						<h2 class="text-center text-success"><strong>Fund Transfer</strong></h2>

						<div class="row">
							<div class="col-lg-4" id="fund_e_cash">
								<?php echo form_open('dashboard/transfer_commison', array('id' => 'fund_e_cash')); ?>
								<div class="form-group">
									<label>Member Code</label>    
									<input type="text" value="" name="verified_member" id="verified_member" class="form-control" required="required"/>
								</div>
								<div class="form-group">
									<label>Add amount</label>

									<input type="text" value="" id="" placeholder="add amount here" required="required" class="form-control" name="amount">
								</div>
								<div class="form-group">
									<label>Description</label>
									<textarea rows="4"  style="resize:none;width:100%" name="descrip" required="required">

									</textarea>
								</div> 
								<?php echo form_submit('submit', 'process', 'class="btn btn-success hide process-btn"'); ?>
								<?php echo form_close(); ?>
							</div>
							<div class="col-lg-offset-2 col-lg-6">
								<div class="hpanel">
									<?php 
									$e_remain = $totalfundecashamount-$totalusedfundecashamount-$getTransferCreditAmountEcash ;
									$total_remian=  ( ($commission['twenty'] + $commission['eighty'] + $commission['one']) - ($commission['total_paid']) - ($commission_used_entry) - ($getTransferCreditAmountCommi) - ($commission_used_entry) );
									?>
									<div class="panel-body text-center h-200">
										<img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

										<h1 class="m-xs">Rs. <?php echo $total_remian + $e_remain ?> </h1>

										<h3 class="font-extra-bold no-margins text-success">
											Your Balance
										</h3>
										<small>
											<strong class="pull-left">Availble-e-cash </strong>  <strong class="pull-right">Rs.<?php echo $e_remain ?></strong>.
											<br/>
											<strong class="pull-left">Availble Commison </strong>  <strong class="pull-right">Rs.<?php echo $total_remian ?></strong>.
											<br/>
											<strong class="pull-left">Used-e-cash </strong>  <strong class="pull-right">Rs.<?php echo $totalusedfundecashamount ?></strong>.
											<br/> 
											<strong class="pull-left">transferd-e-cash </strong>  <strong class="pull-right">Rs.<?php echo $getTransferCreditAmountEcash ?></strong>.
											<br/>

											<strong class="pull-left">Used in Entry </strong>  <strong class="pull-right">Rs.<?php echo $commission_used_entry ?></strong>.
											<br/>
											<strong class="pull-left">transferd Commison </strong>  <strong class="pull-right">Rs.<?php echo $getTransferCreditAmountCommi ?></strong>.
										</small>

									</div>
									<div class="panel-footer">
										Lorem ipsum dolor sit amet, consectetur 
									</div>
								</div>
							</div>
						</div>
					</div>   
				</div>