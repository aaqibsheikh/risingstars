<div class="normalheader transition animated fadeIn">
	<div class="hpanel">
		<div class="panel-body">
			<a class="small-header-action" href="">
				<div class="clip-header">
					<i class="fa fa-arrow-up"></i>
				</div>
			</a>

			<div id="hbreadcrumb" class="pull-right m-t-lg">
				<ol class="hbreadcrumb breadcrumb">
					<li>Dashboard</li>
					<!--                    <li>
											<span>introduce by you</span>
										</li>-->
					<li class="active">
						<span>Introduce by you</span>
					</li>
				</ol>
			</div>
			<h2 class="font-light m-b-xs">
				Introduce By You 
			</h2>
			<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
		</div>
	</div>
</div>
<div class="content animate-panel">


		<div class="row">
			<div class="col-lg-12">

				<div class="hpanel">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="showhide"><i class="fa fa-chevron-up"></i></a>
							<a class=""><i class="fa fa-times"></i></a>
						</div>
						<h2>Total Direct Referrals : <?php echo $total_reffrals; ?></h2>
					</div>






					<div class="panel-body">
						<table id="example2" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>

									<th>Serial Number</th>
									<th>Member ID</th>
									<th>Name</th>

									<th>Package</th>
									<th>Joining Date</th>
	<!--                                <th>Designation</th>
									
									<th>Salary</th>-->
								</tr>
							</thead>
							<tbody>

								<?php
								$serial_number = 1;
								foreach ($dataarray->result() as $row) {
									?>    
									<tr>

										<td><?php echo $serial_number++ ?></td>
										<td><?php echo $row->member_Id ?></td>
										<td><?php echo $row->Name ?></td>
										<td><?php echo $row->pakage ?></td>
										<td><?php
											$datetime = explode(" ", $row->joining_date);
											$registration_date = $datetime[0];
											$registration_date = date("j F, Y", strtotime($registration_date));
											echo $registration_date;
											?></td>

									</tr>

								<?php } ?>

							</tbody>
						</table>

					</div>
				</div>
			</div>

		</div>
	</div>
	
