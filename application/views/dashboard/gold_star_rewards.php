

<div class="normalheader transition animated fadeIn">
	<div class="hpanel">
		<div class="panel-body">
			<a class="small-header-action" href="">
				<div class="clip-header">
					<i class="fa fa-arrow-up"></i>
				</div>
			</a>

			<div id="hbreadcrumb" class="pull-right m-t-lg">
				<ol class="hbreadcrumb breadcrumb">
					<li>Dashboard</li>
					<!--                    <li>
											<span>introduce by you</span>
										</li>-->
					<li class="active">
						<span>Gold Star Rewards</span>
					</li>
				</ol>
			</div>
			<h2 class="font-light m-b-xs">
				Gold Star Rewards
			</h2>
			<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
		</div>
	</div>
</div>
<div class="content animate-panel">


	<div class="row">
		<div class="col-lg-12">

			<div class="hpanel">
				<div class="panel-heading">
					<div class="panel-tools">
						<a class="showhide"><i class="fa fa-chevron-up"></i></a>
						<a class=""><i class="fa fa-times"></i></a>
					</div>
					<h2>Your Total Lines : <?php echo $total_reffrals; ?></h2>
				</div>






				<div class="panel-body">
					<table id="example2" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>

								<th>Serial Number</th>
								<th>Member ID</th>
								<th>reward Count</th>
								<th>remaining amount  </th>

<!--                                <th>Designation</th>
		  
		  <th>Salary</th>-->
							</tr>
						</thead>
						<tbody>

							<?php
							$serial_number = 1;
							if(!empty($rewards)){
							foreach ($rewards as $key => $value) {
								?>
								<tr>
									<td><?php echo $serial_number++ ?></td>
									<td><?php $user = $this->aauth->get_user($key); echo $user->username?></td>
									<td><?php echo $value['gold_count']; ?></td>
									<td><?php echo $value['remaing']; ?></td>
								</tr>

							<?php }} ?>

						</tbody>
					</table>

				</div>

			</div>
		</div>

	</div>
</div>
