
<?php //if ($_SESSION['group_id'] == 1) {
	?>
	<!-- <style>
		#admin-link-visible{
			display:block !important;
		}
		#admin-link-rising{
			display:none !important;
		}
	</style> -->
	<?php
//}
	?>
	<div class="content animate-panel"> 
		<div class="row">
			<div class="col-lg-12 text-center m-t-md">
				<h2>
					Welcome to Dashboard
				</h2>

				<p id="admin-link-rising">
					Lorem ipsum <strong>Rising Star International </strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.
				</p>
				<p id="admin-link-visible" style="display:none;">
					go to  <strong><a href="<?php echo site_url('super_admin/index') ?>"> Admin panel </a></strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.
				</p>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-4">
				<div class="hpanel">
					<div class="panel-body text-center h-200">
						<img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

						<h1 class="m-xs">Rs. <?php echo ( ($commission['twenty'] + $commission['eighty'] + $commission['one'] + $commission['zero']) - ($commission['total_paid']) - ($commission_used_entry) ); ?></h1>

						<h3 class="font-extra-bold no-margins text-success">
							Total Earned
						</h3>
						<small>
							<strong class="pull-left">Total Paid </strong>  <strong class="pull-right">Rs.<?php echo $commission['total_paid'] ?></strong>.
							<br/>
							<strong class="pull-left">Total Used in Entry </strong>  <strong class="pull-right">Rs.<?php echo $commission_used_entry ?></strong>.
							<br/>


							<!-- <strong class="pull-left"> </strong>  <strong class="pull-right">Rs.<?php //echo $commission['twenty'] ?></strong>. -->

						</small>
					</div>
					<div class="panel-footer">
						Lorem ipsum dolor sit amet, consectetur 
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="hpanel stats">
					<div class="panel-body h-200">
						<div class="stats-title pull-left">
							<h4>Total Direct/Indirect team levels</h4>
						</div>
						<div class="stats-icon pull-right">
							<img src="<?php echo base_url(); ?>assets/images/dashboard/team-level.png" width="" height="100%" />

						</div>
						<div class="m-t-xl">
							<h3 class="m-b-xs text-success">
								<?php
								echo $userlevel['userlevel'];
								?>
							</h3>
							<span class="font-bold no-margins">
								Team levels
							</span>
							<div class="progress m-t-xs full progress-small">
								<div style="width: 55%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="55" role="progressbar" class=" progress-bar progress-bar-success">
									<span class="sr-only">35% Complete (success)</span>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6">
									<small class="stats-label">Direct</small>
									<h4>
										<?php
										echo $userlevel['direct'];
										?>   
									</h4>
								</div>

								<?php if($userlevel['direct'] >=7){?>
								<div class="col-xs-6">
									<br>
									<h4>
									<strong class="stats-label text-success">Unlimited</strong>
									</h4>
								</div>
								<?php } ?>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					Lorem ipsum dolor sit amet, consectetur 
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="hpanel stats">
				<div class="panel-body h-200">
					<div class="stats-title pull-left">
						<h4><span class="font-bold no-margins">
							Commission Report
						</span></h4>
					</div>
					<div class="stats-icon pull-right">

						<img src="<?php echo base_url(); ?>assets/images/dashboard/pending-commission.png" width="" height="100%" />
					</div>
					<div class="m-t-xl">
						<h1 class="text-success">Rs. <?php echo ( ($commission['twenty'] + $commission['eighty'] + $commission['one'] + $commission['zero']) ) ; ?></h1>

						<small>
							<strong class="pull-left">your 80 % Commission </strong>  <strong class="pull-right">Rs.<?php echo $commission['eighty'] ?></strong>.
							<br/>
							<strong class="pull-left">your 20 % Commission </strong>  <strong class="pull-right">Rs.<?php echo $commission['twenty'] ?></strong>.
							<br/>
							<strong class="pull-left">your 1 % Commission </strong>  <strong class="pull-right">Rs.<?php echo $commission['one'] ?></strong>.
							<br/>
							<strong class="pull-left">JackPot MeeNuu </strong>  <strong class="pull-right">Rs.<?php echo $commission['zero'] ?></strong>.
						</small>
					</div>
				</div>
				<div class="panel-footer">
					dynamic test data in panel 
				</div>
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-lg-4">
			<div class="hpanel">
				<div class="panel-body text-center h-200">
					<img src="<?php echo base_url(); ?>assets/images/dashboard/team-members.png" width="" height="100%" />

					<h1 class="m-xs"><?php
						echo $totalmember;
						?></h1>

						<h3 class="font-extra-bold no-margins text-success">
							Total Team Members
						</h3>
						<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
					</div>
					<div class="panel-footer">
						Lorem ipsum dolor sit amet, consectetur 
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="hpanel">
					<div class="panel-body text-center h-200">
						<img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

						<h1 class="m-xs">Rs. <?php echo $commission['missed'] ?></h1>

						<h3 class="font-extra-bold no-margins text-success">
							Missed Commission
						</h3>
						<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
					</div>
					<div class="panel-footer">
						Lorem ipsum dolor sit amet, consectetur 
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="hpanel">
					<div class="panel-body text-center h-200">
						<img src="<?php echo base_url(); ?>assets/images/dashboard/total comission.png" width="" height="100%" />

						<h1 class="m-xs">Rs. <?php echo $totalfundecashamount; ?></h1>

						<h3 class="font-extra-bold no-margins text-success">
							E Cash
						</h3>
						<small>
							<strong class="pull-left">Availble-e-cash </strong>  <strong class="pull-right">Rs.<?php echo 
							$totalfundecashamount-$totalusedfundecashamount-$getTransferCreditAmountEcash ?></strong>.
							<br/>
							<strong class="pull-left">Used-e-cash </strong>  <strong class="pull-right">Rs.<?php echo $totalusedfundecashamount ?></strong>.
							<br/>
							<strong class="pull-left">transferd-e-cash </strong>  <strong class="pull-right">Rs.<?php echo $getTransferCreditAmountEcash ?></strong>.
							<br/>
							

						</small>
					</div>
					<div class="panel-footer">
						Lorem ipsum dolor sit amet, consectetur 
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-9 animated-panel zoomIn" style="animation-delay: 1.2s;">
				<div class="hpanel">
					<div class="panel-heading">
						<div class="panel-tools">
							<a class="showhide"><i class="fa fa-chevron-up"></i></a>
							<a class="closebox"><i class="fa fa-times"></i></a>
						</div>
						<span>Recent Commissions <span style="margin-left:30px;"></span></span>
					</div>
					<div class="panel-body list">
						<div class="table-responsive project-list">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>ID</th>                           
										<th>Name</th>                              
										<th>&nbsp;</th>
										<th>Amount</th>
										<th>package</th>
										<th>commission%</th>
										<th>transaction_type</th>
										<th>Date</th>  
									</tr>
								</thead>
								<tbody>
									<?php
									$serail_no = 1;
									foreach ($commision_detail as $row) {
										;
										?>  
										<tr>
											<td><div class="icheckbox_square-green checked" style="position: relative;"><input class="i-checks" checked="" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div></td>
											<td>Commision get by  <strong><?php echo $row['Username'] ?></strong>
												<br>
												<small><i class="fa fa-user"></i> User code:<?php echo $row['user_code'] ?></small>
											</td>
											<td>
												<span class="pie" style="display: none;">1/5</span><svg class="peity" height="16" width="16"><path d="M 8 0 A 8 8 0 0 1 15.60845213036123 5.52786404500042 L 8 8" fill="#62cb31"/><path d="M 15.60845213036123 5.52786404500042 A 8 8 0 1 1 7.999999999999998 0 L 8 8" fill="#edf0f5"/></svg>
											</td>
											<td><strong><?php echo $row['amount'] ?></strong></td>
											<td><?php echo $row['package'] ?></td>
											<td><?php echo $row['commision_type'] ?></td>
											<td><?php echo $row['transaction_type'] ?></td>
											<td><small><?php echo $row['created_at'] ?></small></td>

										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
