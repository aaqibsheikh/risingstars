
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<div class="row">
	<div class="col-lg-12">
		<h1 class="text-center">Create Product
			<a href="<?php echo site_url('dashboard/show_product'); ?>" class="btn btn-danger pull-right marginR20">All Products</a>
		</h1>
	</div>
</div>
<?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>

<?php if(isset($msg) || validation_errors() !== ''): ?>
  <div class="alert alert-warning alert-dismissible">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
	  <?= validation_errors();?>
	  <?= isset($msg)? $msg: ''; ?>
  </div>
<?php endif; ?>

<?php echo form_open_multipart("dashboard/edit_product/".$products['id'].""); ?>

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="row">
				<div class="form-group col-lg-12">
					<label>Product Title</label>
					<input type="text" class="form-control" name="title" value="<?php if(isset($products['title'])) echo $products['title'];?>">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-6">
					<label>Cost Price</label>
					<input type="text" class="form-control" name="cost_price" value="<?php if(isset($products['cost_price'])) echo $products['cost_price'];?>">
				</div>

				<div class="form-group col-lg-6">
					<label>Retail Price</label>
					<input type="text" class="form-control" name="retail_price" value="<?php if(isset($products['retail_price'])) echo $products['retail_price'];?>">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-12">
					<label>Select Images</label>
					<input type="file" class="form-control" id="upload" name="upload_files[]" multiple>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div id="image-holder"></div>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-6">
				  <label for="status">Status</label>
				  <select class="form-control" name="status">
					<option <?php if(isset($products['status'])) echo $products['status'] == '1'? 'selected':''; ?> value="1">Active</option>
					<option <?php if(isset($products['status'])) echo $products['stock'] == '0'? 'selected':''; ?> value="0">Inactive</option>
				  </select>
				</div>

				<div class="form-group col-lg-6">
				  <label for="status">Stock</label>
				  <select class="form-control" name="stock">
					<option <?php if(isset($products['stock'])) echo $products['stock'] == '1'? 'selected':''; ?> value="1">In</option>
					<option <?php if(isset($products['stock'])) echo $products['stock'] == '0'? 'selected':''; ?> value="0">Out</option>
				  </select>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-3">
					<label>MeeNuu stars</label>
					<input type="text" class="form-control" name="meenuu_stars" value="<?php if(isset($products['meenuu_stars'])) echo $products['meenuu_stars'];?>">
				</div>

				<div class="form-group col-lg-2">
					<label>SS</label>
					<input type="text" class="form-control" name="ss" value="<?php if(isset($products['ss'])) echo $products['ss'];?>">
				</div>

				<div class="form-group col-lg-2">
					<label>GS</label>
					<input type="text" class="form-control" name="gs" value="<?php if(isset($products['gs'])) echo $products['gs'];?>">
				</div>

				<div class="form-group col-lg-2">
					<label>PS</label>
					<input type="text" class="form-control" name="ps" value="<?php if(isset($products['ps'])) echo $products['ps'];?>">
				</div>

				<div class="form-group col-lg-2">
					<label>DS</label>
					<input type="text" class="form-control" name="ds" value="<?php if(isset($products['ds'])) echo $products['ds'];?>">
				</div>
			</div>


				<div class="form-group col-lg-12">
					<input type="submit" class="btn btn-success" name="edit_product" value="Update">
				</div>

		 
			</div>
		</div>
	</div>
<?php form_close() ?>


<style>

	.img-width{
		width: 210px !important;
		height: 190px !important;
		margin-left: 65px !important;
		margin-right: 20px !important;
	}

	.marginR20{margin-right: 20px !important;}

</style>

<script>
	$("#upload").on('change', function () {

	 //Get count of selected files
	 var countFiles = $(this)[0].files.length;

	 var imgPath = $(this)[0].value;
	 var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
	 var image_holder = $("#image-holder");
	 console.log(image_holder);
	 //image_holder.empty();

	 if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		 if (typeof (FileReader) != "undefined") {

			 //loop for each file selected for uploaded.
			 for (var i = 0; i < countFiles; i++) {

				 var reader = new FileReader();
				 reader.onload = function (e) {
					 $("<img />", {
						"src": e.target.result,
						"class": "thumb-image img-width"
					 }).appendTo(image_holder);
				 }

				 image_holder.show();
				 reader.readAsDataURL($(this)[0].files[i]);
			 }

		 } else {
			 alert("This browser does not support FileReader.");
		 }
	 } else {
		 alert("Pls select only images");
	 }
 });
</script>