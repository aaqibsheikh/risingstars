<div class="normalheader transition animated fadeIn">
	<div class="hpanel">
		<div class="panel-body">
			<a class="small-header-action" href="">
				<div class="clip-header">
					<i class="fa fa-arrow-up"></i>
				</div>
			</a>

			<div id="hbreadcrumb" class="pull-right m-t-lg">

				<ol class="hbreadcrumb breadcrumb">
					<li><a href="<?php echo site_url('dashboard/index') ?>">Dashboard</a></li>

					<!--                    <li>
											<span>introduce by you</span>
										</li>-->
					<li class="active">
						<span>Account statement</span>
					</li>
				</ol>
			</div>
			<h2 class="font-light m-b-xs">
				Account Statement
			</h2>
			<small>Account Statement Details</small>
		</div>
	</div>
</div>

<div class="content animate-panel">
	<div class="row">
		<div class="col-lg-12">

			<div class="hpanel">
				<div class="panel-heading">
					<div class="panel-tools">
						<a class="showhide"><i class="fa fa-chevron-up"></i></a>
						<a class=""><i class="fa fa-times"></i></a>
					</div>
					<h3>account statement</h3>
				</div>

				<div class="panel-body">
					<p class="">
						<span class="pull-right" style="margin-left:30px;">Total Paid<strong><?php echo $total_paid ?></strong></span>
						<span class="pull-left">Total Recived:<strong><?php echo $total_recived ?></strong></span>

					</p>
					<table id="process-withdraw" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Serial No</th>
								<th>Name</th>
								<th>Date</th>                           
								<th style="width:30%">Description</th>                              
								<th>Received </th>
								<th>Paid</th>
							</tr>
						</thead>
					   <tbody>
							<?php
							$serail_no = 1;
							foreach ($fund_e_cash_histry as $row) {
								$id = urlencode(base64_encode($row['id']));
								?>  
								<tr>
									<td><?php echo 'ahmad';echo $serail_no++ ?></td>
									<td><?php echo $row['created_at']; ?></td>
									<td><?php echo $row['member_code']; ?></td>
									<td><?php $descripion = ((!isset($row['description'])) ? "withdraw by Admin" : $row['description']);echo $descripion; ?></td>
									<td><?php  $credit = ((!isset($row['credit'])) ? "00" : $row['credit']);echo $credit; ?></td>
									<td><?php
										$debit = ((!isset($row['amount'])) ? "00" : $row['amount']);
										echo $debit;
										?>
									</td>
								</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>