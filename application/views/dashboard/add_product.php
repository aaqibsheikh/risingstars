
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<div class="container">
<div class="row">
	<div class="col-lg-12">
		<h1 class="text-center">Add Product
			<a href="<?php echo site_url('dashboard/show_product'); ?>" class="btn btn-danger pull-right marginR20">All Products</a>
		</h1>
	</div>
</div>
<?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>

<?php if(isset($msg) || validation_errors() !== ''): ?>
  <div class="alert alert-warning alert-dismissible">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	  <h4><i class="icon fa fa-warning"></i> Alert!</h4>
	  <?= validation_errors();?>
	  <?= isset($msg)? $msg: ''; ?>
  </div>
<?php endif; ?>

<?php echo form_open_multipart('dashboard/add_product'); ?>

	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="form-group col-lg-12">
					<label>Product Title</label>
					<input type="text" class="form-control" name="title">
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-lg-6">
					<label>Cost Price</label>
					<input type="text" class="form-control" name="cost_price">
				</div>

				<div class="form-group col-lg-6">
					<label>Retail Price</label>
					<input type="text" class="form-control" name="retail_price">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-6">
				  <label for="status">Status</label>
				  <select class="form-control" name="status">
					<option value="1">Active</option>
					<option value="0">Inactive</option>
				  </select>
				</div>

				<div class="form-group col-lg-6">
				  <label for="status">Stock</label>
				  <select class="form-control" name="stock">
					<option value="1">In</option>
					<option value="0">Out</option>
				  </select>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-3">
					<label>MeeNuu stars</label>
					<input type="text" class="form-control" name="meenuu_stars">
				</div>

				<div class="form-group col-lg-2">
					<label>SS</label>
					<input type="text" class="form-control" name="ss">
				</div>

				<div class="form-group col-lg-2">
					<label>GS</label>
					<input type="text" class="form-control" name="gs">
				</div>

				<div class="form-group col-lg-2">
					<label>PS</label>
					<input type="text" class="form-control" name="ps">
				</div>

				<div class="form-group col-lg-2">
					<label>DS</label>
					<input type="text" class="form-control" name="ds">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-12">
					<label>Select Feature Image</label>
					<input type="file" class="form-control" name="featured_image">
				</div>
			</div>

			<div class="row">
				<div class="form-group col-lg-12">
					<label>Select Images</label>
					<input type="file" class="form-control" id="files" name="files[]" multiple>
				</div>
			</div>

			<div id="selectedFiles"></div>

			<div class="marginT30"></div>


				<div class="form-group col-lg-12">
					<input type="submit" class="btn btn-success" name="add_product" value="Add Product">
				</div>

		 
			</div>
		</div>
	</div>
<?php form_close() ?>
</div>

<style>

	.img-width{
		width: 100px !important;
		height: 100px !important;
		margin-left: 0px !important;
		margin-right: 20px !important;
	}

	.block{display: inline-block;}

	.marginR20{margin-right: 20px !important;}
	.marginT30{margin-top: 30px !important;}

	.cross{
	  position: relative;
	  top: -43px;
	  right: 38px;
	  color: #000;
	  background: #fff;
	  font-weight: bold;
	  border-radius: 50px;
	  padding: 2px 7px 2px 7px;
	  text-align: center;
	  font-size: 11px;
	  cursor: pointer;
	}

</style>

<script>
	// $(document).ready(function(){

	//     var img_array = [];
	//     $(document).on('click','.thumb-image', function(){
	//         var img_index = $(this).index();
	//         var image_array = jQuery.makeArray($('#upload')[0].files);
	//         image_array.splice(img_index, 1);
	//         $(this).remove();

			
	//     });
		
	//     $("#upload").on('change', function () {
	//       var img_obj = this.files;
		  
	//       for(var i = 0 ; i< this.files.length; i++){
			
	//         img_array.push(img_obj[i].name);
	//         $('#upload_images').append(`
	//             <input type="hidden" name="upload_images" value="`+img_obj[i].name+`"/>
	//         `);

	//       }



	//      //Get count of selected files
	//      var countFiles = $(this)[0].files.length;

	//      var imgPath = $(this)[0].value;
	//      var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
	//      var image_holder = $("#image-holder");

	//      if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
	//          if (typeof (FileReader) != "undefined") {

	//              //loop for each file selected for uploaded.
	//              for (var i = 0; i < countFiles; i++) {

	//                  var reader = new FileReader();
	//                  reader.onload = function (e) {
	//                      $("<img />", {
	//                         "src": e.target.result,
	//                         "class": "thumb-image img-width"
	//                      }).appendTo(image_holder);
	//                  }

	//                  image_holder.show();
	//                  reader.readAsDataURL($(this)[0].files[i]);
	//              }

	//          } else {
	//              alert("This browser does not support FileReader.");
	//          }
	//      } else {
	//          alert("Pls select only images");
	//      }
	//     });
	// });
</script>

 <script>
	var selDiv = "";
	var storedFiles = [];
	
	$(document).ready(function() {
		$("#files").on("change", handleFileSelect);
		
		selDiv = $("#selectedFiles"); 
		$("#myForm").on("submit", handleForm);
		
		$("body").on("click", ".selFile", removeFile);
	});
		
	function handleFileSelect(e) {
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		filesArr.forEach(function(f) {          

			if(!f.type.match("image.*")) {
				return;
			}
			storedFiles.push(f);
			
			var reader = new FileReader();
			reader.onload = function (e) {
				var html = "<div class='block'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='img-rounded img-width' title='Click to remove'><span class='selFile cross'>X</span></div>";
				selDiv.append(html);
				
			}
			reader.readAsDataURL(f); 
		});
		
	}
		
	function handleForm(e) {
		e.preventDefault();
		var data = new FormData();
		
		for(var i=0, len=storedFiles.length; i<len; i++) {
			data.append('files', storedFiles[i]);   
		}
		
		var xhr = new XMLHttpRequest();
		xhr.open('POST', 'handler.cfm', true);
		
		xhr.onload = function(e) {
			if(this.status == 200) {
				console.log(e.currentTarget.responseText);  
				alert(e.currentTarget.responseText + ' items uploaded.');
			}
		}
		
		xhr.send(data);
	}
		
	function removeFile(e) {
		var file = $(this).parent().data("file");
		for(var i=0;i<storedFiles.length;i++) {
			if(storedFiles[i].name === file) {
				storedFiles.splice(i,1);
				break;
			}
		}
		$(this).parent().remove();
	}
	</script>