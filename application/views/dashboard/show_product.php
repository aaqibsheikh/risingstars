
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="text-center">Products
				<a href="<?php echo site_url('dashboard/add_product') ?>" class="btn btn-danger pull-right marginR20">Add Product</a>
			</h2>
		</div>
	</div>
  <table class="table table-bordered">
	<thead>
	  <tr>
		<th>Title</th>
		<th>Cost Price</th>
		<th>Retail Price</th>
		<th>Images</th>
		<th>Status</th>
		<th>Stock</th>
		<th>Meenu Stars</th>
		<th>SS</th>
		<th>GS</th>
		<th>PS</th>
		<th>DS</th>
		<th>Action</th>
	  </tr>
	</thead>
	<tbody>
	<?php foreach($products->result() as $p): ?>
	  <tr>
		<td><?php echo $p->title; ?></td>
		<td><?php echo $p->cost_price; ?></td>
		<td><?php echo $p->retail_price; ?></td>
		<td><?php
				$images = explode(',', $p->images); 
				foreach($images as $img):
			?>
			<img src="<?php echo base_url(); ?>/assets/upload/<?php echo $img;?>" width="50" height="50">
			<?php endforeach; ?></td>
		<td><?php echo $p->status == 1 ? 'Active': 'Inactive'; ?></td>
		<td><?php echo $p->stock == 1 ? 'In': 'Out'; ?></td>
		<td><?php echo $p->meenuu_stars; ?></td>
		<td><?php echo $p->ss; ?></td>
		<td><?php echo $p->gs; ?></td>
		<td><?php echo $p->ps; ?></td>
		<td><?php echo $p->ds; ?></td>
		<td>
			<a title="Edit" class="btn btn-sm btn-primary pull-left" href="<?php echo base_url(); ?>dashboard/edit_product/<?php echo $p->id; ?>" style="margin-right: 10px;"> 
				<i class="fa fa-pencil-square-o"></i>
			</a>
			<a title="Delete" class="btn btn-sm btn-danger pull-left" 
			href="<?php echo base_url(); ?>dashboard/delete_product/<?php echo $p->id; ?>"> 
				<i class="fa fa-trash-o"></i>
			</a>
		</td>
	  </tr>
	<?php endforeach; ?>
	</tbody>
  </table>
</div>
