<div class="normalheader transition animated fadeIn">
	<div class="hpanel">
		<div class="panel-body">
			<a class="small-header-action" href="">
				<div class="clip-header">
					<i class="fa fa-arrow-up"></i>
				</div>
			</a>

			<div id="hbreadcrumb" class="pull-right m-t-lg">
				<ol class="hbreadcrumb breadcrumb">
					<li>Dashboard</li>
					<!--                    <li>
											<span>introduce by you</span>
										</li>-->
					<li class="active">
						<span>Register Distributor</span>
					</li>
				</ol>
			</div>
			<h2 class="font-light m-b-xs">
				Introduce By You 
			</h2>
			<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
		</div>
	</div>
</div>


<h1 class="text-center">
	Please Register to System
</h1>
<?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>

<?php echo form_open('user/register', array('id' => 'loginForm')); ?>

<div class="row">
	<div class="form-group col-lg-12">
		<label>Full Name</label>
		<input type="text" value="<?php echo set_value('username') ?>" id="" required="required" class="form-control" name="username">
	</div>
	<div class="form-group col-lg-4">
		<label>Father name</label>
		<input type="" value="<?php echo set_value('fname') ?>" id="" required="required" class="form-control" name="fname">
	</div>
	<div class="form-group col-lg-4">
		<label>CNIC #</label>
		<input type="" value="<?php echo set_value('cnic') ?>" id="" placeholder="13 digit CNIC number" required="required" class="form-control" name="cnic">
	</div>
	<div class="form-group col-lg-4">
		<label>Full Address </label>
		<input type="" value="<?php echo set_value('address') ?>" id="" required="required" class="form-control" name="address">
	</div>
	<div class="form-group col-lg-4">
		<label>kin Name</label>
		<input type="" value="<?php echo set_value('kinname') ?>" id="" required="required" class="form-control" name="kinname">
	</div>
	<div class="form-group col-lg-4">
		<label>kin relation</label>
		<input type="" value="<?php echo set_value('kinrelation') ?>" id="" required="required" class="form-control" name="kinrelation">
	</div>
	<div class="form-group col-lg-4">
		<label>kin CNIC #</label>
		<input type="" value="<?php echo set_value('kincnic') ?>" id="" placeholder="13 digit CNIC number" required="required" class="form-control" name="kincnic">
	</div> 


	<div class="form-group col-lg-4">
		<label>Mobile #</label>
		<input type="" value="<?php echo set_value('mobile') ?>" id="" placeholder="10 digit Mobile number" required="required" class="form-control" name="mobile">
	</div>
	<div class="form-group col-lg-4">
		<label>Password</label>
		<input type="password" value="<?php echo set_value('password') ?>" id="" required="required" class="form-control" name="password">
	</div>
	<div class="form-group col-lg-4">
		<label>Repeat Password</label>
		<input type="password" value="<?php echo set_value('conpassword') ?>" id="" required="required" class="form-control" name="conpassword">
	</div>
	<div class="form-group col-lg-4">
		<label>Email Address</label>
		<input type="email"  value="<?php echo set_value('email') ?>"  id="" required="required" class="form-control" name="email">
	</div>
	<div class="form-group col-lg-4">
		<label>introducer code</label>
		<input type="" value="<?php echo set_value('introducer') ?>" id="" required="required" class="form-control" name="introducer">
	</div>

	<div class="form-group col-lg-4">
		<label>joining package</label>

		<select class="form-control" name="package">

			<?php foreach ($data->result() as $row) { ?>    
				<option value="<?php echo $row->Package_ID; ?>"><?php echo $row->PackageName . ' ' . $row->PackagePrice; ?></option>
			<?php } ?>

		</select>
	</div>

</div>
<br>
<br>
<br>
<br>
<br>
<div class="text-center">
	<!--<button type="submit" class="btn btn-success">Register</button>-->
	<?php echo form_submit('submit', 'Register','', 'class="btn btn-success me-hide"'); ?>
	
	<a class="btn btn-default" href="login" >Cancel</a>
</div>

<?php form_close() ?>