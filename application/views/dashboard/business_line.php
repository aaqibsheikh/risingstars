<div class="normalheader transition animated fadeIn">
	<div class="hpanel">
		<div class="panel-body">
			<a class="small-header-action" href="">
				<div class="clip-header">
					<i class="fa fa-arrow-up"></i>
				</div>
			</a>

			<div id="hbreadcrumb" class="pull-right m-t-lg">

				<ol class="hbreadcrumb breadcrumb">
					<li>Dashboard</li>
					<li class="active">
						<span>introduce by you</span>
					</li>
				</ol>
			</div>
			<h2 class="font-light m-b-xs">
				Your business line 
			</h2>
			<small>Lorem ipsum dolor sit amet, consectetur adipiscing elit vestibulum.</small>
		</div>
	</div>
</div>

<br>

<div class="normalheader transition animated fadeIn">
	<div class="hpanel">
		<div class="panel-body">
			<a class="small-header-action" href="">
				<div class="clip-header">
					<i class="fa fa-arrow-up"></i>
				</div>
			</a>

			<div id="hbreadcrumb" class="pull-right m-t-lg">

				<span><?php
					if (is_array($crumb)) {
						echo "<ol class='hbreadcrumb breadcrumb'><li><a href='#'>" . implode("</li><li>", $crumb) . "</a></li></ol>";
					} else {
						echo "<ol class='hbreadcrumb breadcrumb'><li><a href='#'>" . $_SESSION['code'] . "</a></li></ol>";
					}
					?>      
				</span>
			</div>
			<h2 class="font-light m-b-xs">
				members tree 
			</h2>
			<small>Lorem ipsum dolor</small>
		</div>
	</div>
</div>
<div class="content animate-panel">
	<div class="row">
		<div class="col-lg-12">

			<div class="hpanel">
				<div class="panel-heading">
					<div class="panel-tools">
						<a class="showhide"><i class="fa fa-chevron-up"></i></a>
						<a class=""><i class="fa fa-times"></i></a>
					</div>
					<h2>Members<?php //echo $total_reffrals?></h2>
				</div>

				<div class="panel-body">
					<table id="example2" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th style="width:100px">Serial Number</th>
								<th>Name</th>
								<th>Member ID</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$serial_number = 1;
							foreach ($dataarray as $row) {
								?>    
								<tr>
									<td><?php echo $serial_number++ ?></td>
									<td><a href="" style="margin-left: 30px;"><?php echo $row['Name'] ?></a></td>
									<td><?php echo $row['member_Id'] ?></td>
									<td>
										<?php 
										if($row['children_count']>0){
										echo '<a class="pull-left"  href="'.site_url('dashboard/business_line/' . $row['userid']).'"> <span class="glyphicon glyphicon-plus-sign"></span> </a>';
										}
										?>
										
										<a href="<?php echo site_url('super_admin/user_detail') . '/' . $row['userid'] ?>" class="pull-right"> <span class="glyphicon glyphicon-user"></span> </a>
									</td>
								</tr>

							<?php } ?>
						</tbody>
					</table>

				</div>
			</div>
		</div>

	</div>
</div>

