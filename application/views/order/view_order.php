<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<div class="content animate-panel" id="printarea">
	<div class="row">
		<div class="col-lg-12">

			<div class="hpanel">


				<div class="panel-body">
					<img width="150" src="<?php echo base_url(); ?>assets/upload/meenuulogo.png">
					<button class="btn btn-success pull-right" id="print">Print</button>
					<div class="row">
						<div class="col-lg-12 marginT30">
							<h4 class="inline">Order ID: 2323</h4>
							<div class="pull-right inline">
								<h4 class="marginR100">Shipping Address</h4>
								<span>Name: <?php echo $order->name; ?></span><br>
								<span>Mobile: <?php echo $order->mobile_number; ?></span><br>
								<span>Address: <?php echo $order->address; ?></span><br>
							</div>
						</div>
					</div>
					<div class="marginT30"></div>
					<div class="row">
						<div class="col-lg-12">
							<table style="width: 100%;" id="example2" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Product Name</th>
										<th>Price</th>
										<th>Quantity</th>
										<th>Brand</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr class="gradeC">
										<td><?php echo $order->pro_title; ?></td>
										<td><?php echo $order->product_price; ?></td>
										<td><?php echo $order->quantity; ?></td>
										<td><?php echo $order->brand_name; ?></td>
										<td><?php echo $order->status; ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
/*
	.marginT30{margin-top:30px;}
	.inline{display: inline-block;}
	.marginR100{margin-right:100px;}
	@media print
{

	.marginT30{margin-top:30px;}
	.inline{display: inline-block;}
	.marginR100{margin-right:100px;}
}*/
</style>
<script>
	$(document).ready(function(){
		// $("#print").click(function () {
	 //    	// $("#printarea").print();
	 //    	printData();
		// });

		function printData()
		{
		   var divToPrint=document.getElementById("printarea");
		   newWin= window.open("");
		   newWin.document.write(divToPrint.outerHTML);
		   newWin.print();
		   newWin.close();
		}

		$('button').on('click',function(){
		printData();
		})
	});

</script>