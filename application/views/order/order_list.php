<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>



<div class="content animate-panel">
	<div class="row">
		<div class="col-lg-12">

			<div class="hpanel">
				<div class="panel-heading">
					<div class="panel-tools">
						<a class="showhide"><i class="fa fa-chevron-up"></i></a>

					</div>
					<h2>Orders</h2>
				</div>

				<div class="panel-body">
					<table id="example2" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Product Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Date</th>
								<th>Promo Code</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
$i = 0;
if (isset($all_orders)) {
	foreach ($all_orders as $value) {
		$i++;

		?>
									<tr class="gradeC">
										<td><?php echo $i; ?></td>
										<td><?php echo $value->pro_title; ?></td>
										<td><?php echo $value->product_price; ?></td>
										<td><?php echo $value->quantity; ?></td>
										<td><?php echo $value->order_date; ?></td>
										<?php if (isset($value->promo_code)): ?>
											<td>
												<input type="text" name="promo_code" class="promo_code" value="<?php echo $value->promo_code; ?>">
												<input type="hidden" name="order_id" class="order_id" value="<?php echo $value->id; ?>">
											</td>
											<?php else: ?>
												<td>
													<input type="text" name="promo_code" class="promo_code" value="">
													<input type="hidden" name="order_id" class="order_id" value="<?php echo $value->id; ?>">
												</td>
											<?php endif;?>
											<td>
												<select name="status" class="status">
													<option value="#" selected disabled>Select Status</option>
													<?php if ($value->status == "Completed" || $value->status == "Cancelled"): ?>
													<option value="Completed" <?php if ($value->status == "Completed") {
			echo 'selected';
		}
		?> disabled>Completed</option>
													<option value="Cancelled" <?php if ($value->status == "Cancelled") {
			echo 'selected';
		}
		?> disabled>Cancelled</option>
													<?php else: ?>

													<option value="Completed">Completed</option>
													<option value="Cancelled">Cancelled</option>

													<?php endif;?>

												</select>
											</td>
											<td><a href="<?php echo base_url(); ?>order/view_order/<?php echo $value->id; ?>" class="btn btn-primary btn-sm">View</a></td>
										</tr>
									<?php }}?>
								</tbody>
							</table>

						</div>
					</div>
				</div>

			</div>
		</div>
























		<!-- <div id="page-wrapper">

			<div class="row">
				<div class="col-lg-10 margin-auto marginT30">

					<div class="panel panel-default">
						<div class="panel-heading">
							Order List
						</div>
						<p class="text-success"> <?php if (isset($success_message)) {echo $success_message;}?>
					</p>
					<div class="alert alert-success">
						<?php //echo $this->session->flashdata('flsh_msg'); ?>
						<?php echo $this->session->flashdata('flsh_msg'); ?>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>#</th>
										<th>Product Name</th>
										<th>Product Price</th>
										<th>Product Quantity</th>
										<th>Date</th>
										<th>Promo Code</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php
$i = 0;
if (isset($all_orders)) {
	foreach ($all_orders as $value) {
		$i++;

		?>
											<tr class="gradeC">
												<td><?php echo $i; ?></td>
												<td><?php echo $value->pro_title; ?></td>
												<td><?php echo $value->product_price; ?></td>
												<td><?php echo $value->quantity; ?></td>
												<td><?php echo $value->order_date; ?></td>
												<?php if (isset($value->promo_code)): ?>
													<td>
														<input type="text" name="promo_code" class="promo_code" value="<?php echo $value->promo_code; ?>">
														<input type="hidden" name="order_id" class="order_id" value="<?php echo $value->id; ?>">
													</td>
													<?php else: ?>
														<td>
															<input type="text" name="promo_code" class="promo_code" value="">
															<input type="hidden" name="order_id" class="order_id" value="<?php echo $value->id; ?>">
														</td>
													<?php endif;?>
													<td>
														<select name="status" class="status">
															<option value="#">Select Status</option>
															<option value="Completed" <?php if ($value->status == "Completed") {
			echo 'selected';
		}
		?>>Completed</option>
															<option value="Cancelled" <?php if ($value->status == "Cancelled") {
			echo 'selected';
		}
		?>>Cancelled</option>
														</select>
													</td>
												</tr>
											<?php }}?>
										</tbody>
									</table>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div> -->
			<script src="<?php echo base_url() ?>assets/back/plugins/dataTables/jquery.dataTables.js"></script>

			<script>
				$(document).ready(function () {
					$('#dataTables-example').dataTable();

					$(document).on('change','.status',function(){
						var status 		= $(this).val();
						var promo_code 	= $(this).parent().siblings().find('.promo_code').val();
						var order_id 	= $(this).parent().siblings().find('.order_id').val();
						if(promo_code == ''){
							alert('Enter Promo Code');
							location.reload();
						}
						else{
							$.ajax({
								url: site_url+'order/update_order/',
								data:{status:status, promo_code:promo_code, order_id:order_id},
								type: "POST",
								error: function () {
									alert("An error ocurred.");
								},
								success: function (data) {
									location.reload();
								}
							});
						}

					});

				});


			</script>