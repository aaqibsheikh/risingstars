<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/back/ckeditor/ckeditor.js"></script>
<!--  page-wrapper -->
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-10 margin-auto marginT30">
			<!-- Form Elements -->
			<div class="panel panel-default">
				<?php echo $this->session->flashdata('flsh_msg'); ?>
				<div class="row">
					<div class="col-lg-6">
						<h4 class="error">
						<?php $msg = $this->session->userdata('error_image');
echo $msg;
$this->session->unset_userdata('error_image');
?>
						</h4>
						<div class="panel-heading">
							Edit Product
							<h4 class="success"> <?php echo $this->session->flashdata('update_pro_msg'); ?></h4>
						</div>
					</div>
					<div class="col-lg-6">
						<a href="<?php echo site_url('product/product_list'); ?>" class="btn btn-product-bg marginTR10 pull-right">Product List</a>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<h5 style='color:red'> <?php echo validation_errors(); ?></h5>
							<?php echo form_open_multipart('product/update_product', ''); ?>

							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label>Add Product Title</label>
										<input type="text" class="form-control" value="<?php echo $all_product[0]['pro_title'] ?>" name="pro_title" required>
										<input type="hidden" class="form-control" value="<?php echo $all_product[0]['pro_id'] ?>" name="pro_id">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Select Category</label>
										<select class="form-control pro_cat" name="pro_cat" required>
											<option>Select One</option>
											<?php
foreach ($all_cat as $category) {?>
											<option
												<?php if ($all_product[0]['pro_cat'] == $category->category_id) {?>
												selected="selected";
												<?php }?>
												value="<?php echo $category->category_id; ?>"><?php echo $category->category_name ?>

											</option>

											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Select Sub Category</label>
										<select class="form-control pro_sub_cat" name="pro_sub_cat" required>
											<option>Select One</option>
											<?php
foreach ($all_sub_cat as $sub_category) {?>
											<option
												<?php if ($all_product[0]['pro_sub_cat'] == $sub_category->sub_cat_id) {?>
												selected="selected";
												<?php }?>
												value="<?php echo $sub_category->sub_cat_id; ?>"><?php echo $sub_category->sub_category_name ?>

											</option>

											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Select Brand</label>
										<select class="form-control" name="pro_brand">
											<option>Select One</option>
											<?php $all_brand = $this->ProductModel->get_all_brand()?>
											<?php foreach ($all_brand as $brand) {?>
											<option
												<?php if ($all_product[0]['pro_brand'] == $brand->brand_id) {?>
												selected="selected";
												<?php }?>
												value="<?php echo $brand->brand_id; ?>"><?php echo $brand->brand_name; ?>
											</option>
											<?php }?>
										</select>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-lg-6">
										<div class="form-group">
											<label>Cost Price</label>
											<input type="number" class="form-control" value="<?php echo $all_product[0]['cost_price'] ?>" name="cost_price" required>
										</div>
									</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Product Price</label>
										<input type="number" class="form-control" value="<?php echo $all_product[0]['pro_price'] ?>" name="pro_price" required="">
									</div>
								</div>

								<div class="col-lg-6">
										<div class="form-group">
											<label>Discount Price</label>
											<input type="number" class="form-control" value="<?php echo $all_product[0]['discount_price'] ?>" name="discount_price" required>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="form-group">
											<label>Price After Discount</label>
											<input type="number" class="form-control" value="<?php echo $all_product[0]['price_after_discount'] ?>" name="price_after_discount" required>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="form-group">
											<label>Delivery Charges</label>
											<input type="number" class="form-control" value="<?php echo $all_product[0]['delivery_charges'] ?>" name="delivery_charges" required>
										</div>
									</div>

								<div class="col-lg-6">
									<div class="form-group">
										<label>Product Quantity</label>
										<input type="number" class="form-control" value="<?php echo $all_product[0]['pro_quantity'] ?>" name="pro_quantity" required="">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Product Status</label>
										<select class="form-control" name="pro_status">

											<?php if ($all_product[0]['pro_status'] == 1) {?>
											<option selected="" value="1">Enable</option>
											<option  value="2">Disable</option>
											<?php } elseif ($all_product[0]['pro_status'] == 2) {?>
											<option  value="1">Enable</option>
											<option selected="" value="2">Disable</option>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Product Availability</label>
										<select class="form-control" name="pro_availability">

											<?php if ($all_product[0]['pro_availability'] == 1) {?>
											<option selected="" value="1">In Stock</option>
											<option value="2">Out Of Stock</option>
											<option value="3">Up Comming</option>
											<?php } elseif ($all_product[0]['pro_availability'] == 2) {?>
											<option value="1">In Stock</option>
											<option selected="" value="2">Out Of Stock</option>
											<option value="3">Up Comming</option>
											<?php } elseif ($all_product[0]['pro_availability'] == 3) {?>
											<option value="1">In Stock</option>
											<option value="2">Out Of Stock</option>
											<option selected="" value="3">Up Comming</option>
											<?php }?>
										</select>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-lg-2">
									<div class="form-group">
										<label>Jackpot</label>
										<input type="number" class="form-control" value="<?php echo $all_product[0]['jackpot'] ?>" name="jackpot">
									</div>
								</div>

								<div class="col-lg-2">
									<div class="form-group">
										<label>SS</label>
										<input type="number" class="form-control" value="<?php echo $all_product[0]['ss'] ?>" name="ss">
									</div>
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										<label>GS</label>
										<input type="number" class="form-control" value="<?php echo $all_product[0]['gs'] ?>" name="gs">
									</div>
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										<label>PS</label>
										<input type="number" class="form-control" value="<?php echo $all_product[0]['ps'] ?>" name="ps">
									</div>
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										<label>DS</label>
										<input type="number" class="form-control" value="<?php echo $all_product[0]['ds'] ?>" name="ds">
									</div>
								</div>
							</div>
							<div class="row">

									<div class="col-lg-2">
										<div class="form-group">
											<label>20%</label>
											<input type="number" class="form-control" value="<?php echo $all_product[0]['twenty_percent'] ?>" name="twenty_percent">
										</div>
									</div>

									<div class="col-lg-2">
										<div class="form-group">
											<label>1%</label>
											<input type="number" class="form-control" value="<?php echo $all_product[0]['one_percent'] ?>" name="one_percent">
										</div>
									</div>
							</div>

							<div class="form-group">
								<label>Upload Featured Image</label>
								<input type="file" name="featured_image" id="featured_image">
							</div>

							<div id="selectedFeaturedFiles">

								<?php if (!empty($all_product[0]['pro_image'])): ?>
								<div class='block'>
									<img src="<?php echo base_url() . 'assets/upload/' . $all_product[0]['pro_image']; ?>" data-file='<?php echo $all_product[0]['pro_image']; ?>' class='img-rounded img-width' title='Click to remove'>
									<input type='hidden' name='featured_image' value='<?php echo $all_product[0]['pro_image']; ?>' />
									<span class='selFeaturedFile cross'>X</span>
								</div>
								<?php endif;?>

							</div>

							<div class="form-group">
								<label>Upload Product Images</label>
								<input type="file" id="files" name="files[]" multiple>
							</div>

							<div id="selectedFiles">
								<?php foreach ($all_product[0]['images'] as $img): ?>
								<?php if (!empty($img)): ?>
								<div class='block'>
									<img src="<?php echo base_url() . 'assets/upload/' . $img; ?>" data-file='<?php echo $img; ?>' class='img-rounded img-width' title='Click to remove'>
									<input type='hidden' name='product_images[]' value='<?php echo $img; ?>' />
									<span class='selFile cross'>X</span>
								</div>
								<?php endif;?>
								<?php endforeach;?>
							</div>

							<div class="marginT30"></div>

							<div class="form-group">
								<label>Top Product</label>
								<div class="checkbox">
									<label>
										<?php if ($all_product[0]['top_product'] == 1) {?>
										<input type="checkbox" name="top_product" value="1" checked="">Select top product
										<?php } else {?>
										<input type="checkbox" name="top_product" value="1">Select top product
										<?php }?>
									</label>
								</div>
							</div>

							<div class="form-group">
									<label>Daily Need Slider Product</label>
									<div class="checkbox">
										<label>
											<?php if ($all_product[0]['daily_need_product'] == 1) {?>
											<input type="checkbox" name="daily_need_product" checked value="1">Select daily need slider product
											<?php } else {?>
											<input type="checkbox" name="daily_need_product" value="1">Select daily need slider product
											<?php }?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<label>Discount Product</label>
									<div class="checkbox">
										<label>
											<?php if ($all_product[0]['discount_product'] == 1) {?>
											<input type="checkbox" name="discount_product" checked value="1">Select discount product
											<?php } else {?>
											<input type="checkbox" name="discount_product" value="1">Select discount product
											<?php }?>
										</label>
									</div>
								</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<label>Add Product Description</label>
										<textarea  id="ck" class="form-control" rows="3" name="pro_desc">
										<?php echo $all_product[0]['pro_desc'] ?>
										</textarea>
										<script>CKEDITOR.replace('ck')</script>
									</div>
								</div>
							</div>

							<input type="submit" class="btn btn-primary" value="Update">
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Form Elements -->
		</div>
	</div>
</div>
<!-- end page-wrapper -->

<style>

	.img-width{
		width: 100px !important;
		height: 100px !important;
		margin-left: 0px !important;
		margin-right: 20px !important;
	}

	.block{display: inline-block;}

	.marginR20{margin-right: 20px !important;}
	.marginT30{margin-top: 30px !important;}

	.cross{
	  position: relative;
	  top: -43px;
	  right: 38px;
	  color: #000;
	  background: #fff;
	  font-weight: bold;
	  border-radius: 50px;
	  padding: 2px 7px 2px 7px;
	  text-align: center;
	  font-size: 11px;
	  cursor: pointer;
	}
</style>

<script>

	var selDiv = "";
	var storedFiles = [];

	var selFeaturedDiv = "";
	var storedFeaturedFiles = [];

	$(document).ready(function() {

		// Multiple product image
		$("#files").on("change", handleFileSelect);
		selDiv = $("#selectedFiles");
		$("body").on("click", ".selFile", removeFile);

		// Featured Image
		$("#featured_image").on("change", handleFeaturedFileSelect);
		selFeaturedDiv = $("#selectedFeaturedFiles");
		$("body").on("click", ".selFeaturedFile", removeFeaturedFile);

	});

	function handleFileSelect(e) {
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		filesArr.forEach(function(f) {

			if(!f.type.match("image.*")) {
				return;
			}
			storedFiles.push(f);

			var reader = new FileReader();
			reader.onload = function (e) {
				var html = "<div class='block'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='img-rounded img-width' title='Click to remove'>\
				<input type='hidden' name='product_images[]' value='"+f.name+"' />\
				<span class='selFile cross'>X</span></div>";
				selDiv.append(html);

			}
			reader.readAsDataURL(f);
		});
	}

	function removeFile(e) {
		var file = $(this).siblings().data("file");
		for(var i=0;i<storedFiles.length;i++) {
			if(storedFiles[i].name === file) {
				storedFiles.splice(i,1);
				break;
			}
		}
		$(this).parent().remove();
	}

	function handleFeaturedFileSelect(e) {
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		filesArr.forEach(function(f) {

			if(!f.type.match("image.*")) {
				return;
			}
			storedFeaturedFiles.push(f);

			var reader = new FileReader();
			reader.onload = function (e) {
				var html = "<div class='block'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='img-rounded img-width' title='Click to remove'>\
				<input type='hidden' name='featured_image' value='"+f.name+"' />\
				<span class='selFeaturedFile cross'>X</span></div>";
				selFeaturedDiv.append(html);

			}
			reader.readAsDataURL(f);
		});
	}

	function removeFeaturedFile(e) {
		var file = $(this).siblings().data("file");
		for(var i=0;i<storedFeaturedFiles.length;i++) {
			if(storedFeaturedFiles[i].name === file) {
				storedFeaturedFiles.splice(i,1);
				break;
			}
		}
		$(this).parent().remove();
	}
</script>

<script>
	$(document).ready(function(){
		$(document).on('change','.pro_cat',function(){
			var category_id = $(this).val();

			$.ajax({
				url: site_url+'category/get_subcategory_by_cat_id/',
				data:{category_id:category_id},
				type: "POST",
				error: function () {
					alert("An error ocurred.");
				},
				success: function (data) {
					// location.reload();
					$(".pro_sub_cat").empty();
					$(".pro_sub_cat").append('<option>Select One</option>');
					var d = JSON.parse(data);
					d.forEach(function (type) {
    	                $(".pro_sub_cat").append("<option value=" + type.sub_cat_id + ">" + type.sub_category_name + "</option>");
                });
				}
			});
		});
	});
</script>