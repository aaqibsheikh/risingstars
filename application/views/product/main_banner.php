
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-10 margin-auto marginT30">
			<!-- Advanced Tables -->
			<div class="panel panel-default">
				<div class="panel-heading">
					Banner Products
				</div>
				<p class="text-success"> <?php if (isset($success_message)) {
	echo $success_message;
}?>
				</p>
				<?php if ($this->session->flashdata('flsh_msg') || $this->session->flashdata('product_delete')): ?>
				<div class="alert alert-success">
					<h4 class="success"><?php echo $this->session->flashdata('product_delete') ?></h4>
					<?php echo $this->session->flashdata('flsh_msg'); ?>
				</div>
			<?php endif;?>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Serial No</th>
									<th>Product Name</th>
									<th>Product Featured Image</th>
									<th>Product Images</th>
									<th>Product Status</th>
									<th>Product Availability</th>
									<th>Cost Price</th>
									<th>Product Price</th>
									<th>Discount Price</th>
									<th>Price After Discount</th>
									<th>Delivery Charges</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
$i = 0;
if (isset($main_banner)) {
	foreach ($main_banner as $value) {
		$i++;
		?>
								<tr class="gradeC">
									<td><?php echo $i; ?></td>
									<td><?php echo $value['pro_title']; ?></td>
									<?php if (!empty($value['pro_image'])): ?>
									<td>
										<img src="<?php echo base_url() . 'assets/upload/' . $value['pro_image']; ?>" width="80px" height="80px"/>
									</td>
									<?php endif;?>

									<td>
										<?php foreach ($value['images'] as $img): ?>
										<?php if (!empty($img)): ?>
										<img src="<?php echo base_url() . 'assets/upload/' . $img; ?>" width="80px" height="80px"/>
										<?php endif;?>
										<?php endforeach;?>
									</td>

									<td>
										<?php if ($value['pro_status'] == 1) {
			echo "Enable";
		} else {
			echo "Disable";
		}?>

									</td>
									<td>
										<?php if ($value['pro_availability'] == 1) {
			echo "In Stock";
		} elseif ($value['pro_availability'] == 2) {
			echo "Out Of Stock";
		} else {
			echo "Up Coming";
		}?>
									</td>
									<td><?php echo $value['cost_price']; ?></td>
									<td><?php echo $value['pro_price']; ?></td>
									<td><?php echo $value['discount_price']; ?></td>
									<td><?php echo $value['price_after_discount']; ?></td>
									<td><?php echo $value['delivery_charges']; ?></td>
									<td>
										<a class="btn btn-info btn-sm" href="<?php echo base_url() ?>product/edit_product/<?php echo $value['pro_id'] ?>">Edit</a>
										<a class="btn btn-danger btn-sm" href="<?php echo base_url() ?>product/delete_product/<?php echo $value['pro_id'] ?>">Delete</a>
									</td>

								</tr>
								<?php }}?>
							</tbody>
						</table>
					</div>

				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		$('#dataTables-example').dataTable();
	});
</script>