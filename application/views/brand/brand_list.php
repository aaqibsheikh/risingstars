
<div id="page-wrapper">

     <div class="row">
        <div class="col-lg-6 margin-auto marginT30">
            <!-- Advanced Tables -->
            <div class="panel panel-default">
                <div class="row">
                     <div class="col-lg-6">
                        <div class="panel-heading">
                            Brand List
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <a href="<?php echo site_url('brand/add_brand'); ?>" class="btn btn-product-bg marginTR10 pull-right">Add Brand</a>
                     </div>
                 </div>

                <p class="text-success"> <?php if (isset($success_message)) {
	echo $success_message;
}?>
                 </p>
                 <?php if ($this->session->flashdata('flsh_msg')): ?>
                 <div class="alert alert-success">
                    <?php echo $this->session->flashdata('flsh_msg'); ?>
                </div>
            <?php endif;?>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Serial No</th>
                                    <th>Brand Name</th>
                                    <th>Brand Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$i = 0;
if (isset($all_brands)) {
	foreach ($all_brands as $value) {
		$i++;

		?>
                                <tr class="gradeC">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $value->brand_name; ?></td>
                                    <?php if (!empty($value->brand_image)): ?>
                                    <td>
                                        <img src="<?php echo base_url() . 'assets/upload/' . $value->brand_image; ?>" width="80px" height="80px"/>
                                    </td>
                                    <?php endif;?>
                                   <td>
                                        <a class="btn btn-info" href="<?php echo base_url(); ?>brand/edit_brand/<?php echo $value->brand_id; ?>">Edit</a>
                                        <a class="btn btn-danger" href="<?php echo base_url(); ?>brand/delete_brand/<?php echo $value->brand_id; ?>">Delete</a>
                                    </td>


                                </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
</div>


    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });
    </script>