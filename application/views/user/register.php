<?php if (validation_errors()) { ?> <div class="alert alert-danger"> <?php echo validation_errors(); ?></div> <?php }; ?>

<?php echo form_open('user/register', array('id' => 'loginForm')); ?>

<div class="row">
    <div class="form-group col-lg-12">
        <label>Full Name</label>
        <input type="text" value="<?php echo set_value('john doe') ?>" id="" required="required" class="form-control" name="username">
    </div>
    <div class="form-group col-lg-4">
        <label>Father name</label>
        <input type="" value="<?php echo set_value('mr.doe') ?>" id="" required="required" class="form-control" name="fname">
    </div>
    <div class="form-group col-lg-4">
        <label>CNIC #</label>
        <input type="" value="<?php echo set_value('1111111111111') ?>" id="" placeholder="13 digit CNIC number" required="required" class="form-control" name="cnic">
    </div>
    <div class="form-group col-lg-4">
        <label>Full Address </label>
        <input type="" value="<?php echo set_value('barclay') ?>" id="" required="required" class="form-control" name="address">
    </div>
    <div class="form-group col-lg-4">
        <label>kin Name</label>
        <input type="" value="<?php echo set_value('liza') ?>" id="" required="required" class="form-control" name="kinname">
    </div>
    <div class="form-group col-lg-4">
        <label>kin relation</label>
        <input type="" value="brother" id="" required="required" class="form-control" name="kinrelation">
    </div>
    <div class="form-group col-lg-4">
        <label>kin CNIC #</label>
        <input type="" value="<?php echo set_value('1111111111111') ?>" id="" placeholder="13 digit CNIC number" required="required" class="form-control" name="kincnic">
    </div> 


    <div class="form-group col-lg-4">
        <label>Mobile #</label>
        <input type="" value="<?php echo set_value('1111111111') ?>" id="" placeholder="10 digit Mobile number" required="required" class="form-control" name="mobile">
    </div>
    <div class="form-group col-lg-4">
        <label>Password</label>
        <input type="password" value="<?php echo set_value('12345') ?>" id="" required="required" class="form-control" name="password">
    </div>
    <div class="form-group col-lg-4">
        <label>Repeat Password</label>
        <input type="password" value="<?php echo set_value('12345') ?>" id="" required="required" class="form-control" name="conpassword">
    </div>
    <div class="form-group col-lg-4">
        <label>Email Address</label>
        <input type="email"  value="<?php echo set_value('jorikafla@gmail.com') ?>"  id="" required="required" class="form-control" name="email">
    </div>
    <div class="form-group col-lg-4">
        <label>introducer code</label>
        <input type="" value="<?php echo set_value($_SESSION['code']) ?>" id="" required="required" class="form-control" name="introducer">
    </div>

    <div class="form-group col-lg-4">
        <label>joining package</label>

        <select class="form-control" name="package">

            <?php foreach ($data->result() as $row) { ?>    
                <option value="<?php echo $row->Package_ID; ?>"><?php echo $row->PackageName . ' ' . $row->PackagePrice; ?></option>
            <?php } ?>

        </select>
    </div>

</div>
<div class="text-center">
    <!--<button type="submit" class="btn btn-success">Register</button>-->
    <?php echo form_submit('submit', 'Register', 'class="btn btn-success"'); ?>
    <a class="btn btn-default" href="login" >Cancel</a>
</div>

<?php form_close() ?>