<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// $route['home/index/(:any)'] = "home/index/$1";
$route['default_controller'] = 'home';
$route['(:any)'] = 'home/index/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;