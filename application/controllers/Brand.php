<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Category Controller
 */
class Brand extends CI_Controller {

	function __construct() {
		parent::__construct();
		$data = array();
		$this->load->library('template');
		$this->load->model('BrandModel');
		$this->load->model('ContactModel');
	}
	public function add_brand() {
		$this->template->render('dashboard');
	}
	public function save_brand() {
		$this->form_validation->set_rules('brand_name', 'Brand Name', 'required|min_length[2]');
		if ($this->form_validation->run()) {
			$product_image = $this->upload_product_image();
			$this->BrandModel->add_brand_model($product_image);
			$this->session->set_flashdata("flsh_msg", "<font class='success'>Brand Added Successfully</font>");
			redirect('brand/brand_list');
		} else {
			$this->add_brand();
		}
	}
	private function upload_product_image() {
		$config['upload_path'] = 'assets/upload/';
		$config['allowed_types'] = 'png|gif|jpg|jpeg';
		$config['max_size'] = 100000; //kb
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('brand_image')) {
			$data = $this->upload->data();
			$image_path = "$data[file_name]";
			return $image_path;
		} else {
			$error = $this->upload->display_errors();
			$this->session->set_userdata('error_image', $error);
		}

	}

	public function brand_list() {
		$all_brands = $this->BrandModel->get_all_footer_brand();
		$this->template->set('all_brands', $all_brands);
		$this->template->render('dashboard');
	}

	public function delete_brand($brand_id) {
		$this->BrandModel->delete_brand_by_id($brand_id);
		$this->session->set_flashdata("flsh_msg", "<font class='success'>Brand Deleted Successfully</font>");
		redirect('brand/brand_list');
	}

	public function edit_brand($brand_id) {
		$brand_by_id = $this->BrandModel->edit_brand_by_id($brand_id);
		$this->template->set('brand_by_id', $brand_by_id);
		$this->template->render('dashboard');
	}

	public function update_brand($brand_id) {
		$this->form_validation->set_rules('brand_name', 'Brand Name', 'required|min_length[2]');
		if ($this->form_validation->run()) {
			$product_image = $this->upload_product_image();
			$this->BrandModel->update_brand_by_id($brand_id, $product_image);
			$this->session->set_flashdata('flsh_msg', 'Brand Updated Successfully', 10);
			// $this->brand_list();
			redirect('brand/brand_list');
		} else {
			$this->add_brand();
		}
	}
}