<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class block_user extends CI_Controller {

    public $current_date;

    function __construct() {
        parent::__construct();
        $this->load->library('template');
        $this->load->model('UserModel');
        $this->load->library('my_mail'); //load myemail library
        $this->load->library('form_validation');
        $this->current_date == date('Y-m-d H:i:s');
        $this->load->library('MY_Email'); // send email
    }

    public function index(){
        $this->template->render('block_user');
    }

}
