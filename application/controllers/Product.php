<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Product Controller
 */
class Product extends CI_Controller {

	function __construct() {
		parent::__construct();
		$data = array();
		$this->load->library('template');
		$this->load->model("ProductModel");
		$this->load->model("CategoryModel");
		$this->load->model("BrandModel");
		$this->load->model("ContactModel");
		$this->load->model("UserModel");
	}
	public function add_product_form() {
		$all_cat = $this->ProductModel->get_all_category();
		$all_sub_cat = $this->ProductModel->get_all_sub_category();
		$this->template->set('all_cat', $all_cat);
		$this->template->set('all_sub_cat', $all_sub_cat);
		$this->template->render('dashboard');
	}
	public function product_list() {
		$all_product = $this->ProductModel->get_all_product();

		// Explode all product images
		for ($i = 0; $i < count($all_product); $i++) {
			$all_product[$i]['images'] = explode(',', $all_product[$i]['images']);
		}

		$this->template->set('all_product', $all_product);
		$this->template->render('dashboard');
	}

	public function main_banner() {
		$main_banner = $this->ProductModel->get_main_banner_product();

		// Explode all product images
		for ($i = 0; $i < count($main_banner); $i++) {
			$main_banner[$i]['images'] = explode(',', $main_banner[$i]['images']);
		}

		$this->template->set('main_banner', $main_banner);
		$this->template->render('dashboard');
	}

	public function insert_product() {
		$product_image = $this->upload_product_image();
		if ($product_image == NULL) {
			redirect("product/add_product_form");
		} else {

			$image = $this->ProductModel->add_product_model($product_image);
			$this->session->set_flashdata("flsh_msg", "<font class='success'>Product Added Successfully</font>");
			redirect('product/product_list');
		}
	}

	public function edit_product($product_id) {
		$all_product = $this->ProductModel->edit_product_model($product_id);

		// Explode all product images
		$all_product[0]['images'] = explode(',', $all_product[0]['images']);

		$all_cat = $this->ProductModel->get_all_category();
		$all_sub_cat = $this->ProductModel->get_all_sub_category();
		$all_brand = $this->ProductModel->get_all_brand();
		$this->template->set('all_product', $all_product);
		$this->template->set('all_cat', $all_cat);
		$this->template->set('all_sub_cat', $all_sub_cat);
		$this->template->set('all_brand', $all_brand);
		$this->template->render('dashboard');
	}

	private function upload_product_image() {
		$config['upload_path'] = 'assets/upload/';
		$config['allowed_types'] = 'png|gif|jpg|jpeg';
		$config['max_size'] = 100000; //kb
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('featured_image')) {
			$data = $this->upload->data();
			$image_path = "$data[file_name]";
			return $image_path;
		} else {
			$error = $this->upload->display_errors();
			$this->session->set_userdata('error_image', $error);
			//redirect("Product/add_product_form");
		}

	}
	public function update_product() {

		// $this->PrductModel->update_product_model();
		if ($_FILES['featured_image']['name'] == "" || $_FILES['featured_image']['size'] == "") {
			$product_image = $this->input->post('featured_image', true);
			$this->ProductModel->update_product_model($product_image);
			$this->session->set_flashdata("update_pro_msg", "Product Updated Successfully");
			$product_id = $this->input->post('pro_id', true);
			redirect('product/edit_product/' . $product_id);

		} else {
			$product_id = $this->input->post('pro_id', true);
			$product_image = $this->upload_product_image();
			if ($product_image == NULL) {
				redirect('product/edit_product/' . $product_id);
			} else {
				$this->ProductModel->update_product_model($product_image);
				//unlink($this->input->post('old_pro_image',true));
				$this->session->set_flashdata("update_pro_msg", "Product Updated Successfully");
				redirect('product/edit_product/' . $product_id);
			}

		}

	}
	public function delete_product($product_id) {
		$this->ProductModel->delete_product_model($product_id);
		$this->session->set_flashdata('product_delete', 'Product Deleted Successfully');
		redirect('product/product_list');
	}

	public function product_details($product_id) {
		$data['brands'] = $this->BrandModel->get_all_brand();
		$data['product_info'] = $this->ProductModel->get_product_by_id($product_id);
		$data['related_products'] = $this->ProductModel->get_related_products($product_id);
		$data['product_images'] = $this->ProductModel->get_product_images($product_id);
		$this->load->view('web/details', $data);
	}

	public function brand_list($brand_id) {
		$data['brands'] = $this->BrandModel->get_all_brand();
		$data['footer_brands'] = $this->BrandModel->get_all_footer_brand();
		$data['product_list'] = $this->ProductModel->get_product_by_brand_id($brand_id);
		$this->load->view('web/listing', $data);
	}

	public function product_list_by_category($subcat_id) {
		$data['brands'] = $this->BrandModel->get_all_brand();
		$data['footer_brands'] = $this->BrandModel->get_all_footer_brand();
		$data['product_list'] = $this->ProductModel->get_product_by_subcat_id($subcat_id);
		$this->load->view('web/listing', $data);
	}

	public function search() {
		if (!empty($_GET['amount'])) {
			$amount = explode('-', $_GET['amount']);
			$price1 = str_replace("Rs", "", $amount[0]);
			$price2 = str_replace("Rs", "", $amount[1]);
			$data['product_list'] = $this->ProductModel->get_product_by_price($price1, $price2);
		}

		if (!empty($_GET['brandType'])) {
			// $brandType = preg_replace("/'/", "\&#39;", $_GET['brandType']);
			$brandType = implode(',', array_map(array($this, 'add_quotes'), $_GET['brandType']));
			$data['product_list'] = $this->ProductModel->get_product_by_brand($brandType);
		}

		if (!empty($_GET['search'])) {
			// $brandType = preg_replace("/'/", "\&#39;", $_GET['brandType']);
			$search = $_GET['search'];
			$data['product_list'] = $this->ProductModel->get_product_by_search($search);
		}

		$data['brands'] = $this->BrandModel->get_all_brand();
		$this->load->view('web/search', $data);
	}

	private function add_quotes($str) {
		return sprintf("'%s'", $str);
	}

	public function buy() {
		$data = array();
		$data['quantity'] = $this->input->post('quantity');
		$data['product_id'] = $this->input->post('product_id');
		$data['brands'] = $this->BrandModel->get_all_brand();
		$this->load->view('web/shipping', $data);
	}

	public function congrats() {
		$data = array();
		$this->form_validation->set_rules('first_name', 'First Name Required', 'required');

		// basic required field with minimum length
		$this->form_validation->set_rules('last_name', 'Last Name Required', 'required');

		// basic required field with exact length
		$this->form_validation->set_rules('mobile_number', 'Mobile Number Required', 'required');

		// basic required field but alphabets only
		$this->form_validation->set_rules('address', 'Address Required', 'required');

		// basic required field but alphanumeric only
		$this->form_validation->set_rules('city', 'City Required', 'required');

		// basic email field with email validation
		$this->form_validation->set_rules('country', 'Country Required', 'required');

		// basic email field with email validation
		$this->form_validation->set_rules('state', 'State Required', 'required');

		if ($this->form_validation->run() == FALSE) {
			$data = array();
			$data['quantity'] = $this->input->post('quantity');
			$data['product_id'] = $this->input->post('product_id');
			$data['brands'] = $this->BrandModel->get_all_brand();
			$this->load->view('web/shipping', $data);
		} else {

			$quantity = $this->input->post('quantity');
			$product_id = $this->input->post('product_id');
			$price_of_product = $this->ProductModel->get_product_price($product_id);
			$total_price = $quantity * $price_of_product;

			if ($this->session->userdata('promo_code') != '') {
				$promo_code = $this->session->userdata('promo_code');
			} else {
				$promo_code = $this->input->post('promo_code');
			}

			$order = array(
				'product_id' => $product_id,
				'quantity' => $quantity,
				'product_price' => $total_price,
				'promo_code' => $promo_code,
			);

			$order_id = $this->ProductModel->save_order($order);
			if (!empty($order)) {
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'email' => $this->input->post('email'),
					'mobile_number' => $this->input->post('mobile_number'),
					'address' => $this->input->post('address'),
					'city' => $this->input->post('city'),
					'country' => $this->input->post('country'),
					'state' => $this->input->post('state'),
					'order_id' => $order_id,
				);

				$customer_mobile_number = $this->input->post('mobile_number');
				$customer_name = $this->input->post('first_name') . ' ' . $this->input->post('last_name');
				$promo_code = ($this->session->has_userdata('promo_code')) ? $this->session->userdata('promo_code') : '';

				$response = $this->ProductModel->save_shipping($data);

				if ($response) {

					if (!empty($promo_code)) {
						$promo_user_detail = $this->UserModel->getUserDetailByUsername($promo_code);

						if (count($promo_user_detail)) {
							$current_mobile_no = current($promo_user_detail);
							$promo_user_mobile = $current_mobile_no['mobile_no'];
							$promo_username = $current_mobile_no['full_name'];

							$promo_details = array(
								'mobile' => !(empty($promo_user_mobile)) ? $promo_user_mobile : '',
								'message' => "Congrats Mr/Miss " . $promo_username . " The Order No. " . $order_id . " has been received to MeeNuu Shopping Mall from your reference.",
							);

							$response = _send_brandsms_to_one($promo_details);
						}

					}

					$customer_details = array(
						'mobile' => !(empty($customer_mobile_number)) ? $customer_mobile_number : '',
						'message' => "Congrats Mr/Miss " . $customer_name . " Your order has been successfully received. Your Order No. is " . $order_id . " MeeNuu Shopping Mall will make you a confirmation call within 12 hours.",
					);

					$response = _send_brandsms_to_one($customer_details);

					$sales_detail = array(
						'mobile' => '03024343627',
						'message' => "Mr/Miss " . $customer_name . " has placed an Order in MeeNuu Shopping Mall against the Order No." . $order_id,
					);

					$response = _send_brandsms_to_one($sales_detail);

					// $user = $this->UserModel->getUserDetailByMobile($mobile_number);
					// $user = $this->UserModel->getUserDetailByUsername($username);

					// $user_details = array(
					//           'mobile' => !(empty($email)) ? $email : '',
					//           'message' => "Congrats Mr/Miss" . $name . "New Pin Code is:" . $new_Pin . "Url:http://risingstarsint.biz/",
					//       );
					//       $response = _send_brandsms_to_one($user_details);
					$data['brands'] = $this->BrandModel->get_all_brand();
					$this->load->view('web/congrats', $data);
				} else {
					redirect("product/product_list_by_category");
				}
			}
		}
	}
}

?>