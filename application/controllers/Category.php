<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Category Controller
 */
class Category extends CI_Controller {

	function __construct() {
		parent::__construct();
		$data = array();
		$this->load->library('template');
		$this->load->model('CategoryModel');
		$this->load->model('ContactModel');
	}
	public function add_category_form() {
		$this->template->render('dashboard');
	}
	public function add_sub_category() {
		$this->template->render('dashboard');
	}
	public function save_category() {
		$this->form_validation->set_rules('category_name', 'Category Name', 'required|min_length[2]');
		if ($this->form_validation->run()) {
			$category_image = $this->upload_product_image();
			$this->CategoryModel->add_category_model($category_image);
			$this->session->set_flashdata("flsh_msg", "<font class='success'>Category Added Successfully</font>");
			redirect('category/category_list');
		} else {
			$this->add_category_form();
		}
	}

	public function save_sub_category() {
		$this->CategoryModel->add_sub_category_model();
		$this->session->set_flashdata("flsh_msg", "<font class='success'>Sub Category Added Successfully</font>");
		redirect('category/sub_category_list');
	}
	private function upload_product_image() {
		$config['upload_path'] = 'assets/upload/';
		$config['allowed_types'] = 'png|gif|jpg|jpeg';
		$config['max_size'] = 100000; //kb
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('category_image')) {
			$data = $this->upload->data();
			$image_path = "$data[file_name]";
			return $image_path;
		} else {
			$error = $this->upload->display_errors();
			$this->session->set_userdata('error_image', $error);
		}

	}
	public function category_list() {
		$all_cats = $this->CategoryModel->get_all_category();
		$this->template->set('all_cats', $all_cats);
		$this->template->render('dashboard');
	}
	public function sub_category_list() {
		$all_sub_cats = $this->CategoryModel->get_all_sub_category();
		$this->template->set('all_sub_cats', $all_sub_cats);
		$this->template->render('dashboard');
	}
	public function delete_category($category_id) {
		$this->CategoryModel->delete_caegory_by_id($category_id);
		$this->session->set_flashdata("flsh_msg", "<font class='success'>Category Deleted Successfully</font>");
		redirect('category/category_list');
	}
	public function delete_sub_category($sub_category_id) {
		$this->CategoryModel->delete_sub_caegory_by_id($sub_category_id);
		$this->session->set_flashdata("flsh_msg", "<font class='success'>Sub Category Deleted Successfully</font>");
		redirect('category/sub_category_list');
	}
	public function edit_category($category_id) {
		$cat_by_id = $this->CategoryModel->edit_caegory_by_id($category_id);
		$this->template->set('cat_by_id', $cat_by_id);
		$this->template->render('dashboard');
	}

	public function edit_sub_category($sub_category_id) {
		$sub_cat_by_id = $this->CategoryModel->edit_sub_caegory_by_id($sub_category_id);
		$this->template->set('sub_cat_by_id', $sub_cat_by_id);
		$this->template->render('dashboard');
	}
	public function update_category($category_id) {

		if ($_FILES['category_image']['name'] == "" || $_FILES['category_image']['size'] == "") {
			$category_image = $this->input->post('category_image', true);
			$this->CategoryModel->update_caegory_by_id($category_id, $category_image);
			$this->session->set_flashdata('flsh_msg', 'Category Updated Successfully', 10);
			redirect('category/category_list');

		} else {
			$category_image = $this->upload_product_image();
			if ($category_image == NULL) {
				$this->session->set_flashdata('flsh_msg', 'Category Not Updated', 10);
				redirect('category/edit_category/' . $category_id);
			} else {
				$this->CategoryModel->update_caegory_by_id($category_id, $category_image);
				$this->session->set_flashdata('flsh_msg', 'Category Updated Successfully', 10);
				redirect('category/category_list');
			}

		}

	}

	public function update_sub_category($sub_category_id) {

		$this->form_validation->set_rules('subcategory_name', 'Subcategory Name', 'required');
		if ($this->form_validation->run()) {
			$this->CategoryModel->update_sub_caegory_by_id($sub_category_id);
			$this->session->set_flashdata('flsh_msg', 'Sub Category Updated Successfully', 10);
			redirect('category/sub_category_list');
		} else {
			$this->add_sub_category();
		}
	}

	public function get_subcategory_by_cat_id() {
		$category_id = $this->input->post('category_id');
		$subcategories = $this->CategoryModel->get_subcategory_by_cat_id($category_id);
		print_r(json_encode($subcategories));

	}

}
