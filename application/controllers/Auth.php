<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class auth extends CI_Controller {
	public $current_date;

	function __construct() {
		parent::__construct();
		$this->load->library('template');
		$this->load->model('UserModel');
		$this->load->model('DashboardModel');
		$this->load->model('superAdminModel');
		$this->load->library('my_mail'); //load myemail library
		$this->load->library('form_validation');
		$this->current_date == date('Y-m-d H:i:s');
		$this->load->library('MY_Email'); // send email
		$this->load->library('session');
		$this->load->helper('url');
		$this->user = $this->aauth->get_user_id();
		date_default_timezone_set("Asia/Karachi");

	}

	public function index() {

		redirect('auth/login');
	}

	public function login() {

		$this->template->set('container_class', 'login');

		$this->load->library('form_validation');

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$this->load->library('form_validation');

			$this->form_validation->set_rules('memeberid', 'Member ID', 'required');

			$this->form_validation->set_rules('password', 'Password', 'required');

			if ($this->form_validation->run() == FALSE) {

				$this->template->render('auth');
			} else {
				$memeberid = $this->input->post('memeberid');
				$password = $this->input->post('password');

				if ($this->aauth->login($memeberid, $password)) {

					$users_grps_array = $this->aauth->get_user_groups();
					$goup_arr = current($users_grps_array);
					$user_group_id = $goup_arr->id;

					$users_grps = array();
					//                    HANDLING SESSION
					$this->load->model('DashboardModel');

					$user = $this->aauth->get_user();

					$active_month = $this->DashboardModel->getUserMonth($user->id);

					$remaining_days = $this->DashboardModel->getUserRemainingDays($user->id);

					$real_name = $this->DashboardModel->userDisplayName();

					$profile_pic = $this->UserModel->getUserProfilePic($user->id);

					$timestamp = $user->date_created;
					$datetime = explode(" ", $timestamp);
					$registration_date = $datetime[0];
					$registration_date = date("j F, Y", strtotime($registration_date));

					$this->session->set_userdata('registration', $registration_date);
					$this->session->set_userdata('name', $real_name);
					$this->session->set_userdata('code', $user->username);
					$this->session->set_userdata('month', $active_month);
					$this->session->set_userdata('days', $remaining_days);
					$this->session->set_userdata('profile_pic_path', $profile_pic);
					$this->session->set_userdata('group_id', $user_group_id);
					$this->session->set_userdata('user_id', $user->id);
					// $this->session->set_userdata('pincode', $user->id );

					$status = $this->UserModel->getUserStatus($user->id); //check user status block or acive
					if ($status) {
						redirect('/block_user/index');
					}
					//                    END SESSION
					foreach ($users_grps_array as $temp_user_grp):
						array_push($users_grps, $temp_user_grp->name);
					endforeach;
					// if ($user_group_id==1){

					//     redirect(base_url().'super_admin/index','refresh');
					// } else {
					redirect('/dashboard/index'); //redirect if user is not block
					//}
				} else {
					$this->template->set('loginerror', 'E-mail or Password is wrong');

					$this->template->render('auth');
				}
			}
		}
		$this->template->render('auth');
	}

	public function register_user() {

		$this->template->set('container_class', 'register');

		$package = $this->UserModel->getPacakges();
		$this->template->set('data', $package);

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$this->load->library('my_mail'); //load myemail library

			$this->load->library('form_validation');

			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('fname', 'Father name', 'required|trim');

			$this->form_validation->set_rules('cnic', 'CNIC', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('kinname', 'Ken name', 'required');
			$this->form_validation->set_rules('kinrelation', 'Ken relation', 'required');
			$this->form_validation->set_rules('kincnic', 'Ken CNIC', 'required');
			$this->form_validation->set_rules('mobile', 'Mobile', 'required|is_unique[rising_users.mobile_no]');
			$this->form_validation->set_rules('email', 'Email', 'required|is_unique[aauth_users.email]');
			$this->form_validation->set_rules('introducer', 'Introducer code', 'required');
			// $this->form_validation->set_rules('package', 'Package', 'required|greater_than[-1]');

			if ($this->form_validation->run() == FALSE) {
				$this->template->render();
			} else {

				$sum_of_package = $_POST['package_entry_price'];
				$package_id = $_POST['package_entry_package'];
				$package_amount_for_commision = $sum_of_package - 1000;

				// load models
				$this->load->model('UserModel');
				$this->load->model('CommissionModel');
				//calculate package of user based upon ammount after deducking price of azaddi package 500

				$admin_cash = 1000;

				$this->session->set_userdata('user_package_id', $package_id);

				$this->session->set_userdata('admin_cash', $admin_cash);

				$this->session->set_userdata('package_amount_for_commision', $package_amount_for_commision);
				// $this->session->package_amount_for_commision

				$last_insert_user_id = $this->UserModel->registerUser($_POST); //sending for register user

				$user_pin_used = $this->aauth->get_user();

				//user amount details

				$current_user = $this->aauth->get_user();

				$totalfundecashamount_credit = $this->DashboardModel->getTotalFundEcashAmount($current_user->username);

				$TotalUsedFundEcashamount_debit = $this->DashboardModel->getTotalUsedFundEcashAmount($current_user->username);
				//check user e_cash for make new entry
				$remaining_e_cash = $totalfundecashamount_credit - $TotalUsedFundEcashamount_debit;

				$pending_commission_arr = $this->DashboardModel->getCommisionByUserID($this->user);
				$total_paid_amount = $this->superAdminModel->getWithdrawAmount($this->user);
				$one_commission = $this->DashboardModel->getCommisionOne($this->user);
				$eighty_commission = $pending_commission_arr['eighty'];
				$twenty_commission = $pending_commission_arr['twenty'];

				$total_commsion_current_user = $one_commission + $eighty_commission + $twenty_commission;

				$remaining_commison = $total_commsion_current_user - $total_paid_amount;

				$total_amount_user = $remaining_commison + $remaining_e_cash;

				//end user amount details
				if ($last_insert_user_id['last_insert_id'] != 0) {

					$introducer_id = $this->UserModel->getuseridbycode($_POST['introducer']);

					$package_id_introducer = $this->UserModel->getUserPackageById($introducer_id['id']);

					$introducer_detail = $this->UserModel->getUserDetailById($introducer_id['id']);

					$introducer_detail = current($introducer_detail);

					if ($package_id_introducer >= 2) {
						$log_direct_introducer__commission_data_re = array('rising_user_id' => $last_insert_user_id['last_insert_id'], 'eligible_user_id' => $introducer_id['id'],
							'commision_amount' => 500, 'package' => 1,
							'commision_type' => '0', 'transaction_type' => 'meenustar');
						$this->CommissionModel->logDirectIndirectCommission($log_direct_introducer__commission_data_re);

						$user_details = array(
							'email' => $introducer_detail['email'],
							'mobile' => isset($introducer_detail['mobile_no']) ? $introducer_detail['mobile_no'] : '',
							'message' => "Congrats you have Recived Rs.500 Meenu Star Commision.Url:http://risingstarsint.biz/",
						);
						$response = _send_brandsms_to_one($user_details);

					} else {
						$log_meeenustar_commission_data = array('rising_user_id' => $last_insert_user_id['last_insert_id'], 'eligible_user_id' => $introducer_id['id'],
							'commision_amount' => 500, 'package' => $package_id,
							'commision_type' => 'meenustar', 'transaction_type' => 'meenustar');

						$this->CommissionModel->logMissedCommission($log_meeenustar_commission_data);

						$user_details = array(
							'email' => $introducer_detail['email'],
							'mobile' => isset($introducer_detail['mobile_no']) ? $introducer_detail['mobile_no'] : '',
							'message' => "You Have Missed Rs.500 Meenu Star Commision .Url:http://risingstarsint.biz/",
						);

						$response = _send_brandsms_to_one($user_details);

					}

					if ($admin_cash == 1000 && $package_amount_for_commision <= 0) {

						if ($admin_cash > $remaining_e_cash) {

							if ($remaining_e_cash > 0) {

								$admin_remaing_amount = $admin_cash - $remaining_e_cash;

								$data = array(
									'member_code' => $user_pin_used->username,
									'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
									'debit' => $remaining_e_cash,
									'type' => 'entry',
									'deduct_from' => 'e',
									'created_at' => date('Y-m-d H:i:s'),
								);

								$fundeCash_result = $this->superAdminModel->LogFundEcash($data);

								$data = array(
									'member_code' => $user_pin_used->username,
									'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
									'debit' => $admin_remaing_amount,
									'type' => 'entry',
									'deduct_from' => 'c',
									'created_at' => date('Y-m-d H:i:s'),
								);

								$fundeCash_result = $this->superAdminModel->LogFundEcash($data);
							} else {
								$data = array(
									'member_code' => $user_pin_used->username,
									'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
									'debit' => $admin_cash,
									'type' => 'entry',
									'deduct_from' => 'c',
									'created_at' => date('Y-m-d H:i:s'),
								);
								$fundeCash_result = $this->superAdminModel->LogFundEcash($data);
							}
						} else {
							$detuct_from = 'e';
							$data = array(
								'member_code' => $user_pin_used->username,
								'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
								'debit' => $admin_cash,
								'type' => 'entry',
								'deduct_from' => 'e',
								'created_at' => date('Y-m-d H:i:s'),
							);
							$fundeCash_result = $this->superAdminModel->LogFundEcash($data);
						}

						$log_indirect_commission_data = array('rising_user_id' => $last_insert_user_id['last_insert_id'], 'eligible_user_id' => 1,
							'commision_amount' => $admin_cash - 500, 'package' => 1,
							'commision_type' => '0', 'transaction_type' => 'azadi');

						$log_azadi_commission_res = $this->CommissionModel->logDirectIndirectCommission($log_indirect_commission_data);

						$user_details = array(
							'email' => $_POST['email'],
							'mobile' => $_POST['mobile'],
							'message' => "Congrats Mr/Miss " . $_POST['username'] . " you have join RSI as a MeeNuu Stars
                            your login code= " . $last_insert_user_id['login_code'] . " & password= 12345678 & pin_code= " . $last_insert_user_id['Pin_Code'] . " Introducer= " . $_POST['introducer'] . " Url:http://risingstarsint.biz/",
						);

						$response = _send_brandsms_to_one($user_details);

						$introducer_id = $this->UserModel->getuseridbycode($_POST['introducer']);

						$introducer_detail = $this->UserModel->getUserDetailById($introducer_id['id']);
						$introducer_detail = current($introducer_detail);

						$user_details = array(
							'email' => $_POST['email'],
							'mobile' => isset($introducer_detail['mobile_no']) ? $introducer_detail['mobile_no'] : '',
							'message' => "Congrats you have introduced Mr/Miss " . $_POST['username'] . " in RSI as a MeeNuu Stars Url:http://risingstarsint.biz/",
						);

						$response = _send_brandsms_to_one($user_details);

						$this->template->set('success_message', sprintf(USER_REGISTER_SUCCESS, $last_insert_user_id['login_code']));

						$this->template->render();

						// echo $package_amount_for_commision."no commision will calculate just log this".$admin_cash."ammount";
					} else {

						$log_azadi_commission_data = array('rising_user_id' => $last_insert_user_id['last_insert_id'], 'eligible_user_id' => 1,
							'commision_amount' => $admin_cash - 500, 'package' => 1,
							'commision_type' => '0', 'transaction_type' => 'azadi');
						$log_azadi_commission_res = $this->CommissionModel->logDirectIndirectCommission($log_azadi_commission_data);

						if ($sum_of_package > $remaining_e_cash) {

							if ($remaining_e_cash > 0) {

								$admin_remaing_amount = $sum_of_package - $remaining_e_cash;

								$data = array(
									'member_code' => $user_pin_used->username,
									'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
									'debit' => $remaining_e_cash,
									'type' => 'entry',
									'deduct_from' => 'e',
									'created_at' => date('Y-m-d H:i:s'),
								);

								$fundeCash_result = $this->superAdminModel->LogFundEcash($data);

								$data = array(
									'member_code' => $user_pin_used->username,
									'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
									'debit' => $admin_remaing_amount,
									'type' => 'entry',
									'deduct_from' => 'c',
									'created_at' => date('Y-m-d H:i:s'),
								);

								$fundeCash_result = $this->superAdminModel->LogFundEcash($data);
							} else {
								$data = array(
									'member_code' => $user_pin_used->username,
									'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
									'debit' => $sum_of_package,
									'type' => 'entry',
									'deduct_from' => 'c',
									'created_at' => date('Y-m-d H:i:s'),
								);
								$fundeCash_result = $this->superAdminModel->LogFundEcash($data);
							}
						} else {
							$detuct_from = 'e';
							$data = array(
								'member_code' => $user_pin_used->username,
								'description' => 'make entry by your account code is:' . $last_insert_user_id['login_code'],
								'debit' => $sum_of_package,
								'type' => 'entry',
								'deduct_from' => 'e',
								'created_at' => date('Y-m-d H:i:s'),
							);
							$fundeCash_result = $this->superAdminModel->LogFundEcash($data);
						}

						if ($package_amount_for_commision > 0) {
							$this->CommissionModel->assignCommission($last_insert_user_id['last_insert_id']);

							$this->CommissionModel->assignOnePercentCommission($last_insert_user_id['last_insert_id']);
						}

						$user_details = array(
							'email' => $_POST['email'],
							'mobile' => $_POST['mobile'],
							'message' => "Congrats Mr/Miss " . $_POST['username'] . " you have join RSI as a MeeNuu Stars your login code= " . $last_insert_user_id['login_code'] . " & password= 12345678 & pin_code= " . $last_insert_user_id['Pin_Code'] . " Introducer= " . $_POST['introducer'] . " .Url:http://risingstarsint.biz/",
						);

						$response = _send_brandsms_to_one($user_details);

						$introducer_id = $this->UserModel->getuseridbycode($_POST['introducer']);

						$introducer_detail = $this->UserModel->getUserDetailById($introducer_id['id']);
						$introducer_detail = current($introducer_detail);
						$user_details = array(
							'email' => $_POST['email'],
							'mobile' => isset($introducer_detail['mobile_no']) ? $introducer_detail['mobile_no'] : '',
							'message' => "Congrats you have introduced Mr/Miss " . $_POST['username'] . " in RSI as a MeeNuu. Stars Url:http://risingstarsint.biz/",
						);
						$response = _send_brandsms_to_one($user_details);
						// die();
						//end brand sms code
						////commet for local
						$this->UserModel->mail_commissions_assign($last_insert_user_id['last_insert_id']);
						$this->template->set('success_message', sprintf(USER_REGISTER_SUCCESS, $last_insert_user_id['login_code']));
						$this->template->render();
					}
				} else {
					//$this->template->set('sucessregister', 'You have successfully register to app Please Login to continue');
					// redirect('');
				}
			}
		}

		$this->template->render('dashboard');
	}

	public function update_user() {
		$result = $this->UserModel->UpdateUser($_POST);

		if ($result == 1) {
			redirect('/auth/profile');
		}
		//        $this->template->render('dashboard');
	}

	public function profile() {
		$user = $this->aauth->get_user();
		$data = $this->UserModel->getUserDetailById($user->id);
		$data = current($data);
		$this->template->set('profile_details', $data);

		$this->template->render('dashboard');
	}

	public function upgrade_package() {

		$this->load->model('CommissionModel');
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->form_validation->set_rules('package', 'Package', 'required|greater_than[-1]');
			$this->form_validation->set_message('greater_than[-1]', 'Please select any package');
			if ($this->form_validation->run() == FALSE) {
				$this->template->render('dashboard');
			} else {
				$update_package_date = array(
					'package' => $this->input->post('package'),
					'user_id' => $this->input->post('user_id'),
				);

				$user_existing_packg = $this->UserModel->getUserPackageById($this->input->post('user_id'));
				$existing_package_price = _get_package_price($user_existing_packg);
				$new_package_price = _get_package_price($this->input->post('package'));

				if ($user_existing_packg == 1) {
					$package_amount_for_commision = _get_package_price($this->input->post('package'));
				} else {
					$existing_package_price = _get_package_price($user_existing_packg);
					$new_package_price = _get_package_price($this->input->post('package'));
					$package_amount_for_commision = $new_package_price - $existing_package_price;
				}
				$package_update_res = $this->UserModel->upgradePackage($update_package_date);
				if ($package_update_res > 0) {

					$user_pin_used = $this->aauth->get_user();
					$upgraded_usercode = $this->aauth->get_user($this->input->post('user_id'));
					$data = array(
						'member_code' => $user_pin_used->username,
						'description' => 'upgradePackage by your account.code is:' . $upgraded_usercode->username,
						'debit' => $package_amount_for_commision,
						'type' => 'entry',
						'deduct_from' => 'e',
						'created_at' => date('Y-m-d H:i:s'),
					);

					$fundeCash_result = $this->superAdminModel->LogFundEcash($data);

					$current_user_location = $this->UserModel->getUserLocationById($package_update_res);
					$even_odd_check = _return_even_or_odd($current_user_location);

					$this->session->set_userdata('package_amount_for_commision', $package_amount_for_commision);
					// give comissions
					if ($even_odd_check != 'Even') {
						$this->CommissionModel->assignCommission($package_update_res); // give direct/indirect/20%
						$this->CommissionModel->assignOnePercentCommission($package_update_res); // give 1%
					} else { //check temporarly
						$this->CommissionModel->assignOnePercentCommission($package_update_res); // give 1%
					}

					//--------------------------------------------------------------------------------

					$package_name = $this->UserModel->getUserPackageNameById($update_package_date['package']);

					$user = $this->aauth->get_user();

					$update_package_data_email = array(
						'introducer' => $this->input->post('introducer'),
						'email' => $user->email,
						'package' => $package_name,
						'username' => $this->input->post('full_name'),
					);

					// $respo = $this->my_mail->mail_package_update($update_package_data_email);
					//
					$this->template->set('success_message', sprintf(USER_PACKAGE_UPDATE_SUCCESS, $package_name));

					$this->template->render();
				}
			}
		}
		$this->template->render('dashboard');
	}
	public function admin_panel() {

		$this->template->render('super_admin');
	}

	public function verify_and_get_introducer_details($introducer_code, $introducer_pin, $qtyre, $qtymeenu, $package_id) {

		$this->load->model('DashboardModel');
		$this->load->model('UserModel');

		if ($package_id == 0) {

		} else {
			$packages = explode('a', $package_id);
		}

		$sum_of_package = 0;
		$admin_cash = 0;
		$meenuu_price = 0;
		$re_price = 0;
		$package_amount_for_commision = 0;

		$re_price = 3500 * $qtyre;
		$meenuu_price = 7000 * $qtymeenu;
		$sum_of_package = $re_price + $meenuu_price;

		if ($package_id == 0) {

		} else {
			foreach ($packages as $key => $value) {
				$sum_of_package += _get_package_price_by_id($value);
			}
		}

		$id = $this->DashboardModel->aauthUserPin($introducer_code);

		if ($id != 0) {
			$details = $this->UserModel->getUserDetailById($id);
			$details["error"] = 0;
		} else {
			$details = array("error" => 1);
		}

		$currentuserpincode = $this->DashboardModel->getCurrentUserPinCode();

		if ($currentuserpincode != $introducer_pin) {
			$details["errorr"] = 1;
		} else {
			$details["errorr"] = 0;
		}

		$current_user = $this->aauth->get_user();

		$totalfundecashamount_credit = $this->DashboardModel->getTotalFundEcashAmount($current_user->username);

		$TotalUsedFundEcashamount_debit = $this->DashboardModel->getTotalUsedFundEcashAmount($current_user->username);
		//check user e_cash for make new entry
		$remaining_e_cash = $totalfundecashamount_credit - $TotalUsedFundEcashamount_debit;

		$pending_commission_arr = $this->DashboardModel->getCommisionByUserID($this->user);
		$total_paid_amount = $this->superAdminModel->getWithdrawAmount($this->user);
		$one_commission = $this->DashboardModel->getCommisionOne($this->user);
		$eighty_commission = $pending_commission_arr['eighty'];
		$twenty_commission = $pending_commission_arr['twenty'];

		$total_commsion_current_user = $one_commission + $eighty_commission + $twenty_commission;

		$remaining_commison = $total_commsion_current_user - $total_paid_amount;
		$total_amount_user = $remaining_commison + $remaining_e_cash;

		// $total_shoping = 0;
		// if($package_id != 0)
		// {
		//     $package_amount    =    $this->DashboardModel->getPackageAmountByPackageID($package_id);

		//     $total_shoping = $package_amount['price'] + $package_product;
		// }else{
		$total_shoping = 0 + $sum_of_package;
		// }

		if ($total_amount_user >= $total_shoping) {
			$details["allow"] = 1;
		} else {
			$details["allow"] = 0;
		}

		echo json_encode($details);
	}

	public function change_password() {

		$new_pass = $_POST['newpassword'];
		$update_pass_res = $this->UserModel->changePasswordById($new_pass);
		$user = $this->UserModel->getUserDetailById($this->aauth->get_user_id());
		$username = current($user);

		$data = array(
			'email' => $username['email'],
			'password' => $new_pass,
			'username' => $username['full_name'],
		);
		if ($update_pass_res) {

			// $this->my_mail->mail_chnage_password($data);
		}
		redirect('/auth/profile');
	}

	public function reset_password() {

		$email = $_POST['useremail'];

		$new_password = $this->UserModel->resetPassword();

		$user = $this->UserModel->getUserDetailById($this->aauth->get_user_id($email));
		if (empty($user)) {
			$this->template->set('success_message', 'email id not found in our system');
//        header("Location:  redirect('/auth/login');");
			redirect('/auth/login');
		}

		$username = current($user);
		$update_pass_res = $this->UserModel->changePasswordById($new_password, $username['id']);
		$data = array(
			'email' => $email,
			'password' => $new_password,
			'username' => $username['username'],
			'full_name' => $username['full_name'],
		);

		if ($update_pass_res) {

			// $this->my_mail->mail_reset_password($data);
		}
		$this->template->set('success_message', 'new password has sent to your email');
//        header("Location:  redirect('/auth/login');");
		redirect('/auth/login');
	}

	public function reset_PIN() {

		$email = $_POST['useremail'];

		$new_Pin = $this->UserModel->resetPin();

		$user = $this->UserModel->getUserDetailByMobile($email);

		if (empty($user)) {
			$this->template->set('success_message', 'email id not found in our system');
//        header("Location:  redirect('/auth/login');");
			redirect('/auth/profile');
		}

		$username = current($user);
		$name = $username['full_name'];
		//     $data = array(
		//     'email' => $email,
		//     'PIN_code' => $new_Pin,
		//     'username' => $name
		//     );

		$user_details = array(
			'mobile' => !(empty($email)) ? $email : '',
			'message' => "Congrats Mr/Miss" . $name . "New Pin Code is:" . $new_Pin . "Url:http://risingstarsint.biz/",
		);
		$response = _send_brandsms_to_one($user_details);

		// $this->my_mail->mail_reset_pin($data);
		redirect('/auth/profile');
	}

	public function logout() {
		$this->aauth->logout();

		redirect('auth/login');
	}

	public function test() {

		$this->load->model('CommissionModel');
		$this->load->model('UserModel');
		$this->load->model('DashboardModel');
		//$this->CommissionModel->assignCommission(25);
		//
		//        $a = $this->DashboardModel->getCommisionByUserID(8);
		//        $result = $this->UserModel->getCommissionReport(1);
		//        echo $result;
		//        $details = $this->UserModel->mail_commissions_assign(41);
		//
		//        $date1 = '2016-06-10';
		//        $date2 = '2016-08-12';
		//
		//        $ts1 = strtotime($date1);
		//        $ts2 = strtotime($date2);
		//
		//        $year1 = date('Y', $ts1);
		//        $year2 = date('Y', $ts2);
		//
		//        $month1 = date('m', $ts1);
		//        $month2 = date('m', $ts2);
		//
		//        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		//        echo $diff;
		//        echo '<br>';
		//        $now = $ts2; // or your date as well
		//        $your_date = strtotime($date1);
		//        $datediff = $now - $your_date;
		//        echo floor($datediff / (60 * 60 * 24));
		//        $childrens = $this->UserModel->getAllChilds(12);
		//        echo $childrens;
		//        $packg_updated_date = $this->UserModel->getPackageUpdatedDate(41);
		//        _pr($packg_updated_date);
		//        $created_user = $this->UserModel->getUserCreatedDate(41);
		//        echo $created_user;
		//        $this->my_mail->mail_commissions_assign();
		//
		//        $eligible_arr=current($result);
		//        echo $eligible_arr->user_id;
	}

	public function my_email() {
		$ci = get_instance();
		$ci->load->library('email');
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "mail.o2obpo.com";
		$config['smtp_port'] = "25";
		$config['smtp_user'] = "no-reply@o2obpo.com";
		$config['smtp_pass'] = "786@r!S!ng";
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$ci->email->initialize($config);

		$from_email = "no-rply@example.com";
		$to_email = "jorikafla@gmail.com";

		//Load email library
		$this->load->library('email');
		$user = $this->aauth->get_user();
		$this->email->from($from_email, $user->username);
		$this->email->to($to_email);
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		//Send mail
		if ($this->email->send()) {
			echo 'good';
			$this->session->set_flashdata("email_sent", "Email sent successfully.");
			return true;
		} else {
			$this->session->set_flashdata("email_sent", "Error in sending Email.");
			return FALSE;
		}
	}

	public function testUser() {

		$id = $this->aauth->create_user('admin@gmail.com', 'rising@star', 'kamal');

		$newUserCode = $this->UserModel->genrateLoginCode($id);

		$update_status = $this->aauth->update_user($id, FALSE, FALSE, $newUserCode);

		$this->aauth->add_member($id, 1);

		$data = array(
			'cnic' => '35201-1234567-9',
			'address' => 'Lahore cantt',
			'kin_name' => 'faraz',
			'kin_relation' => 'brother',
			'kin_cnic' => '35202-1234567-8',
			'father_name' => '',
			'mobile_no' => '0343-4832895',
			'full_name' => 'Imran Rehmat',
			'user_pic_path' => 'https://scontent.flhe3-1.fna.fbcdn.net/v/t1.0-9/18557027_1657527027610639_2858257023122103052_n.jpg?_nc_cat=0&oh=6ab94ab7f84a7060537f70df3ba304db&oe=5B7CD36B',
			'parent_id' => '0',
			'introducer_code' => '1808MeeNuu.00000000',
			'location' => '0',
			'user_id' => '1',
			'user_pin' => '1234',
			'package_id' => '5',
			'previous' => date('Y-m-d'),
		);

		$result = $this->db->insert('rising_users', $data);
	}

}
