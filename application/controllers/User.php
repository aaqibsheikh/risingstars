<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('template');
    }

    public function index() {

        redirect('user/login');
    }

//     public function login() {

//         if($this->auth->is_loggedin()){
            
//             redirect('dashboard/index');
//         }
        
//         $this->template->set('container_class', 'login');

//         $this->load->library('form_validation');

//         if ($_SERVER['REQUEST_METHOD'] == 'POST') {


//             $this->load->library('form_validation');

//             $this->form_validation->set_rules('memeberid', 'Member ID', 'required');

//             $this->form_validation->set_rules('password', 'Password', 'required');

//             if ($this->form_validation->run() == FALSE) {

//                 $this->template->render('user');
//             } else {
//                 $memeberid = $this->input->post('memeberid');
//                 $password = $this->input->post('password');





//                 if ($this->aauth->login($memeberid, $password)) {

//                     $users_grps_array = $this->aauth->get_user_groups();

//                     $users_grps = array();

                    
                    
                    
                    
                    
// //                    HANDLING SESSION
//                     $this->load->model('DashboardModel');

//                     $user = $this->aauth->get_user();
                    
                   

//                     $active_month = $this->DashboardModel->get_user_Month($user->id);

                    
//                     $remaining_days = $this->DashboardModel->get_user_remaining_days($user->id);

//                     $real_name = $this->DashboardModel->user_displayname();
                   
                    
                    
                    
//                     $this->session->set_userdata('name', $real_name);
//                     $this->session->set_userdata('code', $user->username);
//                     $this->session->set_userdata('month', $active_month);
//                     $this->session->set_userdata('days', $remaining_days);




// //                    END SESSION
//                     foreach ($users_grps_array as $temp_user_grp):
//                         array_push($users_grps, $temp_user_grp->name);
//                     endforeach;

//                     if (in_array('Admin', $users_grps)) {

//                         redirect('user/admin_panel');
//                     } else {
//                         redirect('/dashboard/index');
//                     }
//                 } else {
//                     $this->template->set('loginerror', 'E-mail or Password is wrong');

//                     $this->template->render('user');
//                 }
//             }
//         }
//         $this->template->render('user');
//     }

    public function admin_panel() {

        $this->template->render('super_admin');
    }

//    public function register() {
//        $this->template->set('container_class', 'register');
//        $this->load->model('UserModel');
//        $package = $this->UserModel->get_pacakges();
//        $this->template->set('data', $package);
//
//        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
////            _pr($_POST, 1);
//            $this->load->library('form_validation');
//
//            $this->form_validation->set_rules('username', 'Username', 'required');
//            $this->form_validation->set_rules('fname', 'father name', 'required|trim');
//
//            $this->form_validation->set_rules('cnic', 'CNIC', 'required');
//
//
//            $this->form_validation->set_rules('address', 'Address', 'required');
//            $this->form_validation->set_rules('kinname', 'Ken name', 'required');
//            $this->form_validation->set_rules('kinrelation', 'ken relation', 'required');
//
//            $this->form_validation->set_rules('kincnic', 'Ken CNIC', 'required');
//
//            $this->form_validation->set_rules('mobile', 'Mobile No', 'required|regex_match[/^[0-9]{10}$/]');
//            $this->form_validation->set_rules('password', 'Password', 'required|matches[conpassword]');
//
//            $this->form_validation->set_rules('conpassword', 'confirm password', 'required');
//
//
//
//            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[aauth_users.email]');
//            $this->form_validation->set_rules('introducer', 'Introducer code', 'required');
//            $this->form_validation->set_rules('package', 'Package', 'required');
//
//
//            if ($this->form_validation->run() == FALSE) {
//                $this->template->render();
//            } else {
//                $this->load->model('UserModel');
//                $result = $this->UserModel->register_user($_POST);
//
//                if ($result != 0) {
//                    $this->load->library('MY_Email');
//
////                    $result = $this->MY_Email->send_registration_Mail($_POST);
////                    _pr($_POST);
//
//                    $this->template->set('sucessregister', 'You have successfully register to app Please Login to continue');
//                    redirect('user/Login');
//                } else {
//                    echo 'issue occur';
//                }
//            }
//        }
//        $this->template->render('user');
//    }

    public function logout() {
        $this->aauth->logout();

        $this->session->sess_destroy();
        redirect('user/login');
    }

}
