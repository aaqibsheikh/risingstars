<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class dashboard extends CI_Controller {

    public $user;

    function __construct() {

        parent::__construct();
        $this->load->library('template');
        $this->load->model('DashboardModel');
        $this->load->library('session');
        //current user
        $this->user = $this->aauth->get_user_id();
        $this->load->model('UserModel');
        $this->load->model('superAdminModel');
    }

    public function index() {
        $current_user = $this->aauth->get_user();
        
        $pending_commission = $this->DashboardModel->getPendingCommission($this->user);
        
        $zero_commision     = $this->DashboardModel->getCommisionZero($this->user);

        $missed_commisson   = $this->DashboardModel->getMissedCommisson($this->user);
        
        $pending_commission_arr = $this->DashboardModel->getCommisionByUserID($this->user);
        $total_paid_amount = $this->superAdminModel->getWithdrawAmount($this->user);
        $one_commission = $this->DashboardModel->getCommisionOne($this->user);

        $commission_used_entry = $this->DashboardModel->getUsedCommissionInEntry($current_user->username);

        $getTransferCreditAmountEcash = $this->DashboardModel->getTransferCreditAmountByUserCode($current_user->username,'e');
        $getTransferCreditAmountCommi = $this->DashboardModel->getTransferCreditAmountByUserCode($current_user->username,'c');

        $eighty_commission = $pending_commission_arr['eighty'];
        $twenty_commission = $pending_commission_arr['twenty'];

        $this->load->model('UserModel');
        $level = $this->UserModel->getUserLevel($this->user);
        $Member = $this->UserModel->getTotalMembers($this->user);

        // _pr($current_user);
        // print($current_user);
        // die;

        $totalfundecashamount = $this->DashboardModel->getTotalFundEcashAmount($current_user->username);
        $totalusedfundecashamount = $this->DashboardModel->getTotalUsedFundEcashAmount($current_user->username);

        
        
        $direct = $this->DashboardModel->introduceByUser($current_user->username);
        $total_direct = $direct->num_rows();
        
        $indirect = 0;
        if ($Member > 0) {
            $indirect = $Member - $total_direct;
        }        
        
        $levelarray = array(
            'direct' => $total_direct,
            'indirect' => $indirect,
            'userlevel' => $level,
            );
        
        $commisson_arr = array(
            'eighty' => $eighty_commission,
            'twenty' => $twenty_commission,
            'missed' => $missed_commisson,
            'one' =>  $one_commission,
            'zero'=> $zero_commision,
            'total_paid' => $total_paid_amount
            );
        $latest_commision = $this->superAdminModel->getLatestCommisiomHistry($this->user);
        $this->template->set('commision_detail', $latest_commision);
        $this->template->set('userlevel', $levelarray);
        $this->template->set('totalmember', $Member);
        $this->template->set('commission', $commisson_arr);
        $this->template->set('totalfundecashamount', $totalfundecashamount);
        $this->template->set('totalusedfundecashamount',$totalusedfundecashamount);
        $this->template->set('commission_used_entry',$commission_used_entry);
        $this->template->set('getTransferCreditAmountEcash',$getTransferCreditAmountEcash);
        $this->template->set('getTransferCreditAmountCommi',$getTransferCreditAmountCommi);
        $this->template->render('dashboard');
    }

    public function add_product(){

        if($this->input->post('add_product')){
            die();

                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('cost_price', 'Cost Price', 'trim|required');
                $this->form_validation->set_rules('retail_price', 'Retail Price', 'trim|required');
                $this->form_validation->set_rules('status', 'Status', 'required');
                $this->form_validation->set_rules('stock', 'Stock', 'required');
                $this->form_validation->set_rules('meenuu_stars', 'MeeNuu Stars', 'trim|required');
                $this->form_validation->set_rules('ss', 'SS', 'trim|required');
                $this->form_validation->set_rules('gs', 'GS', 'trim|required');
                $this->form_validation->set_rules('ps', 'PS', 'trim|required');
                $this->form_validation->set_rules('ds', 'DS', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->template->render('dashboard');
                }
                else{

            
                    //Check whether user upload picture
                    if(!empty($_FILES['featured_image']['name'])){


                         $_FILES['upload_File']['name'] = $_FILES['featured_image']['name']; 
                        $_FILES['upload_File']['type'] = $_FILES['featured_image']['type']; 
                        $_FILES['upload_File']['tmp_name'] = $_FILES['featured_image']['tmp_name']; 
                        $_FILES['upload_File']['error'] = $_FILES['featured_image']['error']; 
                        $_FILES['upload_File']['size'] = $_FILES['featured_image']['size']; 
                        $uploadPath = 'assets/upload/'; 
                        $config['upload_path'] = $uploadPath; 
                        $config['allowed_types'] = 'gif|jpg|png'; 
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('upload_File')){
                            $fileData = $this->upload->data();
                            $featured_image = $fileData['file_name'];
                        }

                    }
                    
                    $data = array(
                        'title' => $this->input->post('title'),
                        'cost_price' => $this->input->post('cost_price'),
                        'retail_price' => $this->input->post('retail_price'),
                        'status' => $this->input->post('status'),
                        'stock' => $this->input->post('stock'),
                        'meenuu_stars' => $this->input->post('meenuu_stars'),
                        'ss' => $this->input->post('ss'),
                        'gs' => $this->input->post('gs'),
                        'ps' => $this->input->post('ps'),
                        'ds' => $this->input->post('ds'),
                        'featured_image' => $featured_image
                        
                    );

                    $data = $this->security->xss_clean($data);
                    $last_insert_product_id = $this->DashboardModel->addProduct($data);
                   

                    $filesCount = count($_FILES['files']['name']);
                    for($i = 0; $i < $filesCount; $i++){
                        $_FILES['upload_File']['name'] = $_FILES['files']['name'][$i]; 
                        $_FILES['upload_File']['type'] = $_FILES['files']['type'][$i]; 
                        $_FILES['upload_File']['tmp_name'] = $_FILES['files']['tmp_name'][$i]; 
                        $_FILES['upload_File']['error'] = $_FILES['files']['error'][$i]; 
                        $_FILES['upload_File']['size'] = $_FILES['files']['size'][$i]; 
                        $uploadPath = 'assets/upload/'; 
                        $config['upload_path'] = $uploadPath; 
                        $config['allowed_types'] = 'gif|jpg|png'; 
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('upload_File')){
                            $fileData = $this->upload->data();
                            $uploadData[$i]['product_id'] = $last_insert_product_id;
                            $uploadData[$i]['images'] = $fileData['file_name'];
                        }
                    }

                    if(!empty($uploadData)){
                        //Insert file information into the database
                        $insert = $this->DashboardModel->addImages($uploadData);
                    }

                    if($insert){
                        $this->session->set_flashdata('msg', 'Product has been added successfully!');
                        redirect('dashboard/show_product');
                    }
                }
        }
        else{
            $this->template->render('dashboard');
        }
    }

    public function show_product(){

        $products = $this->DashboardModel->getProducts();

        $this->template->set('products', $products);
        $this->template->render('dashboard');
    }

    public function delete_product($id = 0){
        $this->db->delete('product', array('id' => $id));
        $this->db->delete('product_images', array('product_id' => $id));
        redirect(base_url('dashboard/show_product'));
    }

    public function edit_product($id = 0){

        if($this->input->post('edit_product')){
                $this->form_validation->set_rules('title', 'Title', 'trim|required');
                $this->form_validation->set_rules('cost_price', 'Cost Price', 'trim|required');
                $this->form_validation->set_rules('retail_price', 'Retail Price', 'trim|required');
                $this->form_validation->set_rules('status', 'Status', 'required');
                $this->form_validation->set_rules('stock', 'Stock', 'required');
                $this->form_validation->set_rules('meenuu_stars', 'MeeNuu Stars', 'trim|required');
                $this->form_validation->set_rules('ss', 'SS', 'trim|required');
                $this->form_validation->set_rules('gs', 'GS', 'trim|required');
                $this->form_validation->set_rules('ps', 'PS', 'trim|required');
                $this->form_validation->set_rules('ds', 'DS', 'trim|required');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->template->render('dashboard/show_product');
                }
                else{
                    $data = array(
                        'title' => $this->input->post('title'),
                        'cost_price' => $this->input->post('cost_price'),
                        'retail_price' => $this->input->post('retail_price'),
                        'status' => $this->input->post('status'),
                        'stock' => $this->input->post('stock'),
                        'meenuu_stars' => $this->input->post('meenuu_stars'),
                        'ss' => $this->input->post('ss'),
                        'gs' => $this->input->post('gs'),
                        'ps' => $this->input->post('ps'),
                        'ds' => $this->input->post('ds')
                        
                    );
                    $data = $this->security->xss_clean($data);
                    $result = $this->DashboardModel->editProduct($data,$id);

                    if(!empty($_FILES['upload_files']['name'])){

                    }
                    $filesCount = count($_FILES['upload_files']['name']);
                    for($i = 0; $i < $filesCount; $i++){
                        $_FILES['upload_File']['name'] = $_FILES['upload_files']['name'][$i]; 
                        $_FILES['upload_File']['type'] = $_FILES['upload_files']['type'][$i]; 
                        $_FILES['upload_File']['tmp_name'] = $_FILES['upload_files']['tmp_name'][$i]; 
                        $_FILES['upload_File']['error'] = $_FILES['upload_files']['error'][$i]; 
                        $_FILES['upload_File']['size'] = $_FILES['upload_files']['size'][$i]; 
                        $uploadPath = 'assets/upload/'; 
                        $config['upload_path'] = $uploadPath; 
                        $config['allowed_types'] = 'gif|jpg|png'; 
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if($this->upload->do_upload('upload_File')){
                            $fileData = $this->upload->data();
                            $uploadData[$i]['product_id'] = $id;
                            $uploadData[$i]['images'] = $fileData['file_name'];
                        }
                    }

                    if(!empty($uploadData)){
                        //Insert file information into the database
                        $update = $this->DashboardModel->editImages($uploadData,$id);
                    }

                    $this->session->set_flashdata('msg', 'Product has been Updated successfully!');
                    redirect('dashboard/show_product');
                }
        }
        else{
            $products = $this->DashboardModel->getSingleProduct($id);
            $this->template->set('products', $products);
            $this->template->render('dashboard');
        }
    }

    public function introduce_by_user() {

        $user = $this->aauth->get_user();

        $result = $this->DashboardModel->introduceByUser($user->username);

        $total_reffrals = $result->num_rows();


        $this->template->set('dataarray', $result);

        $this->template->set('total_reffrals', $total_reffrals);


        $this->template->render('dashboard');
    }

    public function commission_report() {
        $commission_report = $this->UserModel->getCommissionReport($this->user);
        $this->template->set('commission_report', $commission_report);
        $this->template->render('dashboard');
    }

    public function business_line($id = -1) {

        if ($id == -1) {
            $user = $this->aauth->get_user();
//            $result = $this->DashboardModel->introduceByUserId($this->user);
            $result=$this->DashboardModel->introduceByUserIdForBuisnessLine($this->user);
            
            $breadcrumbs = $user->username . '';
        } else {

            $result = $this->DashboardModel->introduceByUserIdForBuisnessLine($id);

            $breadcrumbs = $this->UserModel->getbreadcrumb($id);
        }

        $this->template->set('crumb', $breadcrumbs);

        $this->template->set('dataarray', $result);
//
        $this->template->render('dashboard');
    }

    public function profile() {

        $this->template->render('dashboard');
    }

    public function gold_star_rewards() {
        $user = $this->aauth->get_user();

        $lines = $this->DashboardModel->introduceByUser($user->username);
        $total_reffrals = $lines->num_rows();
        $reward_summary = $this->UserModel->giveRewards($user->id);

        $this->template->set('total_reffrals', $total_reffrals);

        $this->template->set('rewards', $reward_summary);
        $this->template->render('dashboard');
    }

    public function account_statement() {
        $user = $this->aauth->get_user();
        
        $fund_e_cash_histry = $this->superAdminModel->getFundECashHistryByMemberCode($user->username);
        $withdraw_histry = $this->superAdminModel->getWithdrawHistryByid($this->user);
        
        function sortArrayByDate($a, $b){
            return strtotime($a["created_at"]) - strtotime($b["created_at"]);
        }
        $final = array_merge($fund_e_cash_histry, $withdraw_histry);
        usort($final, "sortArrayByDate");
        $this->template->set('fund_e_cash_histry', $final);
        $this->template->set('total_paid',$this->superAdminModel->getWithdrawAmount($this->user));//total paid amount);
        $this->template->set('total_recived',$this->superAdminModel->getTotalRecivedAmount($user->username));//total recived amount);
        $this->template->render('dashboard');
        
    }
    public function transfer_e_commison() {

        $current_user = $this->aauth->get_user();

        $pending_commission = $this->DashboardModel->getPendingCommission($this->user);
        
        $missed_commisson = $this->DashboardModel->getMissedCommisson($this->user);
        
        $pending_commission_arr = $this->DashboardModel->getCommisionByUserID($this->user);
        $total_paid_amount = $this->superAdminModel->getWithdrawAmount($this->user);
        $one_commission = $this->DashboardModel->getCommisionOne($this->user);

        $commission_used_entry = $this->DashboardModel->getUsedCommissionInEntry($current_user->username);

        

        $eighty_commission = $pending_commission_arr['eighty'];
        $twenty_commission = $pending_commission_arr['twenty'];



        
        $totalfundecashamount = $this->DashboardModel->getTotalFundEcashAmount($current_user->username);
        $totalusedfundecashamount = $this->DashboardModel->getTotalUsedFundEcashAmount($current_user->username);
        
        $getTransferCreditAmountEcash = $this->DashboardModel->getTransferCreditAmountByUserCode($current_user->username,'e');
        $getTransferCreditAmountCommi = $this->DashboardModel->getTransferCreditAmountByUserCode($current_user->username,'c');


        $commisson_arr = array(
            'eighty' => $eighty_commission,
            'twenty' => $twenty_commission,
            'missed' => $missed_commisson,
            'one' => $one_commission,
            'total_paid' => $total_paid_amount
            );


        $this->template->set('commission', $commisson_arr);
        $this->template->set('totalfundecashamount', $totalfundecashamount);
        $this->template->set('totalusedfundecashamount',$totalusedfundecashamount);
        $this->template->set('commission_used_entry',$commission_used_entry);
        $this->template->set('getTransferCreditAmountEcash',$getTransferCreditAmountEcash);
        $this->template->set('getTransferCreditAmountCommi',$getTransferCreditAmountCommi);

        $this->template->render('dashboard');   

    }

    public function transfer_commison(){

        $user_pin_used = $this->aauth->get_user();
        $totalfundecashamount = $this->DashboardModel->getTotalFundEcashAmount($user_pin_used->username);
        $totalusedfundecashamount = $this->DashboardModel->getTotalUsedFundEcashAmount($user_pin_used->username);
        $remaining_e_cash = $totalfundecashamount - $totalusedfundecashamount;
        $amount_transfer = $_POST['amount'];
        
        if($amount_transfer > $remaining_e_cash){

            if($remaining_e_cash > 0){

                $admin_remaing_amount = $amount_transfer-$remaining_e_cash;

                $data = array(
                    'sender_code'  => $user_pin_used->username,
                    'reciver_code' => $_POST['verified_member'],
                    'description'  => $_POST['descrip'],
                    'debit'        => $remaining_e_cash,
                    'credit'       => 0,
                    'deduct_from'  => 'e',
                    'created_at'   => date('Y-m-d H:i:s'),
                    );

                $fundeCash_result = $this->DashboardModel->logFundTransfer($data);
                $data = array(
                    'sender_code'  => $user_pin_used->username,
                    'reciver_code' => $_POST['verified_member'],
                    'description'  => $_POST['descrip'],
                    'deduct_from'  => 'e',
                    'debit'        => 0,
                    'credit'       => $remaining_e_cash,
                    'created_at'   => date('Y-m-d H:i:s'),
                    );

                $fundeCash_result = $this->DashboardModel->logFundTransfer($data);
                /////////////////////////////////////////////////////////////////

                $data = array(
                    'sender_code' => $user_pin_used->username,
                    'reciver_code' => $_POST['verified_member'],
                    'description' => $_POST['descrip'],
                    'debit' => $admin_remaing_amount,
                    'credit'       => 0,
                    'deduct_from'  => 'c',
                    'created_at' => date('Y-m-d H:i:s'),
                    );

                $fundeCash_result = $this->DashboardModel->logFundTransfer($data);
                $data = array(
                    'sender_code'  => $user_pin_used->username,
                    'reciver_code' => $_POST['verified_member'],
                    'description'  => $_POST['descrip'],
                    'deduct_from'  => 'e',
                    'debit'        => 0,
                    'credit'       => $admin_remaing_amount,
                    'created_at'   => date('Y-m-d H:i:s'),
                    );

                $fundeCash_result = $this->DashboardModel->logFundTransfer($data);
                //////////////////////////////////////////////////////////
            }else{
                $data = array(
                    'sender_code' => $user_pin_used->username,
                    'reciver_code' => $_POST['verified_member'],
                    'description' => $_POST['descrip'],
                    'debit' => $amount_transfer,
                    'credit'       => 0,
                    'deduct_from'  => 'c',
                    'created_at' => date('Y-m-d H:i:s'),
                    );
                $fundeCash_result = $this->DashboardModel->logFundTransfer($data);

                $data = array(
                    'sender_code' => $user_pin_used->username,
                    'reciver_code' => $_POST['verified_member'],
                    'description' => $_POST['descrip'],
                    'debit' => 0,
                    'credit'       => $amount_transfer,
                    'deduct_from'  => 'c',
                    'created_at' => date('Y-m-d H:i:s'),
                    );
                $fundeCash_result = $this->DashboardModel->logFundTransfer($data);
            }
        }
        else
        {
            $detuct_from = 'e';
            $data = array(
                'sender_code' => $user_pin_used->username,
                'reciver_code' => $_POST['verified_member'],
                'description' => $_POST['descrip'],
                'debit' => $amount_transfer,
                'credit'       => 0,
                'deduct_from'  => 'e',
                'created_at' => date('Y-m-d H:i:s'),
                );

            $fundeCash_result = $this->DashboardModel->logFundTransfer($data);

            $data = array(
                'sender_code'  => $user_pin_used->username,
                'reciver_code' => $_POST['verified_member'],
                'description'  => $_POST['descrip'],
                'debit'        => 0,
                'credit'       => $amount_transfer,
                'deduct_from'  => 'e',
                'created_at'   => date('Y-m-d H:i:s'),
                );
            $fundeCash_result = $this->DashboardModel->logFundTransfer($data);
        }

        $this->template->set('success_message', 'You have Successfully Transfer Amount');

        redirect('dashboard/transfer_e_commison');
    }

    public function test() {

        $current_user_id = $this->aauth->get_user_id();
        echo $this->aauth->get_user();
        echo $current_user_id;
        echo 'hello';
        die;
    }

}
