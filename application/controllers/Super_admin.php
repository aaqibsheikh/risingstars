<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class super_admin extends CI_Controller {

    public $user;

    function __construct() {

        parent::__construct();
        $this->load->library('template');
        $this->load->library('session');
        $this->user = $this->aauth->get_user_id();    //current user
        $this->load->model('DashboardModel');
        $this->load->model('UserModel');
        $this->load->library('my_mail'); //load myemail library
        $this->load->model('superAdminModel');

    }

    public function index() {
        //setting the values to view
        $this->template->set('Histry_detail', $this->superAdminModel->getLatestWithdrawHistry()/* return latest 5 records proccess by admin withdraw */);
        $this->template->set('commision_detail', $this->superAdminModel->getLatestCommisiomHistry($this->user)/* reurn latest 5 commsiion by id */);
        $this->template->set('total_users', $this->UserModel->gettotalRegisterUsersCount()/* return the count of all regiter users */);
        $this->template->set('total_block_users', $this->UserModel->getTotalBlockUnblockCount(1)/* parameter 1 for active and 0 for block return total rows count */);
        $this->template->set('total_active_users', $this->UserModel->getTotalBlockUnblockCount(0)/* parameter 1 for active and 0 for block return total rows count */);
        $this->template->set('total_amount', $this->superAdminModel->getToalPaidAmount($this->user)/* get total earned amount by id */);

        $this->template->render('super_admin');
    }

    public function withdraw_process_list() {

        $user = $this->UserModel->getAllUsersDetail();
        $data = array();
        $user_detail_arr = array();
        foreach ($user as $row) {
            $commissions = $this->DashboardModel->getCommisionByUserID($row['id']);
            $data['id'] = $row['id'];
            $data['code'] = $row['member_code'];
            $data['username'] = $row['full_name'];
            $data['twenty'] = $commissions['twenty'];
            $data['eighty'] = $commissions['eighty'];
            $data['one'] = $this->DashboardModel->getCommisionOne($row['id']);

            // get current user picture
            $user_pic_path_arr = current($this->UserModel->getAllRisingUsersDetail($row['id'], array('user_pic_path')));
            $data['user_pic_path'] = $user_pic_path_arr['user_pic_path'];

            // get total amount paid by the admin
            $data['total_paid'] = $this->superAdminModel->getWithdrawAmount($row['id']);
            array_push($user_detail_arr, $data);
        }
        $this->template->set('user_detail', $user_detail_arr);
        $this->template->render('super_admin');
    }

    public function withdraw_process_modal_info($encoded_id, $flag = false) {

        $id = base64_decode(urldecode($encoded_id));

        $user_detail_arr = array();
        // get all commisions - amount earned
        $commissions = $this->DashboardModel->getCommisionByUserID($id);
        // get total amount paid by the admin
        $user_detail_arr['total_paid'] = $this->superAdminModel->getWithdrawAmount($id);

        // fill data to pass in view 
        $user_detail_arr['id'] = $id;
        $user_detail_arr['twenty'] = $commissions['twenty'];
        $user_detail_arr['eighty'] = $commissions['eighty'];
        $user_detail_arr['one'] = $this->DashboardModel->getCommisionOne($id);
        $user_detail_arr['error'] = false;

        // use flag for ajax call
        if ($flag) {
            return json_encode($user_detail_arr);
        } else {
            echo json_encode($user_detail_arr);
        }
    }

    // withdraw bonus amount
    public function withdraw_bonus($id, $amount) {

        $commision_info = $this->withdraw_process_modal_info(urlencode(base64_encode($id)), true);
        $commision_info = json_decode($commision_info);
        if ($amount > 0) {
            $user_detail = $this->UserModel->getUserDetailById($id);
            $user_detail = current($user_detail);
            $data['user_id'] = $id;
            $data['member_code'] = $user_detail['username'];

            $bonus = $commision_info->twenty + $commision_info->eighty + $commision_info->one; // get total bonus without withdraw amount
            $total_paid = $this->superAdminModel->getWithdrawAmount($id);
            $amount_remaining = $bonus - $total_paid;
            if ($amount < $amount_remaining) {
                $data['amount'] = $amount;

                if ($this->superAdminModel->logWithdrawProcess($data)){//sucessfull log check
//                    seting detail for email notification
                    $transaction_information = $this->withdraw_process_modal_info(urlencode(base64_encode(1)), TRUE);
                    $transaction_information = json_decode($transaction_information);
                    $email_data_array['total_paid'] = $transaction_information->total_paid;
                    $email_data_array['twenty'] = $transaction_information->twenty;
                    $email_data_array['eighty'] = $transaction_information->eighty;
                    $email_data_array['one'] = $transaction_information->one;
                    $email_data_array['current'] = ( ($email_data_array['twenty'] + $email_data_array['eighty'] + $email_data_array['one']) - ($email_data_array['total_paid']) );
                    $email_data_array['member_code'] = $user_detail['username'];
                    $email_data_array['amount'] = $data['amount'];
                    $email_data_array['full_name'] = $this->UserModel->getUserNameById($id);
                    $aauth_user = $this->aauth->get_user($id); //get user function for email 
                    $email_data_array['email'] = $aauth_user->email;
                    $transaction_information=$this->withdraw_process_modal_info(urlencode(base64_encode($id)),TRUE);
                    // $this->my_mail->mail_wihdraw_commission($email_data_array); //send mail to user for transaction
                    $this->withdraw_process_modal_info(urlencode(base64_encode($id)));
                    
                }
                else
                {
                    return false;
                }
            } else {
                $data['error'] = true;
                echo json_encode($data);
            }
        }
    }

    public function register_users() {//display all register users in the system
        $users = $user = $this->UserModel->getAllUsersDetail();

        $month = [];
        foreach ($users as $key => $value) {

            $month[$value['id']]=$this->UserModel->getUserMonth($value['id']);

        }

        $this->template->set('user_detail', $users);
        $this->template->set('month', $month);
        $this->template->render('super_admin');
    }

    public function active_rs_user($user_id) {//activate the user through user_id
        $active_rs_user = $this->UserModel->ActiveUser($user_id);
        return $active_rs_user;
    }

    public function block_rs_user($user_id) {//this will block the user by id 
        $block_rs_user = $this->UserModel->BlockUser($user_id);
        return $block_rs_user;
    }

    public function active_users() {
        $users = $user = $this->UserModel->getAllActiveUsers(); //get all active users in the system
        $this->template->set('user_detail', $users);
        $this->template->render('super_admin');
    }

    public function block_users() {
        $users = $user = $this->UserModel->getAllBlockUsers(); //get all block users in the system

        $this->template->set('user_detail', $users);
        $this->template->render('super_admin');
    }

    public function withdraw_history() {//display all the withdraw histry
        $histry = $this->superAdminModel->getWithdrawHistry();
        $this->template->set('Histry_detail', $histry);
        $this->template->render('super_admin');
    }

    public function htmlpage() {
        $this->template->render('super_admin');
    }

    public function user_detail($id) {
        $result = $this->UserModel->getUserDetailById($id);
        $result = current($result);
        $package_name = $this->UserModel->getUserPackageNameById($result['package_id']);
        $level = $this->UserModel->getUserLevel($id);
        $Member = $this->UserModel->getTotalMembers($id);
        $this->template->set('level', $level);
        $this->template->set('member', $Member);
        $this->template->set('user_detail', $result);
        $this->template->set('package_name', $package_name);
        $this->template->render('super_admin');
    }

    public function fund_e_cash() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $data = array(
                'member_code' => $_POST['verified_member'],
                'description' => $_POST['descrip'],
                'credit' => $_POST['amount'],
                'type'  => 'transfer',
                'created_at' => date('Y-m-d H:i:s'),
                );
            $fundeCash_result = $this->superAdminModel->LogFundEcash($data);

            if ($fundeCash_result) {

                redirect('super_admin/fund_e_cash_histry');
            } else {
                echo 'loging error';
            }
        } else {
            $this->template->render('super_admin');
        }
    }

    public function fund_e_cash_histry() {

        $fund_e_cash_histry = $this->superAdminModel->getFundECashHistry();
        $withdraw_histry = $this->superAdminModel->getWithdrawHistry();

        function sortArrayByDate($a, $b) {
            return strtotime($a["created_at"]) - strtotime($b["created_at"]);
        }

        $final = array_merge($fund_e_cash_histry, $withdraw_histry);
        usort($final, "sortArrayByDate");
        $this->template->set('fund_e_cash_histry', $final);
        $this->template->render('super_admin');
    }

    public function getFullCommisionHistryForSuperAdmin() {
        $this->template->set('commission_report', $this->superAdminModel->getFullCommisionHistryForSuperAdmin()/* reurn latest 5 commsiion by id */);
        $this->template->render('super_admin');
    }

    public function missed_commisson(){
        $this->template->set('commission_report', $this->superAdminModel->getFullMissedHistryForSuperAdmin());
        $this->template->render('super_admin');
    }


    public function test() {

        $fund_e_cash_histry = $this->superAdminModel->getFundECashHistry();
        $withdraw_histry = $this->superAdminModel->getWithdrawHistry();


        $final = array_merge($fund_e_cash_histry, $withdraw_histry);
        usort($final, "sortArrayByDate");




        _pr($final);

        die;
        $transaction_information = $this->withdraw_process_modal_info(urlencode(base64_encode(1)), TRUE);
        $transaction_information = json_decode($transaction_information);
        _pr($transaction_information);
        echo $transaction_information->total_paid;
//        $aauth_user = $this->aauth->get_user();
//        echo $aauth_user->email;
        die;
//        echo $this->superAdminModel->getWithdrawAmount(1);

        $user = $this->UserModel->getAllUsersDetail();
        $total_block = $this->UserModel->getTotalBlockUnblockCount(1);
        echo $total_block;


        die;
        $data = array();
        $user_detail_with_commision_log = array();

        foreach ($user as $row) {
            $commissions = $this->DashboardModel->getCommisionByUserID($row['id']);
            $one_percent = $this->DashboardModel->getCommisionOne($row['id']);
            $data['code'] = $row['member_code'];
            $data['username'] = $row['full_name'];
            $data['twenty'] = $commissions['twenty'];
            $data['eighty'] = $commissions['eighty'];
            $data['one'] = $one_percent;
            $data['total_paid'] = 0;
            $data['current_bonus'] = $commissions['twenty'] + $commissions['eighty'] + $one_percent;
            array_push($user_detail_with_commision_log, $data);
        }

//
        _pr($user_detail_with_commision_log);
    }

    public function testing()
    {
        echo "in";
    }

}
