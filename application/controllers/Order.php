<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Category Controller
 */
class Order extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('template');
		$this->load->model('OrderModel');
	}

	public function order_list() {
		$all_orders = $this->OrderModel->get_all_orders();
		$this->template->set('all_orders', $all_orders);
		$this->template->render('dashboard');
	}

	public function update_order() {

		$status = $this->input->post('status');
		$promo_code = $this->input->post('promo_code');
		$order_id = $this->input->post('order_id');

		if (!empty($status) && !empty($promo_code) && !empty($order_id)) {

			$this->OrderModel->update_order($status, $promo_code, $order_id);
			$this->session->set_flashdata('flsh_msg', 'Order Complete Successfully', 10);

		} else {

		}
	}

	public function view_order($id) {
		$order = $this->OrderModel->get_order_by_id($id);
		$this->template->set('order', $order);
		$this->template->render('dashboard');
	}
}