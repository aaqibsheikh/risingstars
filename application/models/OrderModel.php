<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderModel extends CI_Model {

	public function get_all_orders() {
		$sql = "SELECT o.id, o.quantity, o.product_price, o.status, o.promo_code, DATE_FORMAT(o.order_date, '%Y-%m-%d') AS 'order_date' ,p.pro_title
					FROM `tbl_order` o
					INNER JOIN tbl_shipping s ON s.order_id = o.id
					INNER JOIN tbl_product p ON p.pro_id = o.product_id";

		$query = $this->db->query($sql);
		$data = $query->result();
		return $data;
	}

	public function update_order($status, $promo_code, $order_id) {
		$this->db->where('id', $order_id);
		$this->db->set('status', $status);
		$this->db->set('promo_code', $promo_code);
		$this->db->update('tbl_order');
	}

	public function get_order_by_id($id) {
		$sql = "SELECT o.id, o.quantity, o.product_price, o.status, o.promo_code, DATE_FORMAT(o.order_date, '%Y-%m-%d') AS 'order_date' ,p.pro_title, b.brand_name, CONCAT(s.first_name,' ',s.last_name) AS 'name', s.mobile_number, CONCAT(s.address,' ', s.city,' ', s.country) AS 'address'
				FROM `tbl_order` o
				INNER JOIN tbl_shipping s ON s.order_id = o.id
				INNER JOIN tbl_product p ON p.pro_id = o.product_id
				INNER JOIN tbl_brand b ON b.brand_id = p.pro_brand
				WHERE o.id = '$id'";

		$query = $this->db->query($sql);
		$data = $query->row();
		return $data;
	}

}