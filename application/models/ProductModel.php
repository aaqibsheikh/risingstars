<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model {

	public function add_product_model($product_image) {
		$data['pro_title'] = cleanquote($this->input->post('pro_title'));
		$data['pro_desc'] = cleanquote($this->input->post('pro_desc'));
		$data['pro_cat'] = cleanquote($this->input->post('pro_cat'));
		$data['pro_sub_cat'] = cleanquote($this->input->post('pro_sub_cat'));
		$data['pro_brand'] = cleanquote($this->input->post('pro_brand'));
		$data['pro_price'] = cleanquote($this->input->post('pro_price'));
		$data['cost_price'] = cleanquote($this->input->post('cost_price'));
		$data['discount_price'] = cleanquote($this->input->post('discount_price'));
		$data['price_after_discount'] = cleanquote($this->input->post('price_after_discount'));
		$data['delivery_charges'] = cleanquote($this->input->post('delivery_charges'));
		$data['pro_quantity'] = cleanquote($this->input->post('pro_quantity'));
		$data['pro_availability'] = cleanquote($this->input->post('pro_availability'));
		$data['pro_status'] = cleanquote($this->input->post('pro_status'));
		$data['pro_image'] = $product_image;
		$data['top_product'] = cleanquote($this->input->post('top_product'));
		$data['daily_need_product'] = cleanquote($this->input->post('daily_need_product'));
		$data['discount_product'] = cleanquote($this->input->post('discount_product'));
		$data['jackpot'] = cleanquote($this->input->post('jackpot'));
		$data['ss'] = cleanquote($this->input->post('ss'));
		$data['gs'] = cleanquote($this->input->post('gs'));
		$data['ps'] = cleanquote($this->input->post('ps'));
		$data['ds'] = cleanquote($this->input->post('ds'));
		$data['twenty_percent'] = cleanquote($this->input->post('twenty_percent'));
		$data['one_percent'] = cleanquote($this->input->post('one_percent'));

		$this->db->insert('tbl_product', $data);
		//  Getting last insert id of product for storing multiple images in product_images table
		$insert_id = $this->db->insert_id();

		// Getting time for Unique image name
		$strtotime = strtotime("now");

		$filesCount = count($_FILES['files']['name']);
		for ($i = 0; $i < $filesCount; $i++) {
			$_FILES['upload_File']['name'] = $_FILES['files']['name'][$i];
			$_FILES['upload_File']['type'] = $_FILES['files']['type'][$i];
			$_FILES['upload_File']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
			$_FILES['upload_File']['error'] = $_FILES['files']['error'][$i];
			$_FILES['upload_File']['size'] = $_FILES['files']['size'][$i];
			$uploadPath = 'assets/upload/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 100000; // Max File Size
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ($this->upload->do_upload('upload_File')) {
				// $fileData = $this->upload->data();
				// $uploadData[$i]['product_id'] = $insert_id;
				// $uploadData[$i]['images'] = $fileData['file_name'];
			}
		}

		$filesCount = count($_POST['product_images']);
		for ($i = 0; $i < $filesCount; $i++) {
			$uploadData[$i]['product_id'] = $insert_id;
			$uploadData[$i]['images'] = $_POST['product_images'][$i];
		}
		$uploadData[$filesCount]['product_id'] = $insert_id;
		$uploadData[$filesCount]['images'] = $product_image;

		if (!empty($uploadData)) {
			$this->db->insert_batch('product_images', $uploadData);
		}
	}
	public function get_all_product_limit() {
		$data = $this->db->select('*')
			->from('tbl_product')
			->order_by('pro_id', 'desc')
			->limit("6")
			->get()
			->result();
		return $data;
	}
	public function get_all_top_product() {
		$sql = "SELECT p.*, b.brand_name
				FROM tbl_product p
				INNER JOIN tbl_brand b ON b.brand_id = p.pro_brand
				WHERE p.top_product = 1
				ORDER BY p.pro_id DESC";
		$query = $this->db->query($sql);
		$data = $query->result();
		return $data;

	}

	public function get_main_banner_product() {

		$sql = "SELECT tp.*, GROUP_CONCAT(pi.images) AS 'images'
				FROM tbl_product tp
				LEFT JOIN product_images pi ON pi.product_id = tp.pro_id
				WHERE tp.top_product = 1
				GROUP BY tp.pro_id";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		return $data;

	}

	public function get_related_products($product_id) {
		$sql = "SELECT t.`pro_brand`
				FROM tbl_product t
				WHERE t.pro_id = '$product_id'";

		$query = $this->db->query($sql);
		$data = $query->row();

		if ($data->pro_brand == 0) {

			$sql0 = "SELECT t.pro_title, c.category_name
					FROM `tbl_product` t
					INNER JOIN tbl_category c ON c.category_id = t.pro_cat
					WHERE t.pro_id = '$product_id'";

			$query0 = $this->db->query($sql0);
			$data = $query0->row();

			$title = $data->pro_title;
			$category = $data->category_name;

			$sql = "SELECT p.*, c.category_name
					FROM tbl_product p
					INNER JOIN tbl_category c ON c.category_id = p.pro_cat
					WHERE p.pro_title LIKE '%$title%' OR c.category_name LIKE '%$category%'
					LIMIT 4";
			$query = $this->db->query($sql);
			$data = $query->result();
			return $data;

		} else {
			$sql0 = "SELECT t.pro_title, c.category_name, b.brand_name
					FROM `tbl_product` t
					INNER JOIN tbl_category c ON c.category_id = t.pro_cat
					INNER JOIN tbl_brand b ON b.brand_id = t.pro_brand
					WHERE t.pro_id = '$product_id'";

			$query0 = $this->db->query($sql0);
			$data = $query0->row();

			$title = $data->pro_title;
			$category = $data->category_name;
			$brand = $data->brand_name;

			$sql = "SELECT p.*, b.brand_name, c.category_name
					FROM tbl_product p
					INNER JOIN tbl_category c ON c.category_id = p.pro_cat
					INNER JOIN tbl_brand b ON b.brand_id = p.pro_brand
					WHERE p.pro_title LIKE '%$title%' OR c.category_name LIKE '%$category%' OR b.brand_name LIKE '%$brand%'
					LIMIT 4";
			$query = $this->db->query($sql);
			$data = $query->result();
			return $data;
		}

	}

	public function get_discount_product() {
		$sql = "SELECT p.*, b.brand_name
				FROM tbl_product p
				INNER JOIN tbl_brand b ON b.brand_id = p.pro_brand
				WHERE p.discount_product = 1
				ORDER BY p.pro_id DESC
				LIMIT 1";
		$query = $this->db->query($sql);
		$data = $query->result();
		if (!empty($data)) {
			return $data[0];
		} else {
			return false;
		}

	}

	public function get_all_daily_need_product() {
		$sql = "SELECT p.*, b.brand_name, GROUP_CONCAT(pi.images) AS `product_images`
				FROM tbl_product p
				INNER JOIN tbl_brand b ON b.brand_id = p.pro_brand
				LEFT JOIN product_images pi ON pi.product_id = p.pro_id
				WHERE p.daily_need_product = 1
				GROUP BY p.pro_id
				ORDER BY p.pro_id DESC
				LIMIT 4";
		$query = $this->db->query($sql);
		$data = $query->result();
		return $data;

	}

	public function get_all_category() {
		$data = $this->db->select('*')
			->from('tbl_category')
			->order_by('category_id', 'desc')
			->get()
			->result();
		return $data;
	}
	public function get_all_sub_category() {
		$data = $this->db->select('*')
			->from('tbl_sub_category')
			->order_by('sub_cat_id', 'desc')
			->get()
			->result();
		return $data;
	}
	public function get_all_brand() {
		$data = $this->db->select('*')
			->from('tbl_brand')
			->order_by('brand_id', 'desc')
			->get()
			->result();
		return $data;
	}
	public function get_all_product() {
		$sql = "SELECT tp.*, GROUP_CONCAT(pi.images) AS 'images'
				FROM tbl_product tp
				LEFT JOIN product_images pi ON pi.product_id = tp.pro_id
				GROUP BY tp.pro_id";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		return $data;
	}
	public function edit_product_model($product_id) {
		$sql = "SELECT tp.*, GROUP_CONCAT(pi.images) AS 'images'
				FROM tbl_product tp
				LEFT JOIN product_images pi ON pi.product_id = tp.pro_id
				WHERE tp.pro_id = '$product_id'
				GROUP BY tp.pro_id";
		$query = $this->db->query($sql);
		$data = $query->result_array();
		return $data;
	}
	public function update_product_model($product_image) {
		$product_id = cleanquote($this->input->post('pro_id'));
		$data['pro_title'] = cleanquote($this->input->post('pro_title'));
		$data['pro_desc'] = cleanquote($this->input->post('pro_desc'));
		$data['pro_cat'] = cleanquote($this->input->post('pro_cat'));
		$data['pro_sub_cat'] = cleanquote($this->input->post('pro_sub_cat'));
		$data['pro_brand'] = cleanquote($this->input->post('pro_brand'));
		$data['pro_price'] = cleanquote($this->input->post('pro_price'));
		$data['cost_price'] = cleanquote($this->input->post('cost_price'));
		$data['discount_price'] = cleanquote($this->input->post('discount_price'));
		$data['price_after_discount'] = cleanquote($this->input->post('price_after_discount'));
		$data['delivery_charges'] = cleanquote($this->input->post('delivery_charges'));
		$data['pro_quantity'] = cleanquote($this->input->post('pro_quantity'));
		$data['pro_availability'] = cleanquote($this->input->post('pro_availability'));
		$data['pro_status'] = cleanquote($this->input->post('pro_status'));
		$data['pro_image'] = $product_image;
		$data['top_product'] = cleanquote($this->input->post('top_product'));
		$data['daily_need_product'] = cleanquote($this->input->post('daily_need_product'));
		$data['discount_product'] = cleanquote($this->input->post('discount_product'));
		$data['jackpot'] = cleanquote($this->input->post('jackpot'));
		$data['ss'] = cleanquote($this->input->post('ss'));
		$data['gs'] = cleanquote($this->input->post('gs'));
		$data['ps'] = cleanquote($this->input->post('ps'));
		$data['ds'] = cleanquote($this->input->post('ds'));
		$data['twenty_percent'] = cleanquote($this->input->post('twenty_percent'));
		$data['one_percent'] = cleanquote($this->input->post('one_percent'));

		$this->db->where('pro_id', $product_id);
		$this->db->update('tbl_product', $data);

		// Getting time for Unique image name
		$strtotime = strtotime("now");

		$filesCount = count($_FILES['files']['name']);
		for ($i = 0; $i < $filesCount; $i++) {
			$_FILES['upload_File']['name'] = $_FILES['files']['name'][$i];
			$_FILES['upload_File']['type'] = $_FILES['files']['type'][$i];
			$_FILES['upload_File']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
			$_FILES['upload_File']['error'] = $_FILES['files']['error'][$i];
			$_FILES['upload_File']['size'] = $_FILES['files']['size'][$i];
			$uploadPath = 'assets/upload/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = 100000; // Max File Size
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ($this->upload->do_upload('upload_File')) {
				// $fileData = $this->upload->data();
				// $uploadData[$i]['product_id'] = $insert_id;
				// $uploadData[$i]['images'] = $fileData['file_name'];
			}
		}

		$this->db->delete('product_images', array('product_id' => $product_id));
		$filesCount = count($_POST['product_images']);

		for ($i = 0; $i < $filesCount; $i++) {
			$uploadData[$i]['product_id'] = $product_id;
			$uploadData[$i]['images'] = $_POST['product_images'][$i];
		}
		$uploadData[$filesCount]['product_id'] = $insert_id;
		$uploadData[$filesCount]['images'] = $product_image;

		if (!empty($uploadData)) {
			$this->db->insert_batch('product_images', $uploadData);
		}
	}

	public function delete_product_model($product_id) {
		$product_image = $this->edit_product_model($product_id);
		unlink($product_image->pro_image);
		$this->db->where('pro_id', $product_id);
		$this->db->delete('tbl_product');
	}

	public function get_product_by_id($product_id) {
		$sql = "SELECT t.`pro_brand`
				FROM tbl_product t
				WHERE t.pro_id = '$product_id'";

		$query = $this->db->query($sql);
		$data = $query->row();

		if ($data->pro_brand == 0) {
			$this->db->select('tbl_product.*');
			$this->db->from('tbl_product');
			$this->db->where('pro_id', $product_id);
			$query = $this->db->get();
			return $query->row();

		} else {
			$this->db->select('tbl_product.*,tbl_brand.brand_name');
			$this->db->from('tbl_product');
			$this->db->where('pro_id', $product_id);
			$this->db->join('tbl_brand', 'tbl_product.pro_brand = tbl_brand.brand_id');
			$query = $this->db->get();
			return $query->row();
		}

	}

	public function get_product_images($product_id) {
		$sql = "SELECT * FROM product_images WHERE product_id = '$product_id'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_product_by_brand_id($brand_id) {
		$sql = "SELECT * FROM tbl_product WHERE pro_brand = '$brand_id'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_product_by_subcat_id($subcat_id) {
		$sql = "SELECT * FROM tbl_product WHERE pro_sub_cat = '$subcat_id'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_product_by_price($price1, $price2) {
		$sql = "SELECT * FROM tbl_product WHERE pro_price BETWEEN '$price1' AND '$price2'";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_product_by_brand($brandType) {
		$sql = "SELECT p.*
				FROM tbl_product p
				INNER JOIN tbl_brand b ON b.brand_id = p.pro_brand
				WHERE b.brand_name IN ($brandType)";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_product_price($product_id) {
		$this->db->select('price_after_discount');
		$this->db->from('tbl_product');
		$this->db->where('pro_id', $product_id);
		$result_array = $this->db->get()->result_array();
		return $result_array[0]['price_after_discount'];
	}

	public function save_order($order) {
		$this->db->insert('tbl_order', $order);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function save_shipping($data) {

		$this->db->insert('tbl_shipping', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function get_product_by_search($search) {
		$sql = "SELECT p.*, b.brand_name
				FROM tbl_product p
				INNER JOIN tbl_brand b ON b.brand_id = p.pro_brand
				WHERE p.pro_title LIKE '%$search%' OR b.brand_name LIKE '%$search%'";
		$query = $this->db->query($sql);
		return $query->result();
	}

}
