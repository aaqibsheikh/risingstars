<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class dashboardModel extends MY_Model {

    // Add Product and images in database
    public function addProduct($data){
        $this->db->insert('product', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function addImages($data){
        $insert = $this->db->insert_batch('product_images',$data);
        return $insert?true:false;
    }

    // Get All Products
    public function getProducts(){
        $sql = "SELECT p.*, group_concat(pi.images) AS 'images'
                FROM product p
                INNER JOIN product_images pi ON pi.product_id = p.id
                GROUP BY p.id";
        $result = $this->db->query($sql);
        return $result;
    }

    public function getSingleProduct($id){
        $sql = "SELECT p.*, group_concat(pi.images) AS 'images'
                FROM product p
                INNER JOIN product_images pi ON pi.product_id = p.id
                WHERE p.id = '$id'
                GROUP BY p.id";
        $result = $this->db->query($sql);
        return $result->row_array();
    }

    public function editProduct($data, $id){
        $this->db->where('id', $id);
        $this->db->update('product', $data);
        return true;
    }
    public function editImages($data,$id){
        $update = $this->db->update_batch('product_images',$data, 'product_id');
        return $update?true:false;
    }

//introduce by user code
    public function introduceByUser($user) {
        $result = $this->db->select('rs.previous as joining_date,au.username as member_Id,rs.full_name as Name,rs.id as Serial_No,rs.user_id as userid,pkg.PackageName as pakage')
        ->from('aauth_users as au')
        ->join('rising_users as rs', 'au.id=rs.user_id')
        ->join('package as pkg', 'pkg.Package_ID=rs.package_id')
        ->where('rs.introducer_code', $user)
        ->order_by("rs.user_id", "asc")
        ->get();
        return $result;
    }

//introduce by user id
    public function introduceByUserId($user) {
        $result = $this->db->select('rs.previous as joining_date,au.username as member_Id,rs.full_name as Name,rs.id as Serial_No,rs.user_id as userid')
        ->from('aauth_users as au')
        ->join('rising_users as rs', 'au.id=rs.user_id')
        ->where('rs.parent_id', $user)
        ->get();
        //Use this technique in buissness line for hide + icon
        // result loop
        // get id of every member
        // get its children
        // append all children or count to main object
        return $result;
    }
    public function introduceByUserIdForBuisnessLine($user) {


        $result = $this->db->select('rs.previous as joining_date,au.username as member_Id,rs.full_name as Name,rs.id as Serial_No,rs.user_id as userid')
        ->from('aauth_users as au')
        ->join('rising_users as rs', 'au.id=rs.user_id')
        ->where('rs.parent_id', $user)
        ->get()->result_array();
        
        foreach ($result as $row_id => $row){
            $result[$row_id]['children_count'] =count($this->introduceByUserId($row['userid'])->result_array());
        }
        //Use this technique in buissness line for hide + icon
        // result loop
        // get id of every member
        // get its children

        // append all children or count to main object
        return $result;
    }

    public function getUserMonth($id) {

        $monthcount;
        $result = $this->db->select('previous')
        ->from('rising_users')
        ->where('rising_users.user_id=' . $id)
        ->get()->row();

        $timestamp = $result->previous;
        $datetime = explode(" ", $timestamp);
        $registration_date = $datetime[0];

        // $time = $datetime[1]; //1 registration date

        $current_date = date('Y-m-d');            //2current date
        $date1 = date_create($registration_date); //registration date
        $date2 = date_create($current_date); //current date
        $diff = date_diff($date1, $date2);
        $days = $diff->format("%R%a");
        if ($days == 0) {
            $days = 1;
        }

        
        $days = str_replace("+", "", $days);
        $resultf = '';
        $month = $days / 30;
        $month = ceil($month);
        $monthcount = $month;
        $monthcount = $monthcount / 2;

        $monthcount = ceil($monthcount);
        if ($monthcount == 0) {
            $monthcount = 1;
        }
        if ($month % 2 == 0) {
            $resultf = $monthcount . ' Even';
        } else {
            $resultf = $monthcount . ' Odd';
        }
        return $resultf;
    }
    public function getUserRemainingDays($id) {

        $result = $this->db->select('previous')
        ->from('rising_users')
        ->where('rising_users.user_id=' . $id)
        ->get()->row();

        $timestamp = $result->previous;
        $datetime = explode(" ", $timestamp);
        $registration_date = $datetime[0];

        // $time = $datetime[1]; //1 registration date

        $current_date = date('Y-m-d');
        //2current date
        $date1 = date_create($registration_date);
        $date2 = date_create($current_date);
        $diff = date_diff($date1, $date2);
        $days = $diff->format("%R%a");
        $days = str_replace("+", "", $days);
        
        $resultf = ' ';
        if ($days <= 30) {
            $resultf = 30 - $days;
        } else {
            $resultf = 30 - ($days % 30);
        }



        return $resultf;
    }

    public function userDisplayName() {
        $user = $this->aauth->get_user();

        $result = $this->db->select('full_name')
        ->from('rising_users')
        ->where('rising_users.user_id=' . $user->id)
        ->get()->row();

        return $result->full_name;
    }

    // sum of 80 and 20 % commission minuse earned commission
    public function getPendingCommission($user_id) {//get current bonus for admin side
        $commision_arr = $this->getCommisionByUserID($user_id);

        if (count($commision_arr) == 2) {
            $total_commision = $commision_arr['eighty'] + $commision_arr['twenty'];
        }

        return $total_commision;
    }

    public function getCommisionByUserID($user_id) {

        $twenty = $this->getCommisionTwenty($user_id);
        $eighty = $this->getCommisionEighty($user_id);
        return array(
            'eighty' => $eighty,
            'twenty' => $twenty
            );
    }

    protected function getCommisionTwenty($user_id) {

        $where = '20';

        $twenty_arr = $this->db->select_sum('rc.commision_amount')
        ->from('rising_commissions as rc')
        ->where('commision_type', $where)
        ->where('rc.eligible_user_id', $user_id)
        ->get()->result_array();

        $twenty = current($twenty_arr);

        $twenty_commisson_amount = empty($twenty['commision_amount']) ? 0 : $twenty['commision_amount'];
        return $twenty_commisson_amount;
    }

    protected function getCommisionEighty($user_id) {

        $where = '80';
        $eighty_arr = $this->db->select_sum('rc.commision_amount')
        ->from('rising_commissions as rc')
        ->where('commision_type', $where)
        ->where('rc.eligible_user_id', $user_id)
        ->get()->result_array();
        $eighty = current($eighty_arr);

        $eighty_commisson_amount = empty($eighty['commision_amount']) ? 0 : $eighty['commision_amount'];
        return $eighty_commisson_amount;
    }

    public function getCommisionOne($user_id) {

        $where = '1';
        $eighty_arr = $this->db->select_sum('rc.commision_amount')
        ->from('rising_commissions as rc')
        ->where('commision_type', $where)
        ->where('rc.eligible_user_id', $user_id)
        ->get()->result_array();
        $eighty = current($eighty_arr);

        $eighty_commisson_amount = empty($eighty['commision_amount']) ? 0 : $eighty['commision_amount'];
        return $eighty_commisson_amount;
    }
    public function getCommisionZero($user_id) {

        $where = '0';
        $eighty_arr = $this->db->select_sum('rc.commision_amount')
        ->from('rising_commissions as rc')
        ->where('commision_type', $where)
        ->where('transaction_type!=','azadi')
        ->where('rc.eligible_user_id', $user_id)
        ->get()->result_array();
        $eighty = current($eighty_arr);

        $eighty_commisson_amount = empty($eighty['commision_amount']) ? 0 : $eighty['commision_amount'];
        return $eighty_commisson_amount;
    }

    public function getMissedCommisson($user_id) {
        $missed_commisson_arr = $this->db->select_sum('rmc.commision_amount')
        ->from('rising_missed_commissions as rmc')
        ->where('rmc.eligible_user_id', $user_id)
        ->get()->result_array();
        $missed_commisson = current($missed_commisson_arr);
        $missed_commisson_amount = empty($missed_commisson['commision_amount']) ? 0 : $missed_commisson['commision_amount'];
        return $missed_commisson_amount;
    }
    public function getTotalFundEcashAmount($user_code){
        $fund_e_cash_arr = $this->db->select_sum('fec.credit')
        ->from('fund_e_cash_histry as fec')
        ->where('member_code', $user_code)
        ->get()->result_array();
        $fund_e_cash_arr = current($fund_e_cash_arr);
        $fund_e_cash = empty($fund_e_cash_arr['credit']) ? 0 : $fund_e_cash_arr['credit'];

        return $fund_e_cash;

    }
    public function getTotalUsedFundEcashAmount($user_code){
        $fund_e_cash_arr = $this->db->select_sum('fec.debit')
        ->from('fund_e_cash_histry as fec')
        ->where('member_code', $user_code)
        ->where('deduct_from','e')
        ->get()->result_array();
        $fund_e_cash_arr = current($fund_e_cash_arr);
        $fund_e_cash = empty($fund_e_cash_arr['debit']) ? 0 : $fund_e_cash_arr['debit'];
        return $fund_e_cash;
    }
    public function getUsedCommissionInEntry($user_code){
        $fund_e_cash_arr = $this->db->select_sum('fec.debit')
        ->from('fund_e_cash_histry as fec')
        ->where('member_code', $user_code)
        ->where('deduct_from','c')
        ->get()->result_array();
        $fund_e_cash_arr = current($fund_e_cash_arr);
        $fund_e_cash = empty($fund_e_cash_arr['debit']) ? 0 : $fund_e_cash_arr['debit'];
        return $fund_e_cash;
    }
    

    public function getPackageAmountByPackageID($package_id){
        $price = $this->db->select('pac.PackagePrice as price')
        ->from('package as pac')
        ->where('Package_ID', $package_id)
        ->get()->result_array();
        $price = current($price);

        return $price;

    }
    public function aauthUserPin($introducer_code) {
        $where = "au.username='$introducer_code'";
        $users_id = $this->db->select('au.id')
        ->from('aauth_users as au')
        ->join('rising_users as rs', 'au.id=rs.user_id', 'inner')
        ->where($where)
        ->get()->result_array();

        if (!empty($users_id)) {
            $user = $users_id[0];
            return $user['id'];
        }

        return 0;
    }
    public function getCurrentUserPinCode()
    {
        $user = $this->aauth->get_user();
        $username = $this->db->select('rs.user_pin')
        ->from('aauth_users as au')
        ->join('rising_users as rs', 'au.id=rs.user_id', 'inner')
        ->where('rs.user_id=' . $user->id)
        ->get()->result_array();
        if (!empty($username)) {
            $name = $username[0];
            return  $name['user_pin'];
        }           
    }
    public function logFundTransfer($data){

        return $this->db->insert('transfer_amount_history', $data);

    }

    public function getTransferCreditAmountByUserCode($user_code,$type){

       $fund_e_cash_arr = $this->db->select_sum('tah.credit')
       ->from('transfer_amount_history as tah')
       ->where('sender_code', $user_code)
       ->where('deduct_from',$type)
       ->get()->result_array();
       $fund_e_cash_arr = current($fund_e_cash_arr);
       $fund_e_cash = empty($fund_e_cash_arr['credit']) ? 0 : $fund_e_cash_arr['credit'];
       return $fund_e_cash;

   }
   public function getTransferDebitAmountByUserCode($user_code){

    $fund_e_cash_arr = $this->db->select_sum('tah.debit')
    ->from('transfer_amount_history as tah')
    ->where('reciver_code', $user_code)
    ->get()->result_array();
    $fund_e_cash_arr = current($fund_e_cash_arr);
    $fund_e_cash = empty($fund_e_cash_arr['debit']) ? 0 : $fund_e_cash_arr['debit'];
    return $fund_e_cash;

}

}