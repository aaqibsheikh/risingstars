<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class superAdminModel extends MY_Model {

    // log amount that is paid by admin
    public function logWithdrawProcess($data) {

        // get data to  log
        $insert_data = array();
        foreach ($data as $k => $v)
            $insert_data[$k] = $v;

        $insert_data['created_at'] = date('Y-m-d H:i:s');
        return $this->db->insert('withdraw_payment_histry', $insert_data);
    }

    public function getWithdrawAmount($id) {

        // get the sum of all amount paid against the id
        $amount = $this->db->select('SUM(amount) as total_amount_paid')
        ->from('withdraw_payment_histry')
        ->where('withdraw_payment_histry.user_id=' . $id)
        ->get()->result();

        $total_amount_paid = current($amount);

        // always return zero in case there is not amount paid
        return ($total_amount_paid->total_amount_paid > 0) ? $total_amount_paid->total_amount_paid : 0;
    }

    public function getToalPaidAmount($user_id) {

        // get the sum of all amount paid by admin
        $amount = $this->db->select('SUM(amount) as total_amount_paid')
        ->from('withdraw_payment_histry')
        ->get()->result();

        $total_amount_paid = current($amount);

        // always return zero in case there is not amount paid
        return ($total_amount_paid->total_amount_paid > 0) ? $total_amount_paid->total_amount_paid : 0;
    }

    public function getWithdrawHistry() {

        // get the sum of all amount paid by admin
        $histry = $this->db->select('*')
        ->from('withdraw_payment_histry')
        ->order_by("id", "desc")
        ->get()->result_array();

//        $total_amount_paid = current($amount);
        // always return zero in case there is not amount paid
        return $histry;
    }
    public function getWithdrawHistryByid($user_id) {

        // get the sum of all amount paid by admin
        $histry = $this->db->select('*')
        ->from('withdraw_payment_histry as wph')
        ->where("wph.user_id",$user_id)
        ->get()->result_array();

//        $total_amount_paid = current($amount);
        // always return zero in case there is not amount paid
        return $histry;
    }

    public function getLatestWithdrawHistry() {
        // get the sum of all amount paid by admin
        $histry = $this->db->select('wph.*,rs.full_name')
        ->from('withdraw_payment_histry as wph')
        ->order_by("wph.id", "desc")
        ->join('rising_users as rs', 'rs.user_id=wph.user_id')
        ->limit(5)
        ->get()->result_array();
//        $total_amount_paid = current($amount); 
        // always return zero in case there is not amount paid
        return $histry;
    }

    public function getLatestCommisiomHistry($eligible_user) {
        $histry = $this->db->select('*')
        ->from('rising_commissions as rs')
        ->order_by("rs.id", "desc")
        ->where('rs.transaction_type!=','azadi')
        ->where('rs.eligible_user_id',$eligible_user)
        ->limit(5)
        ->get()->result_array();

        $commison_detail_arr = array();
        $commison_latest_arr = array();
        foreach ($histry as $row) {

            $result = $this->UserModel->getUserDetailById($row['rising_user_id']);
            $user_detail = current($result);
            $commison_detail_arr['Username'] = $this->UserModel->getUserNameById($row['rising_user_id']);
            $commison_detail_arr['amount'] = $row['commision_amount'];
            $commison_detail_arr['package'] = $this->UserModel->getUserPackageNameById($row['package']);
            $commison_detail_arr['commision_type'] = $row['commision_type'];
            $commison_detail_arr['transaction_type'] = $row['transaction_type'];

            $commison_detail_arr['user_code'] = $user_detail['username'];
            $commison_detail_arr['created_at'] = $row['created_at'];
            array_push($commison_latest_arr, $commison_detail_arr);
        }
        return $commison_latest_arr;
    }
    public function getFullMissedHistryForSuperAdmin() {

        $histry = $this->db->select('*')
        ->from('rising_missed_commissions as rs')
        ->where('rs.transaction_type!=','azadi')
        ->order_by("rs.id", "desc")

        ->get()->result_array();

        $commison_detail_arr = array();
        $commison_latest_arr = array();
        foreach ($histry as $row) {

            $result = $this->UserModel->getUserDetailById($row['rising_user_id']);
            $result_elig = $this->UserModel->getUserDetailById($row['eligible_user_id']);
            $result_elig = current($result_elig);

            $user_detail = current($result);
            $commison_detail_arr['Username'] = $this->UserModel->getUserNameById($row['rising_user_id']);
            $commison_detail_arr['amount'] = $row['commision_amount'];
            $commison_detail_arr['package'] = $this->UserModel->getUserPackageNameById($row['package']);
            $commison_detail_arr['commision_type'] = $row['commision_type'];
            $commison_detail_arr['transaction_type'] = $row['transaction_type'];

            $commison_detail_arr['user_code'] = $user_detail['username'];
            $commison_detail_arr['elig_code'] = $result_elig['username'];
            $commison_detail_arr['created_at'] = $row['created_at'];
            array_push($commison_latest_arr, $commison_detail_arr);
        }
        return $commison_latest_arr;
    }


    public function LogFundEcash($data) {
        return $this->db->insert('fund_e_cash_histry', $data);
    }

    public function getFundECashHistry() {
        $fund_e_cash_histry = $this->db->select('*')
        ->from('fund_e_cash_histry as fec')
        ->order_by("fec.id", "desc")
        ->get()->result_array();
        return $fund_e_cash_histry;
    }

    public function getFundECashHistryByMemberCode($member_code) {//fund e cash by member code eg.160600001
        $fund_e_cash_histry = $this->db->select('*')
        ->from('fund_e_cash_histry as fec')
        ->where('fec.member_code', $member_code)
        ->where('fec.deduct_from', null)
        ->get()->result_array();
        return $fund_e_cash_histry;
    }

    public function getTotalRecivedAmount($member_code) {//total recived amount for user dashboard
        $missed_commisson_arr = $this->db->select_sum('fec.credit')
        ->from('fund_e_cash_histry as fec')
        ->where('fec.member_code', $member_code)
        ->get()->result_array();
        
        $missed_commisson = current($missed_commisson_arr);

        $missed_commisson_amount = empty($missed_commisson['credit']) ? 0 : $missed_commisson['credit'];
        return $missed_commisson_amount;
    }

}
