<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class usermodel extends MY_Model {

    public function registerUser($data) {



//        $username = $this->random_username_string();
        $id = $this->aauth->create_user($data['email'], $data['password'], $data['email']);




        if ($id > 0) {
            $this->aauth->add_member($id, 4);

            $newUserCode = $this->genrateLoginCode($id);
            $update_status = $this->aauth->update_user($id, FALSE, FALSE, $newUserCode);

            $data = array(
                'cnic' => $data['cnic'],
                'address' => $data['address'],
                'kin_name' => $data['kinname'],
                'kin_relation' => $data['kinrelation'],
                'kin_cnic' => $data['kincnic'],
                'father_name' => $data['mobile'],
                'introducer_code' => $data['introducer'],
                'parent_id' => $this->aauth->get_user_id(),
                'user_id' => $id,
                'package_id' => $data['package'],
                'full_name' => $data['username']
            );

            $result = $this->db->insert('rising_users', $data);

//            echo $result;
//            if ($result) {
//
//
//
//
//
//
//                $parentParents = $this->db->select('parents')
//                                ->from('rising_users_tree')
//                                ->where('rising_users_tree.aauth_user_id=' . $this->aauth->get_user_id())
//                                ->get()->result();
//
//                $parent = current($parentParents);
//
//                $parentParents = $parent . ',' . $this->aauth->get_user_id();
//
//                $data = array('aauth_user_id' => $id, 'parents' => $parentParents);
//
//                $this->addAffiliate($data);
//
//
//
//
//
//                die;
//
////                $comma_separated_parents = implode(",", $array);
//            } 


            if ($result) {

                $insert_affilate_id = $this->addAffiliate($this->aauth->get_user_id());
                if ($insert_affilate_id) {

                    $this->addLevel($insert_affilate_id);
                    $this->addMember($insert_affilate_id);
                }
            }



            return $result;
        }
        return 0;
    }

    public function genrateLoginCode($id) {

        $year = date("y");
        $month = date('m', strtotime('0 month'));

        $length = strlen($id);
        if ($length == 1) {
            return $newUserCode = $year . $month . '0000' . $id;
        } elseif ($length == 2) {
            return $newUserCode = $year . $month . '000' . $id;
        } elseif ($length == 3) {
            return $newUserCode = $year . $month . '00' . $id;
        } elseif ($length == 4) {
            return $newUserCode = $year . $month . '0' . $id;
        }
    }

    public function getPacakges() {

        $query = $this->db->get('package');

        return $query;
    }

    //array=parent_id and user_id
    protected function addAffiliate($user_id) {

        //SELECT * FROM Customers
        //WHERE City IN ('Paris','London'); 
        $parentParents = $this->db->select('parents')
                        ->from('rising_users_tree')
                        ->where('rising_users_tree.aauth_user_id=' . $user_id)
                        ->get()->result();

        $current_custum = current($parentParents);
        
        _pr($current_custum);
        echo 'parent of current user'.$current_custum->parents;
        
        
        
        $parents = $this->aauth->get_user_id();
        echo 'child'.$parents;
        if (strpos($current_custum->parents, '0') === FALSE) {
            $parents = $parents . ',' . $current_custum->parents;
        }


        $data = array('aauth_user_id' => $user_id, 'parents' => $parents);


        $result = $this->db->insert('rising_users_tree', $data);
        echo 'insert_id'.$this->db->insert_id();
        die;
        if ($result) {
            return $this->db->insert_id();
        }

        return 0;
    }

    public function addLevel($affilate_id) {

        $parent_id = $this->getParentById($affilate_id);

        $level = $this->getLevelByID($parent_id);


        if ($level > 0) {
            $parents = $this->getParentParentsById($parent_id);
            $this->db->query("UPDATE rising_users_tree SET user_tree_level = user_tree_level + 1 WHERE aauth_user_id IN ($parents)");
        }
    }

    protected function addMember($affilate_id) {

        $parent_id = $this->getParentById($affilate_id);

        $parents = $this->getParentParentsById($parent_id);


        $this->db->query("UPDATE rising_users_tree SET user_team_members = user_team_members + 1 WHERE aauth_user_id IN ($parents)");
    }

    public function getParentById($id) {
        $parent = $this->db->select('parent_id')
                        ->from('rising_users')
                        ->where('rising_users.id=' . $id)
                        ->get()->result();
        $parent = $parent[0];
        if ($parent) {
            return $parent->parent_id;
        }
    }

    public function getLevelByID($id) {

        $levels = $this->db->select('user_tree_level')
                        ->from('rising_users_tree')
                        ->where('rising_users_tree.aauth_user_id=' . $id)
                        ->get()->result();
        $level = $levels[0];
        return $level->user_tree_level;
    }

    public function getParentParentsById($id) {

        $parentParents = $this->db->select('parents')
                        ->from('rising_users_tree')
                        ->where('rising_users_tree.aauth_user_id=' . $id)
                        ->get()->result();
        $parents = $parentParents[0];
        return $parents->parents;
    }

    public function getUserLevel($id) {
        $userLevel = $this->db->select('user_tree_level')
                        ->from('rising_users_tree')
                        ->where('rising_users_tree.aauth_user_id=' . $id)
                        ->get()->result();
        $userTreeLevel = $userLevel[0];
        return $userTreeLevel->user_tree_level;
    }

    public function getTotalMembers($id) {
        $userLevel = $this->db->select('user_team_members')
                        ->from('rising_users_tree')
                        ->where('rising_users_tree.aauth_user_id=' . $id)
                        ->get()->result();
        $userTreeLevel = $userLevel[0];
        return $userTreeLevel->user_tree_level;
    }

}

