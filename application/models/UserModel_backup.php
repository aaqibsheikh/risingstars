<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class usermodel extends MY_Model {

    public function registerUser($data) {


        $active_user = $this->aauth->get_user();
        $active_user_id = $active_user->id ;
        if($active_user_id == 1)
            $previous_date = _ConvertDbDatetoUserDate($data['previous']);
        else
            $previous_date = _ConvertDbDatetoUserDate(date('d/m/Y H:i:s'));


        // echo $this->session->user_package_id;
        //          echo 'package_id'.'<br>';        
        // echo $this->session->admin_cash;
        //          echo 'admin_cash'.'<br>';
        // echo $this->session->package_amount_for_commision;
        //          echo 'calculation'.'<br>';

        $path = ASSETS_URL . '/uploads/no-pic.png';

        // if (LIVE) {
        //     $data['password'] = '786@star'; //;$this->genrateUserPassword();
        // } else {
        $data['password'] = '123456';
        // }

        // $password_genrated = $this->genrateUserPassword();//genrated  random password
        $password_genrated = $data['password'];

        $id = $this->aauth->create_user($data['email'],$data['password'],$data['email']); // create auth user

        if ($id > 0) {

            // required models/library/helper function
            $this->load->model('DashboardModel');

            $this->aauth->add_member($id, 2); // make public user

            $newUserCode = $this->genrateLoginCode($id); // create user code e-i 160600006
            $update_status = $this->aauth->update_user($id, FALSE, FALSE, $newUserCode); // replace the name with user code created above
            $parentId = $this->getuserIdbyCode($data['introducer']);
            $introducer_id = $parentId;
            $path = empty($data['user_pic']) ? $path : $data['user_pic'];
            $parentId = empty($parentId) ? '' : $parentId;

            $direct_reffrals = $this->DashboardModel->introduceByUser($data['introducer']);
            $total_reffrals = $direct_reffrals->num_rows(); // get total number of hands - we will use it to save location

            if ($total_reffrals >= 0) {
                $total_reffrals = $total_reffrals + 1;
            }

            // data for rising_users table

            $UserPin = $this->genrateUserPin();
            $data = array(
                'cnic' => $data['cnic'],
                'address' => $data['address'],
                'kin_name' => $data['kinname'],
                'kin_relation' => $data['kinrelation'],
                'kin_cnic' => $data['kincnic'],
                'father_name' => $data['fname'],
                'mobile_no' => $data['mobile'],
                'introducer_code' => $data['introducer'],
                'parent_id' => $parentId['id'],
                'user_id' => $id,
                'package_id' => $this->session->user_package_id, //change package id according to cash amount
                'full_name' => $data['username'],
                'location' => $total_reffrals,
                'user_pic_path' => $path,
                'previous'=> $previous_date,
                'user_pin' => $UserPin,
                'package_updated_at' => serialize(array($this->session->user_package_id => date('Y-m-d H:i:s')))
            );

            $res_rising_users = $this->db->insert('rising_users', $data);
            $last_insert_id = $this->db->insert_id();

            $ids = array(
                'id' => $id,
                'user_id' => $introducer_id['id'],
            );

            if ($res_rising_users) {

                $insert_affilate_id = $this->addAffiliate($ids); // add in rising user tree table 

                if ($insert_affilate_id) {

                    $this->addLevel($insert_affilate_id);
                    $this->addMember($insert_affilate_id);
                }
            }
            $id_password = array(
                'password' => $data['password'],
                'login_code' => $newUserCode,
                'Pin_Code' => $UserPin,
                'last_insert_id' => $last_insert_id
            );

            return $id_password; // return if true
        }
        return 0; // return if false
    }

    public function getlastpackageUpdate($user_id) {
        $lastpackage = $this->db->select('package_updated_at')
        ->from('rising_users')
        ->where('rising_users.user_id=', $user_id)
        ->get()->result_array();

        if ($lastpackage) {
            $upaadted = current($lastpackage);
            return $upaadted['package_updated_at'];
        }
    }

    public function UpdateUser($data) {

        // print_r($data['user_pic']);

        $user = $this->aauth->get_user();

        $dataa = array(
            'cnic' => $data['cnic'],
            'address' => $data['address'],
            'kin_name' => $data['kinname'],
            'kin_relation' => $data['kinrelation'],
            'kin_cnic' => $data['kincnic'],
            'father_name' => $data['fname'],
            'mobile_no' => $data['mobile'],
            'full_name' => $data['username'],
            'user_pic_path' => $data['user_pic']
        );

        $this->db->where('id', $user->id);
        $result = $this->db->update('rising_users', $dataa);

        

        return $result;
    }

    public function upgradePackage($data) {

        $serialized_package_data = $this->getlastpackageUpdate($data['user_id']);
        $package = $data['package'];
        $updated_package_date = array();

        $unseralize_package_data = unserialize($serialized_package_data);


        if (is_array($unseralize_package_data)) {
            foreach ($unseralize_package_data as $k => $p_data)
                if (is_array($p_data))
                    array_push($updated_package_date, $p_data);
            }
            array_push($updated_package_date, array('date' => date('Y-m-d H:i:s'), 'package_id' => $package));

            $update_package = array(
                'package_id' => $package,
                'updated_at' => date('Y-m-d H:i:s'),
                'package_updated_at' => serialize($updated_package_date)
            );

            $this->db->where('user_id', $data['user_id']);
            $result = $this->db->update('rising_users', $update_package);

            if ($result) {
                return $data['user_id'];
            } else {
                return 0;
            }
        }

        public function genrateLoginCode($id) {

            $year = date("y");
            $month = date('m', strtotime('0 month'));

            $length = strlen($id);
            if ($length == 1) {
                return $newUserCode = $year . $month . '0000' . $id;
            } elseif ($length == 2) {
                return $newUserCode = $year . $month . '000' . $id;
            } elseif ($length == 3) {
                return $newUserCode = $year . $month . '00' . $id;
            } elseif ($length == 4) {
                return $newUserCode = $year . $month . '0' . $id;
            }
        }

        public function getPacakges() {

            $query = $this->db->get('package');

            return $query;
        }

    //array=parent_id and user_id
        protected function addAffiliate($user_id) {

        /* takes two ids
         * loged in user id is user_id
         * last inserted user id is id
         */
//        echo '<li>---------2------------';
//        _pr($user_id);
//        echo '<li>---------------------';
//        echo 'userid' . $user_id['user_id'];
        //SELECT * FROM Customers
        //WHERE City IN ('Paris','London'); 
        // selecting parents of loged in user so that we can pass it to tree table
        $parentParents = $this->db->select('parents')
        ->from('rising_users_tree')
        ->where('rising_users_tree.aauth_user_id=' . $user_id['user_id'])
        ->get()->result();


        $current_custum = current($parentParents);


        $parents = $user_id['user_id'];


        if (strpos($current_custum->parents, '0') === FALSE) {
            $parents = $user_id['user_id'] . ',' . $current_custum->parents;
        }


        $data = array('aauth_user_id' => $user_id['id'], 'parents' => $parents);


        $result = $this->db->insert('rising_users_tree', $data);
        if ($result) {

            return $this->db->insert_id();
        }

        return 0;
    }

    public function addLevel($affilate_id) {

        echo $affilate_id.'in add level';        

        $parent_id = $this->getParentById($affilate_id);
//        echo 'parentid ' . $parent_id;
        $level = $this->getLevelByID($parent_id);
//        echo 'parentlevel ' . $level;

        if ($level == 0) {
            $parents = $this->getParentParentsById($parent_id);

            if ($parents == 0)
                $parents = $parent_id;
            else
                $parents = $parent_id . ',' . $parents;

            $this->db->query("UPDATE rising_users_tree SET user_tree_level = user_tree_level + 1 WHERE aauth_user_id IN ($parents)");
//            echo '<li>' . _last_query() . '' . '<br>';
        }
    }

    protected function addMember($affilate_id) {

       // echo $affilate_id.'in add member';

       $parent_id = $this->getParentById($affilate_id);


//        echo '<li> parentid in addMember ' . $parent_id;
       $parents = $this->getParentParentsById($parent_id);

       if ($parents == 0)
        $parents = $parent_id;
    else
        $parents = $parent_id . ',' . $parents;

    $this->db->query("UPDATE rising_users_tree SET user_team_members = user_team_members + 1 WHERE aauth_user_id IN ($parents)");
//        echo '<li>' . _last_query() . '<br>';
}

    //get parent of current user by id
public function getParentById($id) {

    // echo $id.'in get parent id';

    // die;

    $parent = $this->db->select('parent_id')
    ->from('rising_users')
        ->where('rising_users.id=' . $id)//
        ->get()->result();
            $parent = $parent[0];
            if ($parent) {
                return $parent->parent_id;
            }
        }

    //get all parents by user_id
        public function getParentsById($id) {
            $parents = $this->db->select('parents')
            ->from('rising_users_tree')
            ->where('rising_users_tree.aauth_user_id=' . $id)
            ->get()->result();
            $parent = $parents[0];
            if ($parent) {
                return $parent->parents;
            }
        }

    //return the code of user againt user_id  
        public function getparentscodebyid($user_id) {
            $query = "aauth_users.id IN($user_id)";
            $parentsCode = $this->db->select("aauth_users.username")->
            from('aauth_users')
            ->where($query)
            ->get()
            ->result_array();


            if ($parentsCode) {

                return $parentsCode;
            }
        }

//return user id against code as 060600001
        public function getuserIdbyCode($code) {

            $userId = $this->db->select("aauth_users.id")->
            from('aauth_users')
            ->where('aauth_users.username', $code)
            ->get()
            ->result_array();
            if ($userId) {

                return current($userId);
            }
        }

    //get level of the user 
        public function getLevelByID($id) {

            $levels = $this->db->select('user_tree_level')
            ->from('rising_users_tree')
            ->where('rising_users_tree.aauth_user_id=' . $id)
            ->get()->result();
            $level = $levels[0];
            return $level->user_tree_level;
        }

        public function getParentParentsById($id) {

            $parentParents = $this->db->select('parents')
            ->from('rising_users_tree')
            ->where('rising_users_tree.aauth_user_id=' . $id)
            ->get()->result();
            $parents = $parentParents[0];
            return $parents->parents;
        }

//get total user levels 
        public function getUserLevel($id) {
            $userLevel = $this->db->select('user_tree_level')
            ->from('rising_users_tree')
            ->where('rising_users_tree.aauth_user_id=' . $id)
            ->get()->result();

            if (!empty($userLevel)) {
                $userTreeLevel = $userLevel[0];
                return $userTreeLevel->user_tree_level;
            }
        }

//get total mebers  
        public function getTotalMembers($id) {
            $userLevel = $this->db->select('user_team_members')
            ->from('rising_users_tree')
            ->where('rising_users_tree.aauth_user_id=' . $id)
            ->get()->result();

            if (!empty($userLevel)) {
                $user_team = $userLevel[0];
                return $user_team->user_team_members;
            }
        }

        public function getCurrentUserPinCode()
        {
            $user = $this->aauth->get_user();
            $username = $this->db->select('rs.user_pin')
            ->from('aauth_users as au')
            ->join('rising_users as rs', 'au.id=rs.user_id', 'inner')
            ->where('rs.user_id=' . $user->id)
            ->get()->result_array();
            if (!empty($username)) {
                $name = $username[0];
                return  $name['user_pin'];
            }           
        }

        public function aauthUserPin($introducer_code) {
//        count perm 
//        if (count($params) != 2) {
//            return 0;
//        }
            $where = "au.username='$introducer_code'";
            $users_id = $this->db->select('au.id')
            ->from('aauth_users as au')
            ->join('rising_users as rs', 'au.id=rs.user_id', 'inner')
            ->where($where)
            ->get()->result_array();

//        echo $this->db->last_query();
//        
//        die;

            if (!empty($users_id)) {
                $user = $users_id[0];
                return $user['id'];
            }

            return 0;
        }

        public function getUserDetailById($user) {

            $result = $this->db->select('*')
            ->from('rising_users as rs')
            ->join('aauth_users as au', 'au.id=rs.user_id', 'inner')
            ->where('rs.user_id=' . $user)
            ->get()->result_array();
//        _pr($result);
            return $result;
        }

        public function getAllUsersDetail() {

            $result = $this->db->select('au.username as member_code,au.id,rs.full_name,au.email,au.banned')
            ->from('rising_users as rs')
            ->join('aauth_users as au', 'au.id=rs.user_id', 'inner')
            ->get()->result_array();

//        _pr($result);
            return $result;
        }
        public function getAllBlockUsers() {
            $result = $this->db->select('*')
            ->from('rising_users as rs')
            ->join('aauth_users as au', 'au.id=rs.user_id', 'inner')
            ->where('au.banned','1')
            ->get()->result_array();
//        _pr($result);
            return $result;
        }
        public function getAllActiveUsers() {
            $result = $this->db->select('*')
            ->from('rising_users as rs')
            ->join('aauth_users as au', 'au.id=rs.user_id', 'inner')
            ->where('au.banned','0')
            ->get()->result_array();
//        _pr($result);
            return $result;
        }

        public function getAllRisingUsersDetail($id = 0, $param = array()) {

            $where = "1 = 1";
            $select = "*";

            if (!empty($param)) {
                $select = implode(',', $param);
            }
            if ($id > 0) {
                $where = "user_id = $id";
            }

            $result = $this->db->select($select)
            ->from('rising_users')
            ->where($where)
            ->get()->result_array();
            return $result;
        }

        public function genrateUserPin() {
            return random_string('numeric', 4);
        }

        public function genrateUserPassword() {

            $user_pin = random_string('alnum', 8);
            return $user_pin;
        }

    public function getUserLocationById($user_id) {// returducer location recive member id
        $result = $this->db->select('location')
        ->from('rising_users as rs')
        ->where('rs.user_id=' . $user_id)
        ->get()->row();
        $location = $result->location;

        return $location;
    }

    public function getUserMonth($id) {//return its month recived id of a user
        $result = $this->db->select('date_created')
        ->from('aauth_users')
        ->where('aauth_users.id =' . $id)
        ->get()->row();

        $timestamp = $result->date_created;
        $datetime = explode(" ", $timestamp);
        $registration_date = $datetime[0];

        $time = $datetime[1]; //1 registration time

        $current_date = date('Y-m-d');            //2current date
        $date1 = date_create($registration_date);
        $date2 = date_create($current_date);
        $diff = date_diff($date1, $date2);
        $days = $diff->format("%R%a days");
        if ($days == 0) {
            $days = 1;
        }

        $resultf = '';
        $month = $days / 30;

        $month = ceil($month);

        if ($month % 2 == 0) {
            $resultf = 'Even';
        } else {
            $resultf = 'Odd';
        }

        return $resultf;
    }

    public function getHandsByUserId($id) {


        $result = $this->db->select('au.date_created as joining_date,au.username as member_Id,rs.full_name as Name,rs.id as Serial_No,rs.user_id as userid')
        ->from('aauth_users as au')
        ->join('rising_users as rs', 'au.id=rs.user_id')
        ->where('rs.parent_id', $id)
        ->get();

        return $result->num_rows();
    }

    public function getUserPackageAmountById($user_id) {

        $package_amount = $this->db->select('pack.PackagePrice as PackagePrice')
        ->from('package as pack')
        ->join('rising_users as rs', 'pack.Package_ID=rs.package_id', 'inner')
        ->where('rs.user_id', $user_id)
        ->get()->row();
        return $package_amount->PackagePrice;
    }

    public function getHandsByPackageId($user_id, $package_id = 0) {

        $ss = ($package_id > 0) ? $package_id : MONTHLY_DEFAULT_PACKAGE_CHECK;
        $where = "rs.parent_id='$user_id' AND rs.package_id>='$ss'";
        $result = $this->db->select('rs.user_id')
        ->from('rising_users as rs')
        ->where($where)
        ->get();
        return $result->num_rows();
    }

    public function getHandsByPackageIdInCurrentMonth($user_id) {



        $start_day = date('Y-m-01');
        $end_day = date('Y-m-t');

        $where = "rs.parent_id='$user_id' AND au.date_created >= '" . $start_day . "' AND au.date_created <= '" . $end_day . "' ";

        $result = $this->db->select('rs.id')
        ->from('rising_users as rs')
        ->join('aauth_users as au', 'au.id=rs.user_id', 'left')
        ->where($where)
        ->get();

//        _pr($result->result_array());
//        echo '<li> current month hands  ' . $result->num_rows() . '</li>';

        return $result->num_rows();
    }

    public function getUserProfilePic($user_id) {

        $ProfilePic = $this->db->select('rs.user_pic_path')
        ->from('rising_users as rs')
        ->where('rs.user_id', $user_id)
        ->get()->row();
        return $ProfilePic->user_pic_path;
    }


    public function getUserPackageById($user_id) {

        $parent = $this->db->select('package_id')
        ->from('rising_users')
        ->where('rising_users.id=' . $user_id)
        ->get()->result();

        if (!empty($parent)) {
            $parent = $parent[0];
            return $parent->package_id;
        }
    }

    public function getUserPackageNameById($package_id) {
        $package_name_arr = $this->db->select('PackageName')
        ->from('package')
        ->where('Package_ID=', $package_id)
        ->get()->row();



        if ($package_name_arr) {

            return $package_name_arr->PackageName;
        }
    }

    public function getUserNameById($user_id) {
        $name_arr = $this->db->select('full_name')
        ->from('rising_users as rs')
        ->where('rs.user_id', $user_id)
        ->get()->row();
        if ($name_arr) {

            return $name_arr->full_name;
        }
    }

//this function will return the commisson report of given user
    public function getCommissionReport($user_id) {
        $result = $this->db->select('au.username,rs.commision_amount,rs.commision_type')
        ->from('rising_commissions as rs')
        ->join('aauth_users as au', 'rs.rising_user_id=au.id')
        ->where('rs.eligible_user_id', $user_id)
        ->get();
        return $result;
    }

    public function changePasswordById($new_pass,$id) {

        $update_status = $this->aauth->update_user($id, FALSE, $new_pass, FALSE);
        return $update_status;
    }

    public function resetPassword() {

        $new_password = $this->genrateUserPassword();

        return $new_password;
    }

    public function resetPin() {

        $new_password = $this->genrateUserPin();


        $data = array(
            'user_pin' => $new_password
        );

        $this->db->where('user_id', $this->aauth->get_user_id());
        $this->db->update('rising_users', $data);

        return $new_password;
    }

    public function mail_commissions_assign($user_id) {

        $this->load->library('my_mail');

        $eligibles_users_detail = $this->db->select("*")
        ->from('rising_commissions as rs')
        ->where('rs.rising_user_id', $user_id)
        ->get()
        ->result_array();

        foreach ($eligibles_users_detail as $key) {

            //getting user detail like name email
            $userdetail = $this->getUserDetailById($key['eligible_user_id']);
            $detail = current($userdetail);
            //get code of inserted user 
            $packagename = current($this->getparentscodebyid($key['rising_user_id']));

            $datae_for_mail = array(//array data for mail
                'username' => $detail['full_name'],
                'email' => $detail['email'],
                'rising_user_id' => $packagename['username'],
                'commission_amount' => $key['commision_amount'],
                'commission_type' => $key['commision_type'],
                'transaction_type' => $key['transaction_type'],
                'package' => $this->getUserPackageNameById($key['package']),
            );
            switch ($key['commision_type']) {

                case "80":

                    //send mail to 80% eligible user
                $this->my_mail->mail_log_eighty_percent_commission($datae_for_mail);





                break;
                case "20":

                    //send mail to 20% eligible user
                $this->my_mail->mail_log_twenty_percent_commission($datae_for_mail);


                case "1":

                    //send mail to 1% eligible user
                $this->my_mail->mail_log_One_percent_commission($datae_for_mail);
                default:
                '';
            }
        }
    }

    public function getbreadcrumb($id) {

        $this->load->model('DashboardModel');
        $arr = array();
        $Parents = $this->getParentsById($id);
        $ParentsCode = $this->getparentscodebyid($Parents);

        $parentsarray = explode(',', $Parents);

        foreach ($ParentsCode as $row) {
            $arr[] = $row['username'];
        }

//        $breadcrumbs = implode('/', $arr);
        return $arr;
    }

    /*
     *  
     * Distribure Rewards Logis Start
     * 
     */

    //this function will recive the id of the user wich we want to give reward
    public function giveRewards($id) {


        //this will get the all lines of given user against this id
        $direct_hands_array = $this->DashboardModel->introduceByUserId($id);

        $user_package = $this->getUserPackageById($id);
        $reward_amount = array();
        if ($user_package >= MIN_PACKAGE_FOR_REWARD) {

            foreach ($direct_hands_array->result() as $row) {

                $sum = $this->getAllChilds($row->userid, $sum = 0); //get all the childs and calculate the sum line by line 

                if ($sum >= 7000) {

                    $reward = (int) ($sum / 7000); //bcz we need count of gold package users 

                    $remaing = (int) ($sum % 7000); //getting the remaining amount ;

                    $reward_amount[$row->userid] = array(//it will store the reward count for all seven lines
                        'gold_count' => $reward,
                        'remaing' => $remaing
                    );
                } else {

                    $reward_amount[$row->userid] = array(//it will store the reward count for all seven lines
                        'gold_count' => '0',
                        'remaing' => $sum
                    );
                }
            }
            return $reward_amount;
        }




        return FALSE;
    }

    //this function will recive the id and calculate the sum of its all childs in tree.
    protected function getAllChilds($parent_id, $sum = 0) {



        $result = $this->db->select('id,package_id')
        ->from('rising_users')
        ->where('parent_id', $parent_id)
        ->get();

        foreach ($result->result() as $key) {

            $updated_date = $this->getPackageUpdatedDate($parent_id);

            $created_date = $this->getUserCreatedDate($key->id);
            if ($updated_date) {

                if ($created_date >= $updated_date) {

                    $sum += _get_package_price($key->package_id);
                    $sum = $this->getAllChilds($key->id, $sum);
                }
            }
        }

        return $sum;
    }

    //this function return package udated date .
    protected function getPackageUpdatedDate($user_id) {

        $result = $this->db->select('package_updated_at')
        ->from('rising_users')
        ->where('user_id', $user_id)
        ->get()
        ->result();

        $result = current($result);

        $seralize_packg_array = $result->package_updated_at;

        $package_array = unserialize($seralize_packg_array);

        foreach ($package_array as $key => $value) {//genrating error on local because this is empty ;so i make sure n registration package update entry should be added;
            if ($key >= 3) {
                return date('Y-m-d', strtotime($value));
            }
            return FALSE;
        }
    }

    //this functin return the user created date .
    protected function getUserCreatedDate($user_id) {

        $user = $this->aauth->get_user($user_id);
        return date('Y-m-d', strtotime($user->date_created));
    }

    function getUserStatus($user_id) {/* Checks if a user is banned @param int $user_id User id to check @return bool False if banned, True if not */

        $result = $this->aauth->is_banned($user_id);
        return $result;
    }

    function blockUser($user_id) {//@param int $user_id User id to block @return bool block fails/succeeds    
        $block_result = $this->aauth->ban_user($user_id);
        return $block_result;
    }

    function activeUser($user_id) {//@param int $user_id User id to active/unblock @return bool Activation fails/succeeds  
        $unblock_result = $this->aauth->unban_user($user_id);
        return $unblock_result;
    }
    public function gettotalRegisterUsersCount() {

       $total_block=$this->db->select('*')
       ->from('aauth_users as au')

       ->get()->num_rows();

       return $total_block;
   }
    public function getTotalBlockUnblockCount($status){//parameter 1 for active and 0 for block return total rows count
       $status = ($status == "1") ? "1" : "0";
       $total_block=$this->db->select('*')
       ->from('aauth_users as au')
       ->where('au.banned',$status)
       ->get()->num_rows();


       return $total_block;
   }

//     public function getUserDetailwithCommisionLogbyid($user){
//
//        $result = $this->db->select('*')
//                        ->from('rising_users as ru')
//                        ->join('aauth_users as au', 'au.id=ru.user_id', 'inner')
//                        ->join('rising_commissions as rs', 'rs.rising_user_id=au.id', 'inner')
//                        ->where('au.id=' . $user)
//                        ->get()->result_array();
////        _pr($result);
//        return $result;
//    }

    /*     * ** * **********************************************************
     *  Distribure REwards Logis end                          *
     * * * ****************************************************** */
}
