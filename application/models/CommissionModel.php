<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class commissionmodel extends MY_Model {

    public function assignCommission($user_id) {

        // $user_id is of current user - $introducer is on current index - $introducer_parent is on index plus 1

        $this->load->model('UserModel');
        if ($user_id > 1) {

            $parents = $this->UserModel->getParentParentsById($user_id); // get parents

            $parentsArr = explode(',', $parents);

            /* if current location is odd then move the indirect commission above
             *  if introducer location (even odd) and introducer parent month (even odd) must be equal 
             *  if equal then it should have hands equal to the level from whoch commission if coming
             */

            // check is this id is even or odd
            $current_user_location = $this->UserModel->getUserLocationById($user_id);
            $even_odd_check = _return_even_or_odd($current_user_location);



            //echo '<li>$current_user_location: '. $current_user_location.' => '. $even_odd_check;
            // if equal to zero it is even
            if ($even_odd_check == 'Even') {

                foreach ($parentsArr as $index => $introducer) {
                    if ($introducer != 1) {
//                        echo '<li>$introducer : ' . $introducer;
//                        echo '<li>$parentsArr : ' . $parentsArr[$index + 1];
                        $introducer_parent = $parentsArr[$index + 1];
                        if ($introducer_parent != 1) {
                            $indirect_commission_check = $this->assignIndirectCommission($user_id, $introducer, $introducer_parent);
                            if ($indirect_commission_check > 0) {
                                return $indirect_commission_check;
                                break;
                            }
                        }
                    } else {
                        // it is company code
                        $package_amount = $this->UserModel->getUserPackageAmountById($user_id); // get commission amount
                        $package_id = $this->UserModel->getUserPackageById($user_id); // get commission amount

                        $commission_arr = array("80", "20");
                        foreach ($commission_arr as $k => $commission) {
                            // $commission_amount = _calculate_percentage($package_amount, $commission);
                            $commission_amount = _calculate_percentage($this->session->package_amount_for_commision, $commission);
                            
                            $log_indirect_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => 1,
                                'commision_amount' => $commission_amount, 'package' => $package_id,
                                'commision_type' => $commission, 'transaction_type' => 'indirect');
                            $log_indirect_commission_res = $this->logDirectIndirectCommission($log_indirect_commission_data);
                        }
                        return $log_indirect_commission_res;
                        break;
                    }
                }
            } else {


//                    echo '<li> in odd - assign direct commission $user_id: '. $user_id;
                //$introducer_parent = $parentsArr[$index + 1];
                $this->assignDirectCommission($user_id);
//                    echo '<li> give direct commissionin else of even odd check';
                // echo "odd";
            }
        }
    }

    protected function assignIndirectCommission($user_id, $introducer, $introducer_parent) {

        $this->load->model('UserModel');

        //$introducer is on current index
        $introducer_location = $this->UserModel->getUserLocationById($introducer); // get location of introducer
        $introducer_location_even_odd_check = _return_even_or_odd($introducer_location);

        //  introducer is on index plus 1
        $introducer_parent_month_even_odd_check = $this->UserModel->getUserMonth($introducer_parent); // get month of introducer parent

        if ($introducer_location_even_odd_check === $introducer_parent_month_even_odd_check) {

            // if equal - get id of eligible user for commission that is introducer parent $introducer_parent
            $user_hand = $this->UserModel->getHandsByUserId($introducer_parent);

            //  get evel of eligible user for commission
            $parents = $this->UserModel->getParentParentsById($user_id);
            $parentsArr = explode(',', $parents);
            $eligible_user_level_from_user = array_search($introducer_parent, $parentsArr);   // $key = 1;$introducer_parent
            // callculate commission -  this part of code calculates 80% commission
            $package_amount = $this->UserModel->getUserPackageAmountById($user_id); // get commission amount
            $package_id = $this->UserModel->getUserPackageById($user_id); // get commission amount
            $parent_package_id = $this->UserModel->getUserPackageById($introducer_parent); // get parent package id

            // $commission_amount = _calculate_percentage($package_amount, 80); // calculate 80% commission of package amount
            
            $commission_amount = _calculate_percentage($this->session->package_amount_for_commision,80);// calculate 80% commission of package amount
            
            //
            // if the hands of eligible user equal to the level of the user of commission
            if ($user_hand >= HANDS_COUNT_FOR_INDIRECT || $user_hand >= ( $eligible_user_level_from_user + 1) ) {

                // prepare data to log

                if($parent_package_id == 1){

                    $log_indirect_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => 1,
                        'commision_amount' => $commission_amount, 'package' => $package_id,
                        'commision_type' => '80', 'transaction_type' => 'indirect');    

                    $log_indirect_missed_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $introducer_parent,
                        'commision_amount' => $commission_amount, 'package' => $package_id,
                        'commision_type' => '80', 'transaction_type' => 'indirect');

                    $log_indirect_commission_res = $this->logMissedCommission($log_indirect_missed_commission_data); // pass data to log

                }else{

                    $log_indirect_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $introducer_parent,
                        'commision_amount' => $commission_amount, 'package' => $package_id,
                        'commision_type' => '80', 'transaction_type' => 'indirect');
                }



                $log_indirect_commission_res = $this->logDirectIndirectCommission($log_indirect_commission_data); // pass data to log

                if ($log_indirect_commission_res) {

                    $twenty_percent_commission_eligible = $this->logTwentyPercentCommission($log_indirect_commission_data); //get eligible user for 20% commision
                    //$TwentyPercentCommissionEligible  id is eligible for 20 percent 
                    if ($twenty_percent_commission_eligible >= 1) {//if this is not a company code
                        $twenty_percent_commission_amount = _calculate_percentage($this->session->package_amount_for_commision,20);
                        // $twenty_percent_commission_amount = _calculate_percentage($package_amount, 20); // calculate 20% commission of package amount 

                        if($parent_package_id != 1){

                            $logTwentyPercentCommission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $twenty_percent_commission_eligible,
                                'commision_amount' => $twenty_percent_commission_amount, 'package' => $package_id,
                                'commision_type' => '20', 'transaction_type' => 'indirect');    
                        }else{

                            $logTwentyPercentCommission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => 1,
                                'commision_amount' => $commission_amount, 'package' => $package_id,
                                'commision_type' => '20', 'transaction_type' => 'indirect');    

                            $log_indirect_missed_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $twenty_percent_commission_eligible,
                                'commision_amount' => $commission_amount, 'package' => $package_id,
                                'commision_type' => '20', 'transaction_type' => 'indirect');

                            $log_indirect_commission_res = $this->logMissedCommission($log_indirect_missed_commission_data);

                        }
                        
                    $logTwentyPercentCommission_res = $this->logDirectIndirectCommission($logTwentyPercentCommission_data); // pass data to log
                }


                return $introducer_parent;
            }
        } else {

                // else log missed commission
            $log_indirect_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $introducer_parent,
                'commision_amount' => $commission_amount, 'package' => $package_id,
                'commision_type' => '80', 'transaction_type' => 'indirect');

                $log_indirect_commission_res = $this->logMissedCommission($log_indirect_commission_data); // pass data to log
            }
        }

        return 0;
    }

    protected function assignDirectCommission($user_id) {

        $this->load->model('UserModel');

        $parent_id = $this->UserModel->getParentById($user_id); // get eligible user of current user - current user location is odd
        $package_amount = $this->UserModel->getUserPackageAmountById($user_id); // get commission amount
        $package_id = $this->UserModel->getUserPackageById($user_id); // get commission amount
        // $commission_amount = _calculate_percentage($package_amount, 80); //close due to some reasons
        $parent_package_id = $this->UserModel->getUserPackageById($parent_id); // get parent package for check direct commision
        $commission_amount = _calculate_percentage($this->session->package_amount_for_commision, 80);
        

        if ($parent_id > 1) {
            if($parent_package_id!=1){
                $log_direct_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $parent_id,
                    'commision_amount' => $commission_amount, 'package' => $package_id,
                    'commision_type' => '80', 'transaction_type' => 'direct');
            }else{
                $log_direct_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => 1,
                    'commision_amount' => $commission_amount, 'package' => $package_id,
                    'commision_type' => '80', 'transaction_type' => 'direct');
            // else log missed commission
                $log_direct_missed_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $parent_id,
                    'commision_amount' => $commission_amount, 'package' => $package_id,
                    'commision_type' => '80', 'transaction_type' => 'direct');

                $log_direct_missed_commission_res = $this->logMissedCommission($log_direct_missed_commission_data); // pass data to log
            }
            
            $log_idirect_commission_res = $this->logDirectIndirectCommission($log_direct_commission_data);

            // if direct commission is success
            // if ($log_idirect_commission_res) {

            //     $twenty_percent_commission_eligible = $this->logTwentyPercentCommission($log_direct_commission_data); //get eligible user for 20% commision

            //     // echo "eligible for twenty percent commision".$twenty_percent_commission_eligible;

            //     //$TwentyPercentCommissionEligible  id is eligible for 20 percent 
            //     if ($twenty_percent_commission_eligible >= 1) {//if this is not a company code
            //         // $twenty_percent_commission_amount = _calculate_percentage($package_amount, 20); // calculate 20% commission of package amount 
            //             $twenty_percent_commission_eligible_packg = $this->UserModel->getUserPackageById($twenty_percent_commission_eligible); // get parent package for check direct commision
            //          $twenty_percent_commission_amount = _calculate_percentage($this->session->package_amount_for_commision, 20); // calculate 20% commission of package amount

            //          if($twenty_percent_commission_eligible_packg != 1)
            //          {

            //            $log_twenty_percent_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $twenty_percent_commission_eligible,
            //             'commision_amount' => $twenty_percent_commission_amount, 'package' => $package_id,
            //             'commision_type' => '20', 'transaction_type' => 'direct');   
            //             $logTwentyPercentCommission_res = $this->logDirectIndirectCommission($log_twenty_percent_commission_data); // pass data to log
            //         }
            //         else
            //         {

                        
            //             $log_twenty_percent_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => 1,
            //                 'commision_amount' => $twenty_percent_commission_amount, 'package' => $package_id,
            //                 'commision_type' => '20', 'transaction_type' => 'direct');
            //             $logTwentyPercentCommission_res = $this->logDirectIndirectCommission($log_twenty_percent_commission_data); // pass data to log
                        

            //             $log_direct_missed_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => $twenty_percent_commission_eligible,
            //                 'commision_amount' => $twenty_percent_commission_amount, 'package' => $package_id,
            //                 'commision_type' => '20', 'transaction_type' => 'direct');

            //             $log_direct_missed_commission_res = $this->logMissedCommission($log_direct_missed_commission_data); // pass data to log
                    

            //         }


            //     }
            // }
        } else {

            $commission_arr = array("80", "20");
            foreach ($commission_arr as $k => $commission) {
                // $commission_amount = _calculate_percentage($package_amount, $commission);
                $commission_amount = _calculate_percentage($this->session->package_amount_for_commision, $commission);

                $log_indirect_commission_data = array('rising_user_id' => $user_id, 'eligible_user_id' => 1,
                    'commision_amount' => $commission_amount, 'package' => $package_id,
                    'commision_type' => $commission, 'transaction_type' => 'direct');

                $log_indirect_commission_res = $this->logDirectIndirectCommission($log_indirect_commission_data);
            }
        }
    }

    // get eligible user to assign 20% commission
    public function logTwentyPercentCommission($data){ // array of data - data will be of user eligible for 80% 
        $this->load->model('UserModel');
        // _pr($data);
        $twenty_percent_assigned = false;
        $parent_id = $this->UserModel->getParentById($data['eligible_user_id']); // gret parent of eligible user
        // echo "parent_id->".$parent_id.'<br>';
        if ($parent_id == 0) { // company code check
            return 0;
        } else {
            while (!$twenty_percent_assigned) {
                // check for company code
                if ($parent_id == 1) {
                    return $parent_id;
                    break;
                }
                // get data for condition - that is min 9 hands of ss and 1 ss in current month
                $num_direct_hands = $this->UserModel->getHandsByPackageId($parent_id);
                $num_direct_hands_current_month = $this->UserModel->getHandsByPackageIdInCurrentMonth($parent_id);

                // check above condition
                if ($num_direct_hands >= MIN_SS_HANDS_CHECK && $num_direct_hands_current_month >= 1) {
                    return $parent_id;
                    break;
                }
                else
                {
                    $parent_id = $this->UserModel->getParentById($parent_id);
                }
                // echo "parent_id->".$parent_id.'$num_direct_hands'.$num_direct_hands.'num_direct_hands_current_month'.$num_direct_hands_current_month.'<br>';
            }
        }
    }

    public function logDirectIndirectCommission($data) {

        $insert_data = array();
        foreach ($data as $k => $v)
            $insert_data[$k] = $v;
        $insert_data['created_at'] = date('Y-m-d H:i:s');

        if ($this->UserModel->getUserStatus($data['eligible_user_id'])) {
            $this->logBlockUserDirectIndirectCommission($data);
        } else {
            $res_rising_commissions = $this->db->insert('rising_commissions', $insert_data);
            return $res_rising_commissions;
        }
    }
    protected function logBlockUserDirectIndirectCommission($data) {
        $insert_data = array();
        foreach ($data as $k => $v)
            $insert_data[$k] = $v;
        $insert_data['created_at'] = date('Y-m-d H:i:s');
        $res_rising_commissions = $this->db->insert('rising_commissions', $insert_data);

        return $res_rising_commissions;
    }

    public function logMissedCommission($data) {

        $insert_data = array();
        foreach ($data as $k => $v)
            $insert_data[$k] = $v;
        $insert_data['created_at'] = date('Y-m-d H:i:s');
        $res_rising_miss_commissions = $this->db->insert('rising_missed_commissions', $insert_data);
        return $res_rising_miss_commissions;
    }

    public function assignOnePercentCommission($user_id) {
//        $parentsArr = explode(',', $parents);

        $package_amount = $this->UserModel->getUserPackageAmountById($user_id);
        $package_id = $this->UserModel->getUserPackageById($user_id);

        // $commission_amount = _calculate_percentage($package_amount, ONE_PERCENT_COMMISSION);
        $commission_amount = _calculate_percentage($this->session->package_amount_for_commision, ONE_PERCENT_COMMISSION);

        $eligible_parent_for_Ds = $this->getimediateParentWithDsPackage($user_id);

        $log_one_percent_commission_data = array(//this is the array of eligibale user for DS
            'rising_user_id' => $user_id,
            'eligible_user_id' => ($eligible_parent_for_Ds >= 1) ? $eligible_parent_for_Ds : 1,
            'commision_amount' => $commission_amount, 'package' => $package_id,
            'commision_type' => '1', 'transaction_type' => 'one_percent');

        $log_one_percent_commission_res = $this->logDirectIndirectCommission($log_one_percent_commission_data);

        //email to user after log 
    }

    public function getimediateParentWithDsPackage($user_id) {
//DS_PACKAGE_ID
        $parents = $this->UserModel->getParentParentsById($user_id); // get parents  

        $query = $this->db->query("select user_id From rising_users Where rising_users.package_id=5 AND user_id IN($parents) ORDER BY  user_id DESC limit 1");
        if ($query->num_rows() > 0) {
            $elligible_user_id_arr = $query->result();

            $elligible_user = current($elligible_user_id_arr);
            return $elligible_user->user_id;
        }

        return FALSE;
    }

}
