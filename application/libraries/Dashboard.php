<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class dashboard extends MY_Controller {

    public $user;

    function __construct() {

        parent::__construct();
        $this->load->library('template');
        $this->load->model('DashboardModel');

        //current user
        $this->user = $this->aauth->get_user_id();
        $this->load->model('UserModel');
        $this->load->model('superAdminModel');
    }

    public function index() {

        $pending_commission = $this->DashboardModel->getPendingCommission($this->user);
        $one_commission = $this->DashboardModel->getCommisionOne($this->user);
        $missed_commisson = $this->DashboardModel->getMissedCommisson($this->user);
        $pending_commission_arr = $this->DashboardModel->getCommisionByUserID($this->user);
        $total_paid_amount = $this->superAdminModel->getToalPaidAmount();

        $eighty_commission = $pending_commission_arr['eighty'];
        $twenty_commission = $pending_commission_arr['twenty'];

        $this->load->model('UserModel');
        $level = $this->UserModel->getUserLevel($this->user);
        $Member = $this->UserModel->getTotalMembers($this->user);

        $user = $this->aauth->get_user();
        $direct = $this->DashboardModel->introduceByUser($user->username);
        $total_direct = $direct->num_rows();

        $indirect = 0;
        if ($Member > 0) {
            $indirect = $Member - $total_direct;
        }


        $levelarray = array(
            'direct' => $total_direct,
            'indirect' => $indirect,
            'userlevel' => $level,
        );
        $commisson_arr = array(
            'pending' => ($pending_commission + $one_commission - $total_paid_amount),
            'eighty' => $eighty_commission,
            'twenty' => $twenty_commission,
            'missed' => $missed_commisson,
            'one' => $one_commission,
            'total_paid' => $total_paid_amount
        );
        $latest_commision = $this->superAdminModel->getLatestCommisiomHistry($this->user);
        $this->template->set('commision_detail', $latest_commision);
        $this->template->set('userlevel', $levelarray);
        $this->template->set('totalmember', $Member);
        $this->template->set('commission', $commisson_arr);

        $this->template->render('dashboard');
    }

    public function introduce_by_user() {

        $user = $this->aauth->get_user();

        $result = $this->DashboardModel->introduceByUser($user->username);

        $total_reffrals = $result->num_rows();


        $this->template->set('dataarray', $result);

        $this->template->set('total_reffrals', $total_reffrals);


        $this->template->render('dashboard');
    }

    public function commission_report() {



        $commission_report = $this->UserModel->getCommissionReport($this->user);

        $this->template->set('commission_report', $commission_report);
        $this->template->render('dashboard');
    }

    public function business_line($id = -1) {



        if ($id == -1) {
            $user = $this->aauth->get_user();
            $result = $this->DashboardModel->introduceByUser($user->username);

            $breadcrumbs = $user->username . '';
        } else {

            $result = $this->DashboardModel->introduceByUserId($id);

            $breadcrumbs = $this->UserModel->getbreadcrumb($id);
        }

        $this->template->set('crumb', $breadcrumbs);


//        die;
//
        $this->template->set('dataarray', $result);
//
        $this->template->render('dashboard');
    }

    public function profile() {

        $this->template->render('dashboard');
    }

    public function gold_star_rewards() {
        $user = $this->aauth->get_user();

        $lines = $this->DashboardModel->introduceByUser($user->username);
        $total_reffrals = $lines->num_rows();
        $reward_summary = $this->UserModel->giveRewards($user->id);

        $this->template->set('total_reffrals', $total_reffrals);

        $this->template->set('rewards', $reward_summary);
        $this->template->render('dashboard');
    }

    public function test() {
        
    }

}
